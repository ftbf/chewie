#!/usr/bin/env python                                                                                 
import os,sys,argparse

# example usage: `python mkexpressblockscript.py 32944` will make the script for run 32944
# or: `python mkexpressblockscript.py 32944 32959` will make the script for all runs from 32944-32959
# or: `python mkexpressblockscript.py 32944 32959 --skip "32945,32947,32948,32952"` will make the script for all except for the four runs specified

# or change the following line and run via `python mkexpressblockscript.py` (no other cmd line args)
#runnumberarray = [32975,32976,32977,32978,32979,32980,32981,32982,32983,32984,32985]
runnumberarray = [51730,51731,51736,51737]
#runnumberarray = [31975,31976,31977,31983,31984,31985,31986,31987,31988,31989,31990,31991,31992,31993,31994,31995,31996,31998,32127,32128,32129,32130,32131,32132,32133,32134,32135,32136,32137,32138]


p = argparse.ArgumentParser()
p.add_argument('startrun', type=int, nargs='?',default=None)
p.add_argument('endrun', type=int, nargs='?',default=None)
p.add_argument('--skip', default="", type=str)
args = p.parse_args()

if args.startrun :
    startrun = args.startrun

    if args.endrun :
        endrun = args.endrun
    else :
        endrun = startrun
    skipstr = args.skip
    
    skipruns = []
    if skipstr != "" :
        skipruns = map(int, (skipstr).split(",")) # turn str into list of ints

    runnumberarray = []
    for run in range(startrun, endrun+1) :
        if run in skipruns : 
            print "skipping run", run
            continue
        runnumberarray.append(run)

chewiedatadir = os.getenv('CHEWIEDATADIR')

if not chewiedatadir : 
    print "Error: Need to source a setup script! e.g. setup2022April.sh in the Chewie directory"
    sys.exit()

filename = open("xml/ChewieExpress_Runs"+str(runnumberarray[0])+"to"+str(runnumberarray[-1])+"_Merged.xml","w+")

filename.write("<?xml version='1.0' encoding='iso-8859-1'?> \n")
filename.write('<!DOCTYPE ChewieExpressConfiguration SYSTEM "./dtd/ExpressConfiguration.dtd"> \n')
filename.write('<ChewieExpressConfiguration> \n')
filename.write('    <Defaults FilesPath="'+chewiedatadir+'/" \n')
filename.write('              Convert="yes" \n')
filename.write('              RunAnalysis="yes" \n')
filename.write('              NumberOfEvents="-1"/> \n')
filename.write('      <Files Configuration="2022December_ChewieConfiguration.xml" OutFileName="Chewie_Runs'+str(runnumberarray[0])+'_'+str(runnumberarray[-1])+'.root"> \n')
for runnumber in runnumberarray:
    filename.write('      <File Name="Run'+str(runnumber)+'_Merged.root"/> \n')
filename.write('    </Files> \n')
filename.write('</ChewieExpressConfiguration> \n')

filename.close()

scriptname = "expressblockscript"+str(runnumberarray[0])+"to"+str(runnumberarray[len(runnumberarray)-1])+".sh"
script = open(scriptname,"w+")
script.write('nohup ./ChewieExpress ChewieExpress_Runs'+str(runnumberarray[0])+'to'+str(runnumberarray[-1])+'_Merged.xml &> ExpressLogs/Runs'+str(runnumberarray[0])+'to'+str(runnumberarray[-1])+'_Merged.log \n')
script.close()
os.chmod(scriptname, 0o755)
print "Done! Now run `./%s`" % scriptname
