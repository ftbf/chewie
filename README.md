# Prerequisites:
In order to install Chewie you first need to install Monicelli

# Checking out the code
```
git clone https://gitlab.cern.ch/ftbf/chewie.git Chewie # or via SSH or KRB5, as you prefer. https is best on teddy01, for the purpose of collaborating
cd Chewie
git checkout TFPX # currently the most up-to-date branch
```

# Setting environmental variables
Once the package is checked out you need to modify one of the setup files, setting up the correct environment variables specific to your computer.

On `teddy01` at FNAL, the USCMS TFPX users should use `setup2023_02_February_T992.sh` or equivalent. This must be sourced every time you want to run or compile Chewie.

The first time you need to compile Chewie:
```
cd Chewie
source setup2023_02_February_T992.sh
./buildAll.sh initial
```

Note for subsequent recompiles (i.e. any time code has changed), you should use `./buildAll.sh` without the additional argument.

Now you are ready to run Chewie:
```
./Chewie
```
