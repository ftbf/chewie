# clean install, only when given the optional argument
if [ $# -ge 1 ] && [ "$1" == "initial" ]; then
    qmake
fi

# (re)compile Chewie
make

# (re)compile ChewieExpress
cd Express
./compile.py
cd ..
