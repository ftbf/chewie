/*===============================================================================
 * Chewie: the FERMILAB MTEST telescope and DUT anaysis tool
 * 
 * Copyright (C) 2014 
 *
 * Authors:
 *
 * Mauro Dinardo      (Universita' Bicocca) 
 * Dario Menasce      (INFN) 
 * Jennifer Ngadiuba  (INFN)
 * Lorenzo Uplegger   (FNAL)
 * Luigi Vigani       (INFN)
 *
 * INFN: Piazza della Scienza 3, Edificio U2, Milano, Italy 20126
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ================================================================================*/

#ifndef CHARGERD53A_H
#define CHARGERD53A_H

#include "Analysis.h"

#include <TTreeFormula.h>

#include <vector>
#include <iostream>


class TH1F;
class TH2F;
class TTreeFormula;

class AnalysisManager;
class WindowsManager;
class PlanesMapping;
class XmlParser;

class CalibrationsManager;

class ChargeRD53A : public Analysis
{
 public:
   ChargeRD53A            (AnalysisManager* analysisManager = 0, int nOfThreads = 1);
  ~ChargeRD53A            (void);

  void beginJob            (void);
  void analyze             (const Data& data, int threadNumber);
  void endJob              (void);

  void setCutsFormula      (std::map<std::string,std::string> cutsList, std::vector<TTree*> tree);
  bool passStandardCuts    (int planeID, const Data& data);

 private:
  void book                (void);
  void destroy             (void);

  void clusterSize         (int planeID, const Data& data, int threadNumber);
  void cellLandau          (bool pass, int planeID, const Data& data, int threadNumber);
  void clusterLandau       (bool pass, int planeID, const Data& data, int threadNumber);
  void planeCharge         (bool pass, int planeID, const Data& data, int threadNumber);
  void cellCharge          (bool pass, int planeID, const Data& data, int threadNumber);
  void xLandau             (bool pass, int planeID, const Data& data, int threadNumber);
  void yLandau             (bool pass, int planeID, const Data& data, int threadNumber);

  void xChargeDivision     (bool pass, int planeID, const Data& data, int threadNumber);
  void xAsymmetry          (bool pass, int planeID, const Data& data, int threadNumber);

  void yChargeDivision     (bool pass, int planeID, const Data& data, int threadNumber);
  void yAsymmetry          (bool pass, int planeID, const Data& data, int threadNumber);

  bool passCalibrationsCut (int  planeID, const Data& data);
  void setErrorsBar        (int  planeID);

  void zoomIn(TH2F* hCharge, TH2F* hChargeNorm, int x_start, int x_end, int y_start, int y_end, int rebinFactor);

  PlanesMapping*                                     thePlaneMapping_;
  std::map< std::string,std::vector<TTreeFormula*> > cutsFormulas_;
  const WindowsManager*                              theWindowsManager_;
  XmlParser*                                         theXmlParser_;

  CalibrationsManager*                               theCalibrationsManager_;

  int standardCutsPixelMinimumCharge_;
  int standardCutsPixelMaximumCharge_;
  int standardCutsClusterMinimumCharge_;
  int standardCutsClusterMaximumCharge_;
  int maxLandauClusterSize_;


  // ##############
  // # Histograms #
  // ##############
  std::vector<TH1F*>    hClusterSize_;
  std::vector<TH1F*>    hClusterFirstRow_;
  std::vector<TH1F*>    hClusterFirstCol_;
  std::vector<TH1F*>    hClusterMinHitChargeRatio_;
  std::vector<TH1F*>    hCellLandau_;
  std::vector<TH1F*>    hCellLandauOdd_;
  std::vector<TH1F*>    hCellLandauOddLeft_;
  std::vector<TH1F*>    hCellLandauOddRight_;
  std::vector<TH1F*>    hCellLandauEven_;
  std::vector<TH1F*>    hCellLandauEvenLeft_;
  std::vector<TH1F*>    hCellLandauEvenRight_;

  std::vector<TH1F*>    hLandauClusterSizeUpToMax_;

  // Up to size 9 is probably overkill, but it doesn't hurt
  std::vector<TH1F*>    hLandauClusterSize1_;
  std::vector<TH1F*>    hLandauClusterSize2_;
  std::vector<TH1F*>    hLandauClusterSize3_;
  std::vector<TH1F*>    hLandauClusterSize4_;
  std::vector<TH1F*>    hLandauClusterSize5_;
  std::vector<TH1F*>    hLandauClusterSize6_;
  std::vector<TH1F*>    hLandauClusterSize7_;
  std::vector<TH1F*>    hLandauClusterSize8_;
  std::vector<TH1F*>    hLandauClusterSize9_;

  // So we can use loops for these things...
  std::vector<std::vector<TH1F*>> hLandauClusterSizeVector_ = {hLandauClusterSize1_, hLandauClusterSize2_, hLandauClusterSize3_, hLandauClusterSize4_, hLandauClusterSize5_, hLandauClusterSize6_, hLandauClusterSize7_, hLandauClusterSize8_, hLandauClusterSize9_};

  std::vector<TH1F*>    hLandauClusterSize2sameCol_;
  std::vector<TH1F*>    hLandauClusterSize2sameRow_;
  std::vector<TH1F*>    hCellLandauSinglePixel_;

  std::vector<TH1F*>    h1DXcellCharge_;
  std::vector<TH1F*>    h1DXcellChargeNorm_;
  
  std::vector<TH1F*>    h1DXcellChargeSecondHit_;
  std::vector<TH1F*>    h1DXcellChargeSecondHitNorm_;
  
  std::vector<TH1F*>    h1DYcellCharge_;
  std::vector<TH1F*>    h1DYcellChargeNorm_;
  
  std::vector<TH1F*>    h1DYcellChargeSecondHit_;
  std::vector<TH1F*>    h1DYcellChargeSecondHitNorm_;


  std::vector<TH2F*>    h2DCharge_;
  std::vector<TH2F*>    h2DChargeNorm_;

  std::vector<TH2F*>    h2DChargeRef_;
  std::vector<TH2F*>    h2DChargeRefNorm_;

  std::vector<TH2F*>    h2DChargeRefRebinned_;
  std::vector<TH2F*>    h2DChargeRefNormRebinned_;

  std::vector<TH2F*>    h2DChargeRefZoomedIn50x50_;
  std::vector<TH2F*>    h2DChargeRefNormZoomedIn50x50_;

  std::vector<TH2F*>    h2DChargeRefZoomedIn25x100_;
  std::vector<TH2F*>    h2DChargeRefNormZoomedIn25x100_;


  std::vector<TH2F*>    h2DCellCharge_;
  std::vector<TH2F*>    h2DCellChargeNorm_;

  std::vector<TH2F*>    h2DCellChargeSize1_;
  std::vector<TH2F*>    h2DCellChargeSize1Norm_;

  std::vector<TH2F*>    h2DCellChargeSize2_;
  std::vector<TH2F*>    h2DCellChargeSize2Norm_;

  std::vector<TH2F*>    h2DCellChargeOdd_;
  std::vector<TH2F*>    h2DCellChargeOddNorm_;

  std::vector<TH2F*>    h2DCellChargeEven_;
  std::vector<TH2F*>    h2DCellChargeEvenNorm_;

  std::vector<TH2F*>    h4CellsCharge_;
  std::vector<TH2F*>    h4CellsChargeNorm_;

  std::vector<TH2F*>    h2DClusterSize_;

  std::vector<TH2F*>    h2DLargeClusterSize_;
  std::vector<TH2F*>    h2DLargeClusterSizeNorm_;

  std::vector<TH2F*>    h2DXcellCharge_;
  std::vector<TH2F*>    h2DXcellChargeSecondHit_;

  std::vector<TH2F*>    h2DYcellCharge_;
  std::vector<TH2F*>    h2DYcellChargeSecondHit_;


  std::vector<TH2F*>    h2DXcellChargeAsymmetry_;
  std::vector<TH1F*>    h1DXcellChargeAsymmetry_;
  std::vector<TH2F*>    h2DXcellChargeAsymmetryInv_;
  std::vector<TH1F*>    h1DXcellChargeAsymmetryInv_;

  std::vector<TH2F*>    h2DYcellChargeAsymmetry_;
  std::vector<TH1F*>    h1DYcellChargeAsymmetry_;
  std::vector<TH2F*>    h2DYcellChargeAsymmetryInv_;
  std::vector<TH1F*>    h1DYcellChargeAsymmetryInv_;
//Hsin-Wei added plots for 225um pixels
  std::vector<TH1F*>    hClusterSize225um_;

  std::vector<TH2F*>    h2DCellChargeNorm225um_;
  std::vector<TH2F*>    h2DClusterSize225um_;

  std::vector<TH2F*>    h2DCharge225um_;
  std::vector<TH2F*>    h2DChargeNorm225um_;

  std::vector<TH2F*>    h2DChargeRef225um_;
  std::vector<TH2F*>    h2DChargeRefNorm225um_;

  std::vector<TH1F*>    hLandauClusterSizeUpToMax225um_;

  // Up to size 9 is probably overkill, but it doesn't hurt
  std::vector<TH1F*>    h225umLandauClusterSize1_;
  std::vector<TH1F*>    h225umLandauClusterSize2_;
  std::vector<TH1F*>    h225umLandauClusterSize3_;
  std::vector<TH1F*>    h225umLandauClusterSize4_;
  std::vector<TH1F*>    h225umLandauClusterSize5_;
  std::vector<TH1F*>    h225umLandauClusterSize6_;
  std::vector<TH1F*>    h225umLandauClusterSize7_;
  std::vector<TH1F*>    h225umLandauClusterSize8_;
  std::vector<TH1F*>    h225umLandauClusterSize9_;

  // So we can use loops for these things...
  std::vector<std::vector<TH1F*>> h225umLandauClusterSizeVector_ = {h225umLandauClusterSize1_, h225umLandauClusterSize2_, h225umLandauClusterSize3_, h225umLandauClusterSize4_, h225umLandauClusterSize5_, h225umLandauClusterSize6_, h225umLandauClusterSize7_, h225umLandauClusterSize8_, h225umLandauClusterSize9_};
};

#endif
