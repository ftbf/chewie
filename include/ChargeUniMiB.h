/*===============================================================================
 * Chewie: the FERMILAB MTEST telescope and DUT anaysis tool
 *
 * Copyright (C) 2014
 *
 * Authors:
 *
 * Mauro Dinardo      (Universita' Bicocca)
 * Dario Menasce      (INFN)
 * Jennifer Ngadiuba  (INFN)
 * Lorenzo Uplegger   (FNAL)
 * Luigi Vigani       (INFN)
 *
 * INFN: Piazza della Scienza 3, Edificio U2, Milano, Italy 20126
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ================================================================================*/

#ifndef CHARGEUNIMIB_H
#define CHARGEUNIMIB_H

#include "Analysis.h"

#include <TTreeFormula.h>

#include <vector>
#include <iostream>

class TF1;
class TH1F;
class TH2F;
class TTreeFormula;

class AnalysisManager;
class WindowsManager;
class PlanesMapping;
class XmlParser;

class CalibrationsManager;

class ChargeUniMiB : public Analysis
{
 public:
   ChargeUniMiB            (AnalysisManager* analysisManager = 0, int nOfThreads = 1);
  ~ChargeUniMiB            (void);

  void beginJob            (void);
  void analyze             (const Data& data, int threadNumber);
  void endJob              (void);

  void setCutsFormula      (std::map<std::string,std::string> cutsList, std::vector<TTree*> tree);
  bool passStandardCuts    (int planeID, const Data& data);

 private:
  void book                (void);
  void destroy             (void);

  void clusterSize         (int planeID, const Data& data, int threadNumber);
  void cellLandau          (bool pass, int planeID, const Data& data, int threadNumber);
  void clusterLandau       (bool pass, int planeID, const Data& data, int threadNumber);
  void planeCharge         (bool pass, int planeID, const Data& data, int threadNumber);
  void cellCharge          (bool pass, int planeID, const Data& data, int threadNumber);
  void xLandau             (bool pass, int planeID, const Data& data, int threadNumber);
  void yLandau             (bool pass, int planeID, const Data& data, int threadNumber);

  void xChargeDivision     (bool pass, int planeID, const Data& data, int threadNumber);
  void xAsymmetry          (bool pass, int planeID, const Data& data, int threadNumber);

  void yChargeDivision     (bool pass, int planeID, const Data& data, int threadNumber);
  void yAsymmetry          (bool pass, int planeID, const Data& data, int threadNumber);

  bool passCalibrationsCut (int  planeID, const Data& data);
  void setErrorsBar        (int  planeID);
  void fitCharge           (int  planeID);
  int  langausFit          (TH1F* histo  , double* fitParameters                       );

  PlanesMapping*                                     thePlaneMapping_;
  TF1*                                               langaus_;
  std::map< std::string,std::vector<TTreeFormula*> > cutsFormulas_;
  const WindowsManager*                              theWindowsManager_;
  XmlParser*                                         theXmlParser_;


  CalibrationsManager*                               theCalibrationsManager_;

  int standardCutsPixelMinimumCharge_;
  int standardCutsPixelMaximumCharge_;
  int standardCutsClusterMinimumCharge_;
  int standardCutsClusterMaximumCharge_;


  // ##############
  // # Histograms #
  // ##############
  std::vector<TH1F*>    hClusterSize_;

  std::vector<TH1F*>    hCellLandau_;
  std::vector<TH1F*>    hCellLandauOddCol_;
  std::vector<TH1F*>    hCellLandauOddColLeft_;
  std::vector<TH1F*>    hCellLandauOddColRight_;
  std::vector<TH1F*>    hCellLandauEvenCol_;
  std::vector<TH1F*>    hCellLandauEvenColLeft_;
  std::vector<TH1F*>    hCellLandauEvenColRight_;
  std::vector<TH1F*>    hCellLandauOddRow_;
  std::vector<TH1F*>    hCellLandauOddRowLeft_;
  std::vector<TH1F*>    hCellLandauOddRowRight_;
  std::vector<TH1F*>    hCellLandauEvenRow_;
  std::vector<TH1F*>    hCellLandauEvenRowLeft_;
  std::vector<TH1F*>    hCellLandauEvenRowRight_;

  std::vector<TH1F*>    hLandauClusterSize1_;
  std::vector<TH1F*>    hLandauClusterSize2_;
  std::vector<TH1F*>    hLandauClusterSize2pointed_;
  std::vector<TH1F*>    hLandauClusterSize2sameCol_;
  std::vector<TH1F*>    hLandauClusterSize2sameRow_;
  std::vector<TH1F*>    hCellLandauSinglePixel_;

  std::vector<TH1F*>    h1DXcellCharge_;
  std::vector<TH1F*>    h1DXcellChargeNorm_;

  std::vector<TH1F*>    h1DXcellChargeSecondHit_;
  std::vector<TH1F*>    h1DXcellChargeSecondHitNorm_;

  std::vector<TH1F*>    h1DYcellCharge_;
  std::vector<TH1F*>    h1DYcellChargeNorm_;

  std::vector<TH1F*>    h1DYcellChargeSecondHit_;
  std::vector<TH1F*>    h1DYcellChargeSecondHitNorm_;

  std::vector<TH2F*>    h2DCharge_;
  std::vector<TH2F*>    h2DChargeNorm_;

  std::vector<TH2F*>    h2DCellCharge_;
  std::vector<TH2F*>    h2DCellChargeNorm_;

  std::vector<TH2F*>    h2DCellChargeOddCol_;
  std::vector<TH2F*>    h2DCellChargeOddColNorm_;

  std::vector<TH2F*>    h2DCellChargeEvenCol_;
  std::vector<TH2F*>    h2DCellChargeEvenColNorm_;

  std::vector<TH2F*>    h2DCellChargeOddRow_;
  std::vector<TH2F*>    h2DCellChargeOddRowNorm_;

  std::vector<TH2F*>    h2DCellChargeEvenRow_;
  std::vector<TH2F*>    h2DCellChargeEvenRowNorm_;

  std::vector<TH2F*>    h4CellsCharge_;
  std::vector<TH2F*>    h4CellsChargeNorm_;

  std::vector<TH2F*>    h2DClusterSize_;

  std::vector<TH2F*>    h2DXcellCharge_;
  std::vector<TH2F*>    h2DXcellChargeSecondHit_;

  std::vector<TH2F*>    h2DYcellCharge_;
  std::vector<TH2F*>    h2DYcellChargeSecondHit_;

  std::vector<TH2F*>    h2DXcellChargeAsymmetry_;
  std::vector<TH1F*>    h1DXcellChargeAsymmetry_;
  std::vector<TH2F*>    h2DXcellChargeAsymmetryInv_;
  std::vector<TH1F*>    h1DXcellChargeAsymmetryInv_;

  std::vector<TH2F*>    h2DYcellChargeAsymmetry_;
  std::vector<TH1F*>    h1DYcellChargeAsymmetry_;
  std::vector<TH2F*>    h2DYcellChargeAsymmetryInv_;
  std::vector<TH1F*>    h1DYcellChargeAsymmetryInv_;

  std::vector<TH2F*>    h2DYcellChargeAsymmetryLeftBump_;
  std::vector<TH1F*>    h1DYcellChargeAsymmetryLeftBump_;
  std::vector<TH2F*>    h2DYcellChargeAsymmetryInvLeftBump_;
  std::vector<TH1F*>    h1DYcellChargeAsymmetryInvLeftBump_;

  std::vector<TH2F*>    h2DYcellChargeAsymmetryRightBump_;
  std::vector<TH1F*>    h1DYcellChargeAsymmetryRightBump_;
  std::vector<TH2F*>    h2DYcellChargeAsymmetryInvRightBump_;
  std::vector<TH1F*>    h1DYcellChargeAsymmetryInvRightBump_;

  std::vector<TH2F*>    h2DYcellChargeAsymmetryLeftNoBump_;
  std::vector<TH1F*>    h1DYcellChargeAsymmetryLeftNoBump_;
  std::vector<TH2F*>    h2DYcellChargeAsymmetryInvLeftNoBump_;
  std::vector<TH1F*>    h1DYcellChargeAsymmetryInvLeftNoBump_;

  std::vector<TH2F*>    h2DYcellChargeAsymmetryRightNoBump_;
  std::vector<TH1F*>    h1DYcellChargeAsymmetryRightNoBump_;
  std::vector<TH2F*>    h2DYcellChargeAsymmetryInvRightNoBump_;
  std::vector<TH1F*>    h1DYcellChargeAsymmetryInvRightNoBump_;
};

#endif
