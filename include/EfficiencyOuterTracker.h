/*===============================================================================
 * Chewie: the FERMILAB MTEST telescope and DUT anaysis tool
 * 
 * Copyright (C) 2014 
 *
 * Authors:
 *
 * Mauro Dinardo      (Universita' Bicocca) 
 * Dario Menasce      (INFN) 
 * Jennifer Ngadiuba  (INFN)
 * Lorenzo Uplegger   (FNAL)
 * Luigi Vigani       (INFN)
 *
 * INFN: Piazza della Scienza 3, Edificio U2, Milano, Italy 20126
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ================================================================================*/

#ifndef EFFICIENCYOUTERTRACKER_H
#define EFFICIENCYOUTERTRACKER_H

#include "Analysis.h"

#include <TTreeFormula.h>

#include <vector>
#include <iostream>


class TH1F;
class TH2F;
class TEfficiency;
class TTreeFormula;

class AnalysisManager;
class WindowsManager;
class PlanesMapping;
class XmlParser;


class EfficiencyOuterTracker : public Analysis
{
 public:
   EfficiencyOuterTracker     (AnalysisManager* analysisManager = 0, int nOfThreads = 1);
  ~EfficiencyOuterTracker     (void);

  void beginJob         (void);
  void analyze          (const Data& data, int threadNumber);
  void endJob           (void);

  void setCutsFormula   (std::map<std::string,std::string> cutsList, std::vector<TTree*> tree);
  bool passStandardCuts (int planeID, const Data& data);

 private:
  const int dut0DetectorNumber_ = 16;
  const int dut1DetectorNumber_ = 17;
  const int stubDetectorNumber_ = 18;

  void book            (void);
  void destroy         (void);

  void planeEfficiency  (bool pass, int planeID, const Data& data, int threadNumber);
  void cellEfficiency   (bool pass, int planeID, const Data& data, int threadNumber);
  void xCellEfficiency  (bool pass, int planeID, const Data& data, int threadNumber);
  void yCellEfficiency  (bool pass, int planeID, const Data& data, int threadNumber);
  void xEdgeEfficiency  (bool pass, int planeID, const Data& data, int threadNumber);
  void yEdgeEfficiency  (bool pass, int planeID, const Data& data, int threadNumber);
  //void countClusterSize (int planeID, const Data &data);

  void setErrorsBar    (int  planeID);

  PlanesMapping*                                     thePlaneMapping_;
  std::map< std::string,std::vector<TTreeFormula*> > cutsFormulas_;
  const WindowsManager*                              theWindowsManager_;
  XmlParser*                                         theXmlParser_;

  float cumulative;
  float nevent;

  // ##############
  // # Histograms #
  // ##############

  std::vector<TEfficiency*> hEfficiency_;
  std::vector<TH1F*> hEfficiencyOld_;
  std::vector<TH1F*> hEfficiencyNorm_;

  std::vector<TEfficiency*> hEfficiencyStrip_;
  std::vector<TH1F*> hEfficiencyStripNorm_;

  std::vector<TEfficiency*> hEfficiencyQuick_;
  std::vector<TH1F*> hEfficiencyQuickNorm_;

  std::vector<TEfficiency*> hEfficiencyCenterStrip_;
  std::vector<TH1F*> hEfficiencyCenterStripNorm_;

  std::vector<TEfficiency*> hEfficiencyBetweenStrips_;
  std::vector<TH1F*> hEfficiencyBetweenStripsNorm_;

  std::vector<TEfficiency*> hEfficiencyBetweenStripsClusterSize1_;
  std::vector<TH1F*> hEfficiencyBetweenStripsClusterSize1Norm_;

  std::vector<TEfficiency*> hEfficiencyBetweenStripsClusterSize2_;
  std::vector<TH1F*> hEfficiencyBetweenStripsClusterSize2Norm_;

  std::vector<TEfficiency*> hEfficiencyStub_;
  std::vector<TH1F*> hEfficiencyStubNorm_;

  std::vector<TEfficiency*> hEfficiencyStubNoCuts_;
  std::vector<TH1F*> hEfficiencyStubNoCutsNorm_;

  std::vector<TEfficiency*> hEfficiencyStubSingleChip_;
  std::vector<TH1F*> hEfficiencyStubSingleChipNorm_;

  std::vector<TEfficiency*> hEfficiencyStubSingleChipNoCuts_;
  std::vector<TH1F*> hEfficiencyStubSingleChipNoCutsNorm_;

  std::vector<TH2F*> h2DEfficiency_;
  std::vector<TH2F*> h2DEfficiencyNorm_;

  std::vector<TH2F*> h2DEfficiencyQuick_;
  std::vector<TH2F*> h2DEfficiencyQuickNorm_;

  std::vector<TH2F*> h2DInefficiencyQuick_;
  std::vector<TH2F*> h2DInefficiencyQuickNorm_;

  std::vector<TH2F*> h2DEfficiencyStub_;
  std::vector<TH2F*> h2DEfficiencyStubNorm_;

  std::vector<TEfficiency*> hEfficiencyStripStub_;
  std::vector<TH1F*> hEfficiencyStripStubNorm_;

  std::vector<TH2F*> h2D4CellEfficiency_;
  std::vector<TH2F*> h2D4CellEfficiencyNorm_;

  std::vector<TH2F*> hCellEfficiency_;
  std::vector<TH2F*> hCellEfficiencyNorm_;

  std::vector<TH2F*> hCellEfficiencyEvenColumns_;
  std::vector<TH2F*> hCellEfficiencyEvenColumnsNorm_;

  std::vector<TH2F*> hCellEfficiencyOddColumns_;
  std::vector<TH2F*> hCellEfficiencyOddColumnsNorm_;

  std::vector<TEfficiency*> h1DXCellEfficiencyFirstHit_;
  std::vector<TEfficiency*> h1DXCellEfficiencySecondHit_;
  std::vector<TH1F*> h1DXCellEfficiencyNorm_;

  std::vector<TEfficiency*> h1DYCellEfficiencyFirstHit_;
  std::vector<TEfficiency*> h1DYCellEfficiencySecondHit_;
  std::vector<TH1F*> h1DYCellEfficiencyNorm_;

  std::vector<TEfficiency*> diffFirstSecondHist_;
  std::vector<TH1F*> hitDiffTwoStrips_;
  std::vector<TH2F*> h2DDeltaXvsPredictedCol_;

  std::vector<TH1F*> h1DClusterSizes_;
  std::vector<TH1F*> h1DClusterSizesCenterStrip_;
  std::vector<TH1F*> h1DClusterSizesBetweenStrips_;

  std::vector<TH1F*> h1DClusterSizeVsXStripPosition_;
  std::vector<TH1F*> h1DClusterSizeVsXStripPosition_norm_;
  std::vector<TH1F*> h1DClusterSize1VsXStripPosition_;
  std::vector<TH1F*> h1DClusterSize2VsXStripPosition_;
  std::vector<TH1F*> h1DClusterSize3VsXStripPosition_;
  std::vector<TH1F*> h1DClusterSize4VsXStripPosition_;
  std::vector<TH1F*> h1DClusterSize5VsXStripPosition_;

  std::vector<TH1F*> hAverageClustersPerEvent_;
  std::vector<TH1F*> TotalEvent_;
  std::vector<TH1F*> CheckIneff_;

  std::vector<TH1F*> Dut23Stub_;
  //nabin

  std::vector<TH1F*> hChi2Distribution_;
  std::vector<TH1F*> hNumberOfClusters_;
  std::vector<TEfficiency*> hEfficiencyStubVsNumberOfClusters_;
  std::vector<TH1F*> hEfficiencyStubVsNumberOfClustersNorm_;
  std::vector<TH1F*> hClusterPositionLocal_;
  std::vector<TH1F*> hEfficiencyStubVsClusterSizesNorm_;
  std::vector<TEfficiency*> hEfficiencyStubVsClusterSizes_;

  std::vector<TH1F*> hNumberOfHitsOnSensor_;

  std::vector<TH1F*>        hEfficiencyStubVsStripNumberNorm_;
  std::vector<TEfficiency*> hEfficiencyStubVsStripNumber_;
  std::vector<TH1F*>        hEfficiencyStubVsDeviationFromPredNorm_;
  std::vector<TEfficiency*> hEfficiencyStubVsDeviationFromPred_;
  std::vector<TH1F*>        hEfficiencyStubVsDeviationFromPredNorm1_;
  std::vector<TEfficiency*> hEfficiencyStubVsDeviationFromPred1_;
  std::vector<TH1F*>        hEfficiencyStubVsDeviationFromPredNorm2_;
  std::vector<TEfficiency*> hEfficiencyStubVsDeviationFromPred2_;
  std::vector<TH1F*>        hEfficiencyStubVsDeviationFromPredNorm3_;
  std::vector<TEfficiency*> hEfficiencyStubVsDeviationFromPred3_;

  template<typename T>
  void destroyHistograms(std::vector<T> theHistogramVector)
  {
    for( auto plot : theHistogramVector) delete plot;
    theHistogramVector.clear();
  }
};

#endif
