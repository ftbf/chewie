/*===============================================================================
 * Chewie: the FERMILAB MTEST telescope and DUT anaysis tool
 *
 * Copyright (C) 2014
 *
 * Authors:
 *
 * Mauro Dinardo      (Universita' Bicocca)
 * Dario Menasce      (INFN)
 * Jennifer Ngadiuba  (INFN)
 * Lorenzo Uplegger   (FNAL)
 * Luigi Vigani       (INFN)
 *
 * INFN: Piazza della Scienza 3, Edificio U2, Milano, Italy 20126
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ================================================================================*/

#include "ChargeRD53A.h"

#include "AnalysisManager.h"
#include "WindowsManager.h"
#include "Window.h"
#include "ThreadUtilities.h"
#include "PlanesMapping.h"
#include "MessageTools.h"
#include "XmlParser.h"
#include "XmlPlane.h"
#include "XmlAnalysis.h"
#include "HistogramWindow.h"

#include "Utilities.h"
#include "CalibrationsManager.h"

#include <TH2F.h>
#include <TF1.h>
#include <TStyle.h>

#include <iostream>
#include <iomanip>
#include <cmath>


// @@@ Hard coded parameters @@@
#define maxChargeDeltaRay 13600. // = 8000 (MPV Landau for 100 um bulk thickness) * 1.7
#define ONLYdoubleHITS    false  // Process only clusters of size 2
#define HITALLSTRIPS      true   // Process tracks with associated hits on each strip plane
#define ETAhalfRANGE      0.5    // Eta fit range = [-ETAhalfRANGE, +ETAhalfRANGE]
#define SPECIALPITCH      25.
// ============================


//=======================================================================
ChargeRD53A::ChargeRD53A(AnalysisManager* analysisManager, int nOfThreads) :
    Analysis(analysisManager, nOfThreads),
    thePlaneMapping_(0),
    theWindowsManager_(0),
    theXmlParser_(analysisManager->getXmlParser()),
    standardCutsPixelMinimumCharge_(0),
    standardCutsPixelMaximumCharge_(0),
    standardCutsClusterMinimumCharge_(0),
    standardCutsClusterMaximumCharge_(0),
    maxLandauClusterSize_(9)
{
    STDLINE("Running ChargeRD53A analysis",ACCyan);

    thePlaneMapping_ = new PlanesMapping();
}

//=======================================================================
ChargeRD53A::~ChargeRD53A(void)
{
    if (thePlaneMapping_) delete thePlaneMapping_;

    destroy();
}

//=======================================================================
void ChargeRD53A::destroy()
{
    if (Analysis::fDoNotDelete_) return;

    std::vector<TH1F*>::iterator it1;
    std::vector<TH2F*>::iterator it2;

    for(it1=hCellLandau_                   .begin(); it1!=hCellLandau_                   .end(); it1++) delete *it1; hCellLandau_                  .clear();
    for(it1=hCellLandauOdd_                .begin(); it1!=hCellLandauOdd_                .end(); it1++) delete *it1; hCellLandauOdd_               .clear();
    for(it1=hCellLandauOddLeft_            .begin(); it1!=hCellLandauOddLeft_            .end(); it1++) delete *it1; hCellLandauOddLeft_           .clear();
    for(it1=hCellLandauOddRight_           .begin(); it1!=hCellLandauOddRight_           .end(); it1++) delete *it1; hCellLandauOddRight_          .clear();
    for(it1=hCellLandauEven_               .begin(); it1!=hCellLandauEven_               .end(); it1++) delete *it1; hCellLandauEven_              .clear();
    for(it1=hCellLandauEvenLeft_           .begin(); it1!=hCellLandauEvenLeft_           .end(); it1++) delete *it1; hCellLandauEvenLeft_          .clear();
    for(it1=hCellLandauEvenRight_          .begin(); it1!=hCellLandauEvenRight_          .end(); it1++) delete *it1; hCellLandauEvenRight_         .clear();
    for(it1=hClusterSize_                  .begin(); it1!=hClusterSize_                  .end(); it1++) delete *it1; hClusterSize_                 .clear();
    for(it1=hClusterFirstRow_              .begin(); it1!=hClusterFirstRow_              .end(); it1++) delete *it1; hClusterFirstRow_             .clear();
    for(it1=hClusterFirstCol_              .begin(); it1!=hClusterFirstCol_              .end(); it1++) delete *it1; hClusterFirstCol_             .clear();

    for(it1=hClusterMinHitChargeRatio_     .begin(); it1!=hClusterMinHitChargeRatio_     .end(); it1++) delete *it1; hClusterMinHitChargeRatio_    .clear();

    for(it1=hLandauClusterSizeUpToMax_     .begin(); it1!=hLandauClusterSizeUpToMax_     .end(); it1++) delete *it1; hLandauClusterSizeUpToMax_    .clear();

    for(int i = 0; i < maxLandauClusterSize_; ++i)
    {
      for(it1=hLandauClusterSizeVector_[i] .begin(); it1!=hLandauClusterSizeVector_[i]   .end(); it1++) delete *it1; hLandauClusterSizeVector_[i]  .clear();
    }

    for(it1=hLandauClusterSize2sameCol_    .begin(); it1!=hLandauClusterSize2sameCol_    .end(); it1++) delete *it1; hLandauClusterSize2sameCol_   .clear();
    for(it1=hLandauClusterSize2sameRow_    .begin(); it1!=hLandauClusterSize2sameRow_    .end(); it1++) delete *it1; hLandauClusterSize2sameRow_   .clear();
    for(it1=hCellLandauSinglePixel_        .begin(); it1!=hCellLandauSinglePixel_        .end(); it1++) delete *it1; hCellLandauSinglePixel_       .clear();


    for(it1=h1DXcellCharge_                .begin(); it1!=h1DXcellCharge_                .end(); it1++) delete *it1; h1DXcellCharge_               .clear();
    for(it1=h1DXcellChargeNorm_            .begin(); it1!=h1DXcellChargeNorm_            .end(); it1++) delete *it1; h1DXcellChargeNorm_           .clear();

    for(it1=h1DXcellChargeSecondHit_       .begin(); it1!=h1DXcellChargeSecondHit_       .end(); it1++) delete *it1; h1DXcellChargeSecondHit_      .clear();
    for(it1=h1DXcellChargeSecondHitNorm_   .begin(); it1!=h1DXcellChargeSecondHitNorm_   .end(); it1++) delete *it1; h1DXcellChargeSecondHitNorm_  .clear();

    for(it1=h1DYcellCharge_                .begin(); it1!=h1DYcellCharge_                .end(); it1++) delete *it1; h1DYcellCharge_               .clear();
    for(it1=h1DYcellChargeNorm_            .begin(); it1!=h1DXcellChargeNorm_            .end(); it1++) delete *it1; h1DXcellChargeNorm_           .clear();

    for(it1=h1DYcellChargeSecondHit_       .begin(); it1!=h1DYcellChargeSecondHit_       .end(); it1++) delete *it1; h1DYcellChargeSecondHit_      .clear();
    for(it1=h1DYcellChargeSecondHitNorm_   .begin(); it1!=h1DYcellChargeSecondHitNorm_   .end(); it1++) delete *it1; h1DYcellChargeSecondHitNorm_  .clear();


    for(it2=h2DClusterSize_                .begin(); it2!=h2DClusterSize_                .end(); it2++) delete *it2; h2DClusterSize_               .clear();
    for(it2=h2DLargeClusterSize_           .begin(); it2!=h2DLargeClusterSize_           .end(); it2++) delete *it2; h2DLargeClusterSize_          .clear();
    for(it2=h2DLargeClusterSizeNorm_       .begin(); it2!=h2DLargeClusterSizeNorm_       .end(); it2++) delete *it2; h2DLargeClusterSizeNorm_      .clear();

    for(it2=h2DCharge_                     .begin(); it2!=h2DCharge_                     .end(); it2++) delete *it2; h2DCharge_                    .clear();
    for(it2=h2DChargeNorm_                 .begin(); it2!=h2DChargeNorm_                 .end(); it2++) delete *it2; h2DChargeNorm_                .clear();

    for(it2=h2DChargeRef_                  .begin(); it2!=h2DChargeRef_                  .end(); it2++) delete *it2; h2DChargeRef_                 .clear();
    for(it2=h2DChargeRefNorm_              .begin(); it2!=h2DChargeRefNorm_              .end(); it2++) delete *it2; h2DChargeRefNorm_             .clear();

    for (it2=h2DChargeRefRebinned_         .begin(); it2!=h2DChargeRefRebinned_          .end(); it2++) delete *it2; h2DChargeRefRebinned_         .clear();
    for (it2=h2DChargeRefNormRebinned_     .begin(); it2!=h2DChargeRefNormRebinned_      .end(); it2++) delete *it2; h2DChargeRefNormRebinned_     .clear();

    for (it2=h2DChargeRefZoomedIn50x50_         .begin(); it2!=h2DChargeRefZoomedIn50x50_          .end(); it2++) delete *it2; h2DChargeRefZoomedIn50x50_         .clear();
    for (it2=h2DChargeRefNormZoomedIn50x50_     .begin(); it2!=h2DChargeRefNormZoomedIn50x50_      .end(); it2++) delete *it2; h2DChargeRefNormZoomedIn50x50_     .clear();

    for (it2=h2DChargeRefZoomedIn25x100_         .begin(); it2!=h2DChargeRefZoomedIn25x100_          .end(); it2++) delete *it2; h2DChargeRefZoomedIn25x100_         .clear();
    for (it2=h2DChargeRefNormZoomedIn25x100_     .begin(); it2!=h2DChargeRefNormZoomedIn25x100_      .end(); it2++) delete *it2; h2DChargeRefNormZoomedIn25x100_     .clear();

    for(it2=h2DCellCharge_                 .begin(); it2!=h2DCellCharge_                 .end(); it2++) delete *it2; h2DCellCharge_                .clear();
    for(it2=h2DCellChargeNorm_             .begin(); it2!=h2DCellChargeNorm_             .end(); it2++) delete *it2; h2DCellChargeNorm_            .clear();

    for(it2=h2DCellChargeSize1_            .begin(); it2!=h2DCellChargeSize1_            .end(); it2++) delete *it2; h2DCellChargeSize1_           .clear();
    for(it2=h2DCellChargeSize1Norm_        .begin(); it2!=h2DCellChargeSize1Norm_        .end(); it2++) delete *it2; h2DCellChargeSize1Norm_       .clear();

    for(it2=h2DCellChargeSize2_            .begin(); it2!=h2DCellChargeSize2_            .end(); it2++) delete *it2; h2DCellChargeSize2_           .clear();
    for(it2=h2DCellChargeSize2Norm_        .begin(); it2!=h2DCellChargeSize2Norm_        .end(); it2++) delete *it2; h2DCellChargeSize2Norm_       .clear();

    for(it2=h2DCellChargeOdd_              .begin(); it2!=h2DCellChargeOdd_              .end(); it2++) delete *it2; h2DCellChargeOdd_             .clear();
    for(it2=h2DCellChargeOddNorm_          .begin(); it2!=h2DCellChargeOddNorm_          .end(); it2++) delete *it2; h2DCellChargeOddNorm_         .clear();

    for(it2=h2DCellChargeEven_             .begin(); it2!=h2DCellChargeEven_             .end(); it2++) delete *it2; h2DCellChargeEven_            .clear();
    for(it2=h2DCellChargeEvenNorm_         .begin(); it2!=h2DCellChargeEvenNorm_         .end(); it2++) delete *it2; h2DCellChargeEvenNorm_        .clear();

    for(it2=h4CellsCharge_                 .begin(); it2!=h4CellsCharge_                 .end(); it2++) delete *it2; h4CellsCharge_                .clear();
    for(it2=h4CellsChargeNorm_             .begin(); it2!=h4CellsChargeNorm_             .end(); it2++) delete *it2; h4CellsChargeNorm_            .clear();

    for(it2=h2DXcellCharge_                .begin(); it2!=h2DXcellCharge_                .end(); it2++) delete *it2; h2DXcellCharge_               .clear();
    for(it2=h2DXcellChargeSecondHit_       .begin(); it2!=h2DXcellChargeSecondHit_       .end(); it2++) delete *it2; h2DXcellChargeSecondHit_      .clear();

    for(it2=h2DYcellCharge_                .begin(); it2!=h2DYcellCharge_                .end(); it2++) delete *it2; h2DYcellCharge_               .clear();
    for(it2=h2DYcellChargeSecondHit_       .begin(); it2!=h2DYcellChargeSecondHit_       .end(); it2++) delete *it2; h2DYcellChargeSecondHit_      .clear();


    for(it2=h2DXcellChargeAsymmetry_       .begin(); it2!=h2DXcellChargeAsymmetry_       .end(); it2++) delete *it2; h2DXcellChargeAsymmetry_      .clear();
    for(it1=h1DXcellChargeAsymmetry_       .begin(); it1!=h1DXcellChargeAsymmetry_       .end(); it1++) delete *it1; h1DXcellChargeAsymmetry_      .clear();
    for(it2=h2DXcellChargeAsymmetryInv_    .begin(); it2!=h2DXcellChargeAsymmetryInv_    .end(); it2++) delete *it2; h2DXcellChargeAsymmetryInv_   .clear();
    for(it1=h1DXcellChargeAsymmetryInv_    .begin(); it1!=h1DXcellChargeAsymmetryInv_    .end(); it1++) delete *it1; h1DXcellChargeAsymmetryInv_   .clear();

    for(it2=h2DYcellChargeAsymmetry_       .begin(); it2!=h2DYcellChargeAsymmetry_       .end(); it2++) delete *it2; h2DYcellChargeAsymmetry_      .clear();
    for(it1=h1DYcellChargeAsymmetry_       .begin(); it1!=h1DYcellChargeAsymmetry_       .end(); it1++) delete *it1; h1DYcellChargeAsymmetry_      .clear();
    for(it2=h2DYcellChargeAsymmetryInv_    .begin(); it2!=h2DYcellChargeAsymmetryInv_    .end(); it2++) delete *it2; h2DYcellChargeAsymmetryInv_   .clear();
    for(it1=h1DYcellChargeAsymmetryInv_    .begin(); it1!=h1DYcellChargeAsymmetryInv_    .end(); it1++) delete *it1; h1DYcellChargeAsymmetryInv_   .clear();
//Hsin-Wei added plots for 225um pixels
    for(it1=hClusterSize225um_                  .begin(); it1!=hClusterSize225um_                  .end(); it1++) delete *it1; hClusterSize225um_                 .clear();
    for(it2=h2DClusterSize225um_                .begin(); it2!=h2DClusterSize225um_                .end(); it2++) delete *it2; h2DClusterSize225um_               .clear();
    for(it2=h2DCharge225um_                     .begin(); it2!=h2DCharge225um_                     .end(); it2++) delete *it2; h2DCharge225um_                    .clear();
    for(it2=h2DChargeNorm225um_                 .begin(); it2!=h2DChargeNorm225um_                 .end(); it2++) delete *it2; h2DChargeNorm225um_                .clear();
    for(it2=h2DCellChargeNorm225um_             .begin(); it2!=h2DCellChargeNorm225um_             .end(); it2++) delete *it2; h2DCellChargeNorm225um_            .clear();
    for(it2=h2DChargeRef225um_                  .begin(); it2!=h2DChargeRef225um_                  .end(); it2++) delete *it2; h2DChargeRef225um_                 .clear();
    for(it2=h2DChargeRefNorm225um_              .begin(); it2!=h2DChargeRefNorm225um_              .end(); it2++) delete *it2; h2DChargeRefNorm225um_             .clear();

    for(it1=hLandauClusterSizeUpToMax225um_     .begin(); it1!=hLandauClusterSizeUpToMax225um_     .end(); it1++) delete *it1; hLandauClusterSizeUpToMax225um_    .clear();

    for(int i = 0; i < maxLandauClusterSize_; ++i)
    {
      for(it1=h225umLandauClusterSizeVector_[i] .begin(); it1!=h225umLandauClusterSizeVector_[i]   .end(); it1++) delete *it1; h225umLandauClusterSizeVector_[i]  .clear();
    }
}

//=======================================================================
void ChargeRD53A::setErrorsBar(int planeID)
{
    std::string       planeName = thePlaneMapping_->getPlaneName(planeID);
    std::stringstream hName;
    double            binError;
    int               nBins;


    theAnalysisManager_->cd("/Charge/" + planeName + "/XAsymmetry");

    hName.str(""); hName << "h1DXcellChargeAsymmetry_" << planeName;
    h1DXcellChargeAsymmetry_.push_back((TH1F*)h2DXcellChargeAsymmetry_[planeID]->ProfileX(hName.str().c_str(),1,-1));

    hName.str(""); hName << "h1DXcellChargeAsymmetryInv_" << planeName;
    h1DXcellChargeAsymmetryInv_.push_back((TH1F*)h2DXcellChargeAsymmetryInv_[planeID]->ProfileX(hName.str().c_str(),1,-1));


    theAnalysisManager_->cd("/Charge/" + planeName + "/YAsymmetry");

    hName.str(""); hName << "h1DYcellChargeAsymmetry_" << planeName;
    h1DYcellChargeAsymmetry_.push_back((TH1F*)h2DYcellChargeAsymmetry_[planeID]->ProfileX(hName.str().c_str(),1,-1));

    hName.str(""); hName << "h1DYcellChargeAsymmetryInv_" << planeName;
    h1DYcellChargeAsymmetryInv_.push_back((TH1F*)h2DYcellChargeAsymmetryInv_[planeID]->ProfileX(hName.str().c_str(),1,-1));


    theAnalysisManager_->cd("/Charge/" + planeName + "/XcellCharge1D");

    nBins = h2DXcellCharge_[planeID]->GetNbinsX();
    TH1D* hXchargeTmp[nBins];
    for (int bX = 1; bX <= nBins; bX++)
    {
        hName.str(""); hName << "Xcharge_Proj_bin_" << bX;
        hXchargeTmp[bX-1] = h2DXcellCharge_[planeID]->ProjectionY(hName.str().c_str(),bX,bX);

        if (hXchargeTmp[bX-1]->GetEntries() != 0)
        {
            binError = hXchargeTmp[bX-1]->GetRMS() / sqrt(hXchargeTmp[bX-1]->GetEntries());
            h1DXcellCharge_[planeID]->SetBinError(bX,binError);
        }
        else continue;
    }
    for (int p = 0; p < nBins; p++) hXchargeTmp[p]->Delete("0");

    nBins = h2DXcellChargeSecondHit_[planeID]->GetNbinsX();
    TH1D* hXchargeSecondHitTmp[nBins];
    for (int bX = 1; bX <= nBins; bX++)
    {
        hName.str(""); hName << "XchargeSecondHit_Proj_bin_" << bX;
        hXchargeSecondHitTmp[bX-1] = h2DXcellChargeSecondHit_[planeID]->ProjectionY(hName.str().c_str(),bX,bX);

        if (hXchargeSecondHitTmp[bX-1]->GetEntries() != 0)
        {
            binError = hXchargeSecondHitTmp[bX-1]->GetRMS() / sqrt(hXchargeSecondHitTmp[bX-1]->GetEntries());
            h1DXcellChargeSecondHit_[planeID]->SetBinError(bX,binError);
        }
        else continue;
    }
    for (int p = 0; p < nBins; p++) hXchargeSecondHitTmp[p]->Delete("0");


    theAnalysisManager_->cd("/Charge/" + planeName + "/YcellCharge1D");

    nBins = h2DYcellCharge_[planeID]->GetNbinsX();
    TH1D* hYchargeTmp[nBins];
    for (int bY = 1; bY <= nBins; bY++)
    {
        hName.str(""); hName << "Ycharge_Proj_bin_" << bY;
        hYchargeTmp[bY-1] = h2DYcellCharge_[planeID]->ProjectionY(hName.str().c_str(),bY,bY);

        if (hYchargeTmp[bY-1]->GetEntries() != 0)
        {
            binError = hYchargeTmp[bY-1]->GetRMS() / sqrt(hYchargeTmp[bY-1]->GetEntries());
            h1DYcellCharge_[planeID]->SetBinError(bY,binError);
        }
        else continue;
    }
    for (int p = 0; p < nBins; p++) hYchargeTmp[p]->Delete("0");

    nBins = h2DYcellChargeSecondHit_[planeID]->GetNbinsX();
    TH1D* hYchargeSecondHitTmp[nBins];
    for(int bY = 1; bY <= nBins; bY++)
    {
        hName.str(""); hName << "YchargeSecondHit_Proj_bin_" << bY;
        hYchargeSecondHitTmp[bY-1] = h2DYcellChargeSecondHit_[planeID]->ProjectionY(hName.str().c_str(),bY,bY);

        if (hYchargeSecondHitTmp[bY-1]->GetEntries() != 0)
        {
            binError = hYchargeSecondHitTmp[bY-1]->GetRMS() / sqrt(hYchargeSecondHitTmp[bY-1]->GetEntries());
            h1DYcellChargeSecondHit_[planeID]->SetBinError(bY,binError);
        }
        else continue;
    }
    for (int p = 0; p < nBins; p++) hYchargeSecondHitTmp[p]->Delete("0");
}

//=======================================================================
void ChargeRD53A::clusterSize(int planeID, const Data& data, int threadNumber)
{
    if (!data.getIsInDetector(planeID) || !data.getHasHit(planeID)) return;


    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    const Window* theWindow    = theWindowsManager_->getWindow(planeID);
    int           rowPredicted = data.getRowPredicted(planeID);
    int           colPredicted = data.getColPredicted(planeID);
    int           run          = data.getRunNumber();

    if (!theWindow->checkWindow(colPredicted,rowPredicted,run)) return;

    int theClusterSize = data.getClusterSize(planeID);
    THREADED(hClusterSize_[planeID])->Fill(theClusterSize);
//Hsin-Wei added 225um plots
    if (planeID == 16){
        if(data.getXPitchLocal(planeID) == 225){
          THREADED(hClusterSize225um_[planeID])->Fill(theClusterSize);
         }
        }

    if(theClusterSize > maxLandauClusterSize_) return;
    int minRow = INT_MAX;
    int minCol = INT_MAX;

    for (int h = 0; h < theClusterSize; h++)
    {
	int row = data.getClusterPixelRow(h,planeID);
	int col = data.getClusterPixelCol(h,planeID);

	if(row < minRow) minRow = row;
	if(col < minCol) minCol = col;
    }
    THREADED(hClusterFirstRow_[planeID])->Fill(minRow);
    THREADED(hClusterFirstCol_[planeID])->Fill(minCol);
}

//=======================================================================
void ChargeRD53A::cellLandau(bool pass, int planeID, const Data& data, int threadNumber)
{
    // #####################
    // # Internal constant #
    // #####################
    int clusterSize = 1;


    if (!pass || !data.getIsInDetector(planeID) || !data.getHasHit(planeID) || data.getClusterSize(planeID) != clusterSize) return;


    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    const Window* theWindow    = theWindowsManager_->getWindow(planeID);
    int           rowPredicted = data.getRowPredicted(planeID);
    int           colPredicted = data.getColPredicted(planeID);
    int           run          = data.getRunNumber();

    if (!theWindow->checkWindow(colPredicted,rowPredicted,run)) return;

    if (!theWindow->checkWindow(data.getClusterPixelCol(0,planeID),data.getClusterPixelRow(0,planeID),data.getRunNumber()) // Hits are in the window
            ||  data.getClusterPixelRow    (0,planeID) != rowPredicted                                                         // Track is on cluster
            ||  data.getClusterPixelCol    (0,planeID) != colPredicted                                                         // Track is on cluster
            || !data.getIsPixelCalibrated  (0,planeID)                                                                         // Pixels are calibrated
            ||  data.getClusterPixelCharge (0,planeID) < standardCutsPixelMinimumCharge_                                       // Charge is over threshold
            ||  data.getClusterPixelCharge (0,planeID) > standardCutsPixelMaximumCharge_)                                      // Maximum allowed charge for this physics
        return;

    THREADED(hCellLandau_[planeID])->Fill(data.getClusterCharge(planeID));

    if (((int)colPredicted)%2 == 0)
    {
        THREADED(hCellLandauEven_[planeID])->Fill(data.getClusterCharge(planeID));

        if (data.getXPixelResidualLocal(planeID) > 0) THREADED(hCellLandauEvenRight_[planeID])->Fill(data.getClusterCharge(planeID));
        else                                          THREADED(hCellLandauEvenLeft_[planeID])->Fill(data.getClusterCharge(planeID));
    }
    else
    {
        THREADED(hCellLandauOdd_[planeID])->Fill(data.getClusterCharge(planeID));

        if (data.getXPixelResidualLocal(planeID) > 0) THREADED(hCellLandauOddRight_[planeID])->Fill(data.getClusterCharge(planeID));
        else                                          THREADED(hCellLandauOddLeft_[planeID])->Fill(data.getClusterCharge(planeID));
    }
}

//=======================================================================
void ChargeRD53A::clusterLandau(bool pass, int planeID, const Data& data, int threadNumber)
{
    // #####################
    // # Internal constant #
    // #####################
    int maxClusterSize = maxLandauClusterSize_;
    int pixelCell = -1;


    if (!pass || !data.getIsInDetector(planeID) || !data.getHasHit(planeID) || data.getClusterSize(planeID) > maxClusterSize) return;
    int clusterSize = data.getClusterSize(planeID);


    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    const Window* theWindow    = theWindowsManager_->getWindow(planeID);
    int           rowPredicted = data.getRowPredicted(planeID);
    int           colPredicted = data.getColPredicted(planeID);
    int           run          = data.getRunNumber();

    if (!theWindow->checkWindow(colPredicted,rowPredicted,run)) return;

    for (int h = 0; h < clusterSize; h++)
    {
        if (!theWindow->checkWindow(data.getClusterPixelCol(h,planeID),data.getClusterPixelRow(h,planeID),run) // Hits are in the window
                || !data.getIsPixelCalibrated(h,planeID)                                                           // Pixels are calibrated
                ||  data.getClusterPixelCharge (h,planeID) < standardCutsPixelMinimumCharge_                       // Charge is over threshold
                ||  data.getClusterPixelCharge (h,planeID) > standardCutsPixelMaximumCharge_)                      // Maximum allowed charge for this physics
            return;

        if (data.getClusterPixelRow(h,planeID) == rowPredicted && data.getClusterPixelCol(h,planeID) == colPredicted) pixelCell = h;
    }

    if(clusterSize == 2) {
      float charge0 = data.getClusterPixelCharge (0,planeID);
      float charge1 = data.getClusterPixelCharge (1,planeID);
      THREADED(hClusterMinHitChargeRatio_[planeID])->Fill( std::min(charge0, charge1) / (charge0+charge1) );
    }

    THREADED(hLandauClusterSizeUpToMax_[planeID])->Fill(data.getClusterCharge(planeID));
    THREADED(hLandauClusterSizeVector_[clusterSize-1][planeID])->Fill(data.getClusterCharge(planeID)); // vector is indexed with hLandauClusterSize1 plot as entry 0, and so on

    if      (pixelCell != -1)  THREADED(hCellLandauSinglePixel_[planeID])->Fill(data.getClusterPixelCharge(pixelCell, planeID));
//Hsin-Wei have added plots for 225um pixels 
    if (planeID == 16){
        if(data.getXPitchLocal(planeID) == 225){
          THREADED(hLandauClusterSizeUpToMax225um_[planeID])->Fill(data.getClusterCharge(planeID));
          THREADED(h225umLandauClusterSizeVector_[clusterSize-1][planeID])->Fill(data.getClusterCharge(planeID)); // vector is indexed with hLandauClusterSize1 plot as entry 0, and so on
         }
        }
}

//=======================================================================
void ChargeRD53A::planeCharge(bool pass, int planeID, const Data& data, int threadNumber)
{
    // #####################
    // # Internal constant #
    // #####################
    int maxClusterSize = 4;
    bool foundCluster = false;


    if (!pass || !data.getIsInDetector(planeID) || !data.getHasHit(planeID) || data.getClusterSize(planeID) > maxClusterSize) return;
    int clusterSize = data.getClusterSize(planeID);


    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    const Window* theWindow    = theWindowsManager_->getWindow(planeID);
    int           rowPredicted = data.getRowPredicted(planeID);
    int           colPredicted = data.getColPredicted(planeID);
    int           run          = data.getRunNumber();

    if (!theWindow->checkWindow(colPredicted,rowPredicted,run)) return;

    for (int h = 0; h < clusterSize; h++)
    {
        if (!theWindow->checkWindow(data.getClusterPixelCol(h,planeID),data.getClusterPixelRow(h,planeID),run) // Hits are in the window
                || !data.getIsPixelCalibrated  (h,planeID)                                                         // Pixels are calibrated
                ||  data.getClusterPixelCharge (h,planeID) < standardCutsPixelMinimumCharge_                       // Charge is over threshold
                ||  data.getClusterPixelCharge (h,planeID) > standardCutsPixelMaximumCharge_)                      // Maximum allowed charge for this physics
            return;

        if ((data.getClusterPixelRow(h,planeID) == rowPredicted) &&
                (data.getClusterPixelCol(h,planeID) == colPredicted))
            foundCluster = true;
    }
    if (foundCluster == false) return;


    // #####################################################################
    // # Compute charge only for cells that are surrounded by "good" cells #
    // #####################################################################
    if (theWindow->checkWindowAbout(colPredicted,rowPredicted,run,thePlaneMapping_->getPlaneType(planeID)))
    {
        THREADED(h2DChargeNorm_[planeID])->Fill(colPredicted,rowPredicted);
        if (data.getHasHit(planeID)) THREADED(h2DCharge_[planeID])->Fill(colPredicted,rowPredicted,data.getClusterCharge(planeID));
//Hsin-Wei added plots for 225um pixels
        if (planeID == 16){
           if(data.getXPitchLocal(planeID) == 225){
           THREADED(h2DChargeNorm225um_[planeID])->Fill(colPredicted,rowPredicted);
           if (data.getHasHit(planeID)) THREADED(h2DCharge225um_[planeID])->Fill(colPredicted,rowPredicted,data.getClusterCharge(planeID));
         }
        }
    }

    // ################################
    // # Compute charge for all cells #
    // ################################
    THREADED(h2DChargeRefNorm_[planeID])->Fill(colPredicted,rowPredicted);
    if (data.getHasHit(planeID)) THREADED(h2DChargeRef_[planeID])->Fill(colPredicted,rowPredicted,data.getClusterCharge(planeID));
//Hsin-Wei added plots for 225um pixels
    if (planeID == 16){
      if(data.getXPitchLocal(planeID) == 225){
         THREADED(h2DChargeRefNorm225um_[planeID])->Fill(colPredicted,rowPredicted);
         if (data.getHasHit(planeID)) THREADED(h2DChargeRef225um_[planeID])->Fill(colPredicted,rowPredicted,data.getClusterCharge(planeID));
         }
        }
}

//=======================================================================
void ChargeRD53A::cellCharge(bool pass, int planeID, const Data& data, int threadNumber)
{
    // #####################
    // # Internal constant #
    // #####################
    int maxClusterSize = 4;

    float maxPitchX;
    float maxPitchY;
    float xPixelResidual;
    float yPixelResidual;
    float xPixelEdgeResidual = 0;
    float yPixelEdgeResidual = 0;


    if (!pass || !data.getIsInDetector(planeID) || !data.getHasHit(planeID) || data.getClusterSize(planeID) > maxClusterSize) return;
    int clusterSize = data.getClusterSize(planeID);

    maxPitchX = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->getCellPitches().first).c_str());
    maxPitchY = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->getCellPitches().second).c_str());
    if (data.getXPitchLocal(planeID) > maxPitchX || data.getYPitchLocal(planeID) > maxPitchY) return;

    xPixelResidual = data.getXPixelResidualLocal(planeID);
    yPixelResidual = data.getYPixelResidualLocal(planeID);

    if (xPixelResidual > 0)       xPixelEdgeResidual = xPixelResidual - data.getXPitchLocal(planeID)/2;
    else if (xPixelResidual <= 0) xPixelEdgeResidual = xPixelResidual + data.getXPitchLocal(planeID)/2;

    if (yPixelResidual > 0)       yPixelEdgeResidual = yPixelResidual - data.getYPitchLocal(planeID)/2;
    else if (yPixelResidual <= 0) yPixelEdgeResidual = yPixelResidual + data.getYPitchLocal(planeID)/2;


    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    const Window* theWindow    = theWindowsManager_->getWindow(planeID);
    int           rowPredicted = data.getRowPredicted(planeID);
    int           colPredicted = data.getColPredicted(planeID);
    int           run          = data.getRunNumber();

    if (!theWindow->checkWindow(colPredicted,rowPredicted,run)) return;

    for (int h = 0; h < clusterSize; h++)
    {
        if (theWindow->checkWindow(data.getClusterPixelCol(h,planeID),data.getClusterPixelRow(h,planeID),run) &&
                data.getClusterPixelRow   (h,planeID) == rowPredicted &&
                data.getClusterPixelCol   (h,planeID) == colPredicted &&
                data.getIsPixelCalibrated (h,planeID)                 &&
                data.getClusterPixelCharge(h,planeID) > standardCutsPixelMinimumCharge_ &&
                data.getClusterPixelCharge(h,planeID) < standardCutsPixelMaximumCharge_)
        {
            THREADED(h4CellsCharge_    [planeID])->Fill(xPixelEdgeResidual,yPixelEdgeResidual,data.getClusterPixelCharge(h,planeID));
            THREADED(h4CellsChargeNorm_[planeID])->Fill(xPixelEdgeResidual,yPixelEdgeResidual);

            THREADED(h2DCellCharge_    [planeID])->Fill(xPixelResidual,yPixelResidual,data.getClusterPixelCharge(h,planeID));
            THREADED(h2DCellChargeNorm_[planeID])->Fill(xPixelResidual,yPixelResidual);

            if (((int)colPredicted)%2 == 0)
            {
                THREADED(h2DCellChargeEven_    [planeID])->Fill(xPixelResidual,yPixelResidual,data.getClusterPixelCharge(h,planeID));
                THREADED(h2DCellChargeEvenNorm_[planeID])->Fill(xPixelResidual,yPixelResidual);
            }
            else
            {
                THREADED(h2DCellChargeOdd_    [planeID])->Fill(xPixelResidual,yPixelResidual,data.getClusterPixelCharge(h,planeID));
                THREADED(h2DCellChargeOddNorm_[planeID])->Fill(xPixelResidual,yPixelResidual);
            }

            if (clusterSize == 1)
            {
                THREADED(h2DCellChargeSize1_    [planeID])->Fill(xPixelResidual,yPixelResidual,data.getClusterPixelCharge(h,planeID));
                THREADED(h2DCellChargeSize1Norm_[planeID])->Fill(xPixelResidual,yPixelResidual);
            }
            else if (clusterSize == 2)
            {
                THREADED(h2DCellChargeSize2_    [planeID])->Fill(xPixelResidual,yPixelResidual,data.getClusterPixelCharge(h,planeID));
                THREADED(h2DCellChargeSize2Norm_[planeID])->Fill(xPixelResidual,yPixelResidual);
            }

            THREADED(h2DClusterSize_[planeID])->Fill(xPixelResidual,yPixelResidual,data.getClusterSize(planeID));
//Hsin-Wei added plots for 225um pixels
            if (planeID == 16){
              if(data.getXPitchLocal(planeID) == 225){
                 THREADED(h2DClusterSize225um_[planeID])->Fill(xPixelResidual,yPixelResidual,data.getClusterSize(planeID));
                 THREADED(h2DCellChargeNorm225um_[planeID])->Fill(xPixelResidual,yPixelResidual);
         		}
        	}	
		
	    if (clusterSize > 2) 
	    {
                THREADED(h2DLargeClusterSize_[planeID])->Fill(xPixelResidual,yPixelResidual,data.getClusterSize(planeID));
                THREADED(h2DLargeClusterSizeNorm_[planeID])->Fill(xPixelResidual,yPixelResidual);
	    }
        }
    }
}

//=======================================================================
void ChargeRD53A::xLandau(bool pass, int planeID, const Data &data, int threadNumber)
{
    // #####################
    // # Internal constant #
    // #####################
    int clusterSize = 2;


    if (!pass || !data.getIsInDetector(planeID) || !data.getHasHit(planeID) || data.getClusterSize(planeID) != clusterSize) return;


    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    const Window* theWindow    = theWindowsManager_->getWindow(planeID);
    int           rowPredicted = data.getRowPredicted(planeID);
    int           colPredicted = data.getColPredicted(planeID);
    int           run          = data.getRunNumber();

    if (!theWindow->checkWindow(colPredicted,rowPredicted,run)) return;

    for (int h = 0; h < clusterSize; h++)
    {
        if (!theWindow->checkWindow(data.getClusterPixelCol(h,planeID),data.getClusterPixelRow(h,planeID),run) // Hits are in the window
                || !data.getIsPixelCalibrated  (h,planeID)                                                         // Pixels are calibrated
                ||  data.getClusterPixelRow    (h,planeID) != rowPredicted                                         // Hits are on the same row (sharing is along the row - x direction)
                ||  data.getClusterPixelCharge (h,planeID) < standardCutsPixelMinimumCharge_                       // Charge is over threshold
                ||  data.getClusterPixelCharge (h,planeID) > standardCutsPixelMaximumCharge_)                      // Maximum allowed charge for this physics
            return;
    }


    THREADED(hLandauClusterSize2sameRow_[planeID])->Fill(data.getClusterCharge(planeID));
}

//=======================================================================
void ChargeRD53A::yLandau(bool pass, int planeID, const Data &data, int threadNumber)
{
    // #####################
    // # Internal constant #
    // #####################
    int clusterSize = 2;


    if (!pass || !data.getHasHit(planeID) || data.getClusterSize(planeID) != clusterSize) return;


    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    const Window* theWindow    = theWindowsManager_->getWindow(planeID);
    int           rowPredicted = data.getRowPredicted(planeID);
    int           colPredicted = data.getColPredicted(planeID);
    int           run          = data.getRunNumber();

    if (!theWindow->checkWindow(colPredicted,rowPredicted,run)) return;

    for (int h = 0; h < clusterSize; h++)
    {
        if (!theWindow->checkWindow(data.getClusterPixelCol(h,planeID),data.getClusterPixelRow(h,planeID),run) // Hits are in the window
                || !data.getIsPixelCalibrated  (h,planeID)                                                         // Pixels are calibrated
                ||  data.getClusterPixelCol    (h,planeID) != colPredicted                                         // Hits are on the same column (sharing is along the columm - y direction)
                ||  data.getClusterPixelCharge (h,planeID) < standardCutsPixelMinimumCharge_                       // Charge is over threshold
                ||  data.getClusterPixelCharge (h,planeID) > standardCutsPixelMaximumCharge_)                      // Maximum allowed charge for this physics
            return;
    }


    THREADED(hLandauClusterSize2sameCol_[planeID])->Fill(data.getClusterCharge(planeID));
}

//=======================================================================
void ChargeRD53A::xChargeDivision(bool pass, int planeID, const Data& data, int threadNumber)
{
    // #####################
    // # Internal constant #
    // #####################
    int maxClusterSize = 2;

    float xRes = 0;
    float maxPitchX;
    float maxPitchY;


    if (!pass||!data.getIsInDetector(planeID)) return;

    maxPitchX = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->getCellPitches().first).c_str());
    maxPitchY = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->getCellPitches().second).c_str());
    if (data.getXPitchLocal(planeID) > maxPitchX || data.getYPitchLocal(planeID) > maxPitchY) return;

    if      (data.getXPixelResidualLocal(planeID) >  0) xRes = data.getXPixelResidualLocal(planeID) - data.getXPitchLocal(planeID)/2;
    else if (data.getXPixelResidualLocal(planeID) <= 0) xRes = data.getXPixelResidualLocal(planeID) + data.getXPitchLocal(planeID)/2;


    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    const Window* theWindow    = theWindowsManager_->getWindow(planeID);
    int           rowPredicted = data.getRowPredicted(planeID);
    int           colPredicted = data.getColPredicted(planeID);
    int           run          = data.getRunNumber();

    if (!theWindow->checkWindow(colPredicted,rowPredicted,run)) return;

    bool myReturn = false;
    if ((theWindow->checkWindowAbout(colPredicted,rowPredicted,run,thePlaneMapping_->getPlaneType(planeID))) && (data.getClusterSize(planeID) <= maxClusterSize))
    {
        for (int h = 0; h < data.getClusterSize(planeID); h++)
        {
            if (!theWindow->checkWindow(data.getClusterPixelCol(h,planeID),data.getClusterPixelRow(h,planeID),run) // Hits are in the window
                    || !data.getIsPixelCalibrated  (h,planeID)                                                         // Pixels are calibrated
                    ||  data.getClusterPixelCharge (h,planeID) < standardCutsPixelMinimumCharge_                       // Charge is over threshold
                    ||  data.getClusterPixelCharge (h,planeID) > standardCutsPixelMaximumCharge_)                      // Maximum allowed charge for this physics
            {
                myReturn = true;
                break;
            }
        }

        THREADED(h1DXcellChargeNorm_         [planeID])->Fill(xRes);
        THREADED(h1DXcellChargeSecondHitNorm_[planeID])->Fill(xRes);

        if ((data.getHasHit(planeID)) && (!myReturn))
        {
            // Special instructions for 25 x 100 sensors: since telescope resolution is very large w.r.t to the short pitch
            // check adjacent rows when performing scan along 100 um pitch.

            for (int h = 0; h < data.getClusterSize(planeID); h++)
            {
                if(((data.getClusterPixelCol(h,planeID) == colPredicted) && (data.getClusterPixelRow(h,planeID) == rowPredicted))                                                                                  ||
                   ((maxPitchY==SPECIALPITCH) && (data.getClusterPixelCol(h,planeID) == colPredicted) && (data.getYPixelResidualLocal(planeID) >  0) && (rowPredicted - data.getClusterPixelRow(h,planeID) == -1))||
                   ((maxPitchY==SPECIALPITCH) && (data.getClusterPixelCol(h,planeID) == colPredicted) && (data.getYPixelResidualLocal(planeID) <= 0) && (rowPredicted - data.getClusterPixelRow(h,planeID) ==  1))  )
                {
                    THREADED(h2DXcellCharge_          [planeID])->Fill(xRes,data.getClusterPixelCharge(h,planeID));
                    THREADED(h2DXcellChargeSecondHit_ [planeID])->Fill(xRes,data.getClusterPixelCharge(h,planeID));
                    THREADED(h1DXcellCharge_          [planeID])->Fill(xRes,data.getClusterPixelCharge(h,planeID));
                    THREADED(h1DXcellChargeSecondHit_ [planeID])->Fill(xRes,data.getClusterPixelCharge(h,planeID));
                    break;
                }
            }

            for (int h = 0; h < data.getClusterSize(planeID); h++)
            {
                if( (data.getClusterPixelRow(h,planeID) == rowPredicted)                                                                                 ||
                   ((maxPitchY==SPECIALPITCH) &&(data.getYPixelResidualLocal(planeID) >  0) && (rowPredicted - data.getClusterPixelRow(h,planeID) == -1))||
                   ((maxPitchY==SPECIALPITCH) &&(data.getYPixelResidualLocal(planeID) <= 0) && (rowPredicted - data.getClusterPixelRow(h,planeID) ==  1))  )
                {
                    if (((xRes >  0) && (colPredicted - data.getClusterPixelCol(h,planeID) ==  1))||
                        ((xRes <= 0) && (colPredicted - data.getClusterPixelCol(h,planeID) == -1))  )
                    {
                        THREADED(h2DXcellChargeSecondHit_[planeID])->Fill(xRes,data.getClusterPixelCharge(h,planeID));
                        THREADED(h1DXcellChargeSecondHit_[planeID])->Fill(xRes,data.getClusterPixelCharge(h, planeID));
                        break;
                    }
                }
            }
        }
    }
}

//=======================================================================
void ChargeRD53A::xAsymmetry(bool pass, int planeID, const Data& data, int threadNumber)
{
    // #####################
    // # Internal constant #
    // #####################
    int maxClusterSize = 4;

    float maxPitchX;


    if (!pass || !data.getIsInDetector(planeID) || !data.getHasHit(planeID) || data.getClusterSize(planeID) > maxClusterSize) return;
    int clusterSize = data.getClusterSize(planeID);

    maxPitchX = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->getCellPitches().first).c_str());
    if (data.getXPitchLocal(planeID) > maxPitchX) return;


    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    const Window* theWindow    = theWindowsManager_->getWindow(planeID);
    int           rowPredicted = data.getRowPredicted(planeID);
    int           colPredicted = data.getColPredicted(planeID);
    int           run          = data.getRunNumber();

    if (!theWindow->checkWindow(colPredicted,rowPredicted,run)) return;

    for (int h = 0; h < clusterSize; h++)
    {
        if (!theWindow->checkWindow(data.getClusterPixelCol(h,planeID),data.getClusterPixelRow(h,planeID),run) // Hits are in the window
                || !data.getIsPixelCalibrated  (h,planeID)                                                         // Pixels are calibrated
                ||  data.getClusterPixelRow    (h,planeID) != rowPredicted                                         // Hits are on the same column (sharing is along the column - y direction)
                ||  data.getClusterPixelCharge (h,planeID) < standardCutsPixelMinimumCharge_                       // Charge is over threshold
                ||  data.getClusterPixelCharge (h,planeID) > standardCutsPixelMaximumCharge_)                      // Maximum allowed charge for this physics
            return;
    }


    if (clusterSize == 2)
    {
        float asymmetry   = 0;
        int   totalCharge = 0;
        int   chargeLeft  = 0;
        int   chargeRight = 0;
        float xPredicted  = data.getXPredictedLocal(planeID);
        float xMeasured   = (data.getXClusterPixelCenterLocal(0, planeID) + data.getXClusterPixelCenterLocal(1, planeID))/2;
        float xResidual   = xPredicted - xMeasured;

        if (data.getXClusterPixelCenterLocal(0, planeID) > data.getXClusterPixelCenterLocal(1, planeID))
        {
            chargeRight = data.getClusterPixelCharge(0, planeID);
            chargeLeft  = data.getClusterPixelCharge(1, planeID);
        }
        else if (data.getXClusterPixelCenterLocal(0, planeID) < data.getXClusterPixelCenterLocal(1, planeID))
        {
            chargeRight = data.getClusterPixelCharge(1, planeID);
            chargeLeft  = data.getClusterPixelCharge(0, planeID);
        }

        totalCharge = chargeLeft + chargeRight;

        if (totalCharge > 0 && totalCharge < maxChargeDeltaRay)
        {
            asymmetry = (float)(chargeLeft - chargeRight) / (float)totalCharge;

            if (totalCharge >= standardCutsPixelMinimumCharge_ && totalCharge <= standardCutsPixelMaximumCharge_)
            {
                THREADED(h2DXcellChargeAsymmetry_   [planeID])->Fill(xResidual, asymmetry);
                THREADED(h2DXcellChargeAsymmetryInv_[planeID])->Fill(asymmetry, xResidual);
            }
        }
    }
}

//=======================================================================
void ChargeRD53A::yChargeDivision(bool pass, int planeID, const Data& data, int threadNumber)
{
    // #####################
    // # Internal constant #
    // #####################
    int maxClusterSize = 2;

    float yRes = 0;
    float maxPitchX;
    float maxPitchY;


    if (!pass || !data.getIsInDetector(planeID)) return;

    maxPitchX = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->getCellPitches().first).c_str());
    maxPitchY = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->getCellPitches().second).c_str());
    if (data.getXPitchLocal(planeID) > maxPitchX || data.getYPitchLocal(planeID) > maxPitchY) return;
    
    if      (data.getYPixelResidualLocal(planeID) >  0) yRes = data.getYPixelResidualLocal(planeID) - data.getYPitchLocal(planeID)/2;
    else if (data.getYPixelResidualLocal(planeID) <= 0) yRes = data.getYPixelResidualLocal(planeID) + data.getYPitchLocal(planeID)/2;


    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    const Window* theWindow    = theWindowsManager_->getWindow(planeID);
    int           rowPredicted = data.getRowPredicted(planeID);
    int           colPredicted = data.getColPredicted(planeID);
    int           run          = data.getRunNumber();

    if (!theWindow->checkWindow(colPredicted,rowPredicted,run)) return;

    bool myReturn = false;
    if ((theWindow->checkWindowAbout(colPredicted,rowPredicted,run,thePlaneMapping_->getPlaneType(planeID))) && (data.getClusterSize(planeID) <= maxClusterSize))
    {
        for (int h = 0; h < data.getClusterSize(planeID); h++)
        {
            if (!theWindow->checkWindow(data.getClusterPixelCol(h,planeID),data.getClusterPixelRow(h,planeID),run) // Hits are in the window
                    || !data.getIsPixelCalibrated  (h,planeID)                                                         // Pixels are calibrated
                    ||  data.getClusterPixelCharge (h,planeID) < standardCutsPixelMinimumCharge_                       // Charge is over threshold
                    ||  data.getClusterPixelCharge (h,planeID) > standardCutsPixelMaximumCharge_)                      // Maximum allowed charge for this physics
            {
                myReturn = true;
                break;
            }
        }

        THREADED(h1DYcellChargeNorm_         [planeID])->Fill(yRes);
        THREADED(h1DYcellChargeSecondHitNorm_[planeID])->Fill(yRes);

        if ((data.getHasHit(planeID)) && (!myReturn ))
        {
            for (int h = 0; h < data.getClusterSize(planeID); h++)
            {
                if (data.getClusterPixelRow(h,planeID) == rowPredicted && data.getClusterPixelCol(h,planeID) == colPredicted)
                {
                    THREADED(h2DYcellCharge_          [planeID])->Fill(yRes,data.getClusterPixelCharge(h,planeID));
                    THREADED(h2DYcellChargeSecondHit_ [planeID])->Fill(yRes,data.getClusterPixelCharge(h,planeID));
                    THREADED(h1DYcellCharge_          [planeID])->Fill(yRes,data.getClusterPixelCharge(h,planeID));
                    THREADED(h1DYcellChargeSecondHit_ [planeID])->Fill(yRes,data.getClusterPixelCharge(h,planeID));
                    break;
                }
            }

            for (int h = 0; h < data.getClusterSize(planeID); h++)
            {
                if(data.getClusterPixelCol(h,planeID) == colPredicted)
                {
                    if(((yRes > 0 ) && (rowPredicted - data.getClusterPixelRow(h,planeID) ==  1))||
                       ((yRes <= 0) && (rowPredicted - data.getClusterPixelRow(h,planeID) == -1))  )
//                        if((rowPredicted - data.getClusterPixelRow(h,planeID) == -1)||
//                           (rowPredicted - data.getClusterPixelRow(h,planeID) ==  1)  )
                    {
                        THREADED(h2DYcellChargeSecondHit_[planeID])->Fill(yRes,data.getClusterPixelCharge(h,planeID));
                        THREADED(h1DYcellChargeSecondHit_[planeID])->Fill(yRes,data.getClusterPixelCharge(h,planeID));
                        break;
                    }
                }
            }
        }
    }
}

//=======================================================================
void ChargeRD53A::yAsymmetry(bool pass, int planeID, const Data& data, int threadNumber)
{
    // #####################
    // # Internal constant #
    // #####################
    int maxClusterSize = 4;

    float maxPitchY;


    if (!pass || !data.getIsInDetector(planeID) || !data.getHasHit(planeID) || data.getClusterSize(planeID) > maxClusterSize) return;
    int clusterSize = data.getClusterSize(planeID);

    maxPitchY = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->getCellPitches().second).c_str());
    if (data.getYPitchLocal(planeID) > maxPitchY) return;


    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    const Window* theWindow    = theWindowsManager_->getWindow(planeID);
    int           rowPredicted = data.getRowPredicted(planeID);
    int           colPredicted = data.getColPredicted(planeID);
    int           run          = data.getRunNumber();

    if (!theWindow->checkWindow(colPredicted,rowPredicted,run)) return;

    for (int h = 0; h < clusterSize; h++)
    {
        if (!theWindow->checkWindow(data.getClusterPixelCol(h,planeID),data.getClusterPixelRow(h,planeID),run) // Hits are in the window
                || !data.getIsPixelCalibrated  (h,planeID)                                                         // Pixels are calibrated
                ||  data.getClusterPixelCol    (h,planeID) != colPredicted                                         // Hits are on the same column (sharing is along the column - y direction)
                ||  data.getClusterPixelCharge (h,planeID) < standardCutsPixelMinimumCharge_                       // Charge is over threshold
                ||  data.getClusterPixelCharge (h,planeID) > standardCutsPixelMaximumCharge_)                      // Maximum allowed charge for this physics
            return;
    }


    if (clusterSize == 2)
    {
        float asymmetry   = 0;
        int   totalCharge = 0;
        int   chargeDown  = 0;
        int   chargeUp    = 0;
        float yPredicted  = data.getYPredictedLocal(planeID);
        float yMeasured   = (data.getYClusterPixelCenterLocal(0, planeID) + data.getYClusterPixelCenterLocal(1, planeID))/2;
        float yResidual   = yPredicted - yMeasured;

        if (data.getYClusterPixelCenterLocal(0, planeID) > data.getYClusterPixelCenterLocal(1, planeID))
        {
            chargeUp   = data.getClusterPixelCharge(0, planeID);
            chargeDown = data.getClusterPixelCharge(1, planeID);
        }
        else if (data.getYClusterPixelCenterLocal(0, planeID) < data.getYClusterPixelCenterLocal(1, planeID))
        {
            chargeUp   = data.getClusterPixelCharge(1, planeID);
            chargeDown = data.getClusterPixelCharge(0, planeID);
        }

        totalCharge = chargeDown + chargeUp;

        if (totalCharge > 0 && totalCharge < maxChargeDeltaRay)
        {
            asymmetry = (float)(chargeDown - chargeUp) / (float)totalCharge;

            if (totalCharge >= standardCutsPixelMinimumCharge_ && totalCharge <= standardCutsPixelMaximumCharge_)
            {
                THREADED(h2DYcellChargeAsymmetry_   [planeID])->Fill(yResidual, asymmetry);
                THREADED(h2DYcellChargeAsymmetryInv_[planeID])->Fill(asymmetry, yResidual);
            }
        }
    }
}

//=======================================================================
void ChargeRD53A::setCutsFormula(std::map<std::string,std::string> cutsList,std::vector<TTree*> tree)
{
    std::vector<TTreeFormula*> formulasVector;

    for (std::map<std::string,std::string>::iterator it = cutsList.begin(); it != cutsList.end(); it++)
    {
        if ((it->first) == "main cut" && (it->second).size() == 0)
            STDLINE("WARNING: no main cut set in charge analysis ! Default value = true !", ACRed);

        formulasVector.clear();
        if ((it->second).size() != 0)
        {
            for (unsigned int t = 0; t < tree.size(); t++)
                formulasVector.push_back(new TTreeFormula((it->second).c_str(),(it->second).c_str(),tree[t]));
            cutsFormulas_[it->first] = formulasVector;
        }
    }
}

//=======================================================================
bool ChargeRD53A::passCalibrationsCut(int planeID, const Data &data)
{
    // #####################
    // # Internal constant #
    // #####################
    int maxClusterSize = 4;


    if (!(theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->useCalibrations()) return true;
    if (data.getClusterSize(planeID) > maxClusterSize)                                             return false;

    return true;
}

//=======================================================================
bool ChargeRD53A::passStandardCuts(int planeID, const Data &data)
{
    if (!theXmlParser_->getAnalysesFromString("Charge")->standardCut()) return true;

    int minHits   = atoi(theXmlParser_->getAnalysesFromString("Charge")->getMinHits().c_str()) - 1;
    int excludeMe = 0;
    if (thePlaneMapping_->getPlaneName(planeID).find("Dut") != std::string::npos) minHits += 1;
    else if (data.getHasHit(planeID) && data.getClusterSize(planeID) <= 2) excludeMe = 1;

    if (data.getNumberOfTelescopeHits() - excludeMe >= minHits) return true;
    else                                                        return false;
}

//=======================================================================
void ChargeRD53A::beginJob(void)
{
    standardCutsPixelMinimumCharge_   = theXmlParser_->getAnalysesFromString("Charge")->getPixelMinimumCharge();
    standardCutsPixelMaximumCharge_   = theXmlParser_->getAnalysesFromString("Charge")->getPixelMaximumCharge();
    standardCutsClusterMinimumCharge_ = theXmlParser_->getAnalysesFromString("Charge")->getClusterMinimumCharge();
    standardCutsClusterMaximumCharge_ = theXmlParser_->getAnalysesFromString("Charge")->getClusterMaximumCharge();

    theWindowsManager_      = theAnalysisManager_->getWindowsManager();
    theCalibrationsManager_ = theAnalysisManager_->getCalibrationsManager();

    book();
}

//=======================================================================
void ChargeRD53A::analyze(const Data& data, int threadNumber)
{
    if (cutsFormulas_.find("main cut") != cutsFormulas_.end() && !cutsFormulas_["main cut"][theAnalysisManager_->getCurrentTreeNumber(threadNumber)]->EvalInstance()) return;

    for (unsigned int p = 0; p < thePlaneMapping_->getNumberOfPlanes(); p++) clusterSize(p,data,threadNumber);

    bool clusterLandauCut = true;
    if(cutsFormulas_.find("cluster Landau") != cutsFormulas_.end())
        clusterLandauCut = cutsFormulas_["cluster Landau"][theAnalysisManager_->getCurrentTreeNumber(threadNumber)]->EvalInstance();

    bool cellLandauCut = true;
    if(cutsFormulas_.find("cell Landau") != cutsFormulas_.end())
        cellLandauCut = cutsFormulas_["cell Landau"][theAnalysisManager_->getCurrentTreeNumber(threadNumber)]->EvalInstance();

    bool cellChargeCut = true;
    if(cutsFormulas_.find("cell charge") != cutsFormulas_.end())
        cellChargeCut = cutsFormulas_["cell charge"][theAnalysisManager_->getCurrentTreeNumber(threadNumber)]->EvalInstance();

    bool cellChargeXCut = true;
    if(cutsFormulas_.find("cell charge X") != cutsFormulas_.end())
        cellChargeXCut = cutsFormulas_["cell charge X"][theAnalysisManager_->getCurrentTreeNumber(threadNumber)]->EvalInstance();

    bool cellChargeYCut = true;
    if(cutsFormulas_.find("cell charge Y") != cutsFormulas_.end())
        cellChargeYCut = cutsFormulas_["cell charge Y"][theAnalysisManager_->getCurrentTreeNumber(threadNumber)]->EvalInstance();


    // ######################################################
    // # Require all telescope planes with cluster size = 2 #
    // ######################################################
    if (ONLYdoubleHITS == true)
    {
        for (unsigned int p = 0; p < thePlaneMapping_->getNumberOfPlanes(); p++)
        {
            if ((thePlaneMapping_->getPlaneName(p).find("Dut")   == std::string::npos) &&
                    (thePlaneMapping_->getPlaneName(p).find("Strip") == std::string::npos) &&
                    (data.getClusterSize(p) != 2)) return;
        }
    }

    // ######################################
    // # Require a hit one each strip plane #
    // ######################################
    if (HITALLSTRIPS == true)
    {
        int stripHits = 0;
        for (unsigned int p = 0; p < thePlaneMapping_->getNumberOfPlanes(); p++)
        {
            if((thePlaneMapping_->getPlaneName(p).find("Strip") != std::string::npos) &&
                    (data.getHasHit(p)))
            {
                stripHits++;
            }
        }

        if(stripHits != data.getNumberOfActiveStripPlanes()) return;
    }

    for (unsigned int p = 0; p < thePlaneMapping_->getNumberOfPlanes(); p++)
    {
        if (!passStandardCuts(p,data)) continue;

        if ((thePlaneMapping_->getPlaneName(p).find("Dut") != std::string::npos) && (!passCalibrationsCut(p,data)))
        {
            std::cout << __PRETTY_FUNCTION__ << "Calibration check not passed" << std::endl;
            return;
        }

        clusterLandau   (clusterLandauCut,p,data,threadNumber);
        cellLandau      (cellLandauCut,   p,data,threadNumber);
        cellCharge      (cellChargeCut,   p,data,threadNumber);
        planeCharge     (true,            p,data,threadNumber);

        xLandau         (cellChargeXCut,  p,data,threadNumber);
        xChargeDivision (cellChargeXCut,  p,data,threadNumber);
        xAsymmetry      (cellChargeXCut,  p,data,threadNumber);

        yLandau         (cellChargeYCut,  p,data,threadNumber);
        yChargeDivision (cellChargeYCut,  p,data,threadNumber);
        yAsymmetry      (cellChargeYCut,  p,data,threadNumber);
    }
}

//=======================================================================
void ChargeRD53A::endJob(void)
{
    std::stringstream ss;
    double charge;

    STDLINE("",ACWhite);

    for (unsigned int p = 0; p < thePlaneMapping_->getNumberOfPlanes(); p++)
    {
        std::string planeName = thePlaneMapping_->getPlaneName(p);
        ss.str("") ; ss << "Adding threads for plane " << p;
        STDLINE(ss.str().c_str(),ACYellow);

        ADD_THREADED(hCellLandau_                             [p]);
        ADD_THREADED(hCellLandauOdd_                          [p]);
        ADD_THREADED(hCellLandauOddLeft_                      [p]);
        ADD_THREADED(hCellLandauOddRight_                     [p]);
        ADD_THREADED(hCellLandauEven_                         [p]);
        ADD_THREADED(hCellLandauEvenLeft_                     [p]);
        ADD_THREADED(hCellLandauEvenRight_                    [p]);
        ADD_THREADED(hClusterSize_                            [p]);
        ADD_THREADED(hClusterMinHitChargeRatio_               [p]);
        ADD_THREADED(hClusterFirstRow_                        [p]);
        ADD_THREADED(hClusterFirstCol_                        [p]);

        ADD_THREADED(hLandauClusterSizeUpToMax_               [p]);

        for(int i = 0; i < maxLandauClusterSize_; ++i)
        {
          ADD_THREADED(hLandauClusterSizeVector_[i]           [p]);
        }

        ADD_THREADED(hLandauClusterSize2sameRow_              [p]);
        ADD_THREADED(hLandauClusterSize2sameCol_              [p]);
        ADD_THREADED(hCellLandauSinglePixel_                  [p]);


        ADD_THREADED(h1DXcellCharge_                          [p]);
        ADD_THREADED(h1DXcellChargeNorm_                      [p]);

        ADD_THREADED(h1DXcellChargeSecondHit_                 [p]);
        ADD_THREADED(h1DXcellChargeSecondHitNorm_             [p]);

        ADD_THREADED(h1DYcellCharge_                          [p]);
        ADD_THREADED(h1DYcellChargeNorm_                      [p]);

        ADD_THREADED(h1DYcellChargeSecondHit_                 [p]);
        ADD_THREADED(h1DYcellChargeSecondHitNorm_             [p]);


        ADD_THREADED(h2DClusterSize_                          [p]);

        ADD_THREADED(h2DLargeClusterSize_                     [p]);
        ADD_THREADED(h2DLargeClusterSizeNorm_                 [p]);

        ADD_THREADED(h2DCharge_              	              [p]);
        ADD_THREADED(h2DChargeNorm_          	              [p]);

        ADD_THREADED(h2DChargeRef_              	      [p]);
        ADD_THREADED(h2DChargeRefNorm_          	      [p]);

        ADD_THREADED(h2DChargeRefRebinned_      	      [p]);
        ADD_THREADED(h2DChargeRefNormRebinned_  	      [p]);

        ADD_THREADED(h2DChargeRefZoomedIn50x50_      	      [p]);
        ADD_THREADED(h2DChargeRefNormZoomedIn50x50_  	      [p]);

        ADD_THREADED(h2DChargeRefZoomedIn25x100_      	      [p]);
        ADD_THREADED(h2DChargeRefNormZoomedIn25x100_  	      [p]);

        ADD_THREADED(h2DCellCharge_                           [p]);
        ADD_THREADED(h2DCellChargeNorm_                       [p]);

        ADD_THREADED(h2DCellChargeOdd_                        [p]);
        ADD_THREADED(h2DCellChargeOddNorm_                    [p]);

        ADD_THREADED(h2DCellChargeEven_                       [p]);
        ADD_THREADED(h2DCellChargeEvenNorm_                   [p]);

        ADD_THREADED(h2DCellChargeSize1_                      [p]);
        ADD_THREADED(h2DCellChargeSize1Norm_                  [p]);

        ADD_THREADED(h2DCellChargeSize2_                      [p]);
        ADD_THREADED(h2DCellChargeSize2Norm_                  [p]);

        ADD_THREADED(h4CellsCharge_                           [p]);
        ADD_THREADED(h4CellsChargeNorm_                       [p]);

        ADD_THREADED(h2DXcellCharge_                          [p]);
        ADD_THREADED(h2DXcellChargeSecondHit_                 [p]);

        ADD_THREADED(h2DYcellCharge_                          [p]);
        ADD_THREADED(h2DYcellChargeSecondHit_                 [p]);


        ADD_THREADED(h2DXcellChargeAsymmetry_                 [p]);
        ADD_THREADED(h2DXcellChargeAsymmetryInv_              [p]);

        ADD_THREADED(h2DYcellChargeAsymmetry_                 [p]);
        ADD_THREADED(h2DYcellChargeAsymmetryInv_              [p]);
//Hsin-Wei have added plots for 225um pixels
        ADD_THREADED(hClusterSize225um_                       [p]);
        ADD_THREADED(h2DClusterSize225um_                     [p]);
        ADD_THREADED(h2DCharge225um_              	      [p]);
        ADD_THREADED(h2DCellChargeNorm225um_                  [p]);
        ADD_THREADED(h2DChargeNorm225um_          	      [p]);
        ADD_THREADED(h2DChargeRef225um_              	      [p]);
        ADD_THREADED(h2DChargeRefNorm225um_          	      [p]);
        ADD_THREADED(hLandauClusterSizeUpToMax225um_               [p]);
        for(int i = 0; i < maxLandauClusterSize_; ++i)
        {
          ADD_THREADED(h225umLandauClusterSizeVector_[i]           [p]);
        }

        STDLINE("Threading phase completed",ACGreen);


        STDLINE("Filling phase...",ACWhite);

        h1DXcellCharge_               [p]->Divide(h1DXcellChargeNorm_          [p]);
        h1DXcellChargeSecondHit_      [p]->Divide(h1DXcellChargeSecondHitNorm_ [p]);

        h1DYcellCharge_               [p]->Divide(h1DYcellChargeNorm_          [p]);
        h1DYcellChargeSecondHit_      [p]->Divide(h1DYcellChargeSecondHitNorm_ [p]);

        h2DCellCharge_                [p]->Divide(h2DCellChargeNorm_           [p]);
        h2DCellChargeSize1_           [p]->Divide(h2DCellChargeSize1Norm_      [p]);
        h2DCellChargeSize2_           [p]->Divide(h2DCellChargeSize2Norm_      [p]);
        h2DCharge_                    [p]->Divide(h2DChargeNorm_               [p]);
        h2DChargeRef_                 [p]->Divide(h2DChargeRefNorm_            [p]);
        h2DCellChargeOdd_             [p]->Divide(h2DCellChargeOddNorm_        [p]);
        h2DCellChargeEven_            [p]->Divide(h2DCellChargeEvenNorm_       [p]);
        h2DClusterSize_               [p]->Divide(h2DCellChargeNorm_           [p]);
        h2DLargeClusterSize_          [p]->Divide(h2DLargeClusterSizeNorm_     [p]);
        h4CellsCharge_                [p]->Divide(h4CellsChargeNorm_           [p]);
//Hsin-Wei have added plots for 225um pixels
        h2DCharge225um_               [p]->Divide(h2DChargeNorm225um_          [p]);
        h2DChargeRef225um_            [p]->Divide(h2DChargeRefNorm225um_       [p]);
        h2DClusterSize225um_          [p]->Divide(h2DCellChargeNorm225um_           [p]);


        // ######################
        // # Setting error bars #
        // ######################
        setErrorsBar(p);


        STDLINE("Setting styles...",ACWhite);

        float xPitch = atof(((theXmlParser_->getPlanes())[planeName]->getCellPitches().first).c_str());
        float yPitch = atof(((theXmlParser_->getPlanes())[planeName]->getCellPitches().second).c_str());

        h1DXcellChargeAsymmetry_   [p]->SetMinimum(-1);
        h1DXcellChargeAsymmetry_   [p]->SetMaximum( 1);
        h1DXcellChargeAsymmetry_   [p]->SetMarkerStyle(20);
        h1DXcellChargeAsymmetry_   [p]->SetMarkerSize(0.6);

        h1DXcellChargeAsymmetryInv_[p]->SetMinimum(-xPitch/2);
        h1DXcellChargeAsymmetryInv_[p]->SetMaximum(xPitch/2);
        h1DXcellChargeAsymmetryInv_[p]->SetMarkerStyle(20);
        h1DXcellChargeAsymmetryInv_[p]->SetMarkerSize(0.6);

        h1DYcellChargeAsymmetry_   [p]->SetMinimum(-1);
        h1DYcellChargeAsymmetry_   [p]->SetMaximum( 1);
        h1DYcellChargeAsymmetry_   [p]->SetMarkerStyle(20);
        h1DYcellChargeAsymmetry_   [p]->SetMarkerSize(0.6);

        h1DYcellChargeAsymmetryInv_[p]->SetMinimum(-yPitch/2);
        h1DYcellChargeAsymmetryInv_[p]->SetMaximum(yPitch/2);
        h1DYcellChargeAsymmetryInv_[p]->SetMarkerStyle(20);
        h1DYcellChargeAsymmetryInv_[p]->SetMarkerSize(0.6);

        h1DXcellCharge_            [p]->SetMarkerStyle(20);
        h1DXcellCharge_            [p]->SetMarkerSize(0.6);

        h1DYcellCharge_            [p]->SetMarkerStyle(20);
        h1DYcellCharge_            [p]->SetMarkerSize(0.6);

        h1DXcellChargeSecondHit_   [p]->SetMarkerStyle(20);
        h1DXcellChargeSecondHit_   [p]->SetMarkerSize(0.6);

        h1DYcellChargeSecondHit_   [p]->SetMarkerStyle(20);
        h1DYcellChargeSecondHit_   [p]->SetMarkerSize(0.6);


        hClusterSize_              [p]->GetXaxis()->SetTitle("cluster size"      );
        hClusterFirstRow_          [p]->GetXaxis()->SetTitle("cluster first row" );
        hClusterFirstCol_          [p]->GetXaxis()->SetTitle("cluster first col" );
        hClusterMinHitChargeRatio_ [p]->GetXaxis()->SetTitle("min pixel hit charge ratio w.r.t. cluster (size 2 only)");
        hCellLandau_               [p]->GetXaxis()->SetTitle("charge (electrons)");
        hCellLandauOdd_            [p]->GetXaxis()->SetTitle("charge (electrons)");
        hCellLandauOddLeft_        [p]->GetXaxis()->SetTitle("charge (electrons)");
        hCellLandauOddRight_       [p]->GetXaxis()->SetTitle("charge (electrons)");
        hCellLandauEven_           [p]->GetXaxis()->SetTitle("charge (electrons)");
        hCellLandauEvenLeft_       [p]->GetXaxis()->SetTitle("charge (electrons)");
        hCellLandauEvenRight_      [p]->GetXaxis()->SetTitle("charge (electrons)");

        hLandauClusterSizeUpToMax_   [p]->GetXaxis()->SetTitle("charge (electrons)");

        for(int i = 0; i < maxLandauClusterSize_; ++i)
        {
          hLandauClusterSizeVector_[i][p]->GetXaxis()->SetTitle("charge (electrons)");
        }

        hLandauClusterSize2sameRow_[p]->GetXaxis()->SetTitle("charge (electrons)");
        hLandauClusterSize2sameCol_[p]->GetXaxis()->SetTitle("charge (electrons)");
        hCellLandauSinglePixel_    [p]->GetXaxis()->SetTitle("charge (electrons)");


        h2DClusterSize_            [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DClusterSize_            [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DLargeClusterSize_       [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DLargeClusterSize_       [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DLargeClusterSizeNorm_   [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DLargeClusterSizeNorm_   [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DCharge_                 [p]->GetXaxis()->SetTitle("column");
        h2DCharge_                 [p]->GetYaxis()->SetTitle("row"   );

        h2DChargeNorm_             [p]->GetXaxis()->SetTitle("column");
        h2DChargeNorm_             [p]->GetYaxis()->SetTitle("row"   );

        h2DChargeRef_              [p]->GetXaxis()->SetTitle("column");
        h2DChargeRef_              [p]->GetYaxis()->SetTitle("row"   );

        h2DChargeRefNorm_          [p]->GetXaxis()->SetTitle("column");
        h2DChargeRefNorm_          [p]->GetYaxis()->SetTitle("row"   );


        h2DCellCharge_             [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DCellCharge_             [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DCellChargeNorm_         [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DCellChargeNorm_         [p]->GetYaxis()->SetTitle("short pitch (um)"  );


        h2DCellChargeSize1_        [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DCellChargeSize1_        [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DCellChargeSize1Norm_    [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DCellChargeSize1Norm_    [p]->GetYaxis()->SetTitle("short pitch (um)"  );


        h2DCellChargeSize2_        [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DCellChargeSize2_        [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DCellChargeSize2Norm_    [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DCellChargeSize2Norm_    [p]->GetYaxis()->SetTitle("short pitch (um)"  );


        h2DCellChargeOdd_          [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DCellChargeOdd_          [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DCellChargeOddNorm_      [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DCellChargeOddNorm_      [p]->GetYaxis()->SetTitle("short pitch (um)"  );


        h2DCellChargeEven_         [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DCellChargeEven_         [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DCellChargeEvenNorm_     [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DCellChargeEvenNorm_     [p]->GetYaxis()->SetTitle("short pitch (um)"  );


        h4CellsCharge_             [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h4CellsCharge_             [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h4CellsChargeNorm_         [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h4CellsChargeNorm_         [p]->GetYaxis()->SetTitle("short pitch (um)"  );


        h1DXcellCharge_            [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h1DXcellCharge_            [p]->GetYaxis()->SetTitle("charge (electrons)");
        h1DXcellChargeNorm_        [p]->GetXaxis()->SetTitle("long pitch (um)"   );

        h1DXcellChargeSecondHit_   [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h1DXcellChargeSecondHit_   [p]->GetYaxis()->SetTitle("charge (electrons)");
        h1DXcellChargeSecondHitNorm_[p]->GetYaxis()->SetTitle("charge (electrons)");

        h2DXcellCharge_            [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DXcellCharge_            [p]->GetYaxis()->SetTitle("charge (electrons)");

        h2DXcellChargeSecondHit_   [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DXcellChargeSecondHit_   [p]->GetYaxis()->SetTitle("charge (electrons)");


        h1DYcellCharge_            [p]->GetXaxis()->SetTitle("short pitch (um)"  );
        h1DYcellCharge_            [p]->GetYaxis()->SetTitle("charge (electrons)");
        h1DYcellChargeNorm_        [p]->GetXaxis()->SetTitle("short pitch (um)"  );

        h1DYcellChargeSecondHit_   [p]->GetXaxis()->SetTitle("short pitch (um)"   );
        h1DYcellChargeSecondHit_   [p]->GetYaxis()->SetTitle("charge (electrons)");
        h1DYcellChargeSecondHitNorm_[p]->GetYaxis()->SetTitle("charge (electrons)");

        h2DYcellCharge_            [p]->GetXaxis()->SetTitle("short pitch (um)"  );
        h2DYcellCharge_            [p]->GetYaxis()->SetTitle("charge (electrons)");

        h2DYcellChargeSecondHit_   [p]->GetXaxis()->SetTitle("short pitch (um)"  );
        h2DYcellChargeSecondHit_   [p]->GetYaxis()->SetTitle("charge (electrons)");


        h2DXcellChargeAsymmetry_   [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DXcellChargeAsymmetry_   [p]->GetYaxis()->SetTitle("Asymmetry"         );

        h1DXcellChargeAsymmetry_   [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h1DXcellChargeAsymmetry_   [p]->GetYaxis()->SetTitle("Asymmetry"         );

        h2DXcellChargeAsymmetryInv_[p]->GetXaxis()->SetTitle("Asymmetry"         );
        h2DXcellChargeAsymmetryInv_[p]->GetYaxis()->SetTitle("long pitch (um)"   );

        h1DXcellChargeAsymmetryInv_[p]->GetXaxis()->SetTitle("Asymmetry"         );
        h1DXcellChargeAsymmetryInv_[p]->GetYaxis()->SetTitle("long pitch (um)"   );


        h2DYcellChargeAsymmetry_   [p]->GetXaxis()->SetTitle("shot pitch (um)"   );
        h2DYcellChargeAsymmetry_   [p]->GetYaxis()->SetTitle("Asymmetry"         );

        h1DYcellChargeAsymmetry_   [p]->GetXaxis()->SetTitle("short pitch (um)"  );
        h1DYcellChargeAsymmetry_   [p]->GetYaxis()->SetTitle("Asymmetry"         );

        h2DYcellChargeAsymmetryInv_[p]->GetXaxis()->SetTitle("Asymmetry"         );
        h2DYcellChargeAsymmetryInv_[p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h1DYcellChargeAsymmetryInv_[p]->GetXaxis()->SetTitle("Asymmetry"         );
        h1DYcellChargeAsymmetryInv_[p]->GetYaxis()->SetTitle("short pitch (um)"  );
//Hsin-Wei have added plots for 225um pixels
        hClusterSize225um_              [p]->GetXaxis()->SetTitle("cluster size"      );
        h2DClusterSize225um_            [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DClusterSize225um_            [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DCharge225um_                 [p]->GetXaxis()->SetTitle("column");
        h2DCharge225um_                 [p]->GetYaxis()->SetTitle("row"   );

        h2DCellChargeNorm225um_         [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DCellChargeNorm225um_         [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DChargeNorm225um_             [p]->GetXaxis()->SetTitle("column");
        h2DChargeNorm225um_             [p]->GetYaxis()->SetTitle("row"   );

        h2DChargeRef225um_              [p]->GetXaxis()->SetTitle("column");
        h2DChargeRef225um_              [p]->GetYaxis()->SetTitle("row"   );

        h2DChargeRefNorm225um_          [p]->GetXaxis()->SetTitle("column");
        h2DChargeRefNorm225um_          [p]->GetYaxis()->SetTitle("row"   );


        hLandauClusterSizeUpToMax225um_   [p]->GetXaxis()->SetTitle("charge (electrons)");
        for(int i = 0; i < maxLandauClusterSize_; ++i)
        {
          h225umLandauClusterSizeVector_[i][p]->GetXaxis()->SetTitle("charge (electrons)");
        }


        STDLINE("Fitting phase",ACWhite);
        gStyle->SetOptFit(1111);

        STDLINE("fXAsymmetryFit",ACWhite);
        TF1* fXAsymmetryFit = new TF1("fXAsymmetryFit","pol1",-ETAhalfRANGE,ETAhalfRANGE);
        if (h1DXcellChargeAsymmetryInv_[p]->GetEntries() != 0) h1DXcellChargeAsymmetryInv_[p]->Fit(fXAsymmetryFit,"R0");

        STDLINE("fYAsymmetryFit",ACWhite);
        TF1* fYAsymmetryFit  = new TF1("fYAsymmetryFit","pol1",-ETAhalfRANGE,ETAhalfRANGE);
        if (h1DYcellChargeAsymmetryInv_[p]->GetEntries() != 0) h1DYcellChargeAsymmetryInv_[p]->Fit(fYAsymmetryFit,"R0");

        // Rebin DUT charge plots by a factor of 8
        int rebinFactor = 8;
        if (planeName.find("Dut") != std::string::npos && h2DChargeRef_[p]->GetNbinsX() % rebinFactor == 0 && h2DChargeRef_[p]->GetNbinsY() % rebinFactor == 0)
        {
            // ##########################
            // Rebinned charge plot #
            // ##########################
            TH2F* h2DChargeNumerator = (TH2F*) h2DChargeRef_[p]->Clone();
            h2DChargeNumerator->Reset();
            h2DChargeNumerator->Multiply(h2DChargeRef_[p],h2DChargeRefNorm_[p]);
            h2DChargeNumerator->Rebin2D(rebinFactor,rebinFactor);

            theAnalysisManager_->cd("Charge/"+planeName+"/2DCharge");
            h2DChargeRefNormRebinned_   [p] = (TH2F*) h2DChargeRefNorm_[p]->Clone();
            h2DChargeRefNormRebinned_   [p]->SetNameTitle("h2DChargeRefNormRebinned_"+TString(planeName),"2D charge ref. normalization rebinned ");
            h2DChargeRefNormRebinned_   [p]->Rebin2D(rebinFactor,rebinFactor);

            h2DChargeRefRebinned_       [p] = (TH2F*) h2DChargeRefNormRebinned_[p]->Clone();
            h2DChargeRefRebinned_       [p]->SetNameTitle("h2DChargeRefRebinned_"+TString(planeName),"2D charge ref. distribution rebinned ");
            h2DChargeRefRebinned_       [p]->Divide(h2DChargeNumerator, h2DChargeRefNormRebinned_[p]);
            delete h2DChargeNumerator; // don't need this plot beyond the charge calculation

            // ########################################
            // Zoomed in and rebinned charge plot #
            // ########################################
            h2DChargeRefNormZoomedIn50x50_   [p] = (TH2F*) h2DChargeRefNormRebinned_[p]->Clone();
            h2DChargeRefNormZoomedIn50x50_   [p]->SetNameTitle("h2DChargeRefNormZoomedIn50x50_"+TString(planeName),"2D charge ref. normalization zoomed in ");

            h2DChargeRefZoomedIn50x50_       [p] = (TH2F*) h2DChargeRefRebinned_[p]->Clone();
            h2DChargeRefZoomedIn50x50_       [p]->SetNameTitle("h2DChargeRefZoomedIn50x50_"+TString(planeName),"2D charge ref. distribution zoomed in ");

            h2DChargeRefNormZoomedIn25x100_   [p] = (TH2F*) h2DChargeRefNormRebinned_[p]->Clone();
            h2DChargeRefNormZoomedIn25x100_   [p]->SetNameTitle("h2DChargeRefNormZoomedIn25x100_"+TString(planeName),"2D charge ref. normalization zoomed in ");

            h2DChargeRefZoomedIn25x100_       [p] = (TH2F*) h2DChargeRefRebinned_[p]->Clone();
            h2DChargeRefZoomedIn25x100_       [p]->SetNameTitle("h2DChargeRefZoomedIn25x100_"+TString(planeName),"2D charge ref. distribution zoomed in ");

            // these are hardcoded row/column numbers for the 50x50 zoomed in plot
            int x_start = 128;
            int x_end = 263;
            int y_start = 0;
            int y_end = 191;

            zoomIn(h2DChargeRefZoomedIn50x50_[p], h2DChargeRefNormZoomedIn50x50_[p], x_start, x_end, y_start, y_end, rebinFactor);

            // for 25x100 sensors
            x_start = 64;
            x_end = 131; // linear FE ends here, but our rebinning by a factor of 8 means the TH2 bin ends at 135--be careful!
            y_start = 0;
            y_end = 383;

            zoomIn(h2DChargeRefZoomedIn25x100_[p], h2DChargeRefNormZoomedIn25x100_[p], x_start, x_end, y_start, y_end, rebinFactor);
        }


        // ###############################
        // # Print mean charge on screen #
        // ###############################
        charge = hCellLandau_[p]->GetMean();

        ss.str("");
        ss << "Detector: " << std::setw(27) << thePlaneMapping_->getPlaneName(p) << " Mean charge: " << std::setw(4) << charge;

        STDLINE(ss.str(),ACLightPurple);
    }
}

//=======================================================================
void ChargeRD53A::book(void)
{
    destroy();

    std::string hName;
    std::string hTitle;
    std::string planeName;
    std::stringstream ss;

    int nBinsX;
    int nBinsY;

    float xPitch;
    float yPitch;
    float binSize = 1.25; // [um]

    int lowerCol;
    int higherCol;
    int lowerRow;
    int higherRow;

    int nBinsCharge = 500;
    int nBinsCell   = 100;


    theAnalysisManager_->cd("/");
    theAnalysisManager_->mkdir("Charge");


    for (unsigned int p = 0; p < thePlaneMapping_->getNumberOfPlanes(); p++)
    {
        planeName = thePlaneMapping_->getPlaneName(p);
        theAnalysisManager_->cd("Charge");
        theAnalysisManager_->mkdir(planeName);


        xPitch    = atof(((theXmlParser_->getPlanes())[planeName]->getCellPitches().first).c_str());
        yPitch    = atof(((theXmlParser_->getPlanes())[planeName]->getCellPitches().second).c_str());

        lowerCol  = atoi(((theXmlParser_->getPlanes())[planeName]->getWindow()->getLowerCol ()).c_str());
        higherCol = atoi(((theXmlParser_->getPlanes())[planeName]->getWindow()->getHigherCol()).c_str());
        lowerRow  = atoi(((theXmlParser_->getPlanes())[planeName]->getWindow()->getLowerRow ()).c_str());
        higherRow = atoi(((theXmlParser_->getPlanes())[planeName]->getWindow()->getHigherRow()).c_str());

        nBinsX    = abs(lowerCol - higherCol) + 1;
        nBinsY    = abs(lowerRow - higherRow) + 1;

        if (nBinsY <= 0) nBinsY = 1; // Planes which are not in the geometry file have lowerRow = higherRow = 0,
        // this produces an unexpected warning


        theAnalysisManager_->mkdir("ClusterSize");


        // #################
        // # 1D histograms #
        // #################
        hName  = "hClusterSize_"              + planeName;
        hTitle = "Cluster size distribution " + planeName;
        hClusterSize_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 10, 0, 10)));

        hName  = "hClusterFirstRow_"               + planeName;
        hTitle = "Cluster first row distribution " + planeName;
        hClusterFirstRow_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 1500, 0, 1500)));

        hName  = "hClusterFirstCol_"               + planeName;
        hTitle = "Cluster first col distribution " + planeName;
        hClusterFirstCol_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 500, 0, 500)));
//Hsin-Wei adeed plots for 225um pixels
        hName  = "hClusterSize225um_"              + planeName;
        hTitle = "Cluster size distribution 225um " + planeName;
        hClusterSize225um_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 10, 0, 10)));



        theAnalysisManager_->cd("Charge/" + planeName);
        theAnalysisManager_->mkdir("Landau");


        // #################
        // # 1D histograms #
        // #################
        hName  = "hCellLandau_"                                              + planeName;
        hTitle = "Charge distribution for single hits in a fiducial window " + planeName;
        hCellLandau_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hCellLandauOdd_"                                                         + planeName;
        hTitle = "Charge distribution for single hits in a fiducial window - odd columns " + planeName;
        hCellLandauOdd_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hCellLandauOddLeft_"                                                          + planeName;
        hTitle = "Charge distribution for single hits in a fiducial window - odd-left columns " + planeName;
        hCellLandauOddLeft_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hCellLandauOddRight_"                                                          + planeName;
        hTitle = "Charge distribution for single hits in a fiducial window - odd-right columns " + planeName;
        hCellLandauOddRight_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hCellLandauEven_"                                                         + planeName;
        hTitle = "Charge distribution for single hits in a fiducial window - even columns " + planeName;
        hCellLandauEven_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hCellLandauEvenLeft_"                                                          + planeName;
        hTitle = "Charge distribution for single hits in a fiducial window - even-left columns " + planeName;
        hCellLandauEvenLeft_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hCellLandauEvenRight_"                                                          + planeName;
        hTitle = "Charge distribution for single hits in a fiducial window - even-right columns " + planeName;
        hCellLandauEvenRight_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hLandauClusterSizeUpToMax_"                                  + planeName;
        hTitle = "Charge distribution for clusters of size up to " + std::to_string(maxLandauClusterSize_) + planeName;
        hLandauClusterSizeUpToMax_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        for(int i = 0; i < maxLandauClusterSize_; ++i)
        {
          hName  = "hLandauClusterSize" + std::to_string(i+1) + "_" + planeName;
          hTitle = "Charge distribution for clusters of size " + std::to_string(i+1) + " " + planeName;
          hLandauClusterSizeVector_[i].push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));
        }

        hName  = "hLandauClusterSize2sameRow_"                               + planeName;
        hTitle = "Charge distribution for clusters of size 2 on same row "   + planeName;
        hLandauClusterSize2sameRow_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hLandauClusterSize2sameCol_"                               + planeName;
        hTitle = "Charge distribution for clusters of size 2 on same col "   + planeName;
        hLandauClusterSize2sameCol_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hCellLandauSinglePixel_"                                   + planeName;
        hTitle = "Charge distribution of the pointed pixel in the cluster "  + planeName;
        hCellLandauSinglePixel_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hClusterMinHitChargeRatio_" + planeName;
        hTitle = "min hit charge ratio distribution for size 2 clusters " + planeName;
        hClusterMinHitChargeRatio_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 50, 0, 0.5)));
//Hsin-Wei have added 225um pixels

        hName  = "hLandauClusterSizeUpToMax225um_"                                  + planeName;
        hTitle = "Charge distribution 225um for clusters of size up to " + std::to_string(maxLandauClusterSize_) + planeName;
        hLandauClusterSizeUpToMax225um_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        for(int i = 0; i < maxLandauClusterSize_; ++i)
        {
          hName  = "h225umLandauClusterSize" + std::to_string(i+1) + "_" + planeName;
          hTitle = "Charge distribution 225um for clusters of size " + std::to_string(i+1) + " " + planeName;
          h225umLandauClusterSizeVector_[i].push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));
        }



        theAnalysisManager_->cd("Charge/" + planeName);
        theAnalysisManager_->mkdir("2DCharge");


        // #################
        // # 2D histograms #
        // #################
        hName  = "h2DCharge_"                                         + planeName;
        hTitle = "2D charge distribution "                            + planeName;
        h2DCharge_.push_back(NEW_THREADED(TH2F(hName.c_str(),hTitle.c_str(),nBinsX,lowerCol,higherCol + 1,nBinsY,lowerRow,higherRow + 1)));

        hName  = "h2DChargeNorm_"                                     + planeName;
        hTitle = "2D charge normalization "                           + planeName;
        h2DChargeNorm_.push_back(NEW_THREADED(TH2F(hName.c_str(),hTitle.c_str(),nBinsX,lowerCol,higherCol + 1,nBinsY,lowerRow,higherRow + 1)));

        hName  = "h2DChargeRef_"                                       + planeName;
        hTitle = "2D charge ref. distribution "                        + planeName;
        h2DChargeRef_.push_back(NEW_THREADED(TH2F(hName.c_str(),hTitle.c_str(),nBinsX,lowerCol,higherCol + 1,nBinsY,lowerRow,higherRow + 1)));

        hName  = "h2DChargeRefNorm_"                                   + planeName;
        hTitle = "2D charge ref. normalization "                       + planeName;
        h2DChargeRefNorm_.push_back(NEW_THREADED(TH2F(hName.c_str(),hTitle.c_str(),nBinsX,lowerCol,higherCol + 1,nBinsY,lowerRow,higherRow + 1)));

        hName  = "h2DChargeRefRebinned_"                               + planeName;
        hTitle = "2D charge ref. distribution rebinned "               + planeName;
        h2DChargeRefRebinned_.push_back(NEW_THREADED(TH2F(hName.c_str(),hTitle.c_str(),nBinsX,lowerCol,higherCol + 1,nBinsY,lowerRow,higherRow + 1)));

        hName  = "h2DChargeRefNormRebinned_"                           + planeName;
        hTitle = "2D charge ref. normalization rebinned "              + planeName;
        h2DChargeRefNormRebinned_.push_back(NEW_THREADED(TH2F(hName.c_str(),hTitle.c_str(),nBinsX,lowerCol,higherCol + 1,nBinsY,lowerRow,higherRow + 1)));

        hName  = "h2DChargeRefZoomedIn50x50_"                               + planeName;
        hTitle = "2D charge ref. distribution zoomed in "              + planeName;
        h2DChargeRefZoomedIn50x50_.push_back(NEW_THREADED(TH2F(hName.c_str(),hTitle.c_str(),nBinsX,lowerCol,higherCol + 1,nBinsY,lowerRow,higherRow + 1)));

        hName  = "h2DChargeRefNormZoomedIn50x50_"                           + planeName;
        hTitle = "2D charge ref. normalization zoomed in "             + planeName;
        h2DChargeRefNormZoomedIn50x50_.push_back(NEW_THREADED(TH2F(hName.c_str(),hTitle.c_str(),nBinsX,lowerCol,higherCol + 1,nBinsY,lowerRow,higherRow + 1)));

        hName  = "h2DChargeRefZoomedIn25x100_"                               + planeName;
        hTitle = "2D charge ref. distribution zoomed in "              + planeName;
        h2DChargeRefZoomedIn25x100_.push_back(NEW_THREADED(TH2F(hName.c_str(),hTitle.c_str(),nBinsX,lowerCol,higherCol + 1,nBinsY,lowerRow,higherRow + 1)));

        hName  = "h2DChargeRefNormZoomedIn25x100_"                           + planeName;
        hTitle = "2D charge ref. normalization zoomed in "             + planeName;
        h2DChargeRefNormZoomedIn25x100_.push_back(NEW_THREADED(TH2F(hName.c_str(),hTitle.c_str(),nBinsX,lowerCol,higherCol + 1,nBinsY,lowerRow,higherRow + 1)));

//Hsin-Wei adeed plots for 225um pixels
        hName  = "h2DCharge225um_"                                         + planeName;
        hTitle = "2D charge distribution 225um "                            + planeName;
        h2DCharge225um_.push_back(NEW_THREADED(TH2F(hName.c_str(),hTitle.c_str(),nBinsX,lowerCol,higherCol + 1,nBinsY,lowerRow,higherRow + 1)));

        hName  = "h2DChargeNorm225um_"                                     + planeName;
        hTitle = "2D charge normalization 225um "                           + planeName;
        h2DChargeNorm225um_.push_back(NEW_THREADED(TH2F(hName.c_str(),hTitle.c_str(),nBinsX,lowerCol,higherCol + 1,nBinsY,lowerRow,higherRow + 1)));

        hName  = "h2DChargeRef225um_"                                       + planeName;
        hTitle = "2D charge ref. distribution 225um "                        + planeName;
        h2DChargeRef225um_.push_back(NEW_THREADED(TH2F(hName.c_str(),hTitle.c_str(),nBinsX,lowerCol,higherCol + 1,nBinsY,lowerRow,higherRow + 1)));

        hName  = "h2DChargeRefNorm225um_"                                   + planeName;
        hTitle = "2D charge ref. normalization 225um "                       + planeName;
        h2DChargeRefNorm225um_.push_back(NEW_THREADED(TH2F(hName.c_str(),hTitle.c_str(),nBinsX,lowerCol,higherCol + 1,nBinsY,lowerRow,higherRow + 1)));

        theAnalysisManager_->cd("Charge/" + planeName);
        theAnalysisManager_->mkdir("2DCellCharge");


        // #################
        // # 2D histograms #
        // #################
        hName  = "h2DCellCharge_"                  + planeName;
        hTitle = "Cell charge 2D distribution "    + planeName;
        h2DCellCharge_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h2DCellChargeNorm_"              + planeName;
        hTitle = "Cell charge normalization "      + planeName;
        h2DCellChargeNorm_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h2DCellChargeSize1_"                           + planeName;
        hTitle = "Cell charge 2D distribution - cluster size 1 " + planeName;
        h2DCellChargeSize1_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h2DCellChargeSize1Norm_"                       + planeName;
        hTitle = "Cell charge normalization - cluster size 1"    + planeName;
        h2DCellChargeSize1Norm_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h2DCellChargeSize2_"                           + planeName;
        hTitle = "Cell charge 2D distribution - cluster size 2 " + planeName;
        h2DCellChargeSize2_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h2DCellChargeSize2Norm_"                       + planeName;
        hTitle = "Cell charge normalization - cluster size 2"    + planeName;
        h2DCellChargeSize2Norm_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h2DCellChargeOdd_"                           + planeName;
        hTitle = "Cell charge 2D distribution - odd columns "  + planeName;
        h2DCellChargeOdd_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h2DCellChargeOddNorm_"                       + planeName;
        hTitle = "Cell charge normalization - odd columns "    + planeName;
        h2DCellChargeOddNorm_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h2DCellChargeEven_"                          + planeName;
        hTitle = "Cell charge 2D distribution - even columns " + planeName;
        h2DCellChargeEven_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h2DCellChargeEvenNorm_"                      + planeName;
        hTitle = "Cell charge normalization - even columns "   + planeName;
        h2DCellChargeEvenNorm_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h4CellsCharge_"                  + planeName;
        hTitle = "4 cells charge 2D distribution " + planeName;
        h4CellsCharge_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h4CellsChargeNorm_"              + planeName;
        hTitle = "4 cells charge normalization "   + planeName;
        h4CellsChargeNorm_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h2DClusterSize_"                 + planeName;
        hTitle = "Cluster size distribution  "     + planeName;
        h2DClusterSize_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h2DLargeClusterSize_"                 + planeName;
        hTitle = "Large Cluster size distribution  "     + planeName;
        h2DLargeClusterSize_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));
        
	hName  = "h2DLargeClusterSizeNorm_"                 + planeName;
        hTitle = "Large Cluster size distribution normalization "     + planeName;
        h2DLargeClusterSizeNorm_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));
//Hsin-Wei have added 225um pixels
        hName  = "h2DCellChargeNorm225um_"              + planeName;
        hTitle = "Cell charge normalization 225um "      + planeName;
        h2DCellChargeNorm225um_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h2DClusterSize225um_"                 + planeName;
        hTitle = "Cluster size distribution 225um  "     + planeName;
        h2DClusterSize225um_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));


        theAnalysisManager_->cd("Charge/" + planeName);
        theAnalysisManager_->mkdir("XcellCharge2D");


        // #################
        // # 2D histograms #
        // #################
        hName  = "h2DXcellCharge_"                                      + planeName;
        hTitle = "Predicted cell charge vs. X coordinate "              + planeName;
        h2DXcellCharge_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/2, -(xPitch/2), xPitch/2, nBinsCharge, 0, 50000)));

        hName  = "h2DXcellChargeSecondHit_"                             + planeName;
        hTitle = "Predicted cell charge vs. X coordinate "              + planeName;
        h2DXcellChargeSecondHit_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/2, -(xPitch/2), xPitch/2, nBinsCharge, 0, 50000)));




        theAnalysisManager_->cd("Charge/" + planeName);
        theAnalysisManager_->mkdir("YcellCharge2D");


        // #################
        // # 2D histograms #
        // #################
        hName  = "h2DYcellCharge_"                                      + planeName;
        hTitle = "Predicted cell charge vs. Y coordinate "              + planeName;
        h2DYcellCharge_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)yPitch/2, -(yPitch/2), yPitch/2, nBinsCharge, 0, 50000)));

        hName  = "h2DYcellChargeSecondHit_"                             + planeName;
        hTitle = "Predicted cell charge vs. Y coordinate "              + planeName;
        h2DYcellChargeSecondHit_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)yPitch/2, -(yPitch/2), yPitch/2, nBinsCharge, 0, 50000)));




        theAnalysisManager_->cd("Charge/" + planeName);
        theAnalysisManager_->mkdir("XcellCharge1D");


        // #################
        // # 1D histograms #
        // #################
        hName  = "h1DXcellCharge_"                                                             + planeName;
        hTitle = "Predicted cell charge - X coordinate (normalized to hits) "                  + planeName;
        h1DXcellCharge_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), (int)xPitch/2,-(xPitch/2),xPitch/2)));

        hName  = "h1DXcellChargeNorm_"                                                         + planeName;
        hTitle = "Predicted cell charge - X coordinate - all hits normalization "              + planeName;
        h1DXcellChargeNorm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), (int)xPitch/2,-(xPitch/2),xPitch/2)));

        hName  = "h1DXcellChargeSecondHit_"                                                              + planeName;
        hTitle = "Up to 2 adjacent hits total charge, second hit - X coordinate (normalized to tracks) " + planeName;
        h1DXcellChargeSecondHit_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), (int)xPitch/2,-(xPitch/2),xPitch/2)));

        hName  = "h1DXcellChargeSecondHitNorm_"                                                + planeName;
        hTitle = "Up to 2 adjacent hits total charge, second hit distribution - X coordinate " + planeName;
        h1DXcellChargeSecondHitNorm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), (int)xPitch/2,-(xPitch/2),xPitch/2)));




        theAnalysisManager_->cd("Charge/" + planeName);
        theAnalysisManager_->mkdir("YcellCharge1D");


        // #################
        // # 1D histograms #
        // #################
        hName  = "h1DYcellCharge_"                                                             + planeName;
        hTitle = "Predicted cell charge - Y coordinate (normalized to hits) "                  + planeName;
        h1DYcellCharge_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), (int)yPitch/2,-(yPitch/2),yPitch/2)));

        hName  = "h1DYcellChargeNorm_"                                                         + planeName;
        hTitle = "Predicted cell charge - Y coordinate - all hits normalization "              + planeName;
        h1DYcellChargeNorm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), (int)yPitch/2,-(yPitch/2),yPitch/2)));

        hName  = "h1DYcellChargeSecondHit_"                                                              + planeName;
        hTitle = "Up to 2 adjacent hits total charge, second hit - Y coordinate (normalized to tracks) " + planeName;
        h1DYcellChargeSecondHit_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), (int)yPitch/2,-(yPitch/2),yPitch/2)));

        hName  = "h1DYcellChargeSecondHitNorm_"                                                + planeName;
        hTitle = "Up to 2 adjacent hits total charge, second hit distribution - X coordinate " + planeName;
        h1DYcellChargeSecondHitNorm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), (int)yPitch/2,-(yPitch/2),yPitch/2)));




        theAnalysisManager_->cd("Charge/" + planeName);
        theAnalysisManager_->mkdir("XAsymmetry");


        // #################
        // # 1D histograms #
        // #################
        hName  = "h2DXcellChargeAsymmetry_"                                         + planeName;
        hTitle = "L/R charge asymmetry - X coordinate "                             + planeName;
        h2DXcellChargeAsymmetry_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch, -(xPitch/2), xPitch/2, nBinsCell, -1.1, 1.1)));

        hName  = "h2DXcellChargeAsymmetryInv_"                                      + planeName;
        hTitle = "L/R charge asymmetry - X coordinate "                             + planeName;
        h2DXcellChargeAsymmetryInv_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), nBinsCell, -1.1, 1.1, (int)xPitch, -(xPitch/2), xPitch/2)));


        theAnalysisManager_->cd("Charge/" + planeName);
        theAnalysisManager_->mkdir("YAsymmetry");

        hName  = "h2DYcellChargeAsymmetry_"                                         + planeName;
        hTitle = "L/R charge asymmetry - Y coordinate "                             + planeName;
        h2DYcellChargeAsymmetry_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)yPitch, -(yPitch/2), yPitch/2 , nBinsCell, -1.1, 1.1)));

        hName  = "h2DYcellChargeAsymmetryInv_"                                      + planeName;
        hTitle = "L/R charge asymmetry - Y coordinate "                             + planeName;
        h2DYcellChargeAsymmetryInv_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), nBinsCell, -1.1, 1.1, (int)yPitch, -(yPitch/2), yPitch/2)));
    }
}

void ChargeRD53A::zoomIn(TH2F* hCharge, TH2F* hChargeNorm, int x_start, int x_end, int y_start, int y_end, int rebinFactor)
{
    hChargeNorm->GetXaxis()->SetRangeUser(x_start,x_end+1);
    hChargeNorm->GetYaxis()->SetRangeUser(y_start,y_end+1);
    hCharge    ->GetXaxis()->SetRangeUser(x_start,x_end+1);
    hCharge    ->GetYaxis()->SetRangeUser(y_start,y_end+1);

    TString label;

    // add our x labels
    for (int xBinLabel = x_start; xBinLabel < x_end; xBinLabel += rebinFactor)
    {
        int ibinx = hChargeNorm->GetXaxis()->FindBin(xBinLabel);
        label.Form("%d-%d", xBinLabel, std::min(xBinLabel+rebinFactor-1, x_end)); // min is needed when the rebinned bin extends past the FE being looked at
        hChargeNorm->GetXaxis()->SetBinLabel(ibinx, label);
        hCharge    ->GetXaxis()->SetBinLabel(ibinx, label);
    }

    // add our y labels
    for (int yBinLabel = y_start; yBinLabel < y_end; yBinLabel += rebinFactor)
    {
        int ibiny = hChargeNorm->GetYaxis()->FindBin(yBinLabel);
        label.Form("%d-%d", yBinLabel, std::min(yBinLabel+rebinFactor-1, y_end));
        hChargeNorm->GetYaxis()->SetBinLabel(ibiny, label);
        hCharge    ->GetYaxis()->SetBinLabel(ibiny, label);
    }

    // remove tickmarks; otherwise we'd need to play around here a bit more to get them aligned right
    hChargeNorm->GetXaxis()->SetTickLength(0);
    hChargeNorm->GetYaxis()->SetTickLength(0);
    hCharge    ->GetXaxis()->SetTickLength(0);
    hCharge    ->GetYaxis()->SetTickLength(0);

    // move the "row" label so it doesn't overlap with the bin labels
    hChargeNorm->GetYaxis()->SetTitleOffset(1.8);
    hCharge    ->GetYaxis()->SetTitleOffset(1.8);
}
