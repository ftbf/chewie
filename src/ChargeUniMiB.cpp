/*===============================================================================
 * Chewie: the FERMILAB MTEST telescope and DUT anaysis tool
 *
 * Copyright (C) 2014
 *
 * Authors:
 *
 * Mauro Dinardo      (Universita' Bicocca)
 * Dario Menasce      (INFN)
 * Jennifer Ngadiuba  (INFN)
 * Lorenzo Uplegger   (FNAL)
 * Luigi Vigani       (INFN)
 *
 * INFN: Piazza della Scienza 3, Edificio U2, Milano, Italy 20126
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ================================================================================*/

#include "ChargeUniMiB.h"

#include "AnalysisManager.h"
#include "WindowsManager.h"
#include "Window.h"
#include "ThreadUtilities.h"
#include "PlanesMapping.h"
#include "MessageTools.h"
#include "XmlParser.h"
#include "XmlPlane.h"
#include "XmlAnalysis.h"
#include "HistogramWindow.h"

#include "Utilities.h"
#include "CalibrationsManager.h"

#include <TH2F.h>
#include <TF1.h>
#include <TStyle.h>

#include <iostream>
#include <iomanip>
#include <cmath>


// @@@ Hard coded parameters @@@
#define maxChargeDeltaRay 12000. // = 7300 (MPV Landau for 100 um bulk thickness) * 1.7
#define ONLYdoubleHITS    false  // Process only clusters of size 2
#define HITALLSTRIPS      true   // Process tracks with associated hits on each strip plane
#define ETAhalfRANGE      0.5    // Eta fit range = [-ETAhalfRANGE, +ETAhalfRANGE]
#define SPECIALPITCH      25.
// ============================


//=======================================================================
ChargeUniMiB::ChargeUniMiB(AnalysisManager* analysisManager, int nOfThreads) :
    Analysis(analysisManager, nOfThreads),
    thePlaneMapping_(0),
    langaus_(0),
    theWindowsManager_(0),
    theXmlParser_(analysisManager->getXmlParser()),
    standardCutsPixelMinimumCharge_(0),
    standardCutsPixelMaximumCharge_(0),
    standardCutsClusterMinimumCharge_(0),
    standardCutsClusterMaximumCharge_(0)
{
    STDLINE("Running ChargeUniMiB analysis",ACCyan);

    thePlaneMapping_ = new PlanesMapping();

    langaus_ = new TF1("langaus",Utilities::langaus,0,60000,4);
    langaus_->SetParNames("Width","MPV","Area","GSigma");
    langaus_->SetLineColor(kBlue);
}

//=======================================================================
ChargeUniMiB::~ChargeUniMiB(void)
{
    if (thePlaneMapping_) delete thePlaneMapping_;

    if(langaus_) delete langaus_;

    destroy();
}

//=======================================================================
void ChargeUniMiB::destroy()
{
    if (Analysis::fDoNotDelete_) return;

    std::vector<TH1F*>::iterator it1;
    std::vector<TH2F*>::iterator it2;

    for(it1=hCellLandau_                    	  .begin(); it1!=hCellLandau_			               .end(); it1++) delete *it1; hCellLandau_ 		   	             .clear();
    for(it1=hCellLandauOddCol_                 	  .begin(); it1!=hCellLandauOddCol_		               .end(); it1++) delete *it1; hCellLandauOddCol_	 	             .clear();
    for(it1=hCellLandauOddColLeft_             	  .begin(); it1!=hCellLandauOddColLeft_		           .end(); it1++) delete *it1; hCellLandauOddColLeft_  	   	         .clear();
    for(it1=hCellLandauOddColRight_            	  .begin(); it1!=hCellLandauOddColRight_		       .end(); it1++) delete *it1; hCellLandauOddColRight_ 	   	         .clear();
    for(it1=hCellLandauEvenCol_                	  .begin(); it1!=hCellLandauEvenCol_		           .end(); it1++) delete *it1; hCellLandauEvenCol_	   	             .clear();
    for(it1=hCellLandauEvenColLeft_            	  .begin(); it1!=hCellLandauEvenColLeft_		       .end(); it1++) delete *it1; hCellLandauEvenColLeft_ 	   	         .clear();
    for(it1=hCellLandauEvenColRight_           	  .begin(); it1!=hCellLandauEvenColRight_  	           .end(); it1++) delete *it1; hCellLandauEvenColRight_	   	         .clear();
    for(it1=hCellLandauOddRow_                 	  .begin(); it1!=hCellLandauOddRow_		               .end(); it1++) delete *it1; hCellLandauOddRow_	 	             .clear();
    for(it1=hCellLandauOddRowLeft_             	  .begin(); it1!=hCellLandauOddRowLeft_		           .end(); it1++) delete *it1; hCellLandauOddRowLeft_  	   	         .clear();
    for(it1=hCellLandauOddRowRight_            	  .begin(); it1!=hCellLandauOddRowRight_		       .end(); it1++) delete *it1; hCellLandauOddRowRight_ 	   	         .clear();
    for(it1=hCellLandauEvenRow_                	  .begin(); it1!=hCellLandauEvenRow_		           .end(); it1++) delete *it1; hCellLandauEvenRow_	   	             .clear();
    for(it1=hCellLandauEvenRowLeft_            	  .begin(); it1!=hCellLandauEvenRowLeft_		       .end(); it1++) delete *it1; hCellLandauEvenRowLeft_ 	   	         .clear();
    for(it1=hCellLandauEvenRowRight_           	  .begin(); it1!=hCellLandauEvenRowRight_  	           .end(); it1++) delete *it1; hCellLandauEvenRowRight_	   	         .clear();


    for(it1=hClusterSize_                   	  .begin(); it1!=hClusterSize_  		               .end(); it1++) delete *it1; hClusterSize_		   	             .clear();

    for(it1=hLandauClusterSize1_            	  .begin(); it1!=hLandauClusterSize1_		           .end(); it1++) delete *it1; hLandauClusterSize1_ 	   	         .clear();
    for(it1=hLandauClusterSize2_            	  .begin(); it1!=hLandauClusterSize2_		           .end(); it1++) delete *it1; hLandauClusterSize2_ 	   	         .clear();
    for(it1=hLandauClusterSize2pointed_     	  .begin(); it1!=hLandauClusterSize2pointed_	       .end(); it1++) delete *it1; hLandauClusterSize2pointed_     	     .clear();
    for(it1=hLandauClusterSize2sameCol_     	  .begin(); it1!=hLandauClusterSize2sameCol_	       .end(); it1++) delete *it1; hLandauClusterSize2sameCol_     	     .clear();
    for(it1=hLandauClusterSize2sameRow_     	  .begin(); it1!=hLandauClusterSize2sameRow_	       .end(); it1++) delete *it1; hLandauClusterSize2sameRow_     	     .clear();
    for(it1=hCellLandauSinglePixel_         	  .begin(); it1!=hCellLandauSinglePixel_	           .end(); it1++) delete *it1; hCellLandauSinglePixel_	   	         .clear();

    for(it1=h1DXcellCharge_                 	  .begin(); it1!=h1DXcellCharge_		               .end(); it1++) delete *it1; h1DXcellCharge_		   	             .clear();
    for(it1=h1DXcellChargeNorm_             	  .begin(); it1!=h1DXcellChargeNorm_		           .end(); it1++) delete *it1; h1DXcellChargeNorm_  	   	         .clear();

    for(it1=h1DXcellChargeSecondHit_        	  .begin(); it1!=h1DXcellChargeSecondHit_	           .end(); it1++) delete *it1; h1DXcellChargeSecondHit_	   	         .clear();
    for(it1=h1DXcellChargeSecondHitNorm_    	  .begin(); it1!=h1DXcellChargeSecondHitNorm_	       .end(); it1++) delete *it1; h1DXcellChargeSecondHitNorm_    	     .clear();

    for(it1=h1DYcellCharge_                 	  .begin(); it1!=h1DYcellCharge_		               .end(); it1++) delete *it1; h1DYcellCharge_		   	             .clear();
    for(it1=h1DYcellChargeNorm_             	  .begin(); it1!=h1DXcellChargeNorm_		           .end(); it1++) delete *it1; h1DXcellChargeNorm_  	   	         .clear();

    for(it1=h1DYcellChargeSecondHit_        	  .begin(); it1!=h1DYcellChargeSecondHit_	           .end(); it1++) delete *it1; h1DYcellChargeSecondHit_	   	         .clear();
    for(it1=h1DYcellChargeSecondHitNorm_    	  .begin(); it1!=h1DYcellChargeSecondHitNorm_	       .end(); it1++) delete *it1; h1DYcellChargeSecondHitNorm_    	     .clear();

    for(it2=h2DClusterSize_                 	  .begin(); it2!=h2DClusterSize_		               .end(); it2++) delete *it2; h2DClusterSize_		   	             .clear();

    for(it2=h2DCharge_                      	  .begin(); it2!=h2DCharge_			                   .end(); it2++) delete *it2; h2DCharge_			   	             .clear();
    for(it2=h2DChargeNorm_                  	  .begin(); it2!=h2DChargeNorm_ 		               .end(); it2++) delete *it2; h2DChargeNorm_		   	             .clear();

    for(it2=h2DCellCharge_                  	  .begin(); it2!=h2DCellCharge_ 		               .end(); it2++) delete *it2; h2DCellCharge_		   	             .clear();
    for(it2=h2DCellChargeNorm_              	  .begin(); it2!=h2DCellChargeNorm_		               .end(); it2++) delete *it2; h2DCellChargeNorm_		   	         .clear();

    for(it2=h2DCellChargeOddCol_               	  .begin(); it2!=h2DCellChargeOddCol_		           .end(); it2++) delete *it2; h2DCellChargeOddCol_		   	         .clear();
    for(it2=h2DCellChargeOddColNorm_           	  .begin(); it2!=h2DCellChargeOddColNorm_  	           .end(); it2++) delete *it2; h2DCellChargeOddColNorm_	   	         .clear();

    for(it2=h2DCellChargeEvenCol_                 .begin(); it2!=h2DCellChargeEvenCol_		           .end(); it2++) delete *it2; h2DCellChargeEvenCol_		      	 .clear();
    for(it2=h2DCellChargeEvenColNorm_             .begin(); it2!=h2DCellChargeEvenColNorm_ 	           .end(); it2++) delete *it2; h2DCellChargeEvenColNorm_	      	 .clear();

    for(it2=h2DCellChargeOddRow_               	  .begin(); it2!=h2DCellChargeOddRow_		           .end(); it2++) delete *it2; h2DCellChargeOddRow_		   	         .clear();
    for(it2=h2DCellChargeOddRowNorm_           	  .begin(); it2!=h2DCellChargeOddRowNorm_  	           .end(); it2++) delete *it2; h2DCellChargeOddRowNorm_	   	         .clear();

    for(it2=h2DCellChargeEvenRow_                 .begin(); it2!=h2DCellChargeEvenRow_		           .end(); it2++) delete *it2; h2DCellChargeEvenRow_		      	 .clear();
    for(it2=h2DCellChargeEvenRowNorm_             .begin(); it2!=h2DCellChargeEvenRowNorm_ 	           .end(); it2++) delete *it2; h2DCellChargeEvenRowNorm_	      	 .clear();

    for(it2=h4CellsCharge_                        .begin(); it2!=h4CellsCharge_ 		               .end(); it2++) delete *it2; h4CellsCharge_		      	         .clear();
    for(it2=h4CellsChargeNorm_                    .begin(); it2!=h4CellsChargeNorm_		               .end(); it2++) delete *it2; h4CellsChargeNorm_		      	     .clear();

    for(it2=h2DXcellCharge_                       .begin(); it2!=h2DXcellCharge_		               .end(); it2++) delete *it2; h2DXcellCharge_		      	         .clear();
    for(it2=h2DXcellChargeSecondHit_              .begin(); it2!=h2DXcellChargeSecondHit_	           .end(); it2++) delete *it2; h2DXcellChargeSecondHit_	      	     .clear();

    for(it2=h2DYcellCharge_                       .begin(); it2!=h2DYcellCharge_		               .end(); it2++) delete *it2; h2DYcellCharge_		      	         .clear();
    for(it2=h2DYcellChargeSecondHit_              .begin(); it2!=h2DYcellChargeSecondHit_	           .end(); it2++) delete *it2; h2DYcellChargeSecondHit_	      	     .clear();

    for(it2=h2DXcellChargeAsymmetry_              .begin(); it2!=h2DXcellChargeAsymmetry_	           .end(); it2++) delete *it2; h2DXcellChargeAsymmetry_	      	     .clear();
    for(it1=h1DXcellChargeAsymmetry_              .begin(); it1!=h1DXcellChargeAsymmetry_	           .end(); it1++) delete *it1; h1DXcellChargeAsymmetry_	      	     .clear();
    for(it2=h2DXcellChargeAsymmetryInv_           .begin(); it2!=h2DXcellChargeAsymmetryInv_	       .end(); it2++) delete *it2; h2DXcellChargeAsymmetryInv_        	 .clear();
    for(it1=h1DXcellChargeAsymmetryInv_           .begin(); it1!=h1DXcellChargeAsymmetryInv_	       .end(); it1++) delete *it1; h1DXcellChargeAsymmetryInv_        	 .clear();

    for(it2=h2DYcellChargeAsymmetry_              .begin(); it2!=h2DYcellChargeAsymmetry_	           .end(); it2++) delete *it2; h2DYcellChargeAsymmetry_	      	     .clear();
    for(it1=h1DYcellChargeAsymmetry_              .begin(); it1!=h1DYcellChargeAsymmetry_	           .end(); it1++) delete *it1; h1DYcellChargeAsymmetry_	      	     .clear();
    for(it2=h2DYcellChargeAsymmetryInv_           .begin(); it2!=h2DYcellChargeAsymmetryInv_	       .end(); it2++) delete *it2; h2DYcellChargeAsymmetryInv_        	 .clear();
    for(it1=h1DYcellChargeAsymmetryInv_           .begin(); it1!=h1DYcellChargeAsymmetryInv_	       .end(); it1++) delete *it1; h1DYcellChargeAsymmetryInv_        	 .clear();

    for(it2=h2DYcellChargeAsymmetryLeftBump_      .begin(); it2!=h2DYcellChargeAsymmetryLeftBump_      .end(); it2++) delete *it2; h2DYcellChargeAsymmetryLeftBump_      .clear();
    for(it1=h1DYcellChargeAsymmetryLeftBump_      .begin(); it1!=h1DYcellChargeAsymmetryLeftBump_      .end(); it1++) delete *it1; h1DYcellChargeAsymmetryLeftBump_      .clear();
    for(it2=h2DYcellChargeAsymmetryInvLeftBump_   .begin(); it2!=h2DYcellChargeAsymmetryInvLeftBump_   .end(); it2++) delete *it2; h2DYcellChargeAsymmetryInvLeftBump_   .clear();
    for(it1=h1DYcellChargeAsymmetryInvLeftBump_   .begin(); it1!=h1DYcellChargeAsymmetryInvLeftBump_   .end(); it1++) delete *it1; h1DYcellChargeAsymmetryInvLeftBump_   .clear();

    for(it2=h2DYcellChargeAsymmetryRightBump_     .begin(); it2!=h2DYcellChargeAsymmetryRightBump_     .end(); it2++) delete *it2; h2DYcellChargeAsymmetryRightBump_     .clear();
    for(it1=h1DYcellChargeAsymmetryRightBump_     .begin(); it1!=h1DYcellChargeAsymmetryRightBump_     .end(); it1++) delete *it1; h1DYcellChargeAsymmetryRightBump_     .clear();
    for(it2=h2DYcellChargeAsymmetryInvRightBump_  .begin(); it2!=h2DYcellChargeAsymmetryInvRightBump_  .end(); it2++) delete *it2; h2DYcellChargeAsymmetryInvRightBump_  .clear();
    for(it1=h1DYcellChargeAsymmetryInvRightBump_  .begin(); it1!=h1DYcellChargeAsymmetryInvRightBump_  .end(); it1++) delete *it1; h1DYcellChargeAsymmetryInvRightBump_  .clear();

    for(it2=h2DYcellChargeAsymmetryLeftNoBump_    .begin(); it2!=h2DYcellChargeAsymmetryLeftNoBump_    .end(); it2++) delete *it2; h2DYcellChargeAsymmetryLeftNoBump_    .clear();
    for(it1=h1DYcellChargeAsymmetryLeftNoBump_    .begin(); it1!=h1DYcellChargeAsymmetryLeftNoBump_    .end(); it1++) delete *it1; h1DYcellChargeAsymmetryLeftNoBump_    .clear();
    for(it2=h2DYcellChargeAsymmetryInvLeftNoBump_ .begin(); it2!=h2DYcellChargeAsymmetryInvLeftNoBump_ .end(); it2++) delete *it2; h2DYcellChargeAsymmetryInvLeftNoBump_ .clear();
    for(it1=h1DYcellChargeAsymmetryInvLeftNoBump_ .begin(); it1!=h1DYcellChargeAsymmetryInvLeftNoBump_ .end(); it1++) delete *it1; h1DYcellChargeAsymmetryInvLeftNoBump_ .clear();

    for(it2=h2DYcellChargeAsymmetryRightNoBump_   .begin(); it2!=h2DYcellChargeAsymmetryRightNoBump_   .end(); it2++) delete *it2; h2DYcellChargeAsymmetryRightNoBump_   .clear();
    for(it1=h1DYcellChargeAsymmetryRightNoBump_   .begin(); it1!=h1DYcellChargeAsymmetryRightNoBump_   .end(); it1++) delete *it1; h1DYcellChargeAsymmetryRightNoBump_   .clear();
    for(it2=h2DYcellChargeAsymmetryInvRightNoBump_.begin(); it2!=h2DYcellChargeAsymmetryInvRightNoBump_.end(); it2++) delete *it2; h2DYcellChargeAsymmetryInvRightNoBump_.clear();
    for(it1=h1DYcellChargeAsymmetryInvRightNoBump_.begin(); it1!=h1DYcellChargeAsymmetryInvRightNoBump_.end(); it1++) delete *it1; h1DYcellChargeAsymmetryInvRightNoBump_.clear();
}

//=======================================================================
void ChargeUniMiB::setErrorsBar(int planeID)
{
    std::string       planeName = thePlaneMapping_->getPlaneName(planeID);
    std::stringstream hName;
    double            binError;
    int               nBins;

    theAnalysisManager_->cd("/Charge/" + planeName + "/XAsymmetry");

    hName.str(""); hName << "h1DXcellChargeAsymmetry_" << planeName;
    h1DXcellChargeAsymmetry_.push_back((TH1F*)h2DXcellChargeAsymmetry_[planeID]->ProfileX(hName.str().c_str(),1,-1));

    hName.str(""); hName << "h1DXcellChargeAsymmetryInv_" << planeName;
    h1DXcellChargeAsymmetryInv_.push_back((TH1F*)h2DXcellChargeAsymmetryInv_[planeID]->ProfileX(hName.str().c_str(),1,-1));

    theAnalysisManager_->cd("/Charge/" + planeName + "/YAsymmetry");

    hName.str(""); hName << "h1DYcellChargeAsymmetry_" << planeName;
    h1DYcellChargeAsymmetry_.push_back((TH1F*)h2DYcellChargeAsymmetry_[planeID]->ProfileX(hName.str().c_str(),1,-1));

    hName.str(""); hName << "h1DYcellChargeAsymmetryInv_" << planeName;
    h1DYcellChargeAsymmetryInv_.push_back((TH1F*)h2DYcellChargeAsymmetryInv_[planeID]->ProfileX(hName.str().c_str(),1,-1));

    hName.str(""); hName << "h1DYcellChargeAsymmetryLeftBump_" << planeName;
    h1DYcellChargeAsymmetryLeftBump_.push_back((TH1F*)h2DYcellChargeAsymmetryLeftBump_[planeID]->ProfileX(hName.str().c_str(),1,-1));

    hName.str(""); hName << "h1DYcellChargeAsymmetryInvLeftBump_" << planeName;
    h1DYcellChargeAsymmetryInvLeftBump_.push_back((TH1F*)h2DYcellChargeAsymmetryInvLeftBump_[planeID]->ProfileX(hName.str().c_str(),1,-1));

    hName.str(""); hName << "h1DYcellChargeAsymmetryRightBump_" << planeName;
    h1DYcellChargeAsymmetryRightBump_.push_back((TH1F*)h2DYcellChargeAsymmetryRightBump_[planeID]->ProfileX(hName.str().c_str(),1,-1));

    hName.str(""); hName << "h1DYcellChargeAsymmetryInvRightBump_" << planeName;
    h1DYcellChargeAsymmetryInvRightBump_.push_back((TH1F*)h2DYcellChargeAsymmetryInvRightBump_[planeID]->ProfileX(hName.str().c_str(),1,-1));

    hName.str(""); hName << "h1DYcellChargeAsymmetryLeftNoBump_" << planeName;
    h1DYcellChargeAsymmetryLeftNoBump_.push_back((TH1F*)h2DYcellChargeAsymmetryLeftNoBump_[planeID]->ProfileX(hName.str().c_str(),1,-1));

    hName.str(""); hName << "h1DYcellChargeAsymmetryInvLeftNoBump_" << planeName;
    h1DYcellChargeAsymmetryInvLeftNoBump_.push_back((TH1F*)h2DYcellChargeAsymmetryInvLeftNoBump_[planeID]->ProfileX(hName.str().c_str(),1,-1));

    hName.str(""); hName << "h1DYcellChargeAsymmetryRightNoBump_" << planeName;
    h1DYcellChargeAsymmetryRightNoBump_.push_back((TH1F*)h2DYcellChargeAsymmetryRightNoBump_[planeID]->ProfileX(hName.str().c_str(),1,-1));

    hName.str(""); hName << "h1DYcellChargeAsymmetryInvRightNoBump_" << planeName;
    h1DYcellChargeAsymmetryInvRightNoBump_.push_back((TH1F*)h2DYcellChargeAsymmetryInvRightNoBump_[planeID]->ProfileX(hName.str().c_str(),1,-1));

    theAnalysisManager_->cd("/Charge/" + planeName + "/XcellCharge1D");

    nBins = h2DXcellCharge_[planeID]->GetNbinsX();
    TH1D* hXchargeTmp[nBins];

    for (int bX = 1; bX <= nBins; bX++)
    {
        hName.str(""); hName << "Xcharge_Proj_bin_" << bX;
        hXchargeTmp[bX-1] = h2DXcellCharge_[planeID]->ProjectionY(hName.str().c_str(),bX,bX);

        if (hXchargeTmp[bX-1]->GetEntries() != 0)
        {
            binError = hXchargeTmp[bX-1]->GetRMS() / sqrt(hXchargeTmp[bX-1]->GetEntries());
            h1DXcellCharge_[planeID]->SetBinError(bX,binError);
        }

        else continue;
    }

    for (int p = 0; p < nBins; p++) hXchargeTmp[p]->Delete("0");

    nBins = h2DXcellChargeSecondHit_[planeID]->GetNbinsX();
    TH1D* hXchargeSecondHitTmp[nBins];

    for (int bX = 1; bX <= nBins; bX++)
    {
        hName.str(""); hName << "XchargeSecondHit_Proj_bin_" << bX;
        hXchargeSecondHitTmp[bX-1] = h2DXcellChargeSecondHit_[planeID]->ProjectionY(hName.str().c_str(),bX,bX);

        if (hXchargeSecondHitTmp[bX-1]->GetEntries() != 0)
        {
            binError = hXchargeSecondHitTmp[bX-1]->GetRMS() / sqrt(hXchargeSecondHitTmp[bX-1]->GetEntries());
            h1DXcellChargeSecondHit_[planeID]->SetBinError(bX,binError);
        }

        else continue;
    }

    for (int p = 0; p < nBins; p++) hXchargeSecondHitTmp[p]->Delete("0");

    theAnalysisManager_->cd("/Charge/" + planeName + "/YcellCharge1D");

    nBins = h2DYcellCharge_[planeID]->GetNbinsX();
    TH1D* hYchargeTmp[nBins];

    for (int bY = 1; bY <= nBins; bY++)
    {
        hName.str(""); hName << "Ycharge_Proj_bin_" << bY;
        hYchargeTmp[bY-1] = h2DYcellCharge_[planeID]->ProjectionY(hName.str().c_str(),bY,bY);

        if (hYchargeTmp[bY-1]->GetEntries() != 0)
        {
            binError = hYchargeTmp[bY-1]->GetRMS() / sqrt(hYchargeTmp[bY-1]->GetEntries());
            h1DYcellCharge_[planeID]->SetBinError(bY,binError);
        }

        else continue;
    }

    for (int p = 0; p < nBins; p++) hYchargeTmp[p]->Delete("0");

    nBins = h2DYcellChargeSecondHit_[planeID]->GetNbinsX();
    TH1D* hYchargeSecondHitTmp[nBins];

    for(int bY = 1; bY <= nBins; bY++)
    {
        hName.str(""); hName << "YchargeSecondHit_Proj_bin_" << bY;
        hYchargeSecondHitTmp[bY-1] = h2DYcellChargeSecondHit_[planeID]->ProjectionY(hName.str().c_str(),bY,bY);

        if (hYchargeSecondHitTmp[bY-1]->GetEntries() != 0)
        {
            binError = hYchargeSecondHitTmp[bY-1]->GetRMS() / sqrt(hYchargeSecondHitTmp[bY-1]->GetEntries());
            h1DYcellChargeSecondHit_[planeID]->SetBinError(bY,binError);
        }

        else continue;
    }

    for (int p = 0; p < nBins; p++) hYchargeSecondHitTmp[p]->Delete("0");
}

//=======================================================================
void ChargeUniMiB::clusterSize(int planeID, const Data& data, int threadNumber)
{
    if (!data.getIsInDetector(planeID) || !data.getHasHit(planeID)) return;

    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    const Window* theWindow    = theWindowsManager_->getWindow(planeID);
    int           rowPredicted = data.getRowPredicted(planeID);
    int           colPredicted = data.getColPredicted(planeID);
    int           run          = data.getRunNumber();

    if (!theWindow->checkWindow(colPredicted,rowPredicted,run)) return;

    THREADED(hClusterSize_[planeID])->Fill(data.getClusterSize(planeID));
}

//=======================================================================
void ChargeUniMiB::cellLandau(bool pass, int planeID, const Data& data, int threadNumber)
{
    // #####################
    // # Internal constant #
    // #####################
    int clusterSize = 1;

    if (!pass || !data.getIsInDetector(planeID) || !data.getHasHit(planeID) || data.getClusterSize(planeID) != clusterSize) return;

    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    const Window* theWindow    = theWindowsManager_->getWindow(planeID);
    int           rowPredicted = data.getRowPredicted(planeID);
    int           colPredicted = data.getColPredicted(planeID);
    int           run          = data.getRunNumber();

    if (!theWindow->checkWindow(colPredicted,rowPredicted,run)) return;

    if (!theWindow->checkWindow(data.getClusterPixelCol(0,planeID),data.getClusterPixelRow(0,planeID),data.getRunNumber()) // Hits are in the window
            ||  data.getClusterPixelRow    (0,planeID) != rowPredicted                                                         // Track is on cluster
            ||  data.getClusterPixelCol    (0,planeID) != colPredicted                                                         // Track is on cluster
            || !data.getIsPixelCalibrated  (0,planeID)                                                                         // Pixels are calibrated
            ||  data.getClusterPixelCharge (0,planeID) < standardCutsPixelMinimumCharge_                                       // Charge is over threshold
            ||  data.getClusterPixelCharge (0,planeID) > standardCutsPixelMaximumCharge_)                                      // Maximum allowed charge for this physics
        return;

    THREADED(hCellLandau_[planeID])->Fill(data.getClusterCharge(planeID));

    if (((int)colPredicted)%2 == 0)
    {
        THREADED(hCellLandauEvenCol_[planeID])->Fill(data.getClusterCharge(planeID));

        if (data.getXPixelResidualLocal(planeID) > 0) THREADED(hCellLandauEvenColRight_[planeID])->Fill(data.getClusterCharge(planeID));
        else                                          THREADED(hCellLandauEvenColLeft_ [planeID])->Fill(data.getClusterCharge(planeID));
    }
    else
    {
        THREADED(hCellLandauOddCol_[planeID])->Fill(data.getClusterCharge(planeID));

        if (data.getXPixelResidualLocal(planeID) > 0) THREADED(hCellLandauOddColRight_[planeID])->Fill(data.getClusterCharge(planeID));
        else                                          THREADED(hCellLandauOddColLeft_ [planeID])->Fill(data.getClusterCharge(planeID));
    }

    if (((int)rowPredicted)%2 == 0)
    {
        THREADED(hCellLandauEvenRow_[planeID])->Fill(data.getClusterCharge(planeID));

        if (data.getXPixelResidualLocal(planeID) > 0) THREADED(hCellLandauEvenRowRight_[planeID])->Fill(data.getClusterCharge(planeID));
        else                                          THREADED(hCellLandauEvenRowLeft_ [planeID])->Fill(data.getClusterCharge(planeID));
    }
    else
    {
        THREADED(hCellLandauOddRow_[planeID])->Fill(data.getClusterCharge(planeID));

        if (data.getXPixelResidualLocal(planeID) > 0) THREADED(hCellLandauOddRowRight_[planeID])->Fill(data.getClusterCharge(planeID));
        else                                          THREADED(hCellLandauOddRowLeft_ [planeID])->Fill(data.getClusterCharge(planeID));
    }
}

//=======================================================================
void ChargeUniMiB::clusterLandau(bool pass, int planeID, const Data& data, int threadNumber)
{
    // #####################
    // # Internal constant #
    // #####################
    int maxClusterSize = 4;
    int pixelCell = -1;

    if (!pass || !data.getIsInDetector(planeID) || !data.getHasHit(planeID) || data.getClusterSize(planeID) > maxClusterSize) return;

    int clusterSize = data.getClusterSize(planeID);

    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    const Window* theWindow    = theWindowsManager_->getWindow(planeID);
    int           rowPredicted = data.getRowPredicted(planeID);
    int           colPredicted = data.getColPredicted(planeID);
    int           run          = data.getRunNumber();

    if (!theWindow->checkWindow(colPredicted,rowPredicted,run)) return;

    for (int h = 0; h < clusterSize; h++)
    {
        if (!theWindow->checkWindow(data.getClusterPixelCol(h,planeID),data.getClusterPixelRow(h,planeID),run) // Hits are in the window
                || !data.getIsPixelCalibrated(h,planeID)                                                           // Pixels are calibrated
                ||  data.getClusterPixelCharge (h,planeID) < standardCutsPixelMinimumCharge_                       // Charge is over threshold
                ||  data.getClusterPixelCharge (h,planeID) > standardCutsPixelMaximumCharge_)                      // Maximum allowed charge for this physics
            return;

        if (data.getClusterPixelRow(h,planeID) == rowPredicted && data.getClusterPixelCol(h,planeID) == colPredicted) pixelCell = h;
    }

    if      (clusterSize == 1) THREADED(hLandauClusterSize1_[planeID])->Fill(data.getClusterCharge(planeID));
    else if (clusterSize == 2) THREADED(hLandauClusterSize2_[planeID])->Fill(data.getClusterCharge(planeID));

    if      (pixelCell != -1)  THREADED(hCellLandauSinglePixel_[planeID])->Fill(data.getClusterPixelCharge(pixelCell, planeID));
}

//=======================================================================
void ChargeUniMiB::planeCharge(bool pass, int planeID, const Data& data, int threadNumber)
{
    // #####################
    // # Internal constant #
    // #####################
    int maxClusterSize = 4;
    bool foundCluster = false;

    if (!pass || !data.getIsInDetector(planeID) || !data.getHasHit(planeID) || data.getClusterSize(planeID) > maxClusterSize) return;

    int clusterSize = data.getClusterSize(planeID);

    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    const Window* theWindow    = theWindowsManager_->getWindow(planeID);
    int           rowPredicted = data.getRowPredicted(planeID);
    int           colPredicted = data.getColPredicted(planeID);
    int           run          = data.getRunNumber();

    if (!theWindow->checkWindow(colPredicted,rowPredicted,run)) return;

    for (int h = 0; h < clusterSize; h++)
    {
        if (!theWindow->checkWindow(data.getClusterPixelCol(h,planeID),data.getClusterPixelRow(h,planeID),run) // Hits are in the window
                || !data.getIsPixelCalibrated  (h,planeID)                                                         // Pixels are calibrated
                ||  data.getClusterPixelCharge (h,planeID) < standardCutsPixelMinimumCharge_                       // Charge is over threshold
                ||  data.getClusterPixelCharge (h,planeID) > standardCutsPixelMaximumCharge_)                      // Maximum allowed charge for this physics
            return;

        if ((data.getClusterPixelRow(h,planeID) == rowPredicted) &&
                (data.getClusterPixelCol(h,planeID) == colPredicted))
            foundCluster = true;
    }

    if (foundCluster == false) return;

    // #####################################################################
    // # Compute charge only for cells that are surrounded by "good" cells #
    // #####################################################################
    if (theWindow->checkWindowAbout(colPredicted,rowPredicted,run,thePlaneMapping_->getPlaneType(planeID)))
    {
        THREADED(h2DChargeNorm_[planeID])->Fill(colPredicted,rowPredicted);
        if (data.getHasHit(planeID)) THREADED(h2DCharge_[planeID])->Fill(colPredicted,rowPredicted,data.getClusterCharge(planeID));
    }
}

//=======================================================================
void ChargeUniMiB::cellCharge(bool pass, int planeID, const Data& data, int threadNumber)
{
    // #####################
    // # Internal constant #
    // #####################
    int maxClusterSize = 4;

    float maxPitchX;
    float maxPitchY;
    float xPixelResidual;
    float yPixelResidual;
    float xPixelEdgeResidual = 0;
    float yPixelEdgeResidual = 0;

    if (!pass || !data.getIsInDetector(planeID) || !data.getHasHit(planeID) || data.getClusterSize(planeID) > maxClusterSize) return;

    int clusterSize = data.getClusterSize(planeID);

    maxPitchX = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->getCellPitches().first).c_str());
    maxPitchY = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->getCellPitches().second).c_str());

    if (data.getXPitchLocal(planeID) > maxPitchX || data.getYPitchLocal(planeID) > maxPitchY) return;

    xPixelResidual = data.getXPixelResidualLocal(planeID);
    yPixelResidual = data.getYPixelResidualLocal(planeID);

    if (xPixelResidual > 0)       xPixelEdgeResidual = xPixelResidual - data.getXPitchLocal(planeID)/2;
    else if (xPixelResidual <= 0) xPixelEdgeResidual = xPixelResidual + data.getXPitchLocal(planeID)/2;

    if (yPixelResidual > 0)       yPixelEdgeResidual = yPixelResidual - data.getYPitchLocal(planeID)/2;
    else if (yPixelResidual <= 0) yPixelEdgeResidual = yPixelResidual + data.getYPitchLocal(planeID)/2;

    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    const Window* theWindow    = theWindowsManager_->getWindow(planeID);
    int           rowPredicted = data.getRowPredicted(planeID);
    int           colPredicted = data.getColPredicted(planeID);
    int           run          = data.getRunNumber();

    if (!theWindow->checkWindow(colPredicted,rowPredicted,run)) return;

    for (int h = 0; h < clusterSize; h++)
    {
        if (theWindow->checkWindow(data.getClusterPixelCol(h,planeID),data.getClusterPixelRow(h,planeID),run) &&
                data.getClusterPixelRow   (h,planeID) == rowPredicted &&
                data.getClusterPixelCol   (h,planeID) == colPredicted &&
                data.getIsPixelCalibrated (h,planeID)                 &&
                data.getClusterPixelCharge(h,planeID) > standardCutsPixelMinimumCharge_ &&
                data.getClusterPixelCharge(h,planeID) < standardCutsPixelMaximumCharge_)
        {
            THREADED(h4CellsCharge_    [planeID])->Fill(xPixelEdgeResidual,yPixelEdgeResidual,data.getClusterPixelCharge(h,planeID));
            THREADED(h4CellsChargeNorm_[planeID])->Fill(xPixelEdgeResidual,yPixelEdgeResidual);

            THREADED(h2DCellCharge_    [planeID])->Fill(xPixelResidual,yPixelResidual,data.getClusterPixelCharge(h,planeID));
            THREADED(h2DCellChargeNorm_[planeID])->Fill(xPixelResidual,yPixelResidual);

            if (((int)colPredicted)%2 == 0)
            {
                THREADED(h2DCellChargeEvenCol_    [planeID])->Fill(xPixelResidual,yPixelResidual,data.getClusterPixelCharge(h,planeID));
                THREADED(h2DCellChargeEvenColNorm_[planeID])->Fill(xPixelResidual,yPixelResidual);
            }
            else
            {
                THREADED(h2DCellChargeOddCol_    [planeID])->Fill(xPixelResidual,yPixelResidual,data.getClusterPixelCharge(h,planeID));
                THREADED(h2DCellChargeOddColNorm_[planeID])->Fill(xPixelResidual,yPixelResidual);
            }

            if (((int)rowPredicted)%2 == 0)
            {
                THREADED(h2DCellChargeEvenRow_    [planeID])->Fill(xPixelResidual,yPixelResidual,data.getClusterPixelCharge(h,planeID));
                THREADED(h2DCellChargeEvenRowNorm_[planeID])->Fill(xPixelResidual,yPixelResidual);
            }
            else
            {
                THREADED(h2DCellChargeOddRow_    [planeID])->Fill(xPixelResidual,yPixelResidual,data.getClusterPixelCharge(h,planeID));
                THREADED(h2DCellChargeOddRowNorm_[planeID])->Fill(xPixelResidual,yPixelResidual);
            }

            THREADED(h2DClusterSize_[planeID])->Fill(xPixelResidual,yPixelResidual,data.getClusterSize(planeID));
        }
    }
}

//=======================================================================
void ChargeUniMiB::xLandau(bool pass, int planeID, const Data &data, int threadNumber)
{
    // #####################
    // # Internal constant #
    // #####################
    int clusterSize = 2;

    if (!pass || !data.getIsInDetector(planeID) || !data.getHasHit(planeID) || data.getClusterSize(planeID) != clusterSize) return;

    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    const Window* theWindow    = theWindowsManager_->getWindow(planeID);
    int           rowPredicted = data.getRowPredicted(planeID);
    int           colPredicted = data.getColPredicted(planeID);
    int           run          = data.getRunNumber();

    if (!theWindow->checkWindow(colPredicted,rowPredicted,run)) return;

    for (int h = 0; h < clusterSize; h++)
    {
        if (!theWindow->checkWindow(data.getClusterPixelCol(h,planeID),data.getClusterPixelRow(h,planeID),run) // Hits are in the window
                || !data.getIsPixelCalibrated  (h,planeID)                                                         // Pixels are calibrated
                ||  data.getClusterPixelRow    (h,planeID) != rowPredicted                                         // Hits are on the same row (sharing is along the row - x direction)
                ||  data.getClusterPixelCharge (h,planeID) < standardCutsPixelMinimumCharge_                       // Charge is over threshold
                ||  data.getClusterPixelCharge (h,planeID) > standardCutsPixelMaximumCharge_)                      // Maximum allowed charge for this physics
            return;
    }

    THREADED(hLandauClusterSize2sameRow_[planeID])->Fill(data.getClusterCharge(planeID));
    THREADED(hLandauClusterSize2pointed_[planeID])->Fill(data.getClusterCharge(planeID));
}

//=======================================================================
void ChargeUniMiB::yLandau(bool pass, int planeID, const Data &data, int threadNumber)
{
    // #####################
    // # Internal constant #
    // #####################
    int clusterSize = 2;

    if (!pass || !data.getHasHit(planeID) || data.getClusterSize(planeID) != clusterSize) return;

    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    const Window* theWindow    = theWindowsManager_->getWindow(planeID);
    int           rowPredicted = data.getRowPredicted(planeID);
    int           colPredicted = data.getColPredicted(planeID);
    int           run          = data.getRunNumber();

    if (!theWindow->checkWindow(colPredicted,rowPredicted,run)) return;

    for (int h = 0; h < clusterSize; h++)
    {
        if (!theWindow->checkWindow(data.getClusterPixelCol(h,planeID),data.getClusterPixelRow(h,planeID),run) // Hits are in the window
                || !data.getIsPixelCalibrated  (h,planeID)                                                         // Pixels are calibrated
                ||  data.getClusterPixelCol    (h,planeID) != colPredicted                                         // Hits are on the same column (sharing is along the columm - y direction)
                ||  data.getClusterPixelCharge (h,planeID) < standardCutsPixelMinimumCharge_                       // Charge is over threshold
                ||  data.getClusterPixelCharge (h,planeID) > standardCutsPixelMaximumCharge_)                      // Maximum allowed charge for this physics
            return;
    }

    THREADED(hLandauClusterSize2sameCol_[planeID])->Fill(data.getClusterCharge(planeID));
    THREADED(hLandauClusterSize2pointed_[planeID])->Fill(data.getClusterCharge(planeID));
}

//=======================================================================
void ChargeUniMiB::xChargeDivision(bool pass, int planeID, const Data& data, int threadNumber)
{
    // #####################
    // # Internal constant #
    // #####################
    int maxClusterSize = 2;

    float xRes = 0;
    float maxPitchX;
    float maxPitchY;

    if (!pass||!data.getIsInDetector(planeID)) return;

    maxPitchX = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->getCellPitches().first).c_str());
    maxPitchY = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->getCellPitches().second).c_str());

    if (data.getXPitchLocal(planeID) > maxPitchX || data.getYPitchLocal(planeID) > maxPitchY) return;

    if      (data.getXPixelResidualLocal(planeID) >  0) xRes = data.getXPixelResidualLocal(planeID) - data.getXPitchLocal(planeID)/2;
    else if (data.getXPixelResidualLocal(planeID) <= 0) xRes = data.getXPixelResidualLocal(planeID) + data.getXPitchLocal(planeID)/2;

    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    const Window* theWindow    = theWindowsManager_->getWindow(planeID);
    int           rowPredicted = data.getRowPredicted(planeID);
    int           colPredicted = data.getColPredicted(planeID);
    int           run          = data.getRunNumber();

    if (!theWindow->checkWindow(colPredicted,rowPredicted,run)) return;

    bool myReturn = false;
    if ((theWindow->checkWindowAbout(colPredicted,rowPredicted,run,thePlaneMapping_->getPlaneType(planeID))) && (data.getClusterSize(planeID) <= maxClusterSize))
    {
        for (int h = 0; h < data.getClusterSize(planeID); h++)
        {
            if (!theWindow->checkWindow(data.getClusterPixelCol(h,planeID),data.getClusterPixelRow(h,planeID),run) // Hits are in the window
                    || !data.getIsPixelCalibrated  (h,planeID)                                                         // Pixels are calibrated
                    ||  data.getClusterPixelCharge (h,planeID) < standardCutsPixelMinimumCharge_                       // Charge is over threshold
                    ||  data.getClusterPixelCharge (h,planeID) > standardCutsPixelMaximumCharge_)                      // Maximum allowed charge for this physics
            {
                myReturn = true;
                break;
            }
        }

        THREADED(h1DXcellChargeNorm_         [planeID])->Fill(xRes);
        THREADED(h1DXcellChargeSecondHitNorm_[planeID])->Fill(xRes);

        if ((data.getHasHit(planeID)) && (!myReturn))
        {
            // Special instructions for 25 x 100 sensors: since telescope resolution is very large w.r.t to the short pitch
            // check adjacent rows when performing scan along 100 um pitch.

            for (int h = 0; h < data.getClusterSize(planeID); h++)
            {
                if(((data.getClusterPixelCol(h,planeID) == colPredicted) && (data.getClusterPixelRow(h,planeID) == rowPredicted))                                                                                  ||
                   ((maxPitchY==SPECIALPITCH) && (data.getClusterPixelCol(h,planeID) == colPredicted) && (data.getYPixelResidualLocal(planeID) >  0) && (rowPredicted - data.getClusterPixelRow(h,planeID) == -1))||
                   ((maxPitchY==SPECIALPITCH) && (data.getClusterPixelCol(h,planeID) == colPredicted) && (data.getYPixelResidualLocal(planeID) <= 0) && (rowPredicted - data.getClusterPixelRow(h,planeID) ==  1))  )
                {
                    THREADED(h2DXcellCharge_          [planeID])->Fill(xRes,data.getClusterPixelCharge(h,planeID));
                    THREADED(h2DXcellChargeSecondHit_ [planeID])->Fill(xRes,data.getClusterPixelCharge(h,planeID));
                    THREADED(h1DXcellCharge_          [planeID])->Fill(xRes,data.getClusterPixelCharge(h,planeID));
                    THREADED(h1DXcellChargeSecondHit_ [planeID])->Fill(xRes,data.getClusterPixelCharge(h,planeID));
                    break;
                }
            }

            for (int h = 0; h < data.getClusterSize(planeID); h++)
            {
                if( (data.getClusterPixelRow(h,planeID) == rowPredicted)                                                                                 ||
                   ((maxPitchY==SPECIALPITCH) &&(data.getYPixelResidualLocal(planeID) >  0) && (rowPredicted - data.getClusterPixelRow(h,planeID) == -1))||
                   ((maxPitchY==SPECIALPITCH) &&(data.getYPixelResidualLocal(planeID) <= 0) && (rowPredicted - data.getClusterPixelRow(h,planeID) ==  1))  )
                {
                    if (((xRes >  0) && (colPredicted - data.getClusterPixelCol(h,planeID) ==  1))||
                        ((xRes <= 0) && (colPredicted - data.getClusterPixelCol(h,planeID) == -1))  )
                    {
                        THREADED(h2DXcellChargeSecondHit_[planeID])->Fill(xRes,data.getClusterPixelCharge(h,planeID));
                        THREADED(h1DXcellChargeSecondHit_[planeID])->Fill(xRes,data.getClusterPixelCharge(h, planeID));
                        break;
                    }
                }
            }
        }
    }
}

//=======================================================================
void ChargeUniMiB::xAsymmetry(bool pass, int planeID, const Data& data, int threadNumber)
{
    // #####################
    // # Internal constant #
    // #####################
    int maxClusterSize = 4;

    float maxPitchX;

    if (!pass || !data.getIsInDetector(planeID) || !data.getHasHit(planeID) || data.getClusterSize(planeID) > maxClusterSize) return;

    int clusterSize = data.getClusterSize(planeID);

    maxPitchX = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->getCellPitches().first).c_str());

    if (data.getXPitchLocal(planeID) > maxPitchX) return;

    /*Davide
    if(data.getXErrorPredictedGlobal(planeID) > 4.3) return;

    if((data.getYPixelResidualLocal(planeID) < -13.) || (data.getYPixelResidualLocal(planeID) > 13.)) return;
    Resolution studies */

    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    const Window* theWindow    = theWindowsManager_->getWindow(planeID);
    int           rowPredicted = data.getRowPredicted(planeID);
    int           colPredicted = data.getColPredicted(planeID);
    int           run          = data.getRunNumber();

    if (!theWindow->checkWindow(colPredicted,rowPredicted,run)) return;

    for (int h = 0; h < clusterSize; h++)
    {
        if (!theWindow->checkWindow(data.getClusterPixelCol(h,planeID),data.getClusterPixelRow(h,planeID),run) // Hits are in the window
                || !data.getIsPixelCalibrated  (h,planeID)                                                         // Pixels are calibrated
                ||  data.getClusterPixelRow    (h,planeID) != rowPredicted                                         // Hits are on the same column (sharing is along the column - y direction)
                ||  data.getClusterPixelCharge (h,planeID) < standardCutsPixelMinimumCharge_                       // Charge is over threshold
                ||  data.getClusterPixelCharge (h,planeID) > standardCutsPixelMaximumCharge_)                      // Maximum allowed charge for this physics
            return;
    }

    if (clusterSize == 2)
    {
        float asymmetry   = 0;
        int   totalCharge = 0;
        int   chargeLeft  = 0;
        int   chargeRight = 0;
        float xPredicted  = data.getXPredictedLocal(planeID);
        float xMeasured   = (data.getXClusterPixelCenterLocal(0, planeID) + data.getXClusterPixelCenterLocal(1, planeID))/2;
        float xResidual   = xPredicted - xMeasured;

        if (data.getXClusterPixelCenterLocal(0, planeID) > data.getXClusterPixelCenterLocal(1, planeID))
        {
            chargeRight = data.getClusterPixelCharge(0, planeID);
            chargeLeft  = data.getClusterPixelCharge(1, planeID);
        }
        else if (data.getXClusterPixelCenterLocal(0, planeID) < data.getXClusterPixelCenterLocal(1, planeID))
        {
            chargeRight = data.getClusterPixelCharge(1, planeID);
            chargeLeft  = data.getClusterPixelCharge(0, planeID);
        }

        totalCharge = chargeLeft + chargeRight;

        if (totalCharge > 0 && totalCharge < maxChargeDeltaRay)
        {
            asymmetry = (float)(chargeLeft - chargeRight) / (float)totalCharge;

            if (totalCharge >= standardCutsPixelMinimumCharge_ && totalCharge <= standardCutsPixelMaximumCharge_)
            {
                THREADED(h2DXcellChargeAsymmetry_   [planeID])->Fill(xResidual, asymmetry);
                THREADED(h2DXcellChargeAsymmetryInv_[planeID])->Fill(asymmetry, xResidual);
            }
        }
    }
}

//=======================================================================
void ChargeUniMiB::yChargeDivision(bool pass, int planeID, const Data& data, int threadNumber)
{
    // #####################
    // # Internal constant #
    // #####################
    int maxClusterSize = 2;

    float yRes = 0;
    float maxPitchX;
    float maxPitchY;

    if (!pass || !data.getIsInDetector(planeID)) return;

    maxPitchX = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->getCellPitches().first).c_str());
    maxPitchY = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->getCellPitches().second).c_str());

    if (data.getXPitchLocal(planeID) > maxPitchX || data.getYPitchLocal(planeID) > maxPitchY) return;
    
    if      (data.getYPixelResidualLocal(planeID) >  0) yRes = data.getYPixelResidualLocal(planeID) - data.getYPitchLocal(planeID)/2;
    else if (data.getYPixelResidualLocal(planeID) <= 0) yRes = data.getYPixelResidualLocal(planeID) + data.getYPitchLocal(planeID)/2;

    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    const Window* theWindow    = theWindowsManager_->getWindow(planeID);
    int           rowPredicted = data.getRowPredicted(planeID);
    int           colPredicted = data.getColPredicted(planeID);
    int           run          = data.getRunNumber();

    if (!theWindow->checkWindow(colPredicted,rowPredicted,run)) return;

    bool myReturn = false;
    if ((theWindow->checkWindowAbout(colPredicted,rowPredicted,run,thePlaneMapping_->getPlaneType(planeID))) && (data.getClusterSize(planeID) <= maxClusterSize))
    {
        for (int h = 0; h < data.getClusterSize(planeID); h++)
        {
            if (!theWindow->checkWindow(data.getClusterPixelCol(h,planeID),data.getClusterPixelRow(h,planeID),run) // Hits are in the window
                    || !data.getIsPixelCalibrated  (h,planeID)                                                         // Pixels are calibrated
                    ||  data.getClusterPixelCharge (h,planeID) < standardCutsPixelMinimumCharge_                       // Charge is over threshold
                    ||  data.getClusterPixelCharge (h,planeID) > standardCutsPixelMaximumCharge_)                      // Maximum allowed charge for this physics
            {
                myReturn = true;
                break;
            }
        }

        THREADED(h1DYcellChargeNorm_         [planeID])->Fill(yRes);
        THREADED(h1DYcellChargeSecondHitNorm_[planeID])->Fill(yRes);

        if ((data.getHasHit(planeID)) && (!myReturn ))
        {
            for (int h = 0; h < data.getClusterSize(planeID); h++)
            {
                if (data.getClusterPixelRow(h,planeID) == rowPredicted && data.getClusterPixelCol(h,planeID) == colPredicted)
                {
                    THREADED(h2DYcellCharge_          [planeID])->Fill(yRes,data.getClusterPixelCharge(h,planeID));
                    THREADED(h2DYcellChargeSecondHit_ [planeID])->Fill(yRes,data.getClusterPixelCharge(h,planeID));
                    THREADED(h1DYcellCharge_          [planeID])->Fill(yRes,data.getClusterPixelCharge(h,planeID));
                    THREADED(h1DYcellChargeSecondHit_ [planeID])->Fill(yRes,data.getClusterPixelCharge(h,planeID));
                    break;
                }
            }

            for (int h = 0; h < data.getClusterSize(planeID); h++)
            {
                if(data.getClusterPixelCol(h,planeID) == colPredicted)
                {
                    if(((yRes > 0 ) && (rowPredicted - data.getClusterPixelRow(h,planeID) ==  1))||
                       ((yRes <= 0) && (rowPredicted - data.getClusterPixelRow(h,planeID) == -1))  )
//                        if((rowPredicted - data.getClusterPixelRow(h,planeID) == -1)||
//                           (rowPredicted - data.getClusterPixelRow(h,planeID) ==  1)  )
                    {
                        THREADED(h2DYcellChargeSecondHit_[planeID])->Fill(yRes,data.getClusterPixelCharge(h,planeID));
                        THREADED(h1DYcellChargeSecondHit_[planeID])->Fill(yRes,data.getClusterPixelCharge(h,planeID));
                        break;
                    }
                }
            }
        }
    }
}

//=======================================================================
void ChargeUniMiB::yAsymmetry(bool pass, int planeID, const Data& data, int threadNumber)
{
    // #####################
    // # Internal constant #
    // #####################
    int maxClusterSize = 4;

    float maxPitchY;

    if (!pass || !data.getIsInDetector(planeID) || !data.getHasHit(planeID) || data.getClusterSize(planeID) > maxClusterSize) return;

    int clusterSize = data.getClusterSize(planeID);

    maxPitchY = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->getCellPitches().second).c_str());

    if (data.getYPitchLocal(planeID) > maxPitchY) return;
    
    /*Davide
    if((data.getYErrorPredictedGlobal(planeID) < 3.6)  || (data.getYErrorPredictedGlobal(planeID) > 3.8)) return;

    if((data.getXPixelResidualLocal(planeID) < -13.) || (data.getXPixelResidualLocal(planeID) > 13.)) return;
    Resolution studies*/

    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    const Window* theWindow    = theWindowsManager_->getWindow(planeID);
    int           rowPredicted = data.getRowPredicted(planeID);
    int           colPredicted = data.getColPredicted(planeID);
    int           run          = data.getRunNumber();

    if (!theWindow->checkWindow(colPredicted,rowPredicted,run)) return;

    for (int h = 0; h < clusterSize; h++)
    {
        if (!theWindow->checkWindow(data.getClusterPixelCol(h,planeID),data.getClusterPixelRow(h,planeID),run) // Hits are in the window
                || !data.getIsPixelCalibrated  (h,planeID)                                                         // Pixels are calibrated
                ||  data.getClusterPixelCol    (h,planeID) != colPredicted                                         // Hits are on the same column (sharing is along the column - y direction)
                ||  data.getClusterPixelCharge (h,planeID) < standardCutsPixelMinimumCharge_                       // Charge is over threshold
                ||  data.getClusterPixelCharge (h,planeID) > standardCutsPixelMaximumCharge_)                      // Maximum allowed charge for this physics
            return;
    }

    if (clusterSize == 2)
    {
        float asymmetry   = 0;
        int   totalCharge = 0;
        int   chargeDown  = 0;
        int   chargeUp    = 0;
        float yPredicted  = data.getYPredictedLocal(planeID);
        float yMeasured   = (data.getYClusterPixelCenterLocal(0, planeID) + data.getYClusterPixelCenterLocal(1, planeID))/2;
        float yResidual   = yPredicted - yMeasured;
        float xPosition   = data.getXPixelResidualLocal(planeID);

        if (data.getYClusterPixelCenterLocal(0, planeID) > data.getYClusterPixelCenterLocal(1, planeID))
        {
            chargeUp   = data.getClusterPixelCharge(0, planeID);
            chargeDown = data.getClusterPixelCharge(1, planeID);
        }
        else if (data.getYClusterPixelCenterLocal(0, planeID) < data.getYClusterPixelCenterLocal(1, planeID))
        {
            chargeUp   = data.getClusterPixelCharge(1, planeID);
            chargeDown = data.getClusterPixelCharge(0, planeID);
        }

        totalCharge = chargeDown + chargeUp;

        if (totalCharge > 0 && totalCharge < maxChargeDeltaRay)
        {
            asymmetry = (float)(chargeDown - chargeUp) / (float)totalCharge;

            if (totalCharge >= standardCutsPixelMinimumCharge_ && totalCharge <= standardCutsPixelMaximumCharge_)
            {
                THREADED(h2DYcellChargeAsymmetry_   [planeID])->Fill(yResidual, asymmetry);
                THREADED(h2DYcellChargeAsymmetryInv_[planeID])->Fill(asymmetry, yResidual);

                int greaterRow = 0;

                if(data.getClusterPixelRow(0,planeID) > data.getClusterPixelRow(1,planeID))
                    greaterRow = data.getClusterPixelRow(0,planeID);
                else
                    greaterRow = data.getClusterPixelRow(1,planeID);

                if(xPosition > 0)
                {
                    if (greaterRow %2 != 0)
                    {
                        THREADED(h2DYcellChargeAsymmetryRightBump_   [planeID])->Fill(yResidual, asymmetry);
                        THREADED(h2DYcellChargeAsymmetryInvRightBump_[planeID])->Fill(asymmetry, yResidual);
                    }
                    else
                    {
                        THREADED(h2DYcellChargeAsymmetryRightNoBump_   [planeID])->Fill(yResidual, asymmetry);
                        THREADED(h2DYcellChargeAsymmetryInvRightNoBump_[planeID])->Fill(asymmetry, yResidual);
                    }
                }
                else
                {
                    if (greaterRow %2 != 0)
                    {
                        THREADED(h2DYcellChargeAsymmetryLeftBump_   [planeID])->Fill(yResidual, asymmetry);
                        THREADED(h2DYcellChargeAsymmetryInvLeftBump_[planeID])->Fill(asymmetry, yResidual);
                    }
                    else
                    {
                        THREADED(h2DYcellChargeAsymmetryLeftNoBump_   [planeID])->Fill(yResidual, asymmetry);
                        THREADED(h2DYcellChargeAsymmetryInvLeftNoBump_[planeID])->Fill(asymmetry, yResidual);
                    }
                }
            }
        }
    }
}

//=======================================================================
void ChargeUniMiB::setCutsFormula(std::map<std::string,std::string> cutsList,std::vector<TTree*> tree)
{
    std::vector<TTreeFormula*> formulasVector;

    for (std::map<std::string,std::string>::iterator it = cutsList.begin(); it != cutsList.end(); it++)
    {
        if ((it->first) == "main cut" && (it->second).size() == 0)
            STDLINE("WARNING: no main cut set in charge analysis ! Default value = true !", ACRed);

        formulasVector.clear();
        if ((it->second).size() != 0)
        {
            for (unsigned int t = 0; t < tree.size(); t++)
                formulasVector.push_back(new TTreeFormula((it->second).c_str(),(it->second).c_str(),tree[t]));
            cutsFormulas_[it->first] = formulasVector;
        }
    }
}

//=======================================================================
bool ChargeUniMiB::passCalibrationsCut(int planeID, const Data &data)
{
    // #####################
    // # Internal constant #
    // #####################
    int maxClusterSize = 4;

    if (!(theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->useCalibrations()) return true;
    if (data.getClusterSize(planeID) > maxClusterSize)                                             return false;

    return true;
}

//=======================================================================
bool ChargeUniMiB::passStandardCuts(int planeID, const Data &data)
{
    if (!theXmlParser_->getAnalysesFromString("Charge")->standardCut()) return true;

    int minHits   = atoi(theXmlParser_->getAnalysesFromString("Charge")->getMinHits().c_str()) - 1;
    int excludeMe = 0;
    if (thePlaneMapping_->getPlaneName(planeID).find("Dut") != std::string::npos) minHits += 1;
    else if (data.getHasHit(planeID) && data.getClusterSize(planeID) <= 2) excludeMe = 1;

    if (data.getNumberOfTelescopeHits() - excludeMe >= minHits) return true;
    else                                                        return false;
}

//=======================================================================
void ChargeUniMiB::beginJob(void)
{
    standardCutsPixelMinimumCharge_   = theXmlParser_->getAnalysesFromString("Charge")->getPixelMinimumCharge();
    standardCutsPixelMaximumCharge_   = theXmlParser_->getAnalysesFromString("Charge")->getPixelMaximumCharge();
    standardCutsClusterMinimumCharge_ = theXmlParser_->getAnalysesFromString("Charge")->getClusterMinimumCharge();
    standardCutsClusterMaximumCharge_ = theXmlParser_->getAnalysesFromString("Charge")->getClusterMaximumCharge();

    theWindowsManager_      = theAnalysisManager_->getWindowsManager();
    theCalibrationsManager_ = theAnalysisManager_->getCalibrationsManager();

    book();
}

//=======================================================================
void ChargeUniMiB::analyze(const Data& data, int threadNumber)
{
    if (cutsFormulas_.find("main cut") != cutsFormulas_.end() && !cutsFormulas_["main cut"][theAnalysisManager_->getCurrentTreeNumber(threadNumber)]->EvalInstance()) return;

    for (unsigned int p = 0; p < thePlaneMapping_->getNumberOfPlanes(); p++) clusterSize(p,data,threadNumber);

    bool clusterLandauCut = true;
    if(cutsFormulas_.find("cluster Landau") != cutsFormulas_.end())
        clusterLandauCut = cutsFormulas_["cluster Landau"][theAnalysisManager_->getCurrentTreeNumber(threadNumber)]->EvalInstance();

    bool cellLandauCut    = true;
    if(cutsFormulas_.find("cell Landau") != cutsFormulas_.end())
        cellLandauCut = cutsFormulas_["cell Landau"][theAnalysisManager_->getCurrentTreeNumber(threadNumber)]->EvalInstance();

    bool cellChargeCut    = true;
    if(cutsFormulas_.find("cell charge") != cutsFormulas_.end())
        cellChargeCut = cutsFormulas_["cell charge"][theAnalysisManager_->getCurrentTreeNumber(threadNumber)]->EvalInstance();

    bool cellChargeXCut   = true;
    if(cutsFormulas_.find("cell charge X") != cutsFormulas_.end())
        cellChargeXCut = cutsFormulas_["cell charge X"][theAnalysisManager_->getCurrentTreeNumber(threadNumber)]->EvalInstance();

    bool cellChargeYCut   = true;
    if(cutsFormulas_.find("cell charge Y") != cutsFormulas_.end())
        cellChargeYCut = cutsFormulas_["cell charge Y"][theAnalysisManager_->getCurrentTreeNumber(threadNumber)]->EvalInstance();


    // ######################################################
    // # Require all telescope planes with cluster size = 2 #
    // ######################################################
    if (ONLYdoubleHITS == true)
    {
        for (unsigned int p = 0; p < thePlaneMapping_->getNumberOfPlanes(); p++)
        {
            if ((thePlaneMapping_->getPlaneName(p).find("Dut")   == std::string::npos) &&
                    (thePlaneMapping_->getPlaneName(p).find("Strip") == std::string::npos) &&
                    (data.getClusterSize(p) != 2)) return;
        }
    }

    // ######################################
    // # Require a hit one each strip plane #
    // ######################################
    if (HITALLSTRIPS == true)
    {
        int stripHits = 0;
        for (unsigned int p = 0; p < thePlaneMapping_->getNumberOfPlanes(); p++)
        {
            if((thePlaneMapping_->getPlaneName(p).find("Strip") != std::string::npos) &&
                    (data.getHasHit(p)))
            {
                stripHits++;
            }
        }

        if(stripHits != data.getNumberOfActiveStripPlanes()) return;
    }

    for (unsigned int p = 0; p < thePlaneMapping_->getNumberOfPlanes(); p++)
    {
        if (!passStandardCuts(p,data)) continue;

        if ((thePlaneMapping_->getPlaneName(p).find("Dut") != std::string::npos) && (!passCalibrationsCut(p,data)))
        {
            std::cout << __PRETTY_FUNCTION__ << "Calibration check not passed" << std::endl;
            return;
        }

        clusterLandau   (clusterLandauCut,p,data,threadNumber);
        cellLandau      (cellLandauCut,   p,data,threadNumber);
        cellCharge      (cellChargeCut,   p,data,threadNumber);
        planeCharge     (true,            p,data,threadNumber);

        xLandau         (cellChargeXCut,  p,data,threadNumber);
        xChargeDivision (cellChargeXCut,  p,data,threadNumber);
        xAsymmetry      (cellChargeXCut,  p,data,threadNumber);

        yLandau         (cellChargeYCut,  p,data,threadNumber);
        yChargeDivision (cellChargeYCut,  p,data,threadNumber);
        yAsymmetry      (cellChargeYCut,  p,data,threadNumber);
    }
}

//=======================================================================
void ChargeUniMiB::endJob(void)
{
    std::stringstream ss;

    STDLINE("",ACWhite);

    for (unsigned int p = 0; p < thePlaneMapping_->getNumberOfPlanes(); p++)
    {
        std::string planeName = thePlaneMapping_->getPlaneName(p);
        ss.str("") ; ss << "Adding threads for plane " << p;
        STDLINE(ss.str().c_str(),ACYellow);

        ADD_THREADED(hCellLandau_                    	   [p]);
        ADD_THREADED(hCellLandauOddCol_                    [p]);
        ADD_THREADED(hCellLandauOddColLeft_                [p]);
        ADD_THREADED(hCellLandauOddColRight_               [p]);
        ADD_THREADED(hCellLandauEvenCol_                   [p]);
        ADD_THREADED(hCellLandauEvenColLeft_               [p]);
        ADD_THREADED(hCellLandauEvenColRight_              [p]);
        ADD_THREADED(hCellLandauOddRow_                    [p]);
        ADD_THREADED(hCellLandauOddRowLeft_                [p]);
        ADD_THREADED(hCellLandauOddRowRight_               [p]);
        ADD_THREADED(hCellLandauEvenRow_                   [p]);
        ADD_THREADED(hCellLandauEvenRowLeft_               [p]);
        ADD_THREADED(hCellLandauEvenRowRight_              [p]);
        ADD_THREADED(hClusterSize_                   	   [p]);

        ADD_THREADED(hLandauClusterSize1_            	   [p]);
        ADD_THREADED(hLandauClusterSize2_            	   [p]);
        ADD_THREADED(hLandauClusterSize2pointed_     	   [p]);
        ADD_THREADED(hLandauClusterSize2sameRow_     	   [p]);
        ADD_THREADED(hLandauClusterSize2sameCol_     	   [p]);
        ADD_THREADED(hCellLandauSinglePixel_         	   [p]);

        ADD_THREADED(h1DXcellCharge_                 	   [p]);
        ADD_THREADED(h1DXcellChargeNorm_             	   [p]);

        ADD_THREADED(h1DXcellChargeSecondHit_        	   [p]);
        ADD_THREADED(h1DXcellChargeSecondHitNorm_    	   [p]);

        ADD_THREADED(h1DYcellCharge_                 	   [p]);
        ADD_THREADED(h1DYcellChargeNorm_             	   [p]);

        ADD_THREADED(h1DYcellChargeSecondHit_        	   [p]);
        ADD_THREADED(h1DYcellChargeSecondHitNorm_    	   [p]);

        ADD_THREADED(h2DClusterSize_                 	   [p]);

        ADD_THREADED(h2DCharge_              	     	   [p]);
        ADD_THREADED(h2DChargeNorm_          	     	   [p]);

        ADD_THREADED(h2DCellCharge_                  	   [p]);
        ADD_THREADED(h2DCellChargeNorm_              	   [p]);

        ADD_THREADED(h2DCellChargeOddCol_                  [p]);
        ADD_THREADED(h2DCellChargeOddColNorm_              [p]);
        ADD_THREADED(h2DCellChargeEvenCol_                 [p]);
        ADD_THREADED(h2DCellChargeEvenColNorm_             [p]);

        ADD_THREADED(h2DCellChargeOddRow_                  [p]);
        ADD_THREADED(h2DCellChargeOddRowNorm_              [p]);
        ADD_THREADED(h2DCellChargeEvenRow_                 [p]);
        ADD_THREADED(h2DCellChargeEvenRowNorm_             [p]);

        ADD_THREADED(h4CellsCharge_                  	   [p]);
        ADD_THREADED(h4CellsChargeNorm_              	   [p]);

        ADD_THREADED(h2DXcellCharge_                 	   [p]);
        ADD_THREADED(h2DXcellChargeSecondHit_        	   [p]);

        ADD_THREADED(h2DYcellCharge_                 	   [p]);
        ADD_THREADED(h2DYcellChargeSecondHit_        	   [p]);

        ADD_THREADED(h2DXcellChargeAsymmetry_        	   [p]);
        ADD_THREADED(h2DXcellChargeAsymmetryInv_     	   [p]);

        ADD_THREADED(h2DYcellChargeAsymmetry_        	   [p]);
        ADD_THREADED(h2DYcellChargeAsymmetryInv_     	   [p]);

        ADD_THREADED(h2DYcellChargeAsymmetryLeftBump_      [p]);
        ADD_THREADED(h2DYcellChargeAsymmetryInvLeftBump_   [p]);

        ADD_THREADED(h2DYcellChargeAsymmetryRightBump_     [p]);
        ADD_THREADED(h2DYcellChargeAsymmetryInvRightBump_  [p]);

        ADD_THREADED(h2DYcellChargeAsymmetryLeftNoBump_    [p]);
        ADD_THREADED(h2DYcellChargeAsymmetryInvLeftNoBump_ [p]);

        ADD_THREADED(h2DYcellChargeAsymmetryRightNoBump_   [p]);
        ADD_THREADED(h2DYcellChargeAsymmetryInvRightNoBump_[p]);

        STDLINE("Threading phase completed",ACGreen);

        STDLINE("Filling phase...",ACWhite);

        h1DXcellCharge_         [p]->Divide(h1DXcellChargeNorm_         [p]);
        h1DXcellChargeSecondHit_[p]->Divide(h1DXcellChargeSecondHitNorm_[p]);

        h1DYcellCharge_         [p]->Divide(h1DYcellChargeNorm_         [p]);
        h1DYcellChargeSecondHit_[p]->Divide(h1DYcellChargeSecondHitNorm_[p]);

        h2DCellCharge_          [p]->Divide(h2DCellChargeNorm_          [p]);
        h2DCharge_              [p]->Divide(h2DChargeNorm_              [p]);
        h2DCellChargeOddCol_    [p]->Divide(h2DCellChargeOddColNorm_    [p]);
        h2DCellChargeEvenCol_   [p]->Divide(h2DCellChargeEvenColNorm_   [p]);
        h2DCellChargeOddRow_    [p]->Divide(h2DCellChargeOddRowNorm_    [p]);
        h2DCellChargeEvenRow_   [p]->Divide(h2DCellChargeEvenRowNorm_   [p]);
        h2DClusterSize_         [p]->Divide(h2DCellChargeNorm_          [p]);
        h4CellsCharge_          [p]->Divide(h4CellsChargeNorm_          [p]);

        // ######################
        // # Setting error bars #
        // ######################
        setErrorsBar(p);

        STDLINE("Setting styles...",ACWhite);

        float xPitch = atof(((theXmlParser_->getPlanes())[planeName]->getCellPitches().first).c_str());
        float yPitch = atof(((theXmlParser_->getPlanes())[planeName]->getCellPitches().second).c_str());

        h1DXcellChargeAsymmetry_              [p]->SetMinimum(-1);
        h1DXcellChargeAsymmetry_              [p]->SetMaximum( 1);
        h1DXcellChargeAsymmetry_              [p]->SetMarkerStyle(20);
        h1DXcellChargeAsymmetry_              [p]->SetMarkerSize(0.6);

        h1DXcellChargeAsymmetryInv_           [p]->SetMinimum(-xPitch/2);
        h1DXcellChargeAsymmetryInv_           [p]->SetMaximum(xPitch/2);
        h1DXcellChargeAsymmetryInv_           [p]->SetMarkerStyle(20);
        h1DXcellChargeAsymmetryInv_           [p]->SetMarkerSize(0.6);

        h1DYcellChargeAsymmetry_              [p]->SetMinimum(-1);
        h1DYcellChargeAsymmetry_              [p]->SetMaximum( 1);
        h1DYcellChargeAsymmetry_              [p]->SetMarkerStyle(20);
        h1DYcellChargeAsymmetry_              [p]->SetMarkerSize(0.6);

        h1DYcellChargeAsymmetryInv_           [p]->SetMinimum(-yPitch/2);
        h1DYcellChargeAsymmetryInv_           [p]->SetMaximum(yPitch/2);
        h1DYcellChargeAsymmetryInv_           [p]->SetMarkerStyle(20);
        h1DYcellChargeAsymmetryInv_           [p]->SetMarkerSize(0.6);

        h1DYcellChargeAsymmetryLeftBump_      [p]->SetMinimum(-1);
        h1DYcellChargeAsymmetryLeftBump_      [p]->SetMaximum( 1);
        h1DYcellChargeAsymmetryLeftBump_      [p]->SetMarkerStyle(20);
        h1DYcellChargeAsymmetryLeftBump_      [p]->SetMarkerSize(0.6);

        h1DYcellChargeAsymmetryInvLeftBump_   [p]->SetMinimum(-yPitch/2);
        h1DYcellChargeAsymmetryInvLeftBump_   [p]->SetMaximum(yPitch/2);
        h1DYcellChargeAsymmetryInvLeftBump_   [p]->SetMarkerStyle(20);
        h1DYcellChargeAsymmetryInvLeftBump_   [p]->SetMarkerSize(0.6);

        h1DYcellChargeAsymmetryRightBump_     [p]->SetMinimum(-1);
        h1DYcellChargeAsymmetryRightBump_     [p]->SetMaximum( 1);
        h1DYcellChargeAsymmetryRightBump_     [p]->SetMarkerStyle(20);
        h1DYcellChargeAsymmetryRightBump_     [p]->SetMarkerSize(0.6);

        h1DYcellChargeAsymmetryInvRightBump_  [p]->SetMinimum(-yPitch/2);
        h1DYcellChargeAsymmetryInvRightBump_  [p]->SetMaximum(yPitch/2);
        h1DYcellChargeAsymmetryInvRightBump_  [p]->SetMarkerStyle(20);
        h1DYcellChargeAsymmetryInvRightBump_  [p]->SetMarkerSize(0.6);

        h1DYcellChargeAsymmetryLeftNoBump_    [p]->SetMinimum(-1);
        h1DYcellChargeAsymmetryLeftNoBump_    [p]->SetMaximum( 1);
        h1DYcellChargeAsymmetryLeftNoBump_    [p]->SetMarkerStyle(20);
        h1DYcellChargeAsymmetryLeftNoBump_    [p]->SetMarkerSize(0.6);

        h1DYcellChargeAsymmetryInvLeftNoBump_ [p]->SetMinimum(-yPitch/2);
        h1DYcellChargeAsymmetryInvLeftNoBump_ [p]->SetMaximum(yPitch/2);
        h1DYcellChargeAsymmetryInvLeftNoBump_ [p]->SetMarkerStyle(20);
        h1DYcellChargeAsymmetryInvLeftNoBump_ [p]->SetMarkerSize(0.6);

        h1DYcellChargeAsymmetryRightNoBump_   [p]->SetMinimum(-1);
        h1DYcellChargeAsymmetryRightNoBump_   [p]->SetMaximum( 1);
        h1DYcellChargeAsymmetryRightNoBump_   [p]->SetMarkerStyle(20);
        h1DYcellChargeAsymmetryRightNoBump_   [p]->SetMarkerSize(0.6);

        h1DYcellChargeAsymmetryInvRightNoBump_[p]->SetMinimum(-yPitch/2);
        h1DYcellChargeAsymmetryInvRightNoBump_[p]->SetMaximum(yPitch/2);
        h1DYcellChargeAsymmetryInvRightNoBump_[p]->SetMarkerStyle(20);
        h1DYcellChargeAsymmetryInvRightNoBump_[p]->SetMarkerSize(0.6);

        h1DXcellCharge_                       [p]->SetMarkerStyle(20);
        h1DXcellCharge_                       [p]->SetMarkerSize(0.6);

        h1DYcellCharge_                       [p]->SetMarkerStyle(20);
        h1DYcellCharge_                       [p]->SetMarkerSize(0.6);

        h1DXcellChargeSecondHit_              [p]->SetMarkerStyle(20);
        h1DXcellChargeSecondHit_              [p]->SetMarkerSize(0.6);

        h1DYcellChargeSecondHit_              [p]->SetMarkerStyle(20);
        h1DYcellChargeSecondHit_              [p]->SetMarkerSize(0.6);

        hClusterSize_                         [p]->GetXaxis()->SetTitle("cluster size"      );
        hCellLandau_                          [p]->GetXaxis()->SetTitle("charge (electrons)");
        hCellLandauOddCol_                    [p]->GetXaxis()->SetTitle("charge (electrons)");
        hCellLandauOddColLeft_                [p]->GetXaxis()->SetTitle("charge (electrons)");
        hCellLandauOddColRight_               [p]->GetXaxis()->SetTitle("charge (electrons)");
        hCellLandauEvenCol_                   [p]->GetXaxis()->SetTitle("charge (electrons)");
        hCellLandauEvenColLeft_               [p]->GetXaxis()->SetTitle("charge (electrons)");
        hCellLandauEvenColRight_              [p]->GetXaxis()->SetTitle("charge (electrons)");
        hCellLandauOddRow_                    [p]->GetXaxis()->SetTitle("charge (electrons)");
        hCellLandauOddRowLeft_                [p]->GetXaxis()->SetTitle("charge (electrons)");
        hCellLandauOddRowRight_               [p]->GetXaxis()->SetTitle("charge (electrons)");
        hCellLandauEvenRow_                   [p]->GetXaxis()->SetTitle("charge (electrons)");
        hCellLandauEvenRowLeft_               [p]->GetXaxis()->SetTitle("charge (electrons)");
        hCellLandauEvenRowRight_              [p]->GetXaxis()->SetTitle("charge (electrons)");

        hLandauClusterSize1_                  [p]->GetXaxis()->SetTitle("charge (electrons)");
        hLandauClusterSize2_                  [p]->GetXaxis()->SetTitle("charge (electrons)");
        hLandauClusterSize2pointed_           [p]->GetXaxis()->SetTitle("charge (electrons)");
        hLandauClusterSize2sameRow_           [p]->GetXaxis()->SetTitle("charge (electrons)");
        hLandauClusterSize2sameCol_           [p]->GetXaxis()->SetTitle("charge (electrons)");
        hCellLandauSinglePixel_               [p]->GetXaxis()->SetTitle("charge (electrons)");

        h2DClusterSize_                       [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DClusterSize_                       [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DCharge_                            [p]->GetXaxis()->SetTitle("column");
        h2DCharge_                            [p]->GetYaxis()->SetTitle("row"	);

        h2DChargeNorm_                        [p]->GetXaxis()->SetTitle("column");
        h2DChargeNorm_                        [p]->GetYaxis()->SetTitle("row"	);

        h2DCellCharge_                        [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DCellCharge_                        [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DCellChargeNorm_                    [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DCellChargeNorm_                    [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DCellChargeOddCol_                  [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DCellChargeOddCol_                  [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DCellChargeOddColNorm_              [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DCellChargeOddColNorm_              [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DCellChargeEvenCol_                 [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DCellChargeEvenCol_                 [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DCellChargeEvenColNorm_             [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DCellChargeEvenColNorm_             [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DCellChargeOddRow_                  [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DCellChargeOddRow_                  [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DCellChargeOddRowNorm_              [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DCellChargeOddRowNorm_              [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DCellChargeEvenRow_                 [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DCellChargeEvenRow_                 [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DCellChargeEvenRowNorm_             [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DCellChargeEvenRowNorm_             [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h4CellsCharge_                        [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h4CellsCharge_                        [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h4CellsChargeNorm_                    [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h4CellsChargeNorm_                    [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h1DXcellCharge_                       [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h1DXcellCharge_                       [p]->GetYaxis()->SetTitle("charge (electrons)");
        h1DXcellChargeNorm_                   [p]->GetXaxis()->SetTitle("long pitch (um)"   );

        h1DXcellChargeSecondHit_              [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h1DXcellChargeSecondHit_              [p]->GetYaxis()->SetTitle("charge (electrons)");
        h1DXcellChargeSecondHitNorm_          [p]->GetYaxis()->SetTitle("charge (electrons)");

        h2DXcellCharge_                       [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DXcellCharge_                       [p]->GetYaxis()->SetTitle("charge (electrons)");

        h2DXcellChargeSecondHit_              [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DXcellChargeSecondHit_              [p]->GetYaxis()->SetTitle("charge (electrons)");

        h1DYcellCharge_                       [p]->GetXaxis()->SetTitle("short pitch (um)"  );
        h1DYcellCharge_                       [p]->GetYaxis()->SetTitle("charge (electrons)");
        h1DYcellChargeNorm_                   [p]->GetXaxis()->SetTitle("short pitch (um)"  );

        h1DYcellChargeSecondHit_              [p]->GetXaxis()->SetTitle("short pitch (um)"   );
        h1DYcellChargeSecondHit_              [p]->GetYaxis()->SetTitle("charge (electrons)");
        h1DYcellChargeSecondHitNorm_          [p]->GetYaxis()->SetTitle("charge (electrons)");

        h2DYcellCharge_                       [p]->GetXaxis()->SetTitle("short pitch (um)"  );
        h2DYcellCharge_                       [p]->GetYaxis()->SetTitle("charge (electrons)");

        h2DYcellChargeSecondHit_              [p]->GetXaxis()->SetTitle("short pitch (um)"  );
        h2DYcellChargeSecondHit_              [p]->GetYaxis()->SetTitle("charge (electrons)");

        h2DXcellChargeAsymmetry_              [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h2DXcellChargeAsymmetry_              [p]->GetYaxis()->SetTitle("Asymmetry"	    );

        h1DXcellChargeAsymmetry_              [p]->GetXaxis()->SetTitle("long pitch (um)"   );
        h1DXcellChargeAsymmetry_              [p]->GetYaxis()->SetTitle("Asymmetry"	    );

        h2DXcellChargeAsymmetryInv_           [p]->GetXaxis()->SetTitle("Asymmetry"	    );
        h2DXcellChargeAsymmetryInv_           [p]->GetYaxis()->SetTitle("long pitch (um)"   );

        h1DXcellChargeAsymmetryInv_           [p]->GetXaxis()->SetTitle("Asymmetry"	    );
        h1DXcellChargeAsymmetryInv_           [p]->GetYaxis()->SetTitle("long pitch (um)"   );

        h2DYcellChargeAsymmetry_              [p]->GetXaxis()->SetTitle("shot pitch (um)"   );
        h2DYcellChargeAsymmetry_              [p]->GetYaxis()->SetTitle("Asymmetry"	    );

        h1DYcellChargeAsymmetry_              [p]->GetXaxis()->SetTitle("short pitch (um)"  );
        h1DYcellChargeAsymmetry_              [p]->GetYaxis()->SetTitle("Asymmetry"	    );

        h2DYcellChargeAsymmetryInv_           [p]->GetXaxis()->SetTitle("Asymmetry"	    );
        h2DYcellChargeAsymmetryInv_           [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h1DYcellChargeAsymmetryInv_           [p]->GetXaxis()->SetTitle("Asymmetry"	    );
        h1DYcellChargeAsymmetryInv_           [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DYcellChargeAsymmetryLeftBump_      [p]->GetXaxis()->SetTitle("shot pitch (um)"   );
        h2DYcellChargeAsymmetryLeftBump_      [p]->GetYaxis()->SetTitle("Asymmetry"	    );

        h1DYcellChargeAsymmetryLeftBump_      [p]->GetXaxis()->SetTitle("short pitch (um)"  );
        h1DYcellChargeAsymmetryLeftBump_      [p]->GetYaxis()->SetTitle("Asymmetry"	    );

        h2DYcellChargeAsymmetryInvLeftBump_   [p]->GetXaxis()->SetTitle("Asymmetry"	    );
        h2DYcellChargeAsymmetryInvLeftBump_   [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h1DYcellChargeAsymmetryInvLeftBump_   [p]->GetXaxis()->SetTitle("Asymmetry"	    );
        h1DYcellChargeAsymmetryInvLeftBump_   [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DYcellChargeAsymmetryRightBump_     [p]->GetXaxis()->SetTitle("shot pitch (um)"   );
        h2DYcellChargeAsymmetryRightBump_     [p]->GetYaxis()->SetTitle("Asymmetry"	    );

        h1DYcellChargeAsymmetryRightBump_     [p]->GetXaxis()->SetTitle("short pitch (um)"  );
        h1DYcellChargeAsymmetryRightBump_     [p]->GetYaxis()->SetTitle("Asymmetry"	    );

        h2DYcellChargeAsymmetryInvRightBump_  [p]->GetXaxis()->SetTitle("Asymmetry"	    );
        h2DYcellChargeAsymmetryInvRightBump_  [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h1DYcellChargeAsymmetryInvRightBump_  [p]->GetXaxis()->SetTitle("Asymmetry"	    );
        h1DYcellChargeAsymmetryInvRightBump_  [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DYcellChargeAsymmetryLeftNoBump_    [p]->GetXaxis()->SetTitle("shot pitch (um)"   );
        h2DYcellChargeAsymmetryLeftNoBump_    [p]->GetYaxis()->SetTitle("Asymmetry"         );

        h1DYcellChargeAsymmetryLeftNoBump_    [p]->GetXaxis()->SetTitle("short pitch (um)"  );
        h1DYcellChargeAsymmetryLeftNoBump_    [p]->GetYaxis()->SetTitle("Asymmetry"         );

        h2DYcellChargeAsymmetryInvLeftNoBump_ [p]->GetXaxis()->SetTitle("Asymmetry"         );
        h2DYcellChargeAsymmetryInvLeftNoBump_ [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h1DYcellChargeAsymmetryInvLeftNoBump_ [p]->GetXaxis()->SetTitle("Asymmetry"         );
        h1DYcellChargeAsymmetryInvLeftNoBump_ [p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h2DYcellChargeAsymmetryRightNoBump_   [p]->GetXaxis()->SetTitle("shot pitch (um)"   );
        h2DYcellChargeAsymmetryRightNoBump_   [p]->GetYaxis()->SetTitle("Asymmetry"         );

        h1DYcellChargeAsymmetryRightNoBump_   [p]->GetXaxis()->SetTitle("short pitch (um)"  );
        h1DYcellChargeAsymmetryRightNoBump_   [p]->GetYaxis()->SetTitle("Asymmetry"         );

        h2DYcellChargeAsymmetryInvRightNoBump_[p]->GetXaxis()->SetTitle("Asymmetry"         );
        h2DYcellChargeAsymmetryInvRightNoBump_[p]->GetYaxis()->SetTitle("short pitch (um)"  );

        h1DYcellChargeAsymmetryInvRightNoBump_[p]->GetXaxis()->SetTitle("Asymmetry"         );
        h1DYcellChargeAsymmetryInvRightNoBump_[p]->GetYaxis()->SetTitle("short pitch (um)"  );

        STDLINE("Fitting phase",ACWhite);
        gStyle->SetOptFit(1111);

        //STDLINE("fXAsymmetryFit",ACWhite);
        //TF1* fXAsymmetryFit = new TF1("fXAsymmetryFit","pol1",-ETAhalfRANGE,ETAhalfRANGE);
        //if (h1DXcellChargeAsymmetryInv_[p]->GetEntries() != 0) h1DXcellChargeAsymmetryInv_[p]->Fit(fXAsymmetryFit,"R");

        //STDLINE("fYAsymmetryFit",ACWhite);
        //TF1* fYAsymmetryFit  = new TF1("fYAsymmetryFit","pol1",-ETAhalfRANGE,ETAhalfRANGE);
        //if (h1DYcellChargeAsymmetryInv_           [p]->GetEntries() != 0) h1DYcellChargeAsymmetryInv_           [p]->Fit(fYAsymmetryFit,"R");
        //if (h1DYcellChargeAsymmetryInvLeftBump_   [p]->GetEntries() != 0) h1DYcellChargeAsymmetryInvLeftBump_ 	[p]->Fit(fYAsymmetryFit,"R");
        //if (h1DYcellChargeAsymmetryInvRightBump_  [p]->GetEntries() != 0) h1DYcellChargeAsymmetryInvRightBump_	[p]->Fit(fYAsymmetryFit,"R");
        //if (h1DYcellChargeAsymmetryInvLeftNoBump_ [p]->GetEntries() != 0) h1DYcellChargeAsymmetryInvLeftNoBump_ [p]->Fit(fYAsymmetryFit,"R");
        //if (h1DYcellChargeAsymmetryInvRightNoBump_[p]->GetEntries() != 0) h1DYcellChargeAsymmetryInvRightNoBump_[p]->Fit(fYAsymmetryFit,"R");

        if((theXmlParser_->getAnalysesFromString("Charge")->doFits()) && (thePlaneMapping_->getPlaneName(p).find("Dut") != std::string::npos))
        {
                ss.str(""); ss << "Fitting Landau distributions for plane " << planeName << " ... ";
                STDLINE(ss.str(),ACWhite);
                fitCharge(p);
        }
    }
}

//=======================================================================
void ChargeUniMiB::book(void)
{
    destroy();

    std::string hName;
    std::string hTitle;
    std::string planeName;
    std::stringstream ss;

    int nBinsX;
    int nBinsY;

    float xPitch;
    float yPitch;
    float binSize = 1.25; // [um]

    int lowerCol;
    int higherCol;
    int lowerRow;
    int higherRow;

    int nBinsCharge = 500;
    int nBinsCell   = 100;

    theAnalysisManager_->cd("/");
    theAnalysisManager_->mkdir("Charge");

    for (unsigned int p = 0; p < thePlaneMapping_->getNumberOfPlanes(); p++)
    {
        planeName = thePlaneMapping_->getPlaneName(p);
        theAnalysisManager_->cd("Charge");
        theAnalysisManager_->mkdir(planeName);

        xPitch    = atof(((theXmlParser_->getPlanes())[planeName]->getCellPitches().first).c_str());
        yPitch    = atof(((theXmlParser_->getPlanes())[planeName]->getCellPitches().second).c_str());

        lowerCol  = atoi(((theXmlParser_->getPlanes())[planeName]->getWindow()->getLowerCol ()).c_str());
        higherCol = atoi(((theXmlParser_->getPlanes())[planeName]->getWindow()->getHigherCol()).c_str());
        lowerRow  = atoi(((theXmlParser_->getPlanes())[planeName]->getWindow()->getLowerRow ()).c_str());
        higherRow = atoi(((theXmlParser_->getPlanes())[planeName]->getWindow()->getHigherRow()).c_str());

        nBinsX    = abs(lowerCol - higherCol) + 1;
        nBinsY    = abs(lowerRow - higherRow) + 1;

        if (nBinsY <= 0) nBinsY = 1; // Planes which are not in the geometry file have lowerRow = higherRow = 0,
        // this produces an unexpected warning

        theAnalysisManager_->mkdir("ClusterSize");

        // #################
        // # 1D histograms #
        // #################
        hName  = "hClusterSize_"              + planeName;
        hTitle = "Cluster size distribution " + planeName;
        hClusterSize_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 10, 0, 10)));

        theAnalysisManager_->cd("Charge/" + planeName);
        theAnalysisManager_->mkdir("Landau");

        // #################
        // # 1D histograms #
        // #################
        hName  = "hCellLandau_"                                                                   + planeName;
        hTitle = "Charge distribution for single hits in a fiducial window "                      + planeName;
        //hCellLandau_               .push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 16, 0, 16)));
        hCellLandau_               .push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hCellLandauOddCol_"                                                                + planeName;
        hTitle = "Charge distribution for single hits in a fiducial window - odd columns "        + planeName;
        hCellLandauOddCol_            .push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hCellLandauOddColLeft_"                                                            + planeName;
        hTitle = "Charge distribution for single hits in a fiducial window - odd columns - left "   + planeName;
        hCellLandauOddColLeft_        .push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hCellLandauOddColRight_"                                                           + planeName;
        hTitle = "Charge distribution for single hits in a fiducial window - odd columns - right "  + planeName;
        hCellLandauOddColRight_       .push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hCellLandauEvenCol_"                                                               + planeName;
        hTitle = "Charge distribution for single hits in a fiducial window - even columns "       + planeName;
        hCellLandauEvenCol_           .push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hCellLandauEvenColLeft_"                                                           + planeName;
        hTitle = "Charge distribution for single hits in a fiducial window - even columns - left "  + planeName;
        hCellLandauEvenColLeft_       .push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hCellLandauEvenColRight_"                                                          + planeName;
        hTitle = "Charge distribution for single hits in a fiducial window - even columns - right " + planeName;
        hCellLandauEvenColRight_      .push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hCellLandauOddRow_"                                                                + planeName;
        hTitle = "Charge distribution for single hits in a fiducial window - odd rows "        + planeName;
        hCellLandauOddRow_            .push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hCellLandauOddRowLeft_"                                                            + planeName;
        hTitle = "Charge distribution for single hits in a fiducial window - odd rows - left "   + planeName;
        hCellLandauOddRowLeft_        .push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hCellLandauOddRowRight_"                                                           + planeName;
        hTitle = "Charge distribution for single hits in a fiducial window - odd rows - right "  + planeName;
        hCellLandauOddRowRight_       .push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hCellLandauEvenRow_"                                                               + planeName;
        hTitle = "Charge distribution for single hits in a fiducial window - even rows "       + planeName;
        hCellLandauEvenRow_           .push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hCellLandauEvenRowLeft_"                                                           + planeName;
        hTitle = "Charge distribution for single hits in a fiducial window - even rows - left "  + planeName;
        hCellLandauEvenRowLeft_       .push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hCellLandauEvenRowRight_"                                                          + planeName;
        hTitle = "Charge distribution for single hits in a fiducial window - even eows - right " + planeName;
        hCellLandauEvenRowRight_      .push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hLandauClusterSize1_"                                                           + planeName;
        hTitle = "Charge distribution for clusters of size 1 "                                    + planeName;
        hLandauClusterSize1_       .push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hLandauClusterSize2_"                                                           + planeName;
        hTitle = "Charge distribution for clusters of size 2 "                                    + planeName;
        hLandauClusterSize2_       .push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hLandauClusterSize2pointed_"                                                    + planeName;
        hTitle = "Charge distribution for clusters of size 2 pointed by a track "                 + planeName;
        hLandauClusterSize2pointed_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hLandauClusterSize2sameRow_"                                                    + planeName;
        hTitle = "Charge distribution for clusters of size 2 on same row "                        + planeName;
        hLandauClusterSize2sameRow_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hLandauClusterSize2sameCol_"                                                    + planeName;
        hTitle = "Charge distribution for clusters of size 2 on same col "                        + planeName;
        hLandauClusterSize2sameCol_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        hName  = "hCellLandauSinglePixel_"                                                        + planeName;
        hTitle = "Charge distribution of the pointed pixel in the cluster "                       + planeName;
        hCellLandauSinglePixel_    .push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsCharge, 0, 50000)));

        theAnalysisManager_->cd("Charge/" + planeName);
        theAnalysisManager_->mkdir("2DCharge");

        // #################
        // # 2D histograms #
        // #################
        hName  = "h2DCharge_"               + planeName;
        hTitle = "2D charge distribution "  + planeName;
        h2DCharge_    .push_back(NEW_THREADED(TH2F(hName.c_str(),hTitle.c_str(),nBinsX,lowerCol,higherCol + 1,nBinsY,lowerRow,higherRow + 1)));

        hName  = "h2DChargeNorm_"           + planeName;
        hTitle = "2D charge normalization " + planeName;
        h2DChargeNorm_.push_back(NEW_THREADED(TH2F(hName.c_str(),hTitle.c_str(),nBinsX,lowerCol,higherCol + 1,nBinsY,lowerRow,higherRow + 1)));

        theAnalysisManager_->cd("Charge/" + planeName);
        theAnalysisManager_->mkdir("2DCellCharge");

        // #################
        // # 2D histograms #
        // #################
        hName  = "h2DCellCharge_"                              + planeName;
        hTitle = "Cell charge 2D distribution "                + planeName;
        h2DCellCharge_        .push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h2DCellChargeNorm_"                          + planeName;
        hTitle = "Cell charge normalization "                  + planeName;
        h2DCellChargeNorm_    .push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h2DCellChargeOddCol_"                           + planeName;
        hTitle = "Cell charge 2D distribution - odd columns "  + planeName;
        h2DCellChargeOddCol_     .push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h2DCellChargeOddColNorm_"                       + planeName;
        hTitle = "Cell charge normalization - odd columns "    + planeName;
        h2DCellChargeOddColNorm_ .push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h2DCellChargeEvenCol_"                          + planeName;
        hTitle = "Cell charge 2D distribution - even columns " + planeName;
        h2DCellChargeEvenCol_    .push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h2DCellChargeEvenColNorm_"                      + planeName;
        hTitle = "Cell charge normalization - even columns "   + planeName;
        h2DCellChargeEvenColNorm_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h2DCellChargeOddRows_"                           + planeName;
        hTitle = "Cell charge 2D distribution - odd rows "  + planeName;
        h2DCellChargeOddRow_     .push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h2DCellChargeOddRowsNorm_"                       + planeName;
        hTitle = "Cell charge normalization - odd rows "    + planeName;
        h2DCellChargeOddRowNorm_ .push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h2DCellChargeEvenRows_"                          + planeName;
        hTitle = "Cell charge 2D distribution - even rows " + planeName;
        h2DCellChargeEvenRow_    .push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h2DCellChargeEvenRowsNorm_"                      + planeName;
        hTitle = "Cell charge normalization - even rows "   + planeName;
        h2DCellChargeEvenRowNorm_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h4CellsCharge_"                              + planeName;
        hTitle = "4 cells charge 2D distribution "             + planeName;
        h4CellsCharge_        .push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h4CellsChargeNorm_"                          + planeName;
        hTitle = "4 cells charge normalization "               + planeName;
        h4CellsChargeNorm_    .push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        hName  = "h2DClusterSize_"                             + planeName;
        hTitle = "Cluster size distribution  "                 + planeName;
        h2DClusterSize_       .push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/binSize, -(xPitch/2), xPitch/2, (int)yPitch/binSize, -(yPitch/2), yPitch/2)));

        theAnalysisManager_->cd("Charge/" + planeName);
        theAnalysisManager_->mkdir("XcellCharge2D");

        // #################
        // # 2D histograms #
        // #################
        hName  = "h2DXcellCharge_"                         + planeName;
        hTitle = "Predicted cell charge vs. X coordinate " + planeName;
        h2DXcellCharge_         .push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/2, -(xPitch/2), xPitch/2, nBinsCharge, 0, 50000)));

        hName  = "h2DXcellChargeSecondHit_"                + planeName;
        hTitle = "Predicted cell charge vs. X coordinate " + planeName;
        h2DXcellChargeSecondHit_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch/2, -(xPitch/2), xPitch/2, nBinsCharge, 0, 50000)));

        theAnalysisManager_->cd("Charge/" + planeName);
        theAnalysisManager_->mkdir("YcellCharge2D");

        // #################
        // # 2D histograms #
        // #################
        hName  = "h2DYcellCharge_"                         + planeName;
        hTitle = "Predicted cell charge vs. Y coordinate " + planeName;
        h2DYcellCharge_         .push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)yPitch/2, -(yPitch/2), yPitch/2, nBinsCharge, 0, 50000)));

        hName  = "h2DYcellChargeSecondHit_"                + planeName;
        hTitle = "Predicted cell charge vs. Y coordinate " + planeName;
        h2DYcellChargeSecondHit_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)yPitch/2, -(yPitch/2), yPitch/2, nBinsCharge, 0, 50000)));

        theAnalysisManager_->cd("Charge/" + planeName);
        theAnalysisManager_->mkdir("XcellCharge1D");

        // #################
        // # 1D histograms #
        // #################
        hName  = "h1DXcellCharge_"                                                                       + planeName;
        hTitle = "Predicted cell charge - X coordinate (normalized to hits) "                            + planeName;
        h1DXcellCharge_             .push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), (int)xPitch/2,-(xPitch/2),xPitch/2)));

        hName  = "h1DXcellChargeNorm_"                                                                   + planeName;
        hTitle = "Predicted cell charge - X coordinate - all hits normalization "                        + planeName;
        h1DXcellChargeNorm_         .push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), (int)xPitch/2,-(xPitch/2),xPitch/2)));

        hName  = "h1DXcellChargeSecondHit_"                                                              + planeName;
        hTitle = "Up to 2 adjacent hits total charge, second hit - X coordinate (normalized to tracks) " + planeName;
        h1DXcellChargeSecondHit_    .push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), (int)xPitch/2,-(xPitch/2),xPitch/2)));

        hName  = "h1DXcellChargeSecondHitNorm_"                                                          + planeName;
        hTitle = "Up to 2 adjacent hits total charge, second hit distribution - X coordinate "           + planeName;
        h1DXcellChargeSecondHitNorm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), (int)xPitch/2,-(xPitch/2),xPitch/2)));

        theAnalysisManager_->cd("Charge/" + planeName);
        theAnalysisManager_->mkdir("YcellCharge1D");

        // #################
        // # 1D histograms #
        // #################
        hName  = "h1DYcellCharge_"                                                                       + planeName;
        hTitle = "Predicted cell charge - Y coordinate (normalized to hits) "                            + planeName;
        h1DYcellCharge_             .push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), (int)yPitch/2,-(yPitch/2),yPitch/2)));

        hName  = "h1DYcellChargeNorm_"                                                                   + planeName;
        hTitle = "Predicted cell charge - Y coordinate - all hits normalization "                        + planeName;
        h1DYcellChargeNorm_         .push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), (int)yPitch/2,-(yPitch/2),yPitch/2)));

        hName  = "h1DYcellChargeSecondHit_"                                                              + planeName;
        hTitle = "Up to 2 adjacent hits total charge, second hit - Y coordinate (normalized to tracks) " + planeName;
        h1DYcellChargeSecondHit_    .push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), (int)yPitch/2,-(yPitch/2),yPitch/2)));

        hName  = "h1DYcellChargeSecondHitNorm_"                                                          + planeName;
        hTitle = "Up to 2 adjacent hits total charge, second hit distribution - X coordinate "           + planeName;
        h1DYcellChargeSecondHitNorm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), (int)yPitch/2,-(yPitch/2),yPitch/2)));


        theAnalysisManager_->cd("Charge/" + planeName);
        theAnalysisManager_->mkdir("XAsymmetry");

        // #################
        // # 1D histograms #
        // #################
        hName  = "h2DXcellChargeAsymmetry_"             + planeName;
        hTitle = "L/R charge asymmetry - X coordinate " + planeName;
        h2DXcellChargeAsymmetry_   .push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch, -(xPitch/2), xPitch/2, nBinsCell, -1.1, 1.1)));

        hName  = "h2DXcellChargeAsymmetryInv_"          + planeName;
        hTitle = "L/R charge asymmetry - X coordinate " + planeName;
        h2DXcellChargeAsymmetryInv_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), nBinsCell, -1.1, 1.1, (int)xPitch, -(xPitch/2), xPitch/2)));

        theAnalysisManager_->cd("Charge/" + planeName);
        theAnalysisManager_->mkdir("YAsymmetry");

        // #################
        // # 1D histograms #
        // #################
        hName  = "h2DYcellChargeAsymmetry_"                                                + planeName;
        hTitle = "L/R charge asymmetry - Y coordinate "                                    + planeName;
        h2DYcellChargeAsymmetry_              .push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)yPitch, -(yPitch/2), yPitch/2 , nBinsCell, -1.1, 1.1)));

        hName  = "h2DYcellChargeAsymmetryInv_"                                             + planeName;
        hTitle = "L/R charge asymmetry - Y coordinate "                                    + planeName;
        h2DYcellChargeAsymmetryInv_           .push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), nBinsCell, -1.1, 1.1, (int)yPitch, -(yPitch/2), yPitch/2)));

        hName  = "h2DYcellChargeAsymmetryLeftBump_"                                        + planeName;
        hTitle = "L/R charge asymmetry - Y coordinate - Left half of the cell - Bump "     + planeName;
        h2DYcellChargeAsymmetryLeftBump_      .push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)yPitch, -(yPitch/2), yPitch/2 , nBinsCell, -1.1, 1.1)));

        hName  = "h2DYcellChargeAsymmetryInvLeftBump_"                                     + planeName;
        hTitle = "L/R charge asymmetry - Y coordinate - Left half of the cell - Bump "     + planeName;
        h2DYcellChargeAsymmetryInvLeftBump_   .push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), nBinsCell, -1.1, 1.1, (int)yPitch, -(yPitch/2), yPitch/2)));

        hName  = "h2DYcellChargeAsymmetryRightBump_"                                       + planeName;
        hTitle = "L/R charge asymmetry - Y coordinate - Right half of the cell - Bump "    + planeName;
        h2DYcellChargeAsymmetryRightBump_     .push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)yPitch, -(yPitch/2), yPitch/2 , nBinsCell, -1.1, 1.1)));

        hName  = "h2DYcellChargeAsymmetryInvRightBump_"                                    + planeName;
        hTitle = "L/R charge asymmetry - Y coordinate - Right half of the cell - Bump "    + planeName;
        h2DYcellChargeAsymmetryInvRightBump_  .push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), nBinsCell, -1.1, 1.1, (int)yPitch, -(yPitch/2), yPitch/2)));

        hName  = "h2DYcellChargeAsymmetryLeftNoBump_"                                      + planeName;
        hTitle = "L/R charge asymmetry - Y coordinate - Left half of the cell - No Bump "  + planeName;
        h2DYcellChargeAsymmetryLeftNoBump_    .push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)yPitch, -(yPitch/2), yPitch/2 , nBinsCell, -1.1, 1.1)));

        hName  = "h2DYcellChargeAsymmetryInvLeftNoBump_"                                   + planeName;
        hTitle = "L/R charge asymmetry - Y coordinate - Left half of the cell - No Bump "  + planeName;
        h2DYcellChargeAsymmetryInvLeftNoBump_ .push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), nBinsCell, -1.1, 1.1, (int)yPitch, -(yPitch/2), yPitch/2)));

        hName  = "h2DYcellChargeAsymmetryRightNoBump_"                                     + planeName;
        hTitle = "L/R charge asymmetry - Y coordinate - Right half of the cell - No Bump " + planeName;
        h2DYcellChargeAsymmetryRightNoBump_   .push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)yPitch, -(yPitch/2), yPitch/2 , nBinsCell, -1.1, 1.1)));

        hName  = "h2DYcellChargeAsymmetryInvRightNoBump_"                                  + planeName;
        hTitle = "L/R charge asymmetry - Y coordinate - Right half of the cell - No Bump " + planeName;
        h2DYcellChargeAsymmetryInvRightNoBump_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), nBinsCell, -1.1, 1.1, (int)yPitch, -(yPitch/2), yPitch/2)));
    }
}
//=======================================================================
void ChargeUniMiB::fitCharge(int planeID)
{
    std::stringstream ss;
    double fitParameters[4];

    STDLINE("All single hits:",ACWhite);

    if(hLandauClusterSize1_[planeID]->GetEntries() != 0)
    {
        langausFit(hLandauClusterSize1_[planeID],fitParameters);

        ss << "Width: " << fitParameters[0]<< " MPV: " << fitParameters[1]<< " Area: " << fitParameters[2]<< " GSigma: " << fitParameters[3];
        STDLINE(ss.str(),ACPurple);
    }

    ss.str("");
    /*--------------------------------------------------------*/
    STDLINE("Single hits in the cell fiducial window:",ACWhite);

    if(hCellLandau_[planeID]->GetEntries() != 0)
    {
        langausFit(hCellLandau_[planeID],fitParameters);

        ss << "Width: " << fitParameters[0]<< " MPV: " << fitParameters[1]<< " Area: " << fitParameters[2]<< " GSigma: " << fitParameters[3];
        STDLINE(ss.str(),ACPurple);
    }

    ss.str("");
    /*--------------------------------------------------------*/
    STDLINE("Single pixel in the cell fiducial window", ACWhite);

    if(hCellLandauSinglePixel_[planeID]->GetEntries() != 0)
    {
        langausFit(hCellLandauSinglePixel_[planeID], fitParameters);

        ss << "Width: " << fitParameters[0]<< " MPV: " << fitParameters[1]<< " Area: " << fitParameters[2]<< " GSigma: " << fitParameters[3];
        STDLINE(ss.str(),ACPurple);
    }

    ss.str("");
    /*--------------------------------------------------------*/
    STDLINE("All double hits:",ACWhite);

    if(hLandauClusterSize2_[planeID]->GetEntries() != 0)
    {
        langausFit(hLandauClusterSize2_[planeID],fitParameters);

        ss << "Width: " << fitParameters[0]<< " MPV: " << fitParameters[1]<< " Area: " << fitParameters[2]<< " GSigma: " << fitParameters[3];
        STDLINE(ss.str(),ACPurple);
    }

    ss.str("");
    /*--------------------------------------------------------*/
    STDLINE("Double hits on the same row:",ACWhite);

    if(hLandauClusterSize2sameRow_[planeID]->GetEntries() != 0)
    {
        langausFit(hLandauClusterSize2sameRow_[planeID],fitParameters);

        ss << "Width: " << fitParameters[0]<< " MPV: " << fitParameters[1]<< " Area: " << fitParameters[2]<< " GSigma: " << fitParameters[3];
        STDLINE(ss.str(),ACPurple);
    }

    ss.str("");
    /*--------------------------------------------------------*/
    STDLINE("Double hits on the same column:",ACWhite);

    if(hLandauClusterSize2sameCol_[planeID]->GetEntries() != 0)
    {
        langausFit(hLandauClusterSize2sameCol_[planeID],fitParameters);

        ss << "Width: " << fitParameters[0]<< " MPV: " << fitParameters[1]<< " Area: " << fitParameters[2]<< " GSigma: " << fitParameters[3];
        STDLINE(ss.str(),ACPurple);
    }

    ss.str("");

    /*--------------------------------------------------------*/
    STDLINE("Double hits associated to a track:",ACWhite);

    if(hLandauClusterSize2pointed_[planeID]->GetEntries() != 0)
    {
        langausFit(hLandauClusterSize2pointed_[planeID],fitParameters);

        ss << "Width: " << fitParameters[0]<< " MPV: " << fitParameters[1]<< " Area: " << fitParameters[2]<< " GSigma: " << fitParameters[3];
        STDLINE(ss.str(),ACPurple);
    }
}
//=======================================================================
int ChargeUniMiB::langausFit(TH1F* histo, double *fitParameters)
{
    TAxis* xAxis           ;
    double range           ;
    double integral        ;
    double gausPar      [3];
    double landauPar    [3];
    double fitRange     [2];
    double startValues  [4];
    double parsLowLimit [4];
    double parsHighLimit[4];

    TF1* landau = new TF1("myLandau","landau",0,60000);
    TF1* gaus   = new TF1("myGaus"  ,"gaus"  ,0,60000);

    fitRange[0]=0.4*(histo->GetMean());
    fitRange[1]=1.8*(histo->GetMean());

    gaus->SetRange(fitRange[0],fitRange[1]);
    histo->Fit(gaus,"0QR");
    for(int p=0; p<3; p++)
        gausPar[p] = gaus->GetParameter(p);

    landau->SetRange(fitRange[0],fitRange[1]);
    histo->Fit(landau,"0QR");
    for(int p=0; p<3; p++)
        landauPar[p] = landau->GetParameter(p);

    xAxis    = histo->GetXaxis();
    range    = xAxis->GetXmax() - xAxis->GetXmin();
    integral = ((histo->Integral())*range)/(histo->GetNbinsX());

    startValues[0]=landauPar[2];
    startValues[1]=landauPar[1];
    startValues[2]=integral    ;
    startValues[3]=gausPar[2]  ;

    parsLowLimit [0] = startValues[0] - 0.68*startValues[0];
    parsHighLimit[0] = startValues[0] + 0.68*startValues[0];
    parsLowLimit [1] = startValues[1] - 0.68*startValues[1];
    parsHighLimit[1] = startValues[1] + 0.68*startValues[1];
    parsLowLimit [2] = startValues[2] - 0.68*startValues[2];
    parsHighLimit[2] = startValues[2] + 0.68*startValues[2];
    parsLowLimit [3] = startValues[3] - 0.68*startValues[3];
    parsHighLimit[3] = startValues[3] + 0.68*startValues[3];

    langaus_->SetRange(fitRange[0],fitRange[1]);
    langaus_->SetParameters(startValues);

    for (int p=0; p<4; p++) {
        langaus_->SetParLimits(p, parsLowLimit[p], parsHighLimit[p]);
    }

    TFitResultPtr r = histo->Fit(langaus_,"QRBL");
    int fitStatus = r;

    langaus_->GetParameters(fitParameters);

    return fitStatus;
}
