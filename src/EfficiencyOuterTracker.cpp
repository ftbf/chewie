/*===============================================================================
 * Chewie: the FERMILAB MTEST telescope and DUT anaysis tool
 *
 * Copyright (C) 2014
 *
 * Authors:
 *
 * Lorenzo Uplegger   (FNAL)
 * Nabin Poudyal      (Wayne State)
 * Michele Mormile    (INFN)
 *
 * INFN: Piazza della Scienza 3, Edificio U2, Milano, Italy 20126
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ================================================================================*/
#include "EfficiencyOuterTracker.h"
#include "AnalysisManager.h"
#include "WindowsManager.h"
#include "Window.h"
#include "ThreadUtilities.h"
#include "PlanesMapping.h"
#include "MessageTools.h"
#include "XmlParser.h"
#include "XmlAnalysis.h"
#include "XmlPlane.h"
#include "HistogramWindow.h"
#include "XmlWindow.h"
#include "XmlScan.h"
#include <TH1F.h>
#include <TH2F.h>
#include <TEfficiency.h>
#include <TFile.h>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
// @@@ Hard coded parameters @@@
#define ONLYdoubleHITS false // Process only clusters of size 2
//=======================================================================
EfficiencyOuterTracker::EfficiencyOuterTracker(AnalysisManager *analysisManager, int nOfThreads) : Analysis(analysisManager, nOfThreads),
    thePlaneMapping_(0),
    theWindowsManager_(0),
    theXmlParser_(analysisManager->getXmlParser())
{
    STDLINE("Running EfficiencyOuterTracker analysis", ACCyan);
    thePlaneMapping_ = new PlanesMapping();
}
//=======================================================================
EfficiencyOuterTracker::~EfficiencyOuterTracker(void)
{
    if (thePlaneMapping_)
        delete thePlaneMapping_;

    destroy();
}
//=======================================================================
void EfficiencyOuterTracker::destroy(void)
{
    if (Analysis::fDoNotDelete_)
        return;

    destroyHistograms(hEfficiency_                             );
    destroyHistograms(hEfficiencyOld_                          );
    destroyHistograms(hEfficiencyNorm_                         );

    destroyHistograms(hEfficiencyStrip_                        );
    destroyHistograms(hEfficiencyStripNorm_                    );

    destroyHistograms(hEfficiencyQuick_                          );
    destroyHistograms(hEfficiencyQuickNorm_                      );

    destroyHistograms(hEfficiencyCenterStrip_                  );
    destroyHistograms(hEfficiencyCenterStripNorm_              );

    destroyHistograms(hEfficiencyBetweenStrips_                );
    destroyHistograms(hEfficiencyBetweenStripsNorm_            );

    destroyHistograms(hEfficiencyBetweenStripsClusterSize1_    );
    destroyHistograms(hEfficiencyBetweenStripsClusterSize1Norm_);

    destroyHistograms(hEfficiencyBetweenStripsClusterSize2_    );
    destroyHistograms(hEfficiencyBetweenStripsClusterSize2Norm_);

    destroyHistograms(hEfficiencyStub_                         );
    destroyHistograms(hEfficiencyStubNorm_                     );

    destroyHistograms(hEfficiencyStubNoCuts_                   );
    destroyHistograms(hEfficiencyStubNoCutsNorm_               );

    destroyHistograms(hEfficiencyStubSingleChip_               );
    destroyHistograms(hEfficiencyStubSingleChipNorm_           );

    destroyHistograms(hEfficiencyStubSingleChipNoCuts_         );
    destroyHistograms(hEfficiencyStubSingleChipNoCutsNorm_     );

    destroyHistograms(h2DEfficiency_                           );
    destroyHistograms(h2DEfficiencyNorm_                       );

    destroyHistograms(h2DEfficiencyQuick_                        );
    destroyHistograms(h2DEfficiencyQuickNorm_                    );

    destroyHistograms(h2DInefficiencyQuick_                        );
    destroyHistograms(h2DInefficiencyQuickNorm_                    );

    destroyHistograms(h2DEfficiencyStub_                       );
    destroyHistograms(h2DEfficiencyStubNorm_                   );

    destroyHistograms(hEfficiencyStripStub_                    );
    destroyHistograms(hEfficiencyStripStubNorm_                );

    destroyHistograms(h2D4CellEfficiency_                      );
    destroyHistograms(h2D4CellEfficiencyNorm_                  );

    destroyHistograms(hCellEfficiency_                         );
    destroyHistograms(hCellEfficiencyNorm_                     );

    destroyHistograms(hCellEfficiencyEvenColumns_              );
    destroyHistograms(hCellEfficiencyEvenColumnsNorm_          );

    destroyHistograms(hCellEfficiencyOddColumns_               );
    destroyHistograms(hCellEfficiencyOddColumnsNorm_           );

    destroyHistograms(h1DXCellEfficiencyFirstHit_              );
    destroyHistograms(h1DXCellEfficiencySecondHit_             );
    destroyHistograms(h1DXCellEfficiencyNorm_                  );

    destroyHistograms(h1DYCellEfficiencyFirstHit_              );
    destroyHistograms(h1DYCellEfficiencySecondHit_             );
    destroyHistograms(h1DYCellEfficiencyNorm_                  );

    destroyHistograms(diffFirstSecondHist_                     );
    destroyHistograms(hitDiffTwoStrips_                        );
    destroyHistograms(h2DDeltaXvsPredictedCol_                 );

    destroyHistograms(h1DClusterSizes_                         );
    destroyHistograms(h1DClusterSizesCenterStrip_              );
    destroyHistograms(h1DClusterSizesBetweenStrips_            );

    destroyHistograms(h1DClusterSizeVsXStripPosition_          );
    destroyHistograms(h1DClusterSizeVsXStripPosition_norm_     );
    destroyHistograms(h1DClusterSize1VsXStripPosition_         );
    destroyHistograms(h1DClusterSize2VsXStripPosition_         );
    destroyHistograms(h1DClusterSize3VsXStripPosition_         );
    destroyHistograms(h1DClusterSize4VsXStripPosition_         );
    destroyHistograms(h1DClusterSize5VsXStripPosition_         );

    destroyHistograms(hAverageClustersPerEvent_                );
    destroyHistograms(TotalEvent_                              );
    destroyHistograms(CheckIneff_                              );

    destroyHistograms(Dut23Stub_                               );
    //nabin

    destroyHistograms(hChi2Distribution_                       );
    destroyHistograms(hNumberOfClusters_                       );
    destroyHistograms(hEfficiencyStubVsNumberOfClusters_       );
    destroyHistograms(hEfficiencyStubVsNumberOfClustersNorm_   );
    destroyHistograms(hClusterPositionLocal_                   );
    destroyHistograms(hEfficiencyStubVsClusterSizesNorm_       );
    destroyHistograms(hEfficiencyStubVsClusterSizes_           );

    destroyHistograms(hNumberOfHitsOnSensor_                   );

    destroyHistograms(hEfficiencyStubVsStripNumberNorm_        );
    destroyHistograms(hEfficiencyStubVsStripNumber_            );
    destroyHistograms(hEfficiencyStubVsDeviationFromPredNorm_  );
    destroyHistograms(hEfficiencyStubVsDeviationFromPred_      );
    destroyHistograms(hEfficiencyStubVsDeviationFromPredNorm1_ );
    destroyHistograms(hEfficiencyStubVsDeviationFromPred1_     );
    destroyHistograms(hEfficiencyStubVsDeviationFromPredNorm2_ );
    destroyHistograms(hEfficiencyStubVsDeviationFromPred2_     );
    destroyHistograms(hEfficiencyStubVsDeviationFromPredNorm3_ );
    destroyHistograms(hEfficiencyStubVsDeviationFromPred3_     );

}

//=======================================================================
void EfficiencyOuterTracker::beginJob(void)
{
    //std::cout << __PRETTY_FUNCTION__ << "Begin Job" << std::endl;
    theWindowsManager_ = theAnalysisManager_->getWindowsManager();

    if (theXmlParser_->getScan()->getScanValues().size() == 0)
        book();
    else
    {
        STDLINE("Unknown option: scan values", ACRed);
        exit(EXIT_FAILURE);
    }
    cumulative = 0;
    nevent = 0;
    //std::cout << __PRETTY_FUNCTION__ << "Done booking" << std::endl;
}

//=======================================================================
void EfficiencyOuterTracker::analyze(const Data &data, int threadNumber)
{
    //std::cout << __PRETTY_FUNCTION__ << "Analyze" << std::endl;
    bool passMainCut = true;
    if (cutsFormulas_.find("main cut") != cutsFormulas_.end())
        passMainCut = cutsFormulas_["main cut"][theAnalysisManager_->getCurrentTreeNumber(threadNumber)]->EvalInstance();

    if (!passMainCut)
        return;

    bool passCellEfficiencyCut = true;
    if (cutsFormulas_.find("cell efficiency") != cutsFormulas_.end())
        passCellEfficiencyCut = cutsFormulas_["cell efficiency"][theAnalysisManager_->getCurrentTreeNumber(threadNumber)]->EvalInstance();

    bool passXCellEfficiencyCut = true;
    if (cutsFormulas_.find("cell efficiency X") != cutsFormulas_.end())
        passXCellEfficiencyCut = cutsFormulas_["cell efficiency X"][theAnalysisManager_->getCurrentTreeNumber(threadNumber)]->EvalInstance();

    //bool passYCellEfficiencyCut = true;
    //if (cutsFormulas_.find("cell efficiency Y") != cutsFormulas_.end())
    //passYCellEfficiencyCut = cutsFormulas_["cell efficiency Y"][theAnalysisManager_->getCurrentTreeNumber(threadNumber)]->EvalInstance();
    // ######################################################
    // # Require all telescope planes with cluster size = 2 #
    // ######################################################
    if (ONLYdoubleHITS == false)
    {
        for (unsigned int p = 0; p < thePlaneMapping_->getNumberOfPlanes(); p++)
        {
            if ((thePlaneMapping_->getPlaneName(p).find("Dut") == std::string::npos) &&
                    (thePlaneMapping_->getPlaneName(p).find("Strip") == std::string::npos) &&
                    (data.getClusterSize(p) != 2))
                return;
        }
    }
    int entry = data.getEventChewieNumber();
    //std::cout<< "number of entry = "<< entry <<" , nStripHits = "<< data.getNumberOfStripHits() <<" , nTelescopeHits = "<< data.getNumberOfTelescopeHits() <<std::endl;
    int run = data.getRunNumber();
    for (unsigned int p = 0; p < thePlaneMapping_->getNumberOfPlanes(); p++)
    {
        if (!passStandardCuts(p, data))
            continue;
        if (!theWindowsManager_->getWindow(p)->checkRunEntry(run, entry))
            continue;
        planeEfficiency(passMainCut,            p, data, threadNumber);
        cellEfficiency (passCellEfficiencyCut,  p, data, threadNumber);
        xCellEfficiency(passXCellEfficiencyCut, p, data, threadNumber);
        //yCellEfficiency (passYCellEfficiencyCut,p,data,threadNumber);
        //xEdgeEfficiency (passXCellEfficiencyCut,p,data,threadNumber);
        //yEdgeEfficiency (passYCellEfficiencyCut,p,data,threadNumber);
        //THREADED(hAverageClustersPerEvent_[p]) = (TH1F*) THREADED(h1DClusterSizes_[p])->Clone();
    }

    //std::cout<<"cumulative = "<<cumulative<<std::endl;
    //std::cout<<"Number of event = "<<nevent<<std::endl;
    //for (unsigned int g = 0; g < thePlaneMapping_->getNumberOfPlanes(); g++){
    //THREADED(hAverageClustersPerEvent_                [g])->Fill(cumulative);
    //     THREADED(TotalEvent_                          [g])->Fill(nevent);
    // }
}

//=======================================================================
void EfficiencyOuterTracker::endJob(void)
{
    std::stringstream ss;
    std::stringstream ss0;
    std::stringstream ss1;
    std::stringstream ss2;
    std::stringstream ss3;
    std::stringstream ss4;
    std::stringstream ss5;

    double efficiency0;
    double Ntracks0;
    double error0;

    double efficiency1;
    double Ntracks1;
    double error1;

    double efficiency2;
    double Ntracks2;
    double error2;

    double efficiency3;
    double Ntracks3;
    double error3;

    for (unsigned int p = 0; p < thePlaneMapping_->getNumberOfPlanes(); p++)
    {
        std::string planeName = thePlaneMapping_->getPlaneName(p);
        ss.str("");
        ss << "Adding threads for plane " << p;
        STDLINE(ss.str().c_str(), ACYellow);


        ADD_THREADED_EFFICIENCY(hEfficiency_[p]);
        ADD_THREADED(hEfficiencyOld_[p]);
        ADD_THREADED(hEfficiencyNorm_[p]);

        ADD_THREADED_EFFICIENCY(hEfficiencyStrip_[p]);
        ADD_THREADED(hEfficiencyStripNorm_[p]);

        ADD_THREADED_EFFICIENCY(hEfficiencyQuick_[p]);
        ADD_THREADED(hEfficiencyQuickNorm_[p]);

        ADD_THREADED_EFFICIENCY(hEfficiencyCenterStrip_[p]);
        ADD_THREADED(hEfficiencyCenterStripNorm_[p]);

        ADD_THREADED_EFFICIENCY(hEfficiencyBetweenStrips_[p]);
        ADD_THREADED(hEfficiencyBetweenStripsNorm_[p]);

        ADD_THREADED_EFFICIENCY(hEfficiencyBetweenStripsClusterSize1_[p]);
        ADD_THREADED(hEfficiencyBetweenStripsClusterSize1Norm_[p]);

        ADD_THREADED_EFFICIENCY(hEfficiencyBetweenStripsClusterSize2_[p]);
        ADD_THREADED(hEfficiencyBetweenStripsClusterSize2Norm_[p]);

        ADD_THREADED_EFFICIENCY(hEfficiencyStub_[p]);
        ADD_THREADED(hEfficiencyStubNorm_[p]);

        ADD_THREADED_EFFICIENCY(hEfficiencyStubNoCuts_[p]);
        ADD_THREADED(hEfficiencyStubNoCutsNorm_[p]);

        ADD_THREADED_EFFICIENCY(hEfficiencyStubSingleChip_[p]);
        ADD_THREADED(hEfficiencyStubSingleChipNorm_[p]);

        ADD_THREADED_EFFICIENCY(hEfficiencyStubSingleChipNoCuts_[p]);
        ADD_THREADED(hEfficiencyStubSingleChipNoCutsNorm_[p]);

        ADD_THREADED(h2DEfficiency_[p]);
        ADD_THREADED(h2DEfficiencyNorm_[p]);

        ADD_THREADED(h2DEfficiencyQuick_[p]);
        ADD_THREADED(h2DEfficiencyQuickNorm_[p]);

        ADD_THREADED(h2DInefficiencyQuick_[p]);
        ADD_THREADED(h2DInefficiencyQuickNorm_[p]);

        ADD_THREADED(h2DEfficiencyStub_[p]);
        ADD_THREADED(h2DEfficiencyStubNorm_[p]);

        ADD_THREADED_EFFICIENCY(hEfficiencyStripStub_[p]);
        ADD_THREADED(hEfficiencyStripStubNorm_[p]);

        ADD_THREADED(h2D4CellEfficiency_[p]);
        ADD_THREADED(h2D4CellEfficiencyNorm_[p]);

        ADD_THREADED(hCellEfficiency_[p]);
        ADD_THREADED(hCellEfficiencyNorm_[p]);

        ADD_THREADED(hCellEfficiencyEvenColumns_[p]);
        ADD_THREADED(hCellEfficiencyEvenColumnsNorm_[p]);

        ADD_THREADED(hCellEfficiencyOddColumns_[p]);
        ADD_THREADED(hCellEfficiencyOddColumnsNorm_[p]);

        ADD_THREADED_EFFICIENCY(h1DXCellEfficiencyFirstHit_[p]);
        ADD_THREADED_EFFICIENCY(h1DXCellEfficiencySecondHit_[p]);
        ADD_THREADED(h1DXCellEfficiencyNorm_[p]);




        // //ADD_THREADED(h1DXcellEdgeRightEfficiency_	      [p]);
        // //ADD_THREADED(h1DXcellEdgeRightEfficiencyNorm_   [p]);

        // //ADD_THREADED(h1DXcellEdgeLeftEfficiency_	      [p]);
        // //ADD_THREADED(h1DXcellEdgeLeftEfficiencyNorm_    [p]);

        // //ADD_THREADED(h1DYcellEdgeUpEfficiency_	      [p]);
        // //ADD_THREADED(h1DYcellEdgeUpEfficiencyNorm_      [p]);

        // //ADD_THREADED(h1DYcellEdgeDownEfficiency_	      [p]);
        // //ADD_THREADED(h1DYcellEdgeDownEfficiencyNorm_    [p]);

        // ADD_THREADED_EFFICIENCY(h1DYCellEfficiencyFirstHit_[p]);
        // ADD_THREADED_EFFICIENCY(h1DYCellEfficiencySecondHit_[p]);
        // ADD_THREADED(h1DYCellEfficiencyNorm_[p]);

        ADD_THREADED_EFFICIENCY(diffFirstSecondHist_[p]);
        ADD_THREADED(hitDiffTwoStrips_[p]);
        ADD_THREADED(h2DDeltaXvsPredictedCol_[p]);

        ADD_THREADED(h1DClusterSizes_[p]);
        ADD_THREADED(h1DClusterSizesCenterStrip_[p]);
        ADD_THREADED(h1DClusterSizesBetweenStrips_[p]);

        ADD_THREADED(h1DClusterSizeVsXStripPosition_[p]);
        ADD_THREADED(h1DClusterSizeVsXStripPosition_norm_[p]);
        ADD_THREADED(h1DClusterSize1VsXStripPosition_[p]);
        ADD_THREADED(h1DClusterSize2VsXStripPosition_[p]);
        ADD_THREADED(h1DClusterSize3VsXStripPosition_[p]);
        ADD_THREADED(h1DClusterSize4VsXStripPosition_[p]);
        ADD_THREADED(h1DClusterSize5VsXStripPosition_[p]);

        ADD_THREADED(hAverageClustersPerEvent_[p]);
        ADD_THREADED(TotalEvent_[p]);
        ADD_THREADED(CheckIneff_[p]);

        ADD_THREADED(Dut23Stub_[p]);
        //nabin

        ADD_THREADED(hChi2Distribution_[p]);
        ADD_THREADED(hNumberOfClusters_[p]);
        ADD_THREADED_EFFICIENCY(hEfficiencyStubVsNumberOfClusters_[p]);
        ADD_THREADED(hEfficiencyStubVsNumberOfClustersNorm_[p]);
        ADD_THREADED(hClusterPositionLocal_[p]);
        ADD_THREADED(hEfficiencyStubVsClusterSizesNorm_[p]);
        ADD_THREADED_EFFICIENCY(hEfficiencyStubVsClusterSizes_[p]);

        ADD_THREADED(hNumberOfHitsOnSensor_[p]);

        ADD_THREADED(       hEfficiencyStubVsStripNumberNorm_[p]);
        ADD_THREADED_EFFICIENCY(hEfficiencyStubVsStripNumber_[p]);
        ADD_THREADED(       hEfficiencyStubVsDeviationFromPredNorm_[p]);
        ADD_THREADED_EFFICIENCY(hEfficiencyStubVsDeviationFromPred_[p]);
        ADD_THREADED(       hEfficiencyStubVsDeviationFromPredNorm1_[p]);
        ADD_THREADED_EFFICIENCY(hEfficiencyStubVsDeviationFromPred1_[p]);
        ADD_THREADED(       hEfficiencyStubVsDeviationFromPredNorm2_[p]);
        ADD_THREADED_EFFICIENCY(hEfficiencyStubVsDeviationFromPred2_[p]);
        ADD_THREADED(       hEfficiencyStubVsDeviationFromPredNorm3_[p]);
        ADD_THREADED_EFFICIENCY(hEfficiencyStubVsDeviationFromPred3_[p]);

        STDLINE("Threading phase completed", ACGreen);

        STDLINE("Filling phase...", ACWhite);

        hEfficiencyOld_                 [p]->Divide(hEfficiencyNorm_                 [p]);

        //hEfficiencyStrip_            [p]->Divide(hEfficiencyStripNorm_            [p]);

        h2DEfficiency_               [p]->Divide(h2DEfficiencyNorm_               [p]);

        //hEfficiencyQuick_              [p]->Divide(hEfficiencyQuickNorm_              [p]);

        h2DEfficiencyQuick_            [p]->Divide(h2DEfficiencyQuickNorm_            [p]);

        h2DInefficiencyQuick_          [p]->Divide(h2DInefficiencyQuickNorm_            [p]);

        //hEfficiencyStub_             [p]->Divide(hEfficiencyStubNorm_             [p]);

        //                 float stubEfficiencyNoXCBC = 0;
        //                 float stubEfficiencyNoXCBCNorm = 0;
        //
        //                 for(unsigned int b=1; b<hEfficiencyStripStub_[p]->GetNbinsX(); b++)
        //                     if(b != 254 && b != 255)
        //                     {
        //                         stubEfficiencyNoXCBC     += hEfficiencyStripStub_    [p]->GetEfficiency(b);
        //                         stubEfficiencyNoXCBCNorm += hEfficiencyStripStubNorm_[p]->GetBinContent(b);
        //                     }
        //                 stubEfficiencyNoXCBC /= stubEfficiencyNoXCBCNorm;
        //                 float errorStubEfficiencyNoXCBC = sqrt(stubEfficiencyNoXCBC * (1 - stubEfficiencyNoXCBC) / stubEfficiencyNoXCBCNorm);

        //hEfficiencyStripStub_        [p]->Divide(hEfficiencyStripStubNorm_        [p]);

        h2DEfficiencyStub_           [p]->Divide(h2DEfficiencyStubNorm_           [p]);

        h2D4CellEfficiency_          [p]->Divide(h2D4CellEfficiencyNorm_          [p]);

        hCellEfficiency_             [p]->Divide(hCellEfficiencyNorm_             [p]);

        hCellEfficiencyEvenColumns_        [p]->Divide(hCellEfficiencyEvenColumnsNorm_  [p]);

        hCellEfficiencyOddColumns_         [p]->Divide(hCellEfficiencyOddColumnsNorm_   [p]);

        //h1DXCellEfficiencyFirstHit_        [p]->Divide(h1DXCellEfficiencyNorm_	        [p]);

        //h1DXCellEfficiencySecondHit_       [p]->Divide(h1DXCellEfficiencyNorm_	        [p]);

        //h1DYCellEfficiencyFirstHit_  [p]->Divide(h1DYCellEfficiencyNorm_	      [p]);
        //h1DYCellEfficiencySecondHit_ [p]->Divide(h1DYCellEfficiencyNorm_	      [p]);

        h1DClusterSizeVsXStripPosition_[p]->Divide(h1DClusterSizeVsXStripPosition_norm_[p]);

        //h1DClusterSizes_                  [p]->Divide(h1DClusterSizes_                       [p]);

        //diffFirstSecondHist_                [p]->Divide(h1DXCellEfficiencyNorm_	                [p]);

        //diffFirstSecondHist_                [p]->Add(h1DXCellEfficiencySecondHit_               [p],-1);

        //hEfficiencyStubVsCBC_             [p]->Divide(hEfficiencyStubVsCBCNorm_             [p]);
        //hEfficiencyStubVsStripNumber_       [p]->Divide(hEfficiencyStubVsStripNumberNorm_       [p]);
        //hEfficiencyStubVsStripNumberWithCBCoverlap_[p]->Divide(hEfficiencyStubVsStripNumberWithCBCoverlapNorm_[p]);
        //hEfficiencyStubVsDeviationFromPred_ [p]->Divide(hEfficiencyStubVsDeviationFromPredNorm_ [p]);
        //hEfficiencyStubVsNumberOfClusters_  [p]->Divide(hEfficiencyStubVsNumberOfClustersNorm_  [p]);
        //hEfficiencyStubVsClusterSizes_      [p]->Divide(hEfficiencyStubVsClusterSizesNorm_      [p]);
        //hEfficiencyStubVsDeviationFromPred1_[p]->Divide(hEfficiencyStubVsDeviationFromPredNorm1_[p]);
        //hEfficiencyStubVsDeviationFromPred2_[p]->Divide(hEfficiencyStubVsDeviationFromPredNorm2_[p]);
        //hEfficiencyStubVsDeviationFromPred3_[p]->Divide(hEfficiencyStubVsDeviationFromPredNorm3_[p]);
        /*
        for (int i = 0; i < h1DXcellEdgeRightEfficiency_[p]->GetNbinsX(); i++)
        {
            if (h1DXcellEdgeRightEfficiencyNorm_[p]->GetBinContent(i+1) > 0)
                h1DXcellEdgeRightEfficiency_[p]->SetBinContent(i+1,h1DXcellEdgeRightEfficiency_[p]->GetBinContent(i+1) / h1DXcellEdgeRightEfficiencyNorm_[p]->GetBinContent(i+1));

            if (h1DXcellEdgeLeftEfficiencyNorm_[p]->GetBinContent(i+1) > 0)
                h1DXcellEdgeLeftEfficiency_[p]->SetBinContent(i+1,h1DXcellEdgeLeftEfficiency_[p]->GetBinContent(i+1) / h1DXcellEdgeLeftEfficiencyNorm_[p]->GetBinContent(i+1));
        }

        for (int i = 0; i < h1DYcellEdgeUpEfficiency_[p]->GetNbinsX(); i++)
        {
            if (h1DYcellEdgeUpEfficiencyNorm_[p]->GetBinContent(i+1) > 0)
                h1DYcellEdgeUpEfficiency_[p]->SetBinContent(i+1,h1DYcellEdgeUpEfficiency_[p]->GetBinContent(i+1) / h1DYcellEdgeUpEfficiencyNorm_[p]->GetBinContent(i+1));

            if (h1DYcellEdgeDownEfficiencyNorm_[p]->GetBinContent(i+1) > 0)
                h1DYcellEdgeDownEfficiency_[p]->SetBinContent(i+1,h1DYcellEdgeDownEfficiency_[p]->GetBinContent(i+1) / h1DYcellEdgeDownEfficiencyNorm_[p]->GetBinContent(i+1));
        }
*/
        //hAverageClustersPerEvent_              [p]->Divide(TotalEvent_[p]);

        // ######################
        // # Setting error bars #
        // ######################
        setErrorsBar(p);

        STDLINE("Setting styles...", ACWhite);

        h1DXCellEfficiencyFirstHit_[p]->SetMarkerStyle(20);
        h1DXCellEfficiencyFirstHit_[p]->SetMarkerSize(0.6);

        hEfficiencyStubVsDeviationFromPred_[p]->SetMarkerStyle(20);
        hEfficiencyStubVsDeviationFromPred_[p]->SetMarkerSize(0.6);

        hEfficiencyStubVsStripNumber_[p]->SetMarkerStyle(20);
        hEfficiencyStubVsStripNumber_[p]->SetMarkerSize(0.6);

        h1DXCellEfficiencySecondHit_[p]->SetMarkerStyle(20);
        h1DXCellEfficiencySecondHit_[p]->SetMarkerSize(0.6);

        h1DClusterSizes_[p]->SetMarkerStyle(20);
        h1DClusterSizes_[p]->SetMarkerSize(0.6);

        h1DClusterSizeVsXStripPosition_[p]->SetMarkerStyle(20);
        h1DClusterSizeVsXStripPosition_[p]->SetMarkerSize(0.6);

        h1DClusterSizeVsXStripPosition_norm_[p]->SetMarkerStyle(20);
        h1DClusterSizeVsXStripPosition_norm_[p]->SetMarkerSize(0.6);

        //h1DYCellEfficiencyFirstHit_        [p]->SetMarkerStyle(20);
        //h1DYCellEfficiencyFirstHit_        [p]->SetMarkerSize(0.6);

        //h1DYCellEfficiencySecondHit_       [p]->SetMarkerStyle(20);
        //h1DYCellEfficiencySecondHit_       [p]->SetMarkerSize(0.6);

        //h1DXcellEdgeRightEfficiency_       [p]->SetMarkerStyle(20);
        //h1DXcellEdgeRightEfficiency_       [p]->SetMarkerSize(0.6);

        //h1DXcellEdgeLeftEfficiency_        [p]->SetMarkerStyle(20);
        //h1DXcellEdgeLeftEfficiency_        [p]->SetMarkerSize(0.6);

        //h1DYcellEdgeUpEfficiency_          [p]->SetMarkerStyle(20);
        //h1DYcellEdgeUpEfficiency_          [p]->SetMarkerSize(0.6);

        //h1DYcellEdgeDownEfficiency_        [p]->SetMarkerStyle(20);
        //h1DYcellEdgeDownEfficiency_        [p]->SetMarkerSize(0.6);
        //test
        hEfficiencyStrip_[p]->SetTitle("; Strip Index number; Matched hit efficiency");

        hEfficiencyStripNorm_[p]->GetXaxis()->SetTitle("Strip Index number");
        hEfficiencyStripNorm_[p]->GetYaxis()->SetTitle("# of tracks(point to DUT)");

        hNumberOfClusters_[p]->GetXaxis()->SetTitle("# of clusters");
        hNumberOfClusters_[p]->GetYaxis()->SetTitle("# of events");

        hEfficiencyStripStub_[p]->SetTitle("; Strip Index number; Matched stub efficiency");

        hEfficiencyStripStubNorm_[p]->GetXaxis()->SetTitle("Strip Index number");
        hEfficiencyStripStubNorm_[p]->GetYaxis()->SetTitle("# of tracks(point to DUT)");

        hEfficiencyStubVsNumberOfClusters_[p]->SetTitle("; Number of clusters; Stub Efficiency");

        hEfficiencyStubVsNumberOfClustersNorm_[p]->GetXaxis()->SetTitle("Number of clusters");
        hEfficiencyStubVsNumberOfClustersNorm_[p]->GetYaxis()->SetTitle("# of events");

        hEfficiencyStubVsClusterSizes_[p]->SetTitle("; Cluster size; Stub Efficiency");

        hEfficiencyStubVsClusterSizesNorm_[p]->GetXaxis()->SetTitle("Cluster size");
        hEfficiencyStubVsClusterSizesNorm_[p]->GetYaxis()->SetTitle("# of events");

        hClusterPositionLocal_[p]->GetXaxis()->SetTitle("Strip Index)");
        hClusterPositionLocal_[p]->GetYaxis()->SetTitle("Cluster hit Counts");

        hNumberOfHitsOnSensor_[p]->GetXaxis()->SetTitle("Strip number");
        hNumberOfHitsOnSensor_[p]->GetYaxis()->SetTitle("# of hits on the sensor");

        hEfficiencyStubVsStripNumberNorm_[p]->GetXaxis()->SetTitle("Strip number");
        hEfficiencyStubVsStripNumberNorm_[p]->GetYaxis()->SetTitle("Cluster count");

        hEfficiencyStubVsStripNumber_[p]->SetTitle("; Strip number; Stub Efficiency");

        hEfficiencyStubVsDeviationFromPredNorm_[p]->GetXaxis()->SetTitle("Distance between cluster and a predicted strip( unit: # of strips)");
        hEfficiencyStubVsDeviationFromPredNorm_[p]->GetYaxis()->SetTitle("Cluster counts");

        hEfficiencyStubVsDeviationFromPred_[p]->SetTitle("; Distance between cluster and a predicted strip( unit: # of strips); Stub Efficiency");

        hEfficiencyStubVsDeviationFromPredNorm1_[p]->GetXaxis()->SetTitle("Distance between cluster and a predicted strip( unit: # of strips)");
        hEfficiencyStubVsDeviationFromPredNorm1_[p]->GetYaxis()->SetTitle("Cluster counts");

        hEfficiencyStubVsDeviationFromPred1_[p]->SetTitle("; Distance between cluster and a predicted strip( unit: # of strips); Stub Efficiency");

        hEfficiencyStubVsDeviationFromPredNorm2_[p]->GetXaxis()->SetTitle("Distance between cluster and a predicted strip( unit: # of strips)");
        hEfficiencyStubVsDeviationFromPredNorm2_[p]->GetYaxis()->SetTitle("Cluster counts");

        hEfficiencyStubVsDeviationFromPred2_[p]->SetTitle("; Distance between cluster and a predicted strip( unit: # of strips); Stub Efficiency");

        hEfficiencyStubVsDeviationFromPredNorm3_[p]->GetXaxis()->SetTitle("Distance between cluster and a predicted strip( unit: # of strips)");
        hEfficiencyStubVsDeviationFromPredNorm3_[p]->GetYaxis()->SetTitle("Cluster counts");

        hEfficiencyStubVsDeviationFromPred3_[p]->SetTitle("; Distance between cluster and a predicted strip( unit: # of strips); Stub Efficiency");

        h2DEfficiency_[p]->SetTitle("; Strip; Projected Traks");

        h2DEfficiencyNorm_[p]->GetXaxis()->SetTitle("Strip");
        h2DEfficiencyNorm_[p]->GetYaxis()->SetTitle("Projected track");

        h2DEfficiencyQuick_[p]->SetTitle("; Strip; Projected Tracks");

        h2DInefficiencyQuick_[p]->SetTitle("; Strip; Projected Tracks");

        h2DEfficiencyQuickNorm_[p]->GetXaxis()->SetTitle("Strip");
        h2DEfficiencyQuickNorm_[p]->GetYaxis()->SetTitle("Projected track");

        h2DEfficiencyStub_[p]->SetTitle("; Strip; Projected Track");

        h2DEfficiencyStubNorm_[p]->GetXaxis()->SetTitle("Strip");
        h2DEfficiencyStubNorm_[p]->GetYaxis()->SetTitle("Projected track");

        h2D4CellEfficiency_[p]->SetTitle("; Long pitch (um); Short pitch (um)");

        h2D4CellEfficiencyNorm_[p]->GetXaxis()->SetTitle("Long pitch (um)");
        h2D4CellEfficiencyNorm_[p]->GetYaxis()->SetTitle("Short pitch (um)");

        hCellEfficiency_[p]->SetTitle("; Long pitch (um); Short pitch (um)");

        hCellEfficiencyNorm_[p]->GetXaxis()->SetTitle("Long pitch (um)");
        hCellEfficiencyNorm_[p]->GetYaxis()->SetTitle("Short pitch (um)");

        //hCellEfficiencyEvenColumns_     [p]->GetXaxis()->SetTitle("Long pitch (um)" );
        //hCellEfficiencyEvenColumns_     [p]->GetYaxis()->SetTitle("Short pitch (um)");

        hCellEfficiencyEvenColumnsNorm_[p]->GetXaxis()->SetTitle("Long pitch (um)");
        hCellEfficiencyEvenColumnsNorm_[p]->GetYaxis()->SetTitle("Short pitch (um)");

        //hCellEfficiencyOddColumns_      [p]->GetXaxis()->SetTitle("Long pitch (um)" );
        //hCellEfficiencyOddColumns_      [p]->GetYaxis()->SetTitle("Short pitch (um)");

        hCellEfficiencyOddColumnsNorm_[p]->GetXaxis()->SetTitle("Long pitch (um)");
        hCellEfficiencyOddColumnsNorm_[p]->GetYaxis()->SetTitle("Short pitch (um)");

        h1DClusterSize2VsXStripPosition_[p]->GetXaxis()->SetTitle("Relative track position(um)");
        h1DClusterSize2VsXStripPosition_[p]->GetYaxis()->SetTitle("# of hits with cluster size 2");

        h1DClusterSize1VsXStripPosition_[p]->GetXaxis()->SetTitle("Relative track position(um)");
        h1DClusterSize1VsXStripPosition_[p]->GetYaxis()->SetTitle("# of hits with cluster size 1");

        h1DXCellEfficiencyFirstHit_[p]->SetTitle("; Relative track position(um); Efficiency");
        h1DXCellEfficiencySecondHit_[p]->SetTitle("; Relative track position(um); Efficiency");
        h1DXCellEfficiencyNorm_[p]->GetXaxis()->SetTitle("Relative track position(um)");

        //h1DXcellEdgeRightEfficiency_    [p]->GetXaxis()->SetTitle("long pitch (um)");
        //h1DXcellEdgeRightEfficiency_    [p]->GetYaxis()->SetTitle("efficiency");
        //h1DXcellEdgeRightEfficiencyNorm_[p]->GetXaxis()->SetTitle("long pitch (um)");

        //h1DXcellEdgeLeftEfficiency_     [p]->GetXaxis()->SetTitle("long pitch (um)");
        //h1DXcellEdgeLeftEfficiency_     [p]->GetYaxis()->SetTitle("efficiency");
        //h1DXcellEdgeLeftEfficiencyNorm_ [p]->GetXaxis()->SetTitle("long pitch (um)");

        //h1DYCellEfficiencyFirstHit_     [p]->GetXaxis()->SetTitle("short pitch (um)");
        //h1DYCellEfficiencyFirstHit_     [p]->GetYaxis()->SetTitle("efficiency");
        //h1DYCellEfficiencySecondHit_    [p]->GetXaxis()->SetTitle("short pitch (um)");
        //h1DYCellEfficiencySecondHit_    [p]->GetYaxis()->SetTitle("efficiency");
        //h1DYCellEfficiencyNorm_         [p]->GetXaxis()->SetTitle("short pitch (um)");

        //h1DYcellEdgeUpEfficiency_       [p]->GetXaxis()->SetTitle("short pitch (um)");
        //h1DYcellEdgeUpEfficiency_       [p]->GetYaxis()->SetTitle("efficiency");
        //h1DYcellEdgeUpEfficiencyNorm_   [p]->GetXaxis()->SetTitle("short pitch (um)");

        //h1DYcellEdgeDownEfficiency_     [p]->GetXaxis()->SetTitle("short pitch (um)");
        //h1DYcellEdgeDownEfficiency_     [p]->GetYaxis()->SetTitle("efficiency");
        //h1DYcellEdgeDownEfficiencyNorm_ [p]->GetXaxis()->SetTitle("short pitch (um)");

        h1DClusterSizes_[p]->GetXaxis()->SetTitle("Cluster size");
        h1DClusterSizes_[p]->GetYaxis()->SetTitle("Count");

        h1DClusterSizesCenterStrip_[p]->GetXaxis()->SetTitle("Cluster size");
        h1DClusterSizesCenterStrip_[p]->GetYaxis()->SetTitle("Count");

        h1DClusterSizesBetweenStrips_[p]->GetXaxis()->SetTitle("Cluster size");
        h1DClusterSizesBetweenStrips_[p]->GetYaxis()->SetTitle("Count");

        h1DClusterSizeVsXStripPosition_[p]->GetXaxis()->SetTitle("Relative track position(um)");
        h1DClusterSizeVsXStripPosition_[p]->GetYaxis()->SetTitle("Average cluster size");

        hAverageClustersPerEvent_[p]->GetXaxis()->SetTitle("Value");
        hAverageClustersPerEvent_[p]->GetYaxis()->SetTitle("Average clusters per event");

        TotalEvent_[p]->GetXaxis()->SetTitle("Value");
        TotalEvent_[p]->GetYaxis()->SetTitle("Total events");

        CheckIneff_[p]->GetXaxis()->SetTitle("Chirality");
        CheckIneff_[p]->GetYaxis()->SetTitle("Number Of Events");

        //diffFirstSecondHist_              [p]->GetXaxis()->SetTitle("Pitch (um)");
        //diffFirstSecondHist_              [p]->GetYaxis()->SetTitle("Efficiency");

        Dut23Stub_[p]->GetXaxis()->SetTitle("#Delta X [strip]");
        Dut23Stub_[p]->GetYaxis()->SetTitle("Number of events");

        // ##############################
        // # Print efficiency on screen #
        // ##############################

        if (p == dut0DetectorNumber_ || p == dut1DetectorNumber_ || p == stubDetectorNumber_)
        {
            efficiency0 = hEfficiencyCenterStrip_[p]->GetEfficiency(1);
            Ntracks0    = hEfficiencyCenterStripNorm_[p]->GetBinContent(1);
            error0      = sqrt(efficiency0 * (1 - efficiency0) / Ntracks0);
            ss0.str("");
            ss0 << "Detector: " << std::setw(27) << thePlaneMapping_->getPlaneName(p)
                << " Efficiency (Center): " << std::setw(4) << std::setprecision(7) << efficiency0 * 100.
                << " +- " << std::setw(4) << std::setprecision(3) << error0 * 100.
                << " Entries: " << Ntracks0;

            efficiency1 = hEfficiencyQuick_[p]->GetEfficiency(1);
            Ntracks1    = hEfficiencyQuickNorm_[p]->GetBinContent(1);
            error1      = sqrt(efficiency1 * (1 - efficiency1) / Ntracks1);
            ss1.str("");
            ss1 << "Detector: " << std::setw(27) << thePlaneMapping_->getPlaneName(p)
                << " Efficiency (Overall Quick): " << std::setw(4) << std::setprecision(7) << efficiency1 * 100.
                << " +- " << std::setw(4) << std::setprecision(3) << error1 * 100.
                << " Entries: " << Ntracks1;

            efficiency2 = hEfficiency_[p]->GetEfficiency(1);
            Ntracks2    = hEfficiencyNorm_[p]->GetBinContent(1);
            error2      = sqrt(efficiency2 * (1 - efficiency2) / Ntracks2);
            ss2.str("");
            ss2 << "Detector: " << std::setw(27) << thePlaneMapping_->getPlaneName(p)
                << " Efficiency: " << std::setw(4) << std::setprecision(7) << efficiency2 * 100.
                << " +- " << std::setw(4) << std::setprecision(3) << error2 * 100.
                << " Entries: " << Ntracks2;

            efficiency3 = hEfficiencyStubNoCuts_[p]->GetEfficiency(1);
            Ntracks3    = hEfficiencyStubNoCutsNorm_[p]->GetBinContent(1);
            error3      = sqrt(efficiency3 * (1 - efficiency3) / Ntracks3);
            ss3.str("");
            ss3 << "Detector: " << std::setw(27) << thePlaneMapping_->getPlaneName(p)
                << " Efficiency (Stub): " << std::setw(4) << std::setprecision(7) << efficiency3 * 100.
                << " +- " << std::setw(4) << std::setprecision(3) << error3 * 100.
                << " Entries: " << Ntracks3;


            ss4.str("");
            ss4 << "Detector: " << std::setw(27) << thePlaneMapping_->getPlaneName(p)
                << " Mean Number of Clusters: " << std::setw(4) << std::setprecision(7) << hNumberOfClusters_[p]->GetMean();

            ss5.str("");
            ss5 << "Detector: " << std::setw(27) << thePlaneMapping_->getPlaneName(p)
                << " Mean cluster size: " << std::setw(4) << std::setprecision(7) << h1DClusterSizes_[p]->GetMean();

            STDLINE(ss0.str(), ACLightPurple);
            STDLINE(ss1.str(), ACLightPurple);
            STDLINE(ss2.str(), ACLightPurple);
            STDLINE(ss3.str(), ACLightPurple);
            STDLINE(ss4.str(), ACLightPurple);
            STDLINE(ss5.str(), ACLightPurple);
        }
    }
}

//=======================================================================
void EfficiencyOuterTracker::book(void)
{
    destroy();

    std::string hName;
    std::string hTitle;
    std::string planeName;

    int nBinsX;
    int nBinsY;

    float xPitch;
    float yPitch;
    float binSize = 5; // [um]

    int lowerCol;
    int higherCol;
    int lowerRow;
    int higherRow;

    theAnalysisManager_->cd("/");
    theAnalysisManager_->mkdir("Efficiency");

    std::string planeNameStub = thePlaneMapping_->getPlaneName(stubDetectorNumber_);

    float xPitchStub = atof(((theXmlParser_->getPlanes())[planeNameStub]->getCellPitches().first).c_str());
    float yPitchStub = atof(((theXmlParser_->getPlanes())[planeNameStub]->getCellPitches().second).c_str());

    int lowerColStub = atoi(((theXmlParser_->getPlanes())[planeNameStub]->getWindow()->getLowerCol()).c_str());
    int higherColStub = atoi(((theXmlParser_->getPlanes())[planeNameStub]->getWindow()->getHigherCol()).c_str());
    int lowerRowStub = atoi(((theXmlParser_->getPlanes())[planeNameStub]->getWindow()->getLowerRow()).c_str());
    int higherRowStub = atoi(((theXmlParser_->getPlanes())[planeNameStub]->getWindow()->getHigherRow()).c_str());

    int nBinsXStub = abs(lowerColStub - higherColStub) + 1;
    int nBinsYStub = abs(lowerRowStub - higherRowStub) + 1;

    if (nBinsYStub <= 0)
        nBinsYStub = 1; // Planes which are not in the geometry file have lowerRow = higherRow = 0,
    // this produces an unexpected warning

    for (unsigned int p = 0; p < thePlaneMapping_->getNumberOfPlanes(); p++)
    {
        planeName = thePlaneMapping_->getPlaneName(p);

        xPitch = atof(((theXmlParser_->getPlanes())[planeName]->getCellPitches().first).c_str());
        yPitch = atof(((theXmlParser_->getPlanes())[planeName]->getCellPitches().second).c_str());

        lowerCol = atoi(((theXmlParser_->getPlanes())[planeName]->getWindow()->getLowerCol()).c_str());
        higherCol = atoi(((theXmlParser_->getPlanes())[planeName]->getWindow()->getHigherCol()).c_str());
        lowerRow = atoi(((theXmlParser_->getPlanes())[planeName]->getWindow()->getLowerRow()).c_str());
        higherRow = atoi(((theXmlParser_->getPlanes())[planeName]->getWindow()->getHigherRow()).c_str());

        nBinsX = abs(lowerCol - higherCol) + 1;
        nBinsY = abs(lowerRow - higherRow) + 1;

        if (nBinsY <= 0)
            nBinsY = 1; // Planes which are not in the geometry file have lowerRow = higherRow = 0,
        // this produces an unexpected warning

        theAnalysisManager_->cd("Efficiency");
        theAnalysisManager_->mkdir(planeName);
        theAnalysisManager_->mkdir("Efficiency");

        // #################
        // # 1D histograms #
        // #################
        //nabin

        hName = "EfficiencyStrip_" + planeName;
        hTitle = "Overall efficiency in strip" + planeName;
        hEfficiencyStrip_.push_back(NEW_THREADED(TEfficiency(hName.c_str(), hTitle.c_str(), nBinsX, lowerCol, higherCol + 1)));

        hName = "EfficiencyStripNorm_" + planeName;
        hTitle = "Overall efficiency normalization " + planeName;
        hEfficiencyStripNorm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsX, lowerCol, higherCol + 1)));

        hName = "EfficiencyStripStub_" + planeName;
        hTitle = "Overall stub efficiency in strip" + planeName;
        hEfficiencyStripStub_.push_back(NEW_THREADED(TEfficiency(hName.c_str(), hTitle.c_str(), nBinsXStub, lowerColStub, higherColStub + 1)));

        hName = "EfficiencyStripStubNorm_" + planeName;
        hTitle = "Overall stub efficiency normalization " + planeName;
        hEfficiencyStripStubNorm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsXStub, lowerColStub, higherColStub + 1)));

        hName = "Efficiency_" + planeName;
        hTitle = "Overall efficiency " + planeName;
        hEfficiency_.push_back(NEW_THREADED(TEfficiency(hName.c_str(), hTitle.c_str(), 1, .5, 1.5)));

        hName = "EfficiencyOld_" + planeName;
        hTitle = "Overall efficiency old style with TH1" + planeName;
        hEfficiencyOld_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 1, .5, 1.5)));

        hName = "EfficiencyNorm_" + planeName;
        hTitle = "Overall efficiency normalization " + planeName;
        hEfficiencyNorm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 1, .5, 1.5)));

        hName = "EfficiencyQuick_" + planeName;
        hTitle = "Overall efficiency quick " + planeName;
        hEfficiencyQuick_.push_back(NEW_THREADED(TEfficiency(hName.c_str(), hTitle.c_str(), 1, .5, 1.5)));

        hName = "EfficiencyQuickNorm_" + planeName;
        hTitle = "Overall efficiency quick normalization " + planeName;
        hEfficiencyQuickNorm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 1, .5, 1.5)));

        hName = "hEfficiencyCenterStrip_" + planeName;
        hTitle = "Overall efficiency strip implant. " + planeName;
        hEfficiencyCenterStrip_.push_back(NEW_THREADED(TEfficiency(hName.c_str(), hTitle.c_str(), 1, .5, 1.5)));

        hName = "hEfficiencyCenterStripNorm_" + planeName;
        hTitle = "Overall efficiency strip implant normalization " + planeName;
        hEfficiencyCenterStripNorm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 1, .5, 1.5)));

        hName = "hEfficiencyBetweenStrips_" + planeName;
        hTitle = "Efficiency between strips. " + planeName;
        hEfficiencyBetweenStrips_.push_back(NEW_THREADED(TEfficiency(hName.c_str(), hTitle.c_str(), 1, .5, 1.5)));

        hName = "hEfficiencyBetweenStripsNorm_" + planeName;
        hTitle = "Efficiency between strips normalization " + planeName;
        hEfficiencyBetweenStripsNorm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 1, .5, 1.5)));

        hName = "hEfficiencyBetweenStripsClusterSize1_" + planeName;
        hTitle = "Efficiency between strips for cluster size 1 " + planeName;
        hEfficiencyBetweenStripsClusterSize1_.push_back(NEW_THREADED(TEfficiency(hName.c_str(), hTitle.c_str(), 1, .5, 1.5)));

        hName = "hEfficiencyBetweenStripsClusterSize1Norm_" + planeName;
        hTitle = "Efficiency between strips for cluster size 1 normalization " + planeName;
        hEfficiencyBetweenStripsClusterSize1Norm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 1, .5, 1.5)));

        hName = "hEfficiencyBetweenStripsClusterSize2_" + planeName;
        hTitle = "Efficiency between strips for cluster size 2 " + planeName;
        hEfficiencyBetweenStripsClusterSize2_.push_back(NEW_THREADED(TEfficiency(hName.c_str(), hTitle.c_str(), 1, .5, 1.5)));

        hName = "hEfficiencyBetweenStripsClusterSize2Norm_" + planeName;
        hTitle = "Efficiency between strips for cluster size 2 normalization " + planeName;
        hEfficiencyBetweenStripsClusterSize2Norm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 1, .5, 1.5)));

        hName = "EfficiencyStub_" + planeName;
        hTitle = "Overall stub efficiency " + planeName;
        hEfficiencyStub_.push_back(NEW_THREADED(TEfficiency(hName.c_str(), hTitle.c_str(), 1, .5, 1.5)));

        hName = "EfficiencyStubNorm_" + planeName;
        hTitle = "Overall stub efficiency normalization " + planeName;
        hEfficiencyStubNorm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 1, .5, 1.5)));

        hName = "EfficiencyStubNoCuts_" + planeName;
        hTitle = "Overall stub efficiency without any cut " + planeName;
        hEfficiencyStubNoCuts_.push_back(NEW_THREADED(TEfficiency(hName.c_str(), hTitle.c_str(), 1, .5, 1.5)));

        hName = "EfficiencyStubNoCutsNorm_" + planeName;
        hTitle = "Overall stub efficiency without any cut normalization " + planeName;
        hEfficiencyStubNoCutsNorm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 1, .5, 1.5)));

        hName = "EfficiencyStubSingleChip_" + planeName;
        hTitle = "Overall stub efficiency single chip" + planeName;
        hEfficiencyStubSingleChip_.push_back(NEW_THREADED(TEfficiency(hName.c_str(), hTitle.c_str(), 1, .5, 1.5)));

        hName = "EfficiencyStubSingleChipNorm_" + planeName;
        hTitle = "Overall stub efficiency single chip normalization " + planeName;
        hEfficiencyStubSingleChipNorm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 1, .5, 1.5)));

        hName = "EfficiencyStubSingleChipNoCuts_" + planeName;
        hTitle = "Overall stub efficiency single chip without any cuts " + planeName;
        hEfficiencyStubSingleChipNoCuts_.push_back(NEW_THREADED(TEfficiency(hName.c_str(), hTitle.c_str(), 1, .5, 1.5)));

        hName = "EfficiencyStubSingleChipNoCutsNorm_" + planeName;
        hTitle = "Overall stub efficiency single chip without any cuts normalization " + planeName;
        hEfficiencyStubSingleChipNoCutsNorm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 1, .5, 1.5)));

        hName = "h1DXCellEfficiencyFirstHit_" + planeName;
        hTitle = "1D Cell efficiency - X coordinate first hit " + planeName;
        h1DXCellEfficiencyFirstHit_.push_back(NEW_THREADED(TEfficiency(hName.c_str(), hTitle.c_str(), xPitch / 5 + 1, -xPitch / 2 - 2.5, xPitch / 2 + 2.5)));

        hName = "hitDiffTwoStrips_" + planeName;
        hTitle = "hitDiffTwoStrips_ " + planeName;
        hitDiffTwoStrips_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 42, -10.25, 10.75)));

        hName = "h2DDeltaXvsPredictedCol_" + planeName;
        hTitle = "h2DDeltaXvsPredictedCol_ " + planeName;
        h2DDeltaXvsPredictedCol_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), nBinsX, lowerCol, higherCol + 1, nBinsY, lowerRow, higherRow + 1)));

        hName = "h1DClusterSize1VsXStripPosition" + planeName;
        hTitle = "Cluster Size1 vs X Coordinate " + planeName;
        h1DClusterSize1VsXStripPosition_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), xPitch / 5 + 1, -xPitch / 2 - 2.5, xPitch / 2 + 2.5)));

        hName = "h1DClusterSize2VsXStripPosition" + planeName;
        hTitle = "Cluster Size2 vs X Coordinate " + planeName;
        h1DClusterSize2VsXStripPosition_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), xPitch / 5 + 1, -xPitch / 2 - 2.5, xPitch / 2 + 2.5)));

        hName = "h1DClusterSize3VsXStripPosition" + planeName;
        hTitle = "Cluster Size3 vs X Coordinate " + planeName;
        h1DClusterSize3VsXStripPosition_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), xPitch / 5 + 1, -xPitch / 2 - 2.5, xPitch / 2 + 2.5)));

        hName = "h1DClusterSize4VsXStripPosition" + planeName;
        hTitle = "Cluster Size4 vs X Coordinate " + planeName;
        h1DClusterSize4VsXStripPosition_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), xPitch / 5 + 1, -xPitch / 2 - 2.5, xPitch / 2 + 2.5)));

        hName = "h1DClusterSize5VsXStripPosition" + planeName;
        hTitle = "Cluster Size5 vs X Coordinate " + planeName;
        h1DClusterSize5VsXStripPosition_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), xPitch / 5 + 1, -xPitch / 2 - 2.5, xPitch / 2 + 2.5)));

        hName = "h1DXCellEfficiencySecondHit_" + planeName;
        hTitle = "1D cell Efficiency - X coordinate second hit " + planeName;
        h1DXCellEfficiencySecondHit_.push_back(NEW_THREADED(TEfficiency(hName.c_str(), hTitle.c_str(), xPitch / 5 + 1, -xPitch / 2 - 2.5, xPitch / 2 + 2.5)));

        hName = "h1DXCellEfficiencyNorm_" + planeName;
        hTitle = "1D cell Efficiency - X coordinate normalization " + planeName;
        h1DXCellEfficiencyNorm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), xPitch / 5 + 1, -xPitch / 2 - 2.5, xPitch / 2 + 2.5)));

        hName = "h1DClusterSizeVsXStripPosition_" + planeName;
        hTitle = "Cluster Size vs X Coordinate " + planeName;
        h1DClusterSizeVsXStripPosition_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), xPitch / 5 + 1, -xPitch / 2 - 2.5, xPitch / 2 + 2.5)));

        hName = "h1DClusterSizeVsXStripPosition_norm_" + planeName;
        hTitle = "Cluster Size vs X Coordinate normalization " + planeName;
        h1DClusterSizeVsXStripPosition_norm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), xPitch / 5 + 1, -xPitch / 2 - 2.5, xPitch / 2 + 2.5)));

        hName = "h1DClusterSizes_" + planeName;
        hTitle = "Counts for Cluster Sizes " + planeName;
        h1DClusterSizes_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 15, -0.5, 14.5)));

        hName = "h1DClusterSizesCenterStrip_" + planeName;
        hTitle = "Counts for Cluster Sizes when track points to strip center " + planeName;
        h1DClusterSizesCenterStrip_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 15, -0.5, 14.5)));

        hName = "h1DClusterSizesBetweenStrips_" + planeName;
        hTitle = "Counts for Cluster Sizes when track points between strips " + planeName;
        h1DClusterSizesBetweenStrips_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 15, -0.5, 14.5)));

        hName = "hAverageClustersPerEvent_" + planeName;
        hTitle = "Average clusters per Event " + planeName;
        hAverageClustersPerEvent_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 1, .5, 1.5)));

        hName = "TotalEvent_" + planeName;
        hTitle = "Total Event " + planeName;
        TotalEvent_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 1, .5, 1.5)));

        hName = "CheckIneff_" + planeName;
        hTitle = "check inefficiency" + planeName;
        CheckIneff_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 3, -1.5, 1.5)));

        hName = "diffFirstSecondHist_" + planeName;
        hTitle = "First Hits Eff - Second Hits Eff " + planeName;
        diffFirstSecondHist_.push_back(NEW_THREADED(TEfficiency(hName.c_str(), hTitle.c_str(), xPitch / 5 + 1, -xPitch / 2 - 2.5, xPitch / 2 + 2.5)));

        hName = "Dut23Stub_" + planeName;
        hTitle = "Stub direction on plane " + planeName;
        Dut23Stub_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 80, -10., 30.)));

        hName = "hChi2Distribution_" + planeName;
        hTitle = "Chi2/DoF " + planeName;
        hChi2Distribution_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 500, 0, 10)));

        hName = "hNumberOfClusters_" + planeName;
        hTitle = "Number of clusters " + planeName;
        hNumberOfClusters_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 13, -0.5, 12.5)));

        hName = "hEfficiencyStubVsNumberOfClusters_" + planeName;
        hTitle = "Stub Efficiency Vs cluster number " + planeName;
        hEfficiencyStubVsNumberOfClusters_.push_back(NEW_THREADED(TEfficiency(hName.c_str(), hTitle.c_str(), 13, -0.5, 12.5)));

        hName = "hEfficiencyStubVsNumberOfClustersNorm_" + planeName;
        hTitle = "Cluster counts" + planeName;
        hEfficiencyStubVsNumberOfClustersNorm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 13, -0.5, 12.5)));

        hName = "hEfficiencyStubVsClusterSizes_" + planeName;
        hTitle = "Stub Efficiency Vs cluster Sizes" + planeName;
        hEfficiencyStubVsClusterSizes_.push_back(NEW_THREADED(TEfficiency(hName.c_str(), hTitle.c_str(), 15, -0.5, 14.5)));

        hName = "hEfficiencyStubVsClusterSizesNorm_" + planeName;
        hTitle = "Cluster counts" + planeName;
        hEfficiencyStubVsClusterSizesNorm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), 15, -0.5, 14.5)));

        hName = "hClusterPositionLocal_" + planeName;
        hTitle = "Cluster number in the strips " + planeName;
        hClusterPositionLocal_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsX, lowerCol, higherCol + 1)));

        hName = "hNumberOfHitsOnSensor_" + planeName;
        hTitle = "Number of hits on the sensor " + planeName;
        hNumberOfHitsOnSensor_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsX, lowerCol, higherCol + 1)));

        //        hName  = "hEfficiencyStubVsCBCNorm_"                + planeName;
        //        hTitle = "Number of clusters in each CBC "          + planeName;
        //        hEfficiencyStubVsCBCNorm_.push_back(NEW_THREADED(TH1F(hName.c_str(),hTitle.c_str(),2,0-0.5,1.5)));

        //        hName  = "hEfficiencyStubVsCBC_"                    + planeName;
        //        hTitle = "Stub efficiency of each CBC "             + planeName;
        //        hEfficiencyStubVsCBC_.push_back(NEW_THREADED(TH1F(hName.c_str(),hTitle.c_str(),2,-0.5,1.5)));

        hName = "hEfficiencyStubVsStripNumberNorm_" + planeName;
        hTitle = "cluster count Vs Strip number " + planeName;
        hEfficiencyStubVsStripNumberNorm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsXStub * 2, -higherColStub - 1, higherColStub + 1)));

        hName = "hEfficiencyStubVsStripNumber_" + planeName;
        hTitle = "Stub efficiency Vs Strip number " + planeName;
        hEfficiencyStubVsStripNumber_.push_back(NEW_THREADED(TEfficiency(hName.c_str(), hTitle.c_str(), nBinsXStub, -higherColStub - 1, higherColStub + 1)));

        //        hName  = "hEfficiencyStubVsStripNumberWithCBCoverlapNorm_"  + planeName;
        //        hTitle = "cluster count Vs Strip number "                   + planeName;
        //        hEfficiencyStubVsStripNumberWithCBCoverlapNorm_.push_back(NEW_THREADED(TH1F(hName.c_str(),hTitle.c_str(),127,-0.5,127.5)));

        //        hName  = "hEfficiencyStubVsStripNumberWithCBCoverlap_"    + planeName;
        //        hTitle = "Stub efficiency Vs Strip number  "              + planeName;
        //        hEfficiencyStubVsStripNumberWithCBCoverlap_.push_back(NEW_THREADED(TH1F(hName.c_str(),hTitle.c_str(),127,-0.5,127.5)));

        hName = "hEfficiencyStubVsDeviationFromPredNorm_" + planeName;
        hTitle = "The cluster distance from predicted hit location(which is 0) " + planeName;
        hEfficiencyStubVsDeviationFromPredNorm_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsXStub * 2, -higherColStub - 1, higherColStub + 1)));
        hName = "hEfficiencyStubVsDeviationFromPred_" + planeName;
        hTitle = "Stub efficiency vs cluster distance " + planeName;
        hEfficiencyStubVsDeviationFromPred_.push_back(NEW_THREADED(TEfficiency(hName.c_str(), hTitle.c_str(), nBinsXStub * 2, -higherColStub - 1, higherColStub + 1)));

        hName = "hEfficiencyStubVsDeviationFromPredNorm1_" + planeName;
        hTitle = "1 cluster distance from predicted hit location(which is 0) " + planeName;
        hEfficiencyStubVsDeviationFromPredNorm1_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsXStub * 2, -higherColStub - 1 - 0.5, higherColStub + 1 - 0.5)));

        hName = "hEfficiencyStubVsDeviationFromPred1_" + planeName;
        hTitle = "Stub efficiency vs 1 cluster distance " + planeName;
        hEfficiencyStubVsDeviationFromPred1_.push_back(NEW_THREADED(TEfficiency(hName.c_str(), hTitle.c_str(), nBinsXStub * 2, -higherColStub - 1 - 0.5, higherColStub + 1 - 0.5)));

        hName = "hEfficiencyStubVsDeviationFromPredNorm2_" + planeName;
        hTitle = "2 cluster distance from predicted hit location(which is 0) " + planeName;
        hEfficiencyStubVsDeviationFromPredNorm2_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsXStub * 2, -higherColStub - 1, higherColStub + 1)));

        hName = "hEfficiencyStubVsDeviationFromPred2_" + planeName;
        hTitle = "Stub efficiency vs 2 cluster distance " + planeName;
        hEfficiencyStubVsDeviationFromPred2_.push_back(NEW_THREADED(TEfficiency(hName.c_str(), hTitle.c_str(), nBinsXStub * 2, -higherColStub - 1, higherColStub + 1)));

        hName = "hEfficiencyStubVsDeviationFromPredNorm3_" + planeName;
        hTitle = "2 cluster distance from predicted hit location(which is 0) " + planeName;
        hEfficiencyStubVsDeviationFromPredNorm3_.push_back(NEW_THREADED(TH1F(hName.c_str(), hTitle.c_str(), nBinsXStub * 2, -higherColStub - 1, higherColStub + 1)));

        hName = "hEfficiencyStubVsDeviationFromPred3_" + planeName;
        hTitle = "Stub efficiency vs 2 cluster distance " + planeName;
        hEfficiencyStubVsDeviationFromPred3_.push_back(NEW_THREADED(TEfficiency(hName.c_str(), hTitle.c_str(), nBinsXStub * 2, -higherColStub - 1, higherColStub + 1)));
        // #################
        // # 2D histograms #
        // #################
        hName = "2DEfficiency_" + planeName;
        hTitle = "2D efficiency distribution " + planeName;
        h2DEfficiency_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), nBinsX, lowerCol, higherCol + 1, nBinsY, lowerRow, higherRow + 1)));

        hName = "2DEfficiencyNorm_" + planeName;
        hTitle = "2D efficiency normalization " + planeName;
        h2DEfficiencyNorm_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), nBinsX, lowerCol, higherCol + 1, nBinsY, lowerRow, higherRow + 1)));

        hName = "2DEfficiencyQuick_" + planeName;
        hTitle = "2D efficiency distribution quick " + planeName;
        h2DEfficiencyQuick_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), nBinsX, lowerCol, higherCol + 1, nBinsY, lowerRow, higherRow + 1)));

        hName = "2DEfficiencyQuickNorm_" + planeName;
        hTitle = "2D efficiency quick normalization " + planeName;
        h2DEfficiencyQuickNorm_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), nBinsX, lowerCol, higherCol + 1, nBinsY, lowerRow, higherRow + 1)));

        hName = "2DInefficiencyQuick_" + planeName;
        hTitle = "2D inefficiency distribution quick " + planeName;
        h2DInefficiencyQuick_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), nBinsX, lowerCol, higherCol + 1, nBinsY, lowerRow, higherRow + 1)));

        hName = "2DInefficiencyQuickNorm_" + planeName;
        hTitle = "2D inefficiency quick normalization " + planeName;
        h2DInefficiencyQuickNorm_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), nBinsX, lowerCol, higherCol + 1, nBinsY, lowerRow, higherRow + 1)));

        hName = "2DEfficiencyStub_" + planeName;
        hTitle = "2D stub efficiency distribution" + planeName;
        h2DEfficiencyStub_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), nBinsXStub, lowerColStub, higherColStub + 1, nBinsYStub, lowerRowStub, higherRowStub + 1)));

        hName = "2DEfficiencyStubNorm_" + planeName;
        hTitle = "2D stub efficiency normalization " + planeName;
        h2DEfficiencyStubNorm_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), nBinsXStub, lowerColStub, higherColStub + 1, nBinsYStub, lowerRowStub, higherRowStub + 1)));

        theAnalysisManager_->cd("Efficiency/" + planeName);
        theAnalysisManager_->mkdir("CellEfficiency");

        // #################
        // # 2D histograms #
        // #################
        hName = "hCellEfficiency_" + planeName;
        hTitle = "Cell efficiency " + planeName;
        hCellEfficiency_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch / binSize, -xPitch / 2, xPitch / 2, (int)yPitch / binSize, -yPitch / 2, yPitch / 2)));

        hName = "hCellEfficiencyNorm_" + planeName;
        hTitle = "Cell efficiency normalization " + planeName;
        hCellEfficiencyNorm_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch / binSize, -xPitch / 2, xPitch / 2, (int)yPitch / binSize, -yPitch / 2, yPitch / 2)));

        hName = "hCellEfficiencyEvenColumns_" + planeName;
        hTitle = "Cell efficiency even columns " + planeName;
        hCellEfficiencyEvenColumns_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch / binSize, -xPitch / 2, xPitch / 2, (int)yPitch / binSize, -yPitch / 2, yPitch / 2)));

        hName = "hCellEfficiencyEvenColumnsNorm_" + planeName;
        hTitle = "Cell efficiency normalization even columns " + planeName;
        hCellEfficiencyEvenColumnsNorm_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch / binSize, -xPitch / 2, xPitch / 2, (int)yPitch / binSize, -yPitch / 2, yPitch / 2)));

        hName = "hCellEfficiencyOddColumns_" + planeName;
        hTitle = "Cell efficiency odd columns " + planeName;
        hCellEfficiencyOddColumns_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch / binSize, -xPitch / 2, xPitch / 2, (int)yPitch / binSize, -yPitch / 2, yPitch / 2)));

        hName = "hCellEfficiencyOddColumnsNorm_" + planeName;
        hTitle = "Cell efficiency normalization odd columns " + planeName;
        hCellEfficiencyOddColumnsNorm_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch / binSize, -xPitch / 2, xPitch / 2, (int)yPitch / binSize, -yPitch / 2, yPitch / 2)));

        hName = "h2D4CellEfficiency_" + planeName;
        hTitle = "4 cell efficiency " + planeName;
        h2D4CellEfficiency_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch / binSize, -xPitch / 2, xPitch / 2, (int)yPitch / binSize, -yPitch / 2, yPitch / 2)));

        hName = "h2D4CellEfficiencyNorm_" + planeName;
        hTitle = "4 cell efficiency normalization " + planeName;
        h2D4CellEfficiencyNorm_.push_back(NEW_THREADED(TH2F(hName.c_str(), hTitle.c_str(), (int)xPitch / binSize, -xPitch / 2, xPitch / 2, (int)yPitch / binSize, -yPitch / 2, yPitch / 2)));
    }
}

//=======================================================================
void EfficiencyOuterTracker::setErrorsBar(int planeID)
{
    double efficiency, efficiencyN;
    double Ntrack, NtrackN;
    double error, errorN;
    int nBins, nBinsN;

    //    nBins = h1DXCellEfficiencyFirstHit_[planeID]->GetNbinsX();
    //    for(int b = 1; b <= nBins; b++)
    //    {
    //        efficiency = h1DXCellEfficiencyFirstHit_[planeID]->GetBinContent(b);
    //        Ntrack     = h1DXCellEfficiencyNorm_[planeID]->GetBinContent(b);
    //        if (Ntrack > 0) error = sqrt(efficiency * (1-efficiency) / Ntrack);
    //        else            error = 0;
    //        h1DXCellEfficiencyFirstHit_[planeID]->SetBinError(b,error);

    //        efficiency = h1DXCellEfficiencySecondHit_[planeID]->GetBinContent(b);
    //        Ntrack     = h1DXCellEfficiencyNorm_[planeID]->GetBinContent(b);
    //        if (Ntrack > 0) error = sqrt(efficiency * (1-efficiency) / Ntrack);
    //        else            error = 0;
    //        h1DXCellEfficiencySecondHit_[planeID]->SetBinError(b,error);
    //    }

    //    nBinsN = hEfficiencyStubVsDeviationFromPred_[planeID]->GetNbinsX();
    //    for(int b = 1; b <= nBinsN; b++)
    //    {
    //        efficiencyN = hEfficiencyStubVsDeviationFromPred_[planeID]->GetBinContent(b);
    //        NtrackN     = hEfficiencyStubVsDeviationFromPredNorm_[planeID]->GetBinContent(b);
    //        if (NtrackN > 0) errorN = sqrt(efficiencyN * (1-efficiencyN) / NtrackN);
    //        else            errorN = 0;
    //        hEfficiencyStubVsDeviationFromPred_[planeID]->SetBinError(b,errorN);
    //    }

    //    nBinsN = hEfficiencyStubVsDeviationFromPred1_[planeID]->GetNbinsX();
    //    for(int b = 1; b <= nBinsN; b++)
    //    {
    //        efficiencyN = hEfficiencyStubVsDeviationFromPred1_[planeID]->GetBinContent(b);
    //        NtrackN     = hEfficiencyStubVsDeviationFromPredNorm1_[planeID]->GetBinContent(b);
    //        if (NtrackN > 0) errorN = sqrt(efficiencyN * (1-efficiencyN) / NtrackN);
    //        else            errorN = 0;
    //        hEfficiencyStubVsDeviationFromPred1_[planeID]->SetBinError(b,errorN);
    //    }

    //    nBinsN = hEfficiencyStubVsDeviationFromPred2_[planeID]->GetNbinsX();
    //    for(int b = 1; b <= nBinsN; b++)
    //    {
    //        efficiencyN = hEfficiencyStubVsDeviationFromPred2_[planeID]->GetBinContent(b);
    //        NtrackN     = hEfficiencyStubVsDeviationFromPredNorm2_[planeID]->GetBinContent(b);
    //        if (NtrackN > 0) errorN = sqrt(efficiencyN * (1-efficiencyN) / NtrackN);
    //        else            errorN = 0;
    //        hEfficiencyStubVsDeviationFromPred2_[planeID]->SetBinError(b,errorN);
    //    }

    //    nBinsN = hEfficiencyStubVsDeviationFromPred3_[planeID]->GetNbinsX();
    //    for(int b = 1; b <= nBinsN; b++)
    //    {
    //        efficiencyN = hEfficiencyStubVsDeviationFromPred3_[planeID]->GetBinContent(b);
    //        NtrackN     = hEfficiencyStubVsDeviationFromPredNorm3_[planeID]->GetBinContent(b);
    //        if (NtrackN > 0) errorN = sqrt(efficiencyN * (1-efficiencyN) / NtrackN);
    //        else            errorN = 0;
    //        hEfficiencyStubVsDeviationFromPred3_[planeID]->SetBinError(b,errorN);
    //    }

    //    nBinsN = hEfficiencyStubVsStripNumber_[planeID]->GetNbinsX();
    //    for(int b = 1; b <= nBinsN; b++)
    //    {
    //        efficiencyN = hEfficiencyStubVsStripNumber_[planeID]->GetBinContent(b);
    //        NtrackN     = hEfficiencyStubVsStripNumberNorm_[planeID]->GetBinContent(b);
    //        if (NtrackN > 0) errorN = sqrt(efficiencyN * (1-efficiencyN) / NtrackN);
    //        else            errorN = 0;
    //        hEfficiencyStubVsStripNumber_[planeID]->SetBinError(b,errorN);
    //    }

    //    nBinsN = hEfficiencyStubVsStripNumberWithCBCoverlap_[planeID]->GetNbinsX();
    //    for(int b = 1; b <= nBinsN; b++)
    //    {
    //        efficiencyN = hEfficiencyStubVsStripNumberWithCBCoverlap_[planeID]->GetBinContent(b);
    //        NtrackN     = hEfficiencyStubVsStripNumberWithCBCoverlapNorm_[planeID]->GetBinContent(b);
    //        if (NtrackN > 0) errorN = sqrt(efficiencyN * (1-efficiencyN) / NtrackN);
    //        else            errorN = 0;
    //        hEfficiencyStubVsStripNumberWithCBCoverlap_[planeID]->SetBinError(b,errorN);
    //    }

    //    nBinsN = hEfficiencyStubVsNumberOfClusters_[planeID]->GetNbinsX();
    //    for(int b = 1; b <= nBinsN; b++)
    //    {
    //        efficiencyN = hEfficiencyStubVsNumberOfClusters_[planeID]->GetBinContent(b);
    //        NtrackN     = hEfficiencyStubVsNumberOfClustersNorm_[planeID]->GetBinContent(b);
    //        if (NtrackN > 0) errorN = sqrt(efficiencyN * (1-efficiencyN) / NtrackN);
    //        else            errorN = 0;
    //        hEfficiencyStubVsNumberOfClusters_[planeID]->SetBinError(b,errorN);

    //    }
    //    nBinsN = hEfficiencyStubVsClusterSizes_[planeID]->GetNbinsX();
    //    for(int b = 1; b <= nBinsN; b++)
    //    {
    //        efficiencyN = hEfficiencyStubVsClusterSizes_[planeID]->GetBinContent(b);
    //        NtrackN     = hEfficiencyStubVsClusterSizesNorm_[planeID]->GetBinContent(b);
    //        if (NtrackN > 0) errorN = sqrt(efficiencyN * (1-efficiencyN) / NtrackN);
    //        else            errorN = 0;
    //        hEfficiencyStubVsClusterSizes_[planeID]->SetBinError(b,errorN);

    //  }
    //    nBinsN = hEfficiencyStubVsCBC_[planeID]->GetNbinsX();
    //    for(int b = 1; b <= nBinsN; b++)
    //    {
    //        efficiencyN = hEfficiencyStubVsCBC_[planeID]->GetBinContent(b);
    //        NtrackN     = hEfficiencyStubVsCBCNorm_[planeID]->GetBinContent(b);
    //        if (NtrackN > 0) errorN = sqrt(efficiencyN * (1-efficiencyN) / NtrackN);
    //        else            errorN = 0;
    //        hEfficiencyStubVsCBC_[planeID]->SetBinError(b,errorN);
    //    }

    //    nBinsN = hEfficiencyStrip_[planeID]->GetNbinsX();
    //    for(int b = 1; b <= nBinsN; b++)
    //    {
    //        efficiencyN = hEfficiencyStrip_[planeID]->GetBinContent(b);
    //        NtrackN     = hEfficiencyStripNorm_[planeID]->GetBinContent(b);
    //        if (NtrackN > 0) errorN = sqrt(efficiencyN * (1-efficiencyN) / NtrackN);
    //        else            errorN = 0;
    //        hEfficiencyStrip_[planeID]->SetBinError(b,errorN);

    //    }

    //    nBinsN = hEfficiencyStripStub_[planeID]->GetNbinsX();
    //    for(int b = 1; b <= nBinsN; b++)
    //    {
    //        efficiencyN = hEfficiencyStripStub_[planeID]->GetBinContent(b);
    //        NtrackN     = hEfficiencyStripStubNorm_[planeID]->GetBinContent(b);
    //        if (NtrackN > 0) errorN = sqrt(efficiencyN * (1-efficiencyN) / NtrackN);
    //        else            errorN = 0;
    //        hEfficiencyStripStub_[planeID]->SetBinError(b,errorN);
    //    }
    //
    //    nBins = h1DXcellEdgeRightEfficiency_[planeID]->GetNbinsX();
    //    for(int b = 1; b <= nBins; b++)
    //    {
    //        efficiency = h1DXcellEdgeRightEfficiency_[planeID]->GetBinContent(b);
    //        Ntrack     = h1DXcellEdgeRightEfficiencyNorm_[planeID]->GetBinContent(b);
    //        if (Ntrack > 0) error = sqrt(efficiency * (1-efficiency) / Ntrack);
    //        else            error = 0;
    //        h1DXcellEdgeRightEfficiency_[planeID]->SetBinError(b,error);

    //        efficiency = h1DXcellEdgeLeftEfficiency_[planeID]->GetBinContent(b);
    //        Ntrack     = h1DXcellEdgeLeftEfficiencyNorm_[planeID]->GetBinContent(b);
    //        if (Ntrack > 0) error = sqrt(efficiency * (1-efficiency) / Ntrack);
    //        else            error = 0;
    //        h1DXcellEdgeLeftEfficiency_[planeID]->SetBinError(b,error);
    //    }

    //    nBins = h1DYCellEfficiencyFirstHit_[planeID]->GetNbinsX();
    //    for(int b = 1; b <= nBins; b++)
    //    {
    //        efficiency = h1DYCellEfficiencyFirstHit_[planeID]->GetBinContent(b);
    //        Ntrack     = h1DYCellEfficiencyNorm_[planeID]->GetBinContent(b);
    //        if (Ntrack > 0) error = sqrt(efficiency * (1-efficiency) / Ntrack);
    //        else            error = 0;
    //        h1DYCellEfficiencyFirstHit_[planeID]->SetBinError(b,error);

    //        efficiency = h1DYCellEfficiencySecondHit_[planeID]->GetBinContent(b);
    //        Ntrack     = h1DYCellEfficiencyNorm_[planeID]->GetBinContent(b);
    //        if (Ntrack > 0) error = sqrt(efficiency * (1-efficiency) / Ntrack);
    //        else            error = 0;
    //        h1DYCellEfficiencySecondHit_[planeID]->SetBinError(b,error);
    //    }

    //    nBins = h1DYcellEdgeUpEfficiency_[planeID]->GetNbinsX();
    //    for(int b = 1; b <= nBins; b++)
    //    {
    //        efficiency = h1DYcellEdgeUpEfficiency_[planeID]->GetBinContent(b);
    //        Ntrack     = h1DYcellEdgeUpEfficiencyNorm_[planeID]->GetBinContent(b);
    //        if (Ntrack > 0) error = sqrt(efficiency * (1-efficiency) / Ntrack);
    //        else            error = 0;
    //        h1DYcellEdgeUpEfficiency_[planeID]->SetBinError(b,error);

    //        efficiency = h1DYcellEdgeDownEfficiency_[planeID]->GetBinContent(b);
    //        Ntrack     = h1DYcellEdgeDownEfficiencyNorm_[planeID]->GetBinContent(b);
    //        if (Ntrack > 0) error = sqrt(efficiency * (1-efficiency) / Ntrack);
    //        else            error = 0;
    //        h1DYcellEdgeDownEfficiency_[planeID]->SetBinError(b,error);
    //    }
    //
}

//=======================================================================
void EfficiencyOuterTracker::planeEfficiency(bool pass, int planeID, const Data &data, int threadNumber)
{
    //    if(planeID  == dut0DetectorNumber_ || planeID  == dut1DetectorNumber_)
    //    {
    //        std::cout << __PRETTY_FUNCTION__
    //                  << "Plane: " << planeID
    //                  << " Event number: " << data.getEventChewieNumber()
    //                  << " pass: " << pass
    //                  << " in detector: " << data.getIsInDetector(planeID)
    //                  << std::endl;
    //    }
    //std::cout << __PRETTY_FUNCTION__ << "plane id: " << planeID << std::endl;
    //std::cout<<thePlaneMapping_->getPlaneName(planeID)<<std::endl;
    if (!pass || !data.getIsInDetector(planeID))
        return;

    const Window *theWindow     = theWindowsManager_->getWindow(planeID);
    const Window *theStubWindow = theWindowsManager_->getWindow(stubDetectorNumber_);
    int rowPredicted = data.getRowPredicted(planeID);
    int colPredicted = data.getColPredicted(planeID); //x-axis
    int colStubPredicted = data.getColPredicted(stubDetectorNumber_);  //x-axis
    int run = data.getRunNumber();
    //    float         maxPitchX        = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->getCellPitches().first).c_str());
    //    float         maxPitchStubX    = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(stubDetectorNumber_)]->getCellPitches().first).c_str());
    double maxPitchX = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->getCellPitches().first).c_str());
    double maxPitchStubX = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(stubDetectorNumber_)]->getCellPitches().first).c_str()); //int           myEventNumber    = data.getEventChewieNumber();
    //int           myNumberOfTracks = data.getNumberOfTracks();
    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    if (!theWindow->checkWindow(colPredicted, rowPredicted, run))
    {
        return;
    }
    // ##################################################
    // # Compute efficiency for all cells in acceptance #
    // ##################################################
    // thus, dubbed "Quick", without check window
    // nabin
    //h2DFakeHitsEfficiency_ h2DFakeHitsEfficiencyNorm_

    //    if(planeID  == dut0DetectorNumber_ && data.getClusterSize(dut0DetectorNumber_) == 1){

    //        //float mysubresidual  = data.getXMeasuredLocal(planeID) - data.getXPredictedLocal(planeID);
    //        //float myresidual     = data.getXTrackResidualLocal(planeID);

    //        //std::cout << "Col predicted    " << data.getColPredicted(dut0DetectorNumber_) << "    Mean col     " << data.getMeanCol(dut0DetectorNumber_)<< std::endl;
    //        //std::cout << "Cluster size  " << data.getClusterSize(dut0DetectorNumber_) << std::endl;
    //        //std::cout << "X measured    " << data.getXMeasuredLocal(dut0DetectorNumber_) << "     X predicted      " << data.getXPredictedLocal(dut0DetectorNumber_)<< std::endl;
    //        //std::cout << "myresidual    " << data.getXTrackResidualLocal(dut0DetectorNumber_) << std::endl;
    //        //std::cout << "mysubresidual " << mysubresidual <<"  and  "<< "myresidual "<< myresidual << std::endl;
    //    }

    //    THREADED(hNumberOfClusters_ [planeID])->Fill(data.getNumberOfClusters(planeID));

    //    if(planeID  == dut0DetectorNumber_ || planeID  == dut1DetectorNumber_)
    //    {
    //        //std::cout << "Plane: " << planeID << " Event number: " << data.getEventChewieNumber() << std::endl;
    //        //float mysubresidual  = data.getXMeasuredLocal(planeID) - data.getXPredictedLocal(planeID);
    //        //float myresidual     = data.getXTrackResidualLocal(planeID);

    //        //std::cout << planeID << " Col predicted " << data.getColPredicted(planeID) << "    Mean col     " << data.getMeanCol(planeID)<< std::endl;
    //        //std::cout << planeID << " Cluster size  " << data.getClusterSize(planeID) << std::endl;
    //        //std::cout << planeID << " X measured    " << data.getXMeasuredLocal(planeID) << "     X predicted      " << data.getXPredictedLocal(planeID)<< std::endl;
    //        //std::cout << planeID << " myresidual    " << data.getXTrackResidualLocal(planeID) << std::endl;
    //        //std::cout << planeID << " mysubresidual " << mysubresidual <<"  and  "<< "myresidual "<< myresidual << std::endl;
    //    }

    //THREADED(hChi2Distribution_[planeID])->Fill(data.getChi2());
    THREADED(hChi2Distribution_[planeID])->Fill(data.getChi2() / data.getNdof());
    //THREADED(hChi2Distribution_[planeID])->Fill(data.getNdof());

    THREADED(hEfficiencyQuickNorm_[planeID])->Fill(1);
    THREADED(h2DEfficiencyQuickNorm_[planeID])->Fill(colPredicted, rowPredicted);
    THREADED(h2DInefficiencyQuickNorm_[planeID])->Fill(colPredicted, rowPredicted);
    //THREADED(hAverageClustersPerEvent_[planeID])->Fill(data.getNumberOfClusters(planeID));
    THREADED(TotalEvent_[planeID])->Fill(1); //Not used yet!
    if (data.getHasHit(planeID))
    {
        THREADED(hNumberOfHitsOnSensor_[planeID])->Fill(data.getMeanCol(planeID));
        THREADED(hEfficiencyQuick_[planeID])->Fill(true, 1);
        THREADED(h2DEfficiencyQuick_[planeID])->Fill(colPredicted, rowPredicted);
    }
    else
    {
        THREADED(hEfficiencyQuick_[planeID])->Fill(false, 1);
        THREADED(h2DInefficiencyQuick_[planeID])->Fill(colPredicted, rowPredicted);
    }
    // #########################################################################
    // # Compute efficiency only for cells that are surrounded by "good" cells #
    // #########################################################################
    int cutUm = 5;
    if (theWindow->checkWindowAbout(colPredicted, rowPredicted, run, 0)) // overriden for strip only
    {
        //if(planeID == dut0DetectorNumber_ || planeID == dut0DetectorNumber_)
        //nabin
        //float x1 = -1, x2 = 1;
        //if(planeID == dut0DetectorNumber_ || planeID == dut1DetectorNumber_){
        //    x1 = data.getXPredictedLocal(planeID);
        //    x2 = data.getXMeasuredLocal(planeID)+45;
        //}
        //std::cout << fabs(x1-x2) << std::endl;
        //if(abs(x2-x1) < 20.0)
        if (data.getXPixelResidualLocal(planeID) > -cutUm && data.getXPixelResidualLocal(planeID) < cutUm)
            THREADED(hEfficiencyCenterStripNorm_[planeID])->Fill(1);
        if (data.getXPixelResidualLocal(planeID) > maxPitchX / 2 - cutUm || data.getXPixelResidualLocal(planeID) < -maxPitchX / 2 + cutUm)
        {
            THREADED(hEfficiencyBetweenStripsNorm_[planeID])->Fill(1);
            if(data.getClusterSize(planeID) == 1)
                THREADED(hEfficiencyBetweenStripsClusterSize1Norm_[planeID])->Fill(1);
            if(data.getClusterSize(planeID) == 2)
                THREADED(hEfficiencyBetweenStripsClusterSize2Norm_[planeID])->Fill(1);
        }
        THREADED(hEfficiencyNorm_[planeID])->Fill(1);
        THREADED(hEfficiencyStripNorm_[planeID])->Fill(colPredicted);
        THREADED(h2DEfficiencyNorm_[planeID])->Fill(colPredicted, rowPredicted);
        if (data.getHasHit(planeID))
        {
            //if(abs(x2-x1) < 20.0)
            if (data.getXPixelResidualLocal(planeID) > -cutUm && data.getXPixelResidualLocal(planeID) < cutUm)
                THREADED(hEfficiencyCenterStrip_[planeID])->Fill(true, 1);
            if (data.getXPixelResidualLocal(planeID) > maxPitchX / 2 - cutUm || data.getXPixelResidualLocal(planeID) < -maxPitchX / 2 + cutUm)
            {
                THREADED(hEfficiencyBetweenStrips_[planeID])->Fill(true, 1);
                if(data.getClusterSize(planeID) == 1)
                    THREADED(hEfficiencyBetweenStripsClusterSize1_[planeID])->Fill(true, 1);
                if(data.getClusterSize(planeID) == 2)
                    THREADED(hEfficiencyBetweenStripsClusterSize2_[planeID])->Fill(true, 1);
            }
            THREADED(hEfficiency_[planeID])->Fill(true, 1);
            THREADED(hEfficiencyOld_[planeID])->Fill(1);
            THREADED(hEfficiencyStrip_[planeID])->Fill(true, colPredicted);
            THREADED(h2DEfficiency_[planeID])->Fill(colPredicted, rowPredicted);
        }
        else
        {
            //if(abs(x2-x1) < 20.0)
            if (data.getXPixelResidualLocal(planeID) > -cutUm && data.getXPixelResidualLocal(planeID) < cutUm)
                THREADED(hEfficiencyCenterStrip_[planeID])->Fill(false, 1);
            if (data.getXPixelResidualLocal(planeID) > maxPitchX / 2 - cutUm || data.getXPixelResidualLocal(planeID) < -maxPitchX / 2 + cutUm)
            {
                THREADED(hEfficiencyBetweenStrips_[planeID])->Fill(false, 1);
                if(data.getClusterSize(planeID) == 1)
                    THREADED(hEfficiencyBetweenStripsClusterSize1_[planeID])->Fill(false, 1);
                if(data.getClusterSize(planeID) == 2)
                    THREADED(hEfficiencyBetweenStripsClusterSize2_[planeID])->Fill(false, 1);
            }
            THREADED(hEfficiency_[planeID])->Fill(false, 1);
            THREADED(hEfficiencyStrip_[planeID])->Fill(false, colPredicted);
        }
    }
    // may be not correct stub efficiency-> CORRECT NOW.
    float dut2col = data.getMeanCol(dut0DetectorNumber_);
    float dut3col = data.getMeanCol(dut1DetectorNumber_);
    if (theStubWindow->checkWindowAbout(colStubPredicted, rowPredicted, run, 0)) // overriden for stub only
    {
        THREADED(hEfficiencyStubNoCutsNorm_[planeID])->Fill(1);
        if(colStubPredicted < 250 || colStubPredicted > 270)
            THREADED(hEfficiencyStubSingleChipNoCutsNorm_[planeID])->Fill(1);
        if (data.getHasHit(stubDetectorNumber_))
        {
            THREADED(hEfficiencyStubNoCuts_[planeID])->Fill(true, 1);
            if(colStubPredicted < 250 || colStubPredicted > 270)
                THREADED(hEfficiencyStubSingleChipNoCuts_[planeID])->Fill(true, 1);
        }
        else
        {
            THREADED(hEfficiencyStubNoCuts_[planeID])->Fill(false, 1);
            if(colStubPredicted < 250 || colStubPredicted > 270)
                THREADED(hEfficiencyStubSingleChipNoCuts_[planeID])->Fill(false, 1);
        }
        if (data.getHasHit(dut0DetectorNumber_) && data.getHasHit(dut1DetectorNumber_))
        {
            THREADED(hEfficiencyStubNorm_[planeID])->Fill(1);
            if(colStubPredicted < 250 || colStubPredicted > 270)
                THREADED(hEfficiencyStubSingleChipNorm_[planeID])->Fill(1);
            THREADED(hEfficiencyStripStubNorm_[planeID])->Fill(colStubPredicted);
            THREADED(h2DEfficiencyStubNorm_[planeID])->Fill(colStubPredicted, rowPredicted);
            THREADED(hEfficiencyStubVsNumberOfClustersNorm_[planeID])->Fill(data.getNumberOfClusters(planeID));
            THREADED(hEfficiencyStubVsClusterSizesNorm_[planeID])->Fill(data.getClusterSize(planeID));

            if (data.getHasHit(stubDetectorNumber_))
            {
                THREADED(hEfficiencyStub_[planeID])->Fill(true, 1);
                if(colStubPredicted < 250 || colStubPredicted > 270)
                    THREADED(hEfficiencyStubSingleChip_[planeID])->Fill(true, 1);
                THREADED(hEfficiencyStripStub_[planeID])->Fill(true, colStubPredicted);
                THREADED(h2DEfficiencyStub_[planeID])->Fill(colStubPredicted, rowPredicted);
                THREADED(hEfficiencyStubVsNumberOfClusters_[planeID])->Fill(true, data.getNumberOfClusters(planeID));
                THREADED(hEfficiencyStubVsClusterSizes_[planeID])->Fill(true, data.getClusterSize(planeID));
                THREADED(Dut23Stub_[dut0DetectorNumber_])->Fill(dut2col - dut3col);
                THREADED(Dut23Stub_[dut1DetectorNumber_])->Fill(dut2col - dut3col);
            }
            else
            {
                THREADED(hEfficiencyStub_[planeID])->Fill(false, 1);
                if(colStubPredicted < 250 || colStubPredicted > 270)
                    THREADED(hEfficiencyStubSingleChip_[planeID])->Fill(false, 1);
                THREADED(hEfficiencyStripStub_[planeID])->Fill(false, colStubPredicted);
                THREADED(hEfficiencyStubVsNumberOfClusters_[planeID])->Fill(false, data.getNumberOfClusters(planeID));
                THREADED(hEfficiencyStubVsClusterSizes_[planeID])->Fill(false, data.getClusterSize(planeID));
            }
        }
    }

    if (planeID != dut0DetectorNumber_ && planeID != dut1DetectorNumber_ && planeID != stubDetectorNumber_)
        return;
    //const int numberOfChips = 2; //----- was 2!!
    int cbcNumberforColPredicted = colPredicted / 127;
    //std::vector<int> chipnumber(data.getNumberOfClusters(planeID), 0);
    //THREADED(hNumberOfClusters_ [planeID])->Fill(data.getNumberOfClusters(planeID));// checking number of clusters in an event only if the dut has hit.
    //if (data.getHasHit(planeID))
    //{

    // I want to check if all the clusters are within 135um from projected hit location
    THREADED(hNumberOfClusters_[planeID])->Fill(data.getNumberOfClusters(planeID));

    if (data.getNumberOfClusters(dut0DetectorNumber_) > data.maxClusters
            || data.getNumberOfClusters(dut1DetectorNumber_) > data.maxClusters
            || data.getNumberOfClusters(stubDetectorNumber_) > data.maxClusters
            )
        return;


    for (unsigned int nC = 0; nC < data.getNumberOfClusters(planeID); nC++)
    {
        int xclusterStripIndex = -1;
        float xClusterMeasuredLocal = data.getXClusterMeasuredLocal(nC, planeID);
        //std::cout << "xClusterMeasuredLocal:   "<< xClusterMeasuredLocal << std::endl;
        if (fmod(xClusterMeasuredLocal, maxPitchX) > 0)
        {
            xclusterStripIndex = xClusterMeasuredLocal / maxPitchX;
        }
        else
        {
            xclusterStripIndex = xClusterMeasuredLocal / maxPitchX - 1;
        }
        //std::cout << "xclusterStripIndex:      " << xclusterStripIndex << std::endl;
        int cbcNumberforCluster = xclusterStripIndex / 127; // since we have 2 cbcs, it is either 0 or 1.
        //std::cout << "cbcNumberforCluster:      "<< cbcNumberforCluster << std::endl;

        int deviationFromPredicted = xclusterStripIndex - colPredicted;
        // new test from here.
        //if( cbcNumberforColPredicted == cbcNumberforCluster)
        //{
        if (theStubWindow->checkWindowAbout(colPredicted, rowPredicted, run, 0)) // overriden for stub only
        {
            if (data.getHasHit(dut0DetectorNumber_) && data.getHasHit(dut1DetectorNumber_))
            {
                THREADED(hEfficiencyStubVsStripNumberNorm_[planeID])->Fill(xclusterStripIndex);
                if (data.getHasHit(stubDetectorNumber_))
                {
                    THREADED(hEfficiencyStubVsStripNumber_[planeID])->Fill(true, xclusterStripIndex);
                }
                else
                {
                    THREADED(hEfficiencyStubVsStripNumber_[planeID])->Fill(false, xclusterStripIndex); // check again
                }
            }
        }
        //}
        // following line means selection same cbc for cluster hit and predicted hit, means separating each cbc
        if (cbcNumberforColPredicted == cbcNumberforCluster)
        {
            THREADED(hClusterPositionLocal_[planeID])->Fill(xclusterStripIndex);
            if (data.getNumberOfClusters(planeID) == 1)
            {
                if (theStubWindow->checkWindowAbout(colPredicted, rowPredicted, run, 0)) // overriden for stub only
                {
                    if (data.getHasHit(dut0DetectorNumber_) && data.getHasHit(dut1DetectorNumber_))
                    {
                        //THREADED(hEfficiencyStubVsStripNumberNorm_       [planeID])->Fill(xclusterStripIndex);  // predicted matched hit cluster number = 1,  events
                        THREADED(hEfficiencyStubVsDeviationFromPredNorm1_[planeID])->Fill(deviationFromPredicted); // when cluster number 1 in an event and hit is not in the same strip but somewhere far strip
                        if (data.getHasHit(stubDetectorNumber_))
                        {
                            //if(data.getHasStub(planeID)){
                            //THREADED(hEfficiencyStubVsStripNumber_       [planeID])->Fill(true,xclusterStripIndex); // check again
                            THREADED(hEfficiencyStubVsDeviationFromPred1_[planeID])->Fill(true, deviationFromPredicted);
                            if (planeID != stubDetectorNumber_ && data.getNumberOfClusters(stubDetectorNumber_) == 2)
                            {
                                //std::cout << std::endl << "Impossible, I have more than 1 stub.                                     plane:" << planeID << "    DUT0:" << data.getNumberOfClusters(dut0DetectorNumber_) <<  "     DUT1:" << data.getNumberOfClusters(dut1DetectorNumber_) << "    DUT2:" << data.getNumberOfClusters(stubDetectorNumber_) << "   Event #:" << myEventNumber << std::endl;
                                for (unsigned int nS = 0; nS < data.getNumberOfClusters(stubDetectorNumber_); nS++)
                                {
                                    int nClusterDUT0 = 0;
                                    int nClusterDUT1 = 0;
                                    if (planeID == dut0DetectorNumber_)
                                        nClusterDUT1 = nS;
                                    if (planeID == dut1DetectorNumber_)
                                        nClusterDUT0 = nS;
                                    int xStubStripIndex;
                                    int xDUT0StripIndex;
                                    int xDUT1StripIndex;

                                    float xDUT0MeasuredLocal = data.getXClusterMeasuredLocal(nClusterDUT0, dut0DetectorNumber_);
                                    if (fmod(xDUT0MeasuredLocal, maxPitchX) > 0)
                                    {
                                        xDUT0StripIndex = xDUT0MeasuredLocal / maxPitchX;
                                    }
                                    else
                                    {
                                        xDUT0StripIndex = xDUT0MeasuredLocal / maxPitchX - 1;
                                    }
                                    float xDUT1MeasuredLocal = data.getXClusterMeasuredLocal(nClusterDUT1, dut1DetectorNumber_);
                                    if (fmod(xDUT1MeasuredLocal, maxPitchX) > 0)
                                    {
                                        xDUT1StripIndex = xDUT1MeasuredLocal / maxPitchX;
                                    }
                                    else
                                    {
                                        xDUT1StripIndex = xDUT1MeasuredLocal / maxPitchX - 1;
                                    }
                                    float xStubMeasuredLocal = data.getXClusterMeasuredLocal(nS, stubDetectorNumber_);
                                    if (fmod(xStubMeasuredLocal, maxPitchX) > 0)
                                    {
                                        xStubStripIndex = xStubMeasuredLocal / maxPitchStubX / 2;
                                    }
                                    else
                                    {
                                        xStubStripIndex = xStubMeasuredLocal / maxPitchStubX / 2 - 1;
                                    }
                                    //int stubDeviationFromPredicted = colPredicted - xStubStripIndex;

                                    //if((xDUT0StripIndex != xDUT1StripIndex) &&  (xDUT0StripIndex != xStubStripIndex) ){
                                    //std::cout << __PRETTY_FUNCTION__ << "Impossible:   " << xDUT0StripIndex << "       " << xDUT1StripIndex << "       " << xStubStripIndex << "    colPredicted  "<< colPredicted << std::endl;

                                    //}
                                }
                            }
                        }
                        else
                        {
                            //THREADED(hEfficiencyStubVsStripNumber_       [planeID])->Fill(false,xclusterStripIndex); // check again
                            THREADED(hEfficiencyStubVsDeviationFromPred1_[planeID])->Fill(false, deviationFromPredicted);
                        }
                    }
                }
            }
            else if (data.getNumberOfClusters(planeID) == 2)
            {
                if (theStubWindow->checkWindowAbout(colPredicted, rowPredicted, run, 0)) // overriden for stub only
                {
                    for (unsigned int nS = 0; nS < data.getNumberOfClusters(stubDetectorNumber_); nS++)
                    {

                        int nClusterDUT0 = 0;
                        int nClusterDUT1 = 0;
                        if (planeID == dut0DetectorNumber_)
                            nClusterDUT1 = nS;
                        if (planeID == dut1DetectorNumber_)
                            nClusterDUT0 = nS;
                        int xStubStripIndex;
                        int xDUT0StripIndex;
                        int xDUT1StripIndex;
                        float xDUT0MeasuredLocal = data.getXClusterMeasuredLocal(nClusterDUT0, dut0DetectorNumber_);
                        if (fmod(xDUT0MeasuredLocal, maxPitchX) > 0)
                        {
                            xDUT0StripIndex = xDUT0MeasuredLocal / maxPitchX;
                        }
                        else
                        {
                            xDUT0StripIndex = xDUT0MeasuredLocal / maxPitchX - 1;
                        }
                        float xDUT1MeasuredLocal = data.getXClusterMeasuredLocal(nClusterDUT1, dut1DetectorNumber_);
                        if (fmod(xDUT1MeasuredLocal, maxPitchX) > 0)
                        {
                            xDUT1StripIndex = xDUT1MeasuredLocal / maxPitchX;
                        }
                        else
                        {
                            xDUT1StripIndex = xDUT1MeasuredLocal / maxPitchX - 1;
                        }
                        float xStubMeasuredLocal = data.getXClusterMeasuredLocal(nS, stubDetectorNumber_);
                        if (fmod(xStubMeasuredLocal, maxPitchX) > 0)
                        {
                            xStubStripIndex = xStubMeasuredLocal / maxPitchStubX / 2;
                        }
                        else
                        {
                            xStubStripIndex = xStubMeasuredLocal / maxPitchStubX / 2 - 1;
                        }
                        //                            int xStubStripIndex;
                        //                            float xStubMeasuredLocal  = data.getXClusterMeasuredLocal(nS,stubDetectorNumber_);
                        //                            if(fmod(xStubMeasuredLocal,maxPitchX) > 0) {xStubStripIndex = xStubMeasuredLocal/maxPitchX;} else {xStubStripIndex = xStubMeasuredLocal/maxPitchX-1; }
                        int stubDeviationFromPredicted = colPredicted - xStubStripIndex;
                        THREADED(hEfficiencyStubVsDeviationFromPredNorm2_[planeID])->Fill(deviationFromPredicted);
                        //THREADED(hEfficiencyStubVsStripNumberNorm_[planeID])->Fill(xclusterStripIndex);  // check again

                        if (abs(deviationFromPredicted - stubDeviationFromPredicted) < 5)
                        {
                            THREADED(hEfficiencyStubVsDeviationFromPred2_[planeID])->Fill(true, deviationFromPredicted);
                            //THREADED(hEfficiencyStubVsStripNumber_[planeID])->Fill(true,xclusterStripIndex); // check again
                            //                                if(planeID != stubDetectorNumber_ && data.getNumberOfClusters(stubDetectorNumber_) <= 2)
                            //                                {
                            //                                    //std::cout << std::endl << "Impossible, I have more than 1 stub.                                     plane:" << planeID << "    DUT0:" << data.getNumberOfClusters(dut0DetectorNumber_) <<  "     DUT1:" << data.getNumberOfClusters(dut1DetectorNumber_) << "    DUT2:" << data.getNumberOfClusters(stubDetectorNumber_) << "   Event #:" << myEventNumber << std::endl;
                            //                                    for(unsigned int nS=0; nS<data.getNumberOfClusters(stubDetectorNumber_); nS++)
                            //                                    {
                            //                                        int nClusterDUT0 = 0;
                            //                                        int nClusterDUT1 = 0;
                            //                                        if(planeID == dut0DetectorNumber_) nClusterDUT1 = nS;
                            //                                        if(planeID == dut1DetectorNumber_) nClusterDUT0 = nS;
                            //                                        int xStubStripIndex;
                            //                                        int xDUT0StripIndex;
                            //                                        int xDUT1StripIndex;
                            //                                        float xDUT0MeasuredLocal  = data.getXClusterMeasuredLocal(nClusterDUT0,dut0DetectorNumber_);
                            //                                        if(fmod(xDUT0MeasuredLocal,maxPitchX) > 0) {xDUT0StripIndex = xDUT0MeasuredLocal/maxPitchX;} else {xDUT0StripIndex = xDUT0MeasuredLocal/maxPitchX-1; }
                            //                                        float xDUT1MeasuredLocal  = data.getXClusterMeasuredLocal(nClusterDUT1,dut1DetectorNumber_);
                            //                                        if(fmod(xDUT1MeasuredLocal,maxPitchX) > 0) {xDUT1StripIndex = xDUT1MeasuredLocal/maxPitchX;} else {xDUT1StripIndex = xDUT1MeasuredLocal/maxPitchX-1; }
                            //                                        float xStubMeasuredLocal  = data.getXClusterMeasuredLocal(nS,stubDetectorNumber_);
                            //                                        if(fmod(xStubMeasuredLocal,maxPitchX) > 0) {xStubStripIndex = xStubMeasuredLocal/maxPitchStubX/2;} else {xStubStripIndex = xStubMeasuredLocal/maxPitchStubX/2-1; }
                            //                                        //int stubDeviationFromPredicted = colPredicted - xStubStripIndex;
                            //                                        //if((xDUT0StripIndex != xDUT1StripIndex) &&  (xDUT0StripIndex != xStubStripIndex) ){
                            //                                            //std::cout << __PRETTY_FUNCTION__ << "Impossible:   " << xDUT0StripIndex << "       " << xDUT1StripIndex << "       " << xStubStripIndex << "    colPredicted  "<< colPredicted << std::endl;
                            //                                        //}
                            //                                    }
                            //                                 }
                        }
                    }
                }
            }
            else if (data.getNumberOfClusters(planeID) == 3)
            {
                if (theStubWindow->checkWindowAbout(colPredicted, rowPredicted, run, 0)) // overriden for stub only
                {
                    for (unsigned int nS = 0; nS < data.getNumberOfClusters(stubDetectorNumber_); nS++)
                    {
                        int xStubStripIndex;
                        float xStubMeasuredLocal = data.getXClusterMeasuredLocal(nS, stubDetectorNumber_);
                        if (fmod(xStubMeasuredLocal, maxPitchX) > 0)
                        {
                            xStubStripIndex = xStubMeasuredLocal / maxPitchX;
                        }
                        else
                        {
                            xStubStripIndex = xStubMeasuredLocal / maxPitchX - 1;
                        }
                        int stubDeviationFromPredicted = colPredicted - xStubStripIndex;
                        THREADED(hEfficiencyStubVsDeviationFromPredNorm3_[planeID])->Fill(deviationFromPredicted);
                        //THREADED(hEfficiencyStubVsStripNumberNorm_[planeID])->Fill(xclusterStripIndex);  // check again

                        if (abs(deviationFromPredicted - stubDeviationFromPredicted) < 5)
                        {
                            THREADED(hEfficiencyStubVsDeviationFromPred3_[planeID])->Fill(true, deviationFromPredicted);
                            //THREADED(hEfficiencyStubVsStripNumber_[planeID])->Fill(true,xclusterStripIndex); // check again
                        }
                    }
                }
            }
            else
            {
                if (theStubWindow->checkWindowAbout(colPredicted, rowPredicted, run, 0)) // overriden for stub only
                {

                    for (unsigned int nS = 0; nS < data.getNumberOfClusters(stubDetectorNumber_); nS++)
                    {
                        int xStubStripIndex;
                        float xStubMeasuredLocal = data.getXClusterMeasuredLocal(nS, stubDetectorNumber_);
                        if (fmod(xStubMeasuredLocal, maxPitchX) > 0)
                        {
                            xStubStripIndex = xStubMeasuredLocal / maxPitchX;
                        }
                        else
                        {
                            xStubStripIndex = xStubMeasuredLocal / maxPitchX - 1;
                        }
                        int stubDeviationFromPredicted = colPredicted - xStubStripIndex;
                        THREADED(hEfficiencyStubVsDeviationFromPredNorm_[planeID])->Fill(deviationFromPredicted);
                        //THREADED(hEfficiencyStubVsStripNumberNorm_[planeID])->Fill(xclusterStripIndex);  // check again
                        if (abs(deviationFromPredicted - stubDeviationFromPredicted) < 5)
                        {
                            THREADED(hEfficiencyStubVsDeviationFromPred_[planeID])->Fill(true, deviationFromPredicted);
                            //THREADED(hEfficiencyStubVsStripNumber_[planeID])->Fill(true,xclusterStripIndex); // check again
                        }
                    }
                }
            }
        } // cbcNumber loop
    }     // cluster loop
    //std::cout << __PRETTY_FUNCTION__ << "Done!" << std::endl;
}
//=======================================================================
void EfficiencyOuterTracker::cellEfficiency(bool pass, int planeID, const Data &data, int threadNumber)
{
    if (!pass || !data.getIsInDetector(planeID))
        return;
    //if(planeID==dut0DetectorNumber_ || planeID==dut1DetectorNumber_)
    //std::cout << __PRETTY_FUNCTION__ <<"PASS --> cell eff" <<std::endl;
    const Window *theWindow = theWindowsManager_->getWindow(planeID);
    float xRes = data.getXPixelResidualLocal(planeID);
    float yRes = data.getYPixelResidualLocal(planeID);
    float xRes4Cells = 0;
    float yRes4Cells = 0;
    int rowPredicted = data.getRowPredicted(planeID);
    int colPredicted = data.getColPredicted(planeID);
    int run = data.getRunNumber();
    float maxPitchX = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->getCellPitches().first).c_str());
    float maxPitchY = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->getCellPitches().second).c_str());
    /*
    if (planeID == dut0DetectorNumber_ || planeID == dut1DetectorNumber_){
        std::cout<<"maxPitchX = "<< maxPitchX <<std::endl;
        std::cout<<"maxPitchY = "<< maxPitchY <<std::endl;

        std::cout<<"data.getXPitchLocal = "<< data.getXPitchLocal(planeID) <<std::endl;
        std::cout<<"data.getYPitchLocal = "<< data.getYPitchLocal(planeID) <<std::endl;
    }
*/
    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    if (!theWindow->checkWindow(colPredicted, rowPredicted, run))
        return;

    if (data.getXPixelResidualLocal(planeID) > 0)
        xRes4Cells = data.getXPixelResidualLocal(planeID) - data.getXPitchLocal(planeID) / 2;
    else if (data.getXPixelResidualLocal(planeID) <= 0)
        xRes4Cells = data.getXPixelResidualLocal(planeID) + data.getXPitchLocal(planeID) / 2;

    if (data.getYPixelResidualLocal(planeID) > 0)
        yRes4Cells = data.getYPixelResidualLocal(planeID) - data.getYPitchLocal(planeID) / 2;
    else if (data.getYPixelResidualLocal(planeID) <= 0)
        yRes4Cells = data.getYPixelResidualLocal(planeID) + data.getYPitchLocal(planeID) / 2;

    if (theWindow->checkWindowAbout(colPredicted, rowPredicted, run, 0) && // chekWindowAbout overriden for strip only
            data.getXPitchLocal(planeID) <= maxPitchX &&
            data.getYPitchLocal(planeID) <= maxPitchY) // overriden for strip only
    {
        THREADED(hCellEfficiencyNorm_[planeID])->Fill(xRes, yRes);
        THREADED(h2D4CellEfficiencyNorm_[planeID])->Fill(xRes4Cells, yRes4Cells);

        if (((int)colPredicted) % 2 == 0)
            THREADED(hCellEfficiencyEvenColumnsNorm_[planeID])->Fill(xRes, yRes);
        else
            THREADED(hCellEfficiencyOddColumnsNorm_[planeID])->Fill(xRes, yRes);

        if (data.getHasHit(planeID))
        {
            THREADED(hCellEfficiency_[planeID])->Fill(xRes, yRes);
            THREADED(h2D4CellEfficiency_[planeID])->Fill(xRes4Cells, yRes4Cells);

            if (((int)colPredicted) % 2 == 0)
                THREADED(hCellEfficiencyEvenColumns_[planeID])->Fill(xRes, yRes);
            else
                THREADED(hCellEfficiencyOddColumns_[planeID])->Fill(xRes, yRes);
        }
    }
}
//=======================================================================
void EfficiencyOuterTracker::xCellEfficiency(bool pass, int planeID, const Data &data, int threadNumber)
{
    // #####################
    // # Internal constant #
    // #####################
    int maxClusterSize = 2;
    if (!pass || !data.getIsInDetector(planeID))
        return;
    const Window *theWindow = theWindowsManager_->getWindow(planeID);
    int rowPredicted = data.getRowPredicted(planeID);
    int colPredicted = data.getColPredicted(planeID);
    int run = data.getRunNumber();
    //int           event        = data.getEventChewieNumber();
    float maxPitchX = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->getCellPitches().first).c_str());
    float xRes = 0.;
    int clusterSize = data.getClusterSize(planeID);
    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    //std::cout<<"Before checkWindow : planeID = "<<planeID<<": clusterSize = " << clusterSize <<std::endl;
    if (!theWindow->checkWindowAbout(colPredicted, rowPredicted, run, 0))
        return;
    //std::cout<<"After checkWindow : planeID = "<<planeID<<": clusterSize = " << clusterSize <<std::endl;
    //nabin
    //if(clusterSize > 2) std::cout << clusterSize << std::endl;
    //    if(data.getHasHit(dut0DetectorNumber_) && data.getHasHit(dut1DetectorNumber_)) // inside this loop, there is nothing from other planes
    //    {
    //        if(data.getClusterSize(dut1DetectorNumber_) == 1 && data.getClusterSize(dut0DetectorNumber_) == 1){//MAKE SURE THIS IS CORRECT
    //            //float meanCol      = data.getMeanCol(dut1DetectorNumber_);
    //            //int rowPredicted23 = data.getRowPredicted(dut1DetectorNumber_);
    //            //float meancolDiff  = data.getMeanCol(dut1DetectorNumber_) - data.getMeanCol(dut0DetectorNumber_);
    //            //THREADED(hitDiffTwoStrips_[planeID])->Fill(meancolDiff);
    //            //THREADED(h2DDeltaXvsPredictedCol_[planeID])->Fill(meanCol,rowPredicted23);
    //            //std::cout << meancolDiff << std::endl;
    //        }
    //    }
    //std::cout << "___________________"<< std::endl;
    //end nabin
    if (data.getXPitchLocal(planeID) != maxPitchX)
        return;
    if (data.getXPixelResidualLocal(planeID) > 0)
        xRes = data.getXPixelResidualLocal(planeID) - maxPitchX / 2;
    else if (data.getXPixelResidualLocal(planeID) <= 0)
        xRes = data.getXPixelResidualLocal(planeID) + maxPitchX / 2;
    //if (planeID==dut0DetectorNumber_ || planeID==dut1DetectorNumber_)
    //std::cout<<"After checkWindowCoarseAbout : planeID = "<<planeID<<": clusterSize = " << clusterSize <<std::endl;
    THREADED(h1DXCellEfficiencyNorm_[planeID])->Fill(xRes); // divided by this to find eff in firsthit
    if (!data.getHasHit(planeID))
    {
        THREADED(h1DXCellEfficiencyFirstHit_[planeID])->Fill(false, xRes);
        THREADED(h1DXCellEfficiencySecondHit_[planeID])->Fill(false, xRes);
        THREADED(diffFirstSecondHist_[planeID])->Fill(false, xRes);
    }
    else
    {
        //cumulative+=clusterSize;
        //nevent+=1;
        //std::cout<<"Average = "<<cumulative/nevent<<std::endl;
        THREADED(h1DClusterSizes_[planeID])->Fill(clusterSize);
        int cutUm = 5;
        if (data.getXPixelResidualLocal(planeID) > -cutUm && data.getXPixelResidualLocal(planeID) < cutUm)
            THREADED(h1DClusterSizesCenterStrip_[planeID])->Fill(clusterSize);
        if (data.getXPixelResidualLocal(planeID) > maxPitchX / 2 - cutUm || data.getXPixelResidualLocal(planeID) < -maxPitchX / 2 + cutUm)
            THREADED(h1DClusterSizesBetweenStrips_[planeID])->Fill(clusterSize);


        THREADED(h1DClusterSizeVsXStripPosition_norm_[planeID])->Fill(xRes, 1);
        THREADED(h1DClusterSizeVsXStripPosition_[planeID])->Fill(xRes, data.getClusterSize(planeID));
        if (data.getClusterSize(planeID) == 1)
            THREADED(h1DClusterSize1VsXStripPosition_[planeID])->Fill(xRes);
        else if (data.getClusterSize(planeID) == 2)
            THREADED(h1DClusterSize2VsXStripPosition_[planeID])->Fill(xRes);
        else if (data.getClusterSize(planeID) == 3)
            THREADED(h1DClusterSize3VsXStripPosition_[planeID])->Fill(xRes);
        else if (data.getClusterSize(planeID) == 4)
            THREADED(h1DClusterSize4VsXStripPosition_[planeID])->Fill(xRes);
        else if (data.getClusterSize(planeID) == 5)
            THREADED(h1DClusterSize5VsXStripPosition_[planeID])->Fill(xRes);

        if (data.getClusterSize(planeID) > maxClusterSize) //maxClusterSize = 2
        {
            THREADED(h1DXCellEfficiencyFirstHit_[planeID])->Fill(true, xRes);
            THREADED(h1DXCellEfficiencySecondHit_[planeID])->Fill(true, xRes);
            THREADED(diffFirstSecondHist_[planeID])->Fill(true, xRes);
        }
        else
        {
            bool found = false;
            for (int h = 0; h < data.getClusterSize(planeID) && !found; h++) // i changed data.getClusterSize(planeID) to maxclusterSize.
                if (data.getClusterPixelCol(h, planeID) == colPredicted)
                    found = true;

            if (found)
            {
                THREADED(h1DXCellEfficiencyFirstHit_[planeID])->Fill(true, xRes);
                THREADED(h1DXCellEfficiencySecondHit_[planeID])->Fill(true, xRes);
                THREADED(diffFirstSecondHist_[planeID])->Fill(true, xRes);
            }
            else
            {
                THREADED(h1DXCellEfficiencyFirstHit_[planeID])->Fill(false, xRes);
                for (int h = 0; h < data.getClusterSize(planeID) && !found; h++)
                    if (((colPredicted - data.getClusterPixelCol(h, planeID)) == 1) || ((colPredicted - data.getClusterPixelCol(h, planeID)) == -1))
                        found = true;

                if (found)
                {
                    //std::cout<<"NO YOU DONT : ClusterSize = "<< h <<" ; colPredicted = "<< colPredicted <<" ; data.getClusterPixelCol(h,planeID) = "<< data.getClusterPixelCol(h,planeID)
                    //        <<" ; xRes = "<< xRes <<std::endl;
                    if (xRes > 0 && xRes < maxPitchX / 2) //beloing to the left strip
                    {
                        THREADED(CheckIneff_[planeID])->Fill(1);
                    }
                    else if (xRes < 0 && xRes > -maxPitchX / 2)
                    {
                        THREADED(CheckIneff_[planeID])->Fill(-1);
                    }
                    else
                    {
                        THREADED(CheckIneff_[planeID])->Fill(0);
                    }
                    THREADED(h1DXCellEfficiencySecondHit_[planeID])->Fill(true, xRes);
                }
                else
                    THREADED(h1DXCellEfficiencySecondHit_[planeID])->Fill(false, xRes);
            }
        }
    }
}
//=======================================================================
void EfficiencyOuterTracker::yCellEfficiency(bool pass, int planeID, const Data &data, int threadNumber)
{
    // #####################
    // # Internal constant #
    // #####################
    int maxClusterSize = 2;

    if (!pass || !data.getIsInDetector(planeID))
        return;

    const Window *theWindow = theWindowsManager_->getWindow(planeID);
    int rowPredicted = data.getRowPredicted(planeID);
    int colPredicted = data.getColPredicted(planeID);
    int run = data.getRunNumber();
    int event = data.getEventChewieNumber();
    float maxPitchY = atof(((theXmlParser_->getPlanes())[thePlaneMapping_->getPlaneName(planeID)]->getCellPitches().second).c_str());
    float yRes = 0.;

    //if(planeID  == dut1DetectorNumber_) std::cout << "maxPitchY: " << maxPitchY << std::endl;
    // #########################################
    // # Check if track and hits are in window #
    // #########################################
    if (!theWindow->checkWindow(colPredicted, rowPredicted, run))
        return;

    if (data.getYPitchLocal(planeID) == maxPitchY)
    {
        if (data.getYPixelResidualLocal(planeID) > 0)
            yRes = data.getYPixelResidualLocal(planeID) - data.getYPitchLocal(planeID) / 2;
        else if (data.getYPixelResidualLocal(planeID) <= 0)
            yRes = data.getYPixelResidualLocal(planeID) + data.getYPitchLocal(planeID) / 2;
    }
    else
        return;

    if (theWindow->checkWindowAbout(colPredicted, rowPredicted, run, 0) &&
            theWindow->checkTimeWindowAbout(colPredicted, event, run))
    {

        if (data.getHasHit(planeID))
        {
            if (data.getClusterSize(planeID) > maxClusterSize)
            {
                THREADED(h1DYCellEfficiencyFirstHit_[planeID])->Fill(true, yRes);
                THREADED(h1DYCellEfficiencySecondHit_[planeID])->Fill(true, yRes);
                return;
            }

            bool isOk = false;
            for (int h = 0; h < data.getClusterSize(planeID); h++)
            {
                if ((data.getClusterPixelCol(h, planeID) == colPredicted) && (data.getClusterPixelRow(h, planeID) == rowPredicted))
                {
                    THREADED(h1DYCellEfficiencyFirstHit_[planeID])->Fill(true, yRes);
                    THREADED(h1DYCellEfficiencySecondHit_[planeID])->Fill(true, yRes);
                    isOk = true;
                    break;
                }
            }

            if (isOk)
                return;
            else
            {
                isOk = false;
                for (int h = 0; h < data.getClusterSize(planeID); h++)
                {
                    if (data.getClusterPixelCol(h, planeID) == colPredicted)
                    {
                        if (((rowPredicted - data.getClusterPixelRow(h, planeID)) == 1) || ((rowPredicted - data.getClusterPixelRow(h, planeID)) == -1))
                        {
                            isOk = true;
                            break;
                        }
                    }
                }

                if (!isOk)
                    return;

                THREADED(h1DYCellEfficiencySecondHit_[planeID])->Fill(true, yRes);
            }
        }
        else
        {

            THREADED(h1DYCellEfficiencyNorm_[planeID])->Fill(yRes);
        }
    }
}
//=======================================================================
void EfficiencyOuterTracker::setCutsFormula(std::map<std::string, std::string> cutsList, std::vector<TTree *> tree)
{
    std::vector<TTreeFormula *> formulasVector;

    for (std::map<std::string, std::string>::iterator it = cutsList.begin(); it != cutsList.end(); it++)
    {
        if ((it->first) == "main cut" && (it->second).size() == 0)
            STDLINE("WARNING: no main cut set in efficiency analysis ! Default value = true !", ACRed);

        formulasVector.clear();
        if ((it->second).size() != 0)
        {
            for (unsigned int t = 0; t < tree.size(); t++)
                formulasVector.push_back(new TTreeFormula((it->second).c_str(), (it->second).c_str(), tree[t]));
            cutsFormulas_[it->first] = formulasVector;
        }
    }
}
//=======================================================================
bool EfficiencyOuterTracker::passStandardCuts(int planeID, const Data &data)
{
    if (!theXmlParser_->getAnalysesFromString("Efficiency")->standardCut())
        return true;

    int minHits = atoi(theXmlParser_->getAnalysesFromString("Efficiency")->getMinHits().c_str()) - 1;
    int excludeMe = 0;
    if (thePlaneMapping_->getPlaneName(planeID).find("Dut") != std::string::npos)
        minHits += 1;
    else if (data.getHasHit(planeID) && data.getClusterSize(planeID) <= 2)
        excludeMe = 1;

    if (data.getNumberOfTelescopeClustersSizeLE2() - excludeMe >= minHits)
        return true;
    else
        return false;
}
//=======================================================================
/*void EfficiencyOuterTracker::countClusterSize(int planeID, const Data &data)
{
    for (int event=0; event<data.; event++)
    {
      if (!planeID == dut0DetectorNumber_ || !planeID == dut1DetectorNumber_) return;

      std::list<std::vector> clusterDataList;

      int xLocalPitch = data.getXPitchLocal(planeID);
      int clusterSize = data.getClusterSize(planeID);
      int eventNumber = data.getEventNumber();

      std::vector<int> clusterData(1, eventNumber);
      clusterData.push_back(clusterSize);
      clusterData.push_back(xLocalPitch);

      clusterDataList.push_back(clusterData);
    }

    int data.getNu

}

*/
