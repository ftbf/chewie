/*===============================================================================
 * Chewie: the FERMILAB MTEST telescope and DUT anaysis tool
 *
 * Copyright (C) 2014
 *
 * Authors:
 *
 * Mauro Dinardo      (Universita' Bicocca)
 * Dario Menasce      (INFN)
 * Jennifer Ngadiuba  (INFN)
 * Lorenzo Uplegger   (FNAL)
 * Luigi Vigani       (INFN)
 *
 * INFN: Piazza della Scienza 3, Edificio U2, Milano, Italy 20126
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ================================================================================*/

#include "HistogramWindow.h"
#include "Data.h"
#include "MessageTools.h"
#include "PlanesMapping.h"

#include <TFile.h>
#include <TH2F.h>
#include <TAxis.h>
#include <TTree.h>
#include <TROOT.h>
#include <math.h>
#include <TFile.h>

#include <iostream>
#include <sstream>
#include <cassert>

///////////////////////////////////////////////////////////////////////////////////////////
HistogramWindow::HistogramWindow(std::string         name             	  ,
                                 int                 binXMin          	  ,
                                 int                 binXMax          	  ,
                                 int                 binYMin          	  ,
                                 int                 binYMax              ,
                                 std::string         pathToChewieFile     ,
                                 std::string         runNumberInChewieFile,
                                 std::map<int,int>   runNumberEntries     ,
                                 TFile             * hitMapsFile           ) :
    Window(name),
    thePlaneMapping_(nullptr)
{
    thePlaneMapping_ = new PlanesMapping();

    nBinsX_ = binXMax - binXMin +1;
    nBinsY_ = binYMax - binYMin +1;

    int coarseNBinsY_ = nBinsY_/10;

    if(coarseNBinsY_ == 0) coarseNBinsY_ = 1;
    for(std::map<int,int>::iterator runIt = runNumberEntries.begin(); runIt != runNumberEntries.end(); runIt++)
    {
        int EventBinSize = 1000;
        int totalEvents  = runIt->second;

        totalEvents/=EventBinSize;
        totalEvents*=EventBinSize;
        totalEvents+=EventBinSize;//to have a number multiple of EventBinSize

        std::stringstream ss;
        ss.str("");
        ss << runIt->first;

        theHRunWindow_              .insert(std::pair<int,TH2F*> (runIt->first , new TH2F ((name_+"_Window_Run"+ss.str()).c_str()                , (name_+"_Window_Run"+ss.str()).c_str()                , nBinsX_, binXMin, binXMax+1, nBinsY_                 , binYMin, binYMax+1  )));
        theHRunWindowCoarse_        .insert(std::pair<int,TH2F*> (runIt->first , new TH2F ((name_+"_WindowCoarse_Run"+ss.str()).c_str()          , (name_+"_WindowCoarse_Run"+ss.str()).c_str()          , nBinsX_, binXMin, binXMax+1, coarseNBinsY_           , binYMin, binYMax+1  )));

        theH2TimeWindow_         .insert(std::pair<int,TH2F*>(runIt->first, new TH2F((name_+"_TimeWindow_H2_Run"        +ss.str()).c_str(), (name_+"_TimeWindow_H2_Run"        +ss.str()).c_str(), nBinsX_, binXMin, binXMax+1, totalEvents/EventBinSize, 0      , totalEvents)));
        theH2TimeWindowNorm_     .insert(std::pair<int,TH2F*>(runIt->first, new TH2F((name_+"_TimeWindowNorm_H2_Run"    +ss.str()).c_str(), (name_+"_TimeWindowNorm_H2_Run"    +ss.str()).c_str(), nBinsX_, binXMin, binXMax+1, totalEvents/EventBinSize, 0      , totalEvents)));
        theH1TimeWindow_         .insert(std::pair<int,TH1F*>(runIt->first, new TH1F((name_+"_TimeWindow_Run"           +ss.str()).c_str(), (name_+"_TimeWindow_Run"           +ss.str()).c_str(), nBinsX_, binXMin, binXMax+1                                                )));

        theHWindowClusterSize1_  .insert(std::pair<int,TH2F*>(runIt->first, new TH2F((name_+"_WindowClusterSize1_Run"   +ss.str()).c_str(), (name_+"_WindowClusterSize1_Run"   +ss.str()).c_str(), nBinsX_, binXMin, binXMax+1, nBinsY_                 , binYMin, binYMax+1  )));
        theHWindowClusterSize2_  .insert(std::pair<int,TH2F*>(runIt->first, new TH2F((name_+"_WindowClusterSize2_Run"   +ss.str()).c_str(), (name_+"_WindowClusterSize2_Run"   +ss.str()).c_str(), nBinsX_, binXMin, binXMax+1, nBinsY_                 , binYMin, binYMax+1  )));
        theHWindowClusterSize3_  .insert(std::pair<int,TH2F*>(runIt->first, new TH2F((name_+"_WindowClusterSize3_Run"   +ss.str()).c_str(), (name_+"_WindowClusterSize3_Run"   +ss.str()).c_str(), nBinsX_, binXMin, binXMax+1, nBinsY_                 , binYMin, binYMax+1  )));
        theHWindowClusterSize4_  .insert(std::pair<int,TH2F*>(runIt->first, new TH2F((name_+"_WindowClusterSize4_Run"   +ss.str()).c_str(), (name_+"_WindowClusterSize4_Run"   +ss.str()).c_str(), nBinsX_, binXMin, binXMax+1, nBinsY_                 , binYMin, binYMax+1  )));

        theEfficiencyVsEvent_    .insert(std::pair<int,TH1F*>(runIt->first, new TH1F((name_+"_EfficiencyVsEvent_Run"    +ss.str()).c_str(), (name_+"_EfficiencyVsEvent_Run"    +ss.str()).c_str(), totalEvents/250, 0, totalEvents)));
        theEfficiencyVsEventNorm_.insert(std::pair<int,TH1F*>(runIt->first, new TH1F((name_+"_EfficiencyVsEventNorm_Run"+ss.str()).c_str(), (name_+"_EfficiencyVsEventNorm_Run"+ss.str()).c_str(), totalEvents/250, 0, totalEvents)));
        theEfficiencyVsEventCut_ .insert(std::pair<int,TH1F*>(runIt->first, new TH1F((name_+"_EfficiencyVsEventCut_Run" +ss.str()).c_str(), (name_+"_EfficiencyVsEventCut_Run" +ss.str()).c_str(), totalEvents/250, 0, totalEvents)));
    }
    if(pathToChewieFile == "")
    {
        windowsFromFile_ = false;
        theHWindow_              = new TH2F ((name_+"_Window").c_str()      , (name_+"_Window").c_str()      , nBinsX_, binXMin, binXMax+1, nBinsY_      , binYMin, binYMax+1  );
        theHWindowCoarse_        = new TH2F ((name_+"_WindowCoarse").c_str(), (name_+"_WindowCoarse").c_str(), nBinsX_, binXMin, binXMax+1, coarseNBinsY_, binYMin, binYMax+1  );

    }
    else
    {
        TDirectory* currentDir = gDirectory->CurrentDirectory();
        windowsFromFile_ = true;
        TFile* file = new TFile(pathToChewieFile.c_str());
        if(!file->IsOpen())
        {
            std::cout << __PRETTY_FUNCTION__ << "Can't open file: " << pathToChewieFile << " for " << name_ << ". This file is defined in the Windows tab.\nIf you don't want to use this file to build your window then you can uncheck the box close to the file name...exiting now" << std::endl;
            assert(0);
        }
        currentDir->cd();
        std::cout << __PRETTY_FUNCTION__ << "look for histo: " << ("/Windows/" + name_ + "/" + name_+"_Window").c_str() << std::endl;
        theHWindow_              = static_cast<TH2F*>(file->Get(("/Windows/" + name_ + "/" + name_+"_Window").c_str())->Clone());
        theHWindowCoarse_        = static_cast<TH2F*>(file->Get(("/Windows/" + name_ + "/" + name_+"_WindowCoarse").c_str())->Clone());
        file->Close();
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
HistogramWindow::~HistogramWindow(void)
{
    if (thePlaneMapping_) delete thePlaneMapping_;

    if(!Window::fDoNotDelete_)
    {
        delete theHWindow_;
        delete theHWindowCoarse_;
    }

    for(std::map<int,TH2F*>::iterator it=theHRunWindow_.begin(); it != theHRunWindow_.end(); ++it)
    {
        if(!Window::fDoNotDelete_ && it->second )
            delete it->second;
    }

    for(std::map<int,TH2F*>::iterator it=theHRunWindowCoarse_.begin(); it != theHRunWindowCoarse_.end(); ++it)
    {
        if(!Window::fDoNotDelete_ && it->second )
            delete it->second;
    }

    for(std::map<int,TH2F*>::iterator it=theHWindowClusterSize1_.begin(); it != theHWindowClusterSize1_.end(); ++it)
    {
        if(!Window::fDoNotDelete_ && it->second )
            delete it->second;
    }

    for(std::map<int,TH2F*>::iterator it=theHWindowClusterSize2_.begin(); it != theHWindowClusterSize2_.end(); ++it)
    {
        if(!Window::fDoNotDelete_ && it->second )
            delete it->second;
    }

    for(std::map<int,TH2F*>::iterator it=theHWindowClusterSize3_.begin(); it != theHWindowClusterSize3_.end(); ++it)
    {
        if(!Window::fDoNotDelete_ && it->second )
            delete it->second;
    }

    for(std::map<int,TH2F*>::iterator it=theHWindowClusterSize4_.begin(); it != theHWindowClusterSize4_.end(); ++it)
    {
        if(!Window::fDoNotDelete_ && it->second )
            delete it->second;
    }

    for(std::map<int,TH1F*>::iterator it=theH1TimeWindow_.begin(); it != theH1TimeWindow_.end(); ++it)
    {
        if(!Window::fDoNotDelete_ && it->second )
            delete it->second;
    }

    for(std::map<int,TH2F*>::iterator it=theH2TimeWindow_.begin(); it != theH2TimeWindow_.end(); ++it)
    {
        if(!Window::fDoNotDelete_ && it->second )
            delete it->second;
    }

    for(std::map<int,TH2F*>::iterator it=theH2TimeWindowNorm_.begin(); it != theH2TimeWindowNorm_.end(); ++it)
    {
        if(!Window::fDoNotDelete_ && it->second )
            delete it->second;
    }

    for(std::map<int,TH1F*>::iterator it=theEfficiencyVsEvent_.begin(); it != theEfficiencyVsEvent_.end(); ++it)
    {
        if(!Window::fDoNotDelete_ && it->second )
            delete it->second;
    }

    for(std::map<int,TH1F*>::iterator it=theEfficiencyVsEventNorm_.begin(); it != theEfficiencyVsEventNorm_.end(); ++it)
    {
        if(!Window::fDoNotDelete_ && it->second )
            delete it->second;
    }

    for(std::map<int,TH1F*>::iterator it=theEfficiencyVsEventCut_.begin(); it != theEfficiencyVsEventCut_.end(); ++it)
    {
        if(!Window::fDoNotDelete_ && it->second )
            delete it->second;
    }

    //if(hitMap_) delete hitMap_;
}

///////////////////////////////////////////////////////////////////////////////////////////
void HistogramWindow::endJob()
{
    calculateTimeEfficiency();
    if(!windowsFromFile_)
    {
        for(std::map<int,TH2F*>::iterator it=theHRunWindow_.begin(); it != theHRunWindow_.end(); ++it)
        {
            theHWindow_->Add(it->second);
        }
        for(std::map<int,TH2F*>::iterator it=theHRunWindowCoarse_.begin(); it != theHRunWindowCoarse_.end(); ++it)
        {
            theHWindowCoarse_->Add(it->second);
        }
    }

}
///////////////////////////////////////////////////////////////////////////////////////////
bool HistogramWindow::checkWindow(float col, float row, int runNumber) const
{
//    TAxis* xAxisH = hitMap_->GetXaxis();
//    TAxis* yAxisH = hitMap_->GetYaxis();

    TH2F* window = theHWindow_;
    if(runNumber != -1)
        window = theHRunWindow_.find(runNumber)->second;
    TAxis* xAxisW = window->GetXaxis();
    TAxis* yAxisW = window->GetYaxis();

//    if((name_.find("Dut")!=std::string::npos) && (hitMap_!=NULL))
//        if(hitMap_->GetBinContent(xAxisH->FindBin(col), yAxisH->FindBin(row)) == 0) return false;

    if((xAxisW->FindBin(col) == 0) || (yAxisW->FindBin(row) == 0)) return false;

    if(window->GetBinContent(xAxisW->FindBin(col), yAxisW->FindBin(row)) != 0)
    {
        return true;
    }
    return false;
}
///////////////////////////////////////////////////////////////////////////////////////////
bool HistogramWindow::checkWindowCoarse(float col, float row, int runNumber) const
{
    TH2F* window = theHWindowCoarse_;
    if(runNumber != -1)
        window = theHRunWindowCoarse_.find(runNumber)->second;
    TAxis* xAxis = window->GetXaxis();
    TAxis* yAxis = window->GetYaxis();

    if((xAxis->FindBin(col) == 0) || (yAxis->FindBin(row) == 0)) return false;
    if(window->GetBinContent(xAxis->FindBin(col), yAxis->FindBin(row)) != 0)
    {
        std::cout << __PRETTY_FUNCTION__ << "col: " << col << " row: " << row << " bin: " << window->GetBinContent(xAxis->FindBin(col), yAxis->FindBin(row)) << std::endl;
        return true;
    }
    return false;
}

///////////////////////////////////////////////////////////////////////////////////////////
bool HistogramWindow::checkWindowAbout(float col, float row, int runNumber, int type) const
{
    TH2F* window = theHWindow_;
    if(runNumber != -1)
        window = theHRunWindow_.find(runNumber)->second;
    TAxis* xAxis = window->GetXaxis() ;
    TAxis* yAxis = window->GetYaxis() ;
    if((xAxis->FindBin(col) == 0) || (yAxis->FindBin(row) == 0)) return false;
    if(type==0)
    {
        if(window->GetBinContent(xAxis->FindBin(col  ),yAxis->FindBin(row  )) != 0 &&
                window->GetBinContent(xAxis->FindBin(col-1),yAxis->FindBin(row  )) != 0 &&
                window->GetBinContent(xAxis->FindBin(col+1),yAxis->FindBin(row  )) != 0 &&
                window->GetBinContent(xAxis->FindBin(col  ),yAxis->FindBin(row-1)) != 0 &&
                window->GetBinContent(xAxis->FindBin(col-1),yAxis->FindBin(row-1)) != 0 &&
                window->GetBinContent(xAxis->FindBin(col+1),yAxis->FindBin(row-1)) != 0 &&
                window->GetBinContent(xAxis->FindBin(col  ),yAxis->FindBin(row+1)) != 0 &&
                window->GetBinContent(xAxis->FindBin(col-1),yAxis->FindBin(row+1)) != 0 &&
                window->GetBinContent(xAxis->FindBin(col+1),yAxis->FindBin(row+1)) != 0 )
            return true;
        else
            return false;
    }
    else if(type==1) //strip
    {
        if(window->GetBinContent(xAxis->FindBin(col),1) != 0 &&
                window->GetBinContent(xAxis->FindBin(col-1),1) != 0 &&
                window->GetBinContent(xAxis->FindBin(col+1),1) != 0 )
            return true;
        else
            return false;
    }
    else
    {
        std::cout << __PRETTY_FUNCTION__ << "I only know 2 types (0, 1) while this is type: " << type << ". IMPOSSIBLE!" << " row: " << row << " col: " << col << std::endl;
        assert(0);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
bool HistogramWindow::checkWindowCoarseAbout(float col, float row, int runNumber) const
{
    TH2F* window = theHWindowCoarse_;
    if(runNumber != -1)
        window = theHRunWindowCoarse_.find(runNumber)->second;

    TAxis* xAxis = window->GetXaxis() ;
    TAxis* yAxis = window->GetYaxis() ;
    if((xAxis->FindBin(col) == 0) || (yAxis->FindBin(row) == 0)) return false;
    if(window->GetBinContent(xAxis->FindBin(col  ),yAxis->FindBin(row  )) != 0 &&
            window->GetBinContent(xAxis->FindBin(col-1),yAxis->FindBin(row  )) != 0 &&
            window->GetBinContent(xAxis->FindBin(col+1),yAxis->FindBin(row  )) != 0 &&
            window->GetBinContent(xAxis->FindBin(col  ),yAxis->FindBin(row-1)) != 0 &&
            window->GetBinContent(xAxis->FindBin(col-1),yAxis->FindBin(row-1)) != 0 &&
            window->GetBinContent(xAxis->FindBin(col+1),yAxis->FindBin(row-1)) != 0 &&
            window->GetBinContent(xAxis->FindBin(col  ),yAxis->FindBin(row+1)) != 0 &&
            window->GetBinContent(xAxis->FindBin(col-1),yAxis->FindBin(row+1)) != 0 &&
            window->GetBinContent(xAxis->FindBin(col+1),yAxis->FindBin(row+1)) != 0 )
        return true;
    return false;
}

////////////////////////////////////////////////////////////////////////////////////
bool HistogramWindow::checkTimeWindow(float col, int eventNumber, int runNumber) const
{
    if(theH1TimeWindow_.find(runNumber)->second->GetBinContent(col) >=  eventNumber)
        return true;
    else
        return false;
}

///////////////////////////////////////////////////////////////////////////////////////
bool HistogramWindow::checkTimeWindowAbout(float col, int eventNumber, int runNumber) const
{
    if(theH1TimeWindow_.find(runNumber)->second->GetBinContent(col-1) >=  eventNumber &&
            theH1TimeWindow_.find(runNumber)->second->GetBinContent(col+1) >=  eventNumber &&
            theH1TimeWindow_.find(runNumber)->second->GetBinContent(col  ) >=  eventNumber )
        return true;
    else
        return false;
}

///////////////////////////////////////////////////////////////////////////////////////
void HistogramWindow::calculateWindow(int planeID, const Data& data, int lowerCol, int higherCol, int lowerRow, int higherRow)
{
//    std::cout<<"-- Calculate Window --"<<std::endl;
//    std::cout<<"Looking at "<<thePlaneMapping_->getPlaneName(planeID)<<std::endl;
//    std::cout<<"data.getMeanCol ("<<planeID<<") = "<<data.getMeanCol(planeID)<<std::endl;
//    std::cout<<"data.getMeanRow ("<<planeID<<") = "<<data.getMeanRow(planeID)<<std::endl;
//    std::cout<<"data.getNumberOfRows ("<<planeID<<") = "<<data.getNumberOfRows(planeID)<<std::endl;
//    std::cout<<"data.getNumberOfCols ("<<planeID<<") = "<<data.getNumberOfCols(planeID)<<std::endl;
//    std::cout<<"data.getRunNumber () = "<<data.getRunNumber()<<std::endl;
//    std::cout<<"data.getClusterSize ("<<planeID<<") = "<<data.getClusterSize(planeID)<<std::endl;
    float col   = data.getMeanCol(planeID);
    float row   = data.getMeanRow(planeID);
    int   rowPredicted = data.getRowPredicted(planeID);
    int   colPredicted = data.getColPredicted(planeID);
    int   nRow  = data.getNumberOfRows(planeID);
    int   nCol  = data.getNumberOfCols(planeID);
    int   run   = data.getRunNumber();
    int   entry = data.getEventChewieNumber();
    int   size  = data.getClusterSize(planeID);

    if (data.getIsInDetector(planeID) && rowPredicted >= lowerRow && rowPredicted <= higherRow && colPredicted >= lowerCol && colPredicted <= higherCol)
    {
        if(theH2TimeWindowNorm_.find(run) == theH2TimeWindowNorm_.end() || theH2TimeWindow_.find(run) == theH2TimeWindow_.end())
        {
            static bool alreadySeen = false ;
            if(  alreadySeen ) return ;
            if( !alreadySeen ) alreadySeen = true ;
            STDLINE("Time Window or ClusterSize data missing: skipping event (this message will NOT be repeated...)",ACRed) ;
        }
        else
        {
            theH2TimeWindowNorm_.find(run)->second->Fill(colPredicted, entry);
            theEfficiencyVsEventNorm_.find(run)->second->Fill(entry);
            if (data.getHasHit(planeID))
            {
                theH2TimeWindow_.find(run)->second->Fill(colPredicted, entry);
                theEfficiencyVsEvent_.find(run)->second->Fill(entry);
            }
        }
    }

    if (    data.getHasHit(planeID)       &&
            data.getIsInDetector(planeID) &&
            row >= lowerRow               &&
            col >= lowerCol               &&
            row <= higherRow              &&
            col <= higherCol                 )
    {

        // Strips only have columns, so use rowPredicted in place of row for them
        int type = thePlaneMapping_->getPlaneType(planeID);
        if (type == 1) row = rowPredicted;

        if (nRow == 1 && nCol == 1)
        {
            theHRunWindow_      .find(run)->second->Fill(col,row);
            theHRunWindowCoarse_.find(run)->second->Fill(col,row);

            if (size == 1)
                theHWindowClusterSize1_.find(run)->second->Fill(col,row);
        }
        else if (nRow > 1 && nCol == 1)
        {
            if (ceil(nRow/2.) != nRow/2.) // nRow odd
            {
                if (size == 2)
                    theHWindowClusterSize2_.find(run)->second->Fill(col,row);
                else if (size == 3)
                    theHWindowClusterSize3_.find(run)->second->Fill(col,row);
                else if (size == 4)
                    theHWindowClusterSize4_.find(run)->second->Fill(col,row);

                for (int r = 1; r < nRow; r++)
                {
                    if (ceil(r/2.) == r/2.)
                    {
                        if ((row+r/2.) <= higherRow)
                        {
                            theHRunWindow_.find(run)->second->Fill(col,row+r/2.);
                            theHRunWindowCoarse_.find(run)->second->Fill(col,row);

                            if (size == 2)
                                theHWindowClusterSize2_.find(run)->second->Fill(col,row+r/2.);
                            else if (size == 3)
                                theHWindowClusterSize3_.find(run)->second->Fill(col,row+r/2.);
                            else if (size == 4)
                                theHWindowClusterSize4_.find(run)->second->Fill(col,row+r/2.);
                        }

                        if ((row-r/2.) >= lowerRow)
                        {
                            theHRunWindow_.find(run)->second->Fill(col,row-r/2.);
                            theHRunWindowCoarse_.find(run)->second->Fill(col,row);

                            if (size == 2)
                                theHWindowClusterSize2_.find(run)->second->Fill(col,row-r/2.);
                            else if (size == 3)
                                theHWindowClusterSize3_.find(run)->second->Fill(col,row-r/2.);
                            else if (size == 4)
                                theHWindowClusterSize4_.find(run)->second->Fill(col,row-r/2.);
                        }
                    }
                }
            }
            else // nRow even
            {
                for (int r = 1; r < nRow; r++)
                {
                    if (ceil(r/2.) != r/2.)
                    {
                        if ((row+r/2.) <= higherRow)
                        {
                            theHRunWindow_.find(run)->second->Fill(col,row+r/2.);
                            theHRunWindowCoarse_.find(run)->second->Fill(col,row);

                            if (size == 2)
                                theHWindowClusterSize2_.find(run)->second->Fill(col,row+r/2.);
                            else if (size == 3)
                                theHWindowClusterSize3_.find(run)->second->Fill(col,row+r/2.);
                            else if (size == 4)
                                theHWindowClusterSize4_.find(run)->second->Fill(col,row+r/2.);
                        }

                        if ((row-r/2.) >= lowerRow)
                        {
                            theHRunWindow_.find(run)->second->Fill(col,row-r/2.);
                            theHRunWindowCoarse_.find(run)->second->Fill(col,row);

                            if (size == 2)
                                theHWindowClusterSize2_.find(run)->second->Fill(col,row-r/2.);
                            else if (size == 3)
                                theHWindowClusterSize3_.find(run)->second->Fill(col,row-r/2.);
                            else if (size == 4)
                                theHWindowClusterSize4_.find(run)->second->Fill(col,row-r/2.);
                        }
                    }
                }
            }
        }
        else if (nCol > 1 && nRow == 1)
        {
            if (ceil(nCol/2.) != nCol/2.) // nCol odd
            {
                theHRunWindow_.find(run)->second->Fill(col,row);
                theHRunWindowCoarse_.find(run)->second->Fill(col,row);

                if (size == 2)
                    theHWindowClusterSize2_.find(run)->second->Fill(col,row);
                if (size == 3)
                    theHWindowClusterSize3_.find(run)->second->Fill(col,row);
                if (size == 4)
                    theHWindowClusterSize4_.find(run)->second->Fill(col,row);

                for (int c=1; c<nCol; c++)
                {
                    if (ceil(c/2.) == c/2.)
                    {
                        if ((col+c/2.) <= higherCol)
                        {
                            theHRunWindow_.find(run)->second->Fill(col+c/2.,row);
                            theHRunWindowCoarse_.find(run)->second->Fill(col,row);

                            if (size == 2)
                                theHWindowClusterSize2_.find(run)->second->Fill(col+c/2.,row);
                            else if (size == 3)
                                theHWindowClusterSize3_.find(run)->second->Fill(col+c/2.,row);
                            else if (size == 4)
                                theHWindowClusterSize4_.find(run)->second->Fill(col+c/2.,row);
                        }

                        if ((col-c/2.) >= lowerCol)
                        {
                            theHRunWindow_.find(run)->second->Fill(col-c/2.,row);
                            theHRunWindowCoarse_.find(run)->second->Fill(col,row);

                            if (size == 2)
                                theHWindowClusterSize2_.find(run)->second->Fill(col-c/2.,row);
                            else if (size == 3)
                                theHWindowClusterSize3_.find(run)->second->Fill(col-c/2.,row);
                            else if (size == 4)
                                theHWindowClusterSize4_.find(run)->second->Fill(col-c/2.,row);
                        }
                    }
                }
            }
            else // nCol even
            {
                for (int c = 1; c < nCol; c++)
                {
                    if (ceil(c/2.) != c/2.)
                    {
                        if ((col+c/2.) <= higherCol)
                        {
                            theHRunWindow_.find(run)->second->Fill(col+c/2.,row);
                            theHRunWindowCoarse_.find(run)->second->Fill(col,row);

                            if (size == 2)
                                theHWindowClusterSize2_.find(run)->second->Fill(col+c/2.,row);
                            else if (size == 3)
                                theHWindowClusterSize3_.find(run)->second->Fill(col+c/2.,row);
                            else if (size == 4)
                                theHWindowClusterSize4_.find(run)->second->Fill(col+c/2.,row);
                        }

                        if ((col-c/2.) >= lowerCol)
                        {
                            theHRunWindow_.find(run)->second->Fill(col-c/2.,row);
                            theHRunWindowCoarse_.find(run)->second->Fill(col,row);

                            if (size == 2)
                                theHWindowClusterSize2_.find(run)->second->Fill(col-c/2.,row);
                            else if (size == 3)
                                theHWindowClusterSize3_.find(run)->second->Fill(col-c/2.,row);
                            else if (size == 4)
                                theHWindowClusterSize4_.find(run)->second->Fill(col-c/2.,row);
                        }
                    }
                }
            }
        }
    }
}

////////////////////////////////////////////////////////////////////////////////////
void HistogramWindow::removePixel(int col, int row)
{
    for(std::map<int,TH2F*>::iterator it=theHRunWindow_.begin(); it != theHRunWindow_.end(); ++it)
    {
        TAxis* xAxis = it->second->GetXaxis() ;
        TAxis* yAxis = it->second->GetYaxis() ;

        it->second->SetBinContent(xAxis->FindBin(col),yAxis->FindBin(row),0);
    }
}

////////////////////////////////////////////////////////////////////////////////////
int HistogramWindow::getNumberOfEvents (void)
{
    int numberOfEvents=0;

    for(std::map<int,TH2F*>::iterator it=theHRunWindow_.begin(); it != theHRunWindow_.end(); ++it)
        numberOfEvents+=it->second->GetEntries();

    return numberOfEvents;
}

/////////////////////////////////////////////////////////////////////////////////////
void HistogramWindow::calculateTimeEfficiency(void)
{
    for(std::map<int,TH2F*>::iterator it=theH2TimeWindow_.begin(); it!=theH2TimeWindow_.end(); ++it)
    {
        it->second->Divide(theH2TimeWindowNorm_.find(it->first)->second);

        for(int c=1; c<=it->second->GetXaxis()->GetNbins(); ++c)
        {
            int eventDCFreezing=0;
            int maxEventBin = it->second->GetYaxis()->GetNbins();

            if(it->second->GetBinContent(c,maxEventBin)!=0) eventDCFreezing=it->second->GetYaxis()->GetBinUpEdge(maxEventBin);

            else
            {
                for(int t=it->second->GetYaxis()->GetNbins(); t>0; --t)
                {
                    if(it->second->GetBinContent(c,t)!=0)
                    {
                        eventDCFreezing=it->second->GetYaxis()->GetBinLowEdge(t);
                        break;
                    }
                }
            }

            theH1TimeWindow_.find(it->first)->second->Fill(it->second->GetXaxis()->GetBinCenter(c),eventDCFreezing);
        }
    }

    for(std::map<int,TH1F*>::iterator it=theEfficiencyVsEvent_.begin(); it!=theEfficiencyVsEvent_.end(); ++it)
    {
        it->second->Divide(theEfficiencyVsEventNorm_[it->first]);
    }

    for(std::map<int,TH1F*>::iterator it=theEfficiencyVsEvent_.begin(); it!=theEfficiencyVsEvent_.end(); ++it)
    {
        double dynamicSum = 0;
        double dynamicMean = 1;
        double dynamicRMS  = 1;
        double sigmaSquared;
        double binContent;
        double oldMean = 1;
        double oldRMS  = 1;
        bool   badBin0 = false;
        bool   badBin1 = false;
        bool   isKeep  = false;
        int    badBin  = it->second->GetXaxis()->GetNbins();

        for(int bin=1; bin<=(it->second->GetXaxis()->GetNbins()); bin++)
        {
            binContent = it->second->GetBinContent(bin);
            dynamicSum += binContent;
            dynamicMean = dynamicSum / bin;
            sigmaSquared = 0;

            if(bin != 1)
            {
                for(int dynamicBin=1; dynamicBin <= bin; dynamicBin++)
                {
                    sigmaSquared += pow((dynamicMean - it->second->GetBinContent(dynamicBin)), 2);
                }

                sigmaSquared /= bin;
                sigmaSquared = sqrt(sigmaSquared);
                if(sigmaSquared < 0.01) sigmaSquared = 0.01;
                dynamicRMS = sigmaSquared;
                if(binContent < (oldMean - 10*oldRMS))
                {
                    isKeep = true;
                    if(badBin0)
                    {
                        if(badBin1)
                        {
                            badBin = bin - 3;
                            if(badBin <= 0) badBin=1;
                            break;
                        }

                        badBin1 = true;
                    }

                    badBin0 = true;
                }
                else
                {
                    badBin0 = false;
                    badBin1 = false;
                    isKeep  = false;
                }
            }
            if(!isKeep)
            {
                oldMean = dynamicMean;
                oldRMS  = dynamicRMS;
            }
        }

        for(int bin=1; bin<=badBin; bin++)
            theEfficiencyVsEventCut_[it->first]->SetBinContent(bin, it->second->GetBinContent(bin));

        //std::cout << __PRETTY_FUNCTION__ << "Last event: " << theEfficiencyVsEventCut_[it->first]->GetXaxis()->GetBinUpEdge(badBin) << std::endl;
        int lastEvent = theEfficiencyVsEventCut_[it->first]->GetXaxis()->GetBinUpEdge(badBin);
        if( lastEvent > 1000)
            lastEvent -= 1000;
        theEfficiencyVsEventCut_[it->first]->GetXaxis()->SetRangeUser(0, lastEvent);
        lastBinCut_[it->first] = lastEvent;
    }
}

///////////////////////////////////////////////////////////////////////////////////////////
bool HistogramWindow::checkRunEntry(int runNumber, int entry) const
{
    if(entry >  lastBinCut_.find(runNumber)->second)
        return false;
    return true;
}
