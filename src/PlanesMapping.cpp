/*===============================================================================
 * Chewie: the FERMILAB MTEST telescope and DUT anaysis tool
 * 
 * Copyright (C) 2014 
 *
 * Authors:
 *
 * Mauro Dinardo      (Universita' Bicocca) 
 * Dario Menasce      (INFN) 
 * Jennifer Ngadiuba  (INFN)
 * Lorenzo Uplegger   (FNAL)
 * Luigi Vigani       (INFN)
 *
 * INFN: Piazza della Scienza 3, Edificio U2, Milano, Italy 20126
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 ================================================================================*/

#include "PlanesMapping.h"
#include "MessageTools.h"
#include <sstream>

//////////////////////////////////////////////////////////////////////////
PlanesMapping::PlanesMapping()
{
    planeToPosition_["Strip_Telescope_Upstream0"]   = 0;
    planeToPosition_["Strip_Telescope_Upstream1"]   = 1;
    planeToPosition_["Strip_Telescope_Upstream2"]   = 2;
    planeToPosition_["Strip_Telescope_Upstream3"]   = 3;
    planeToPosition_["Strip_Telescope_Upstream4"]   = 4;
    planeToPosition_["Strip_Telescope_Upstream5"]   = 5;
    planeToPosition_["Telescope_Downstream0"]       = 6;
    planeToPosition_["Telescope_Downstream1"]       = 7;
    planeToPosition_["Telescope_Downstream2"]       = 8;
    planeToPosition_["Telescope_Downstream3"]       = 9;
    planeToPosition_["Strip_Telescope_Downstream0"] = 10;
    planeToPosition_["Strip_Telescope_Downstream1"] = 11;
    planeToPosition_["Strip_Telescope_Downstream2"] = 12;
    planeToPosition_["Strip_Telescope_Downstream3"] = 13;
    planeToPosition_["Strip_Telescope_Downstream4"] = 14;
    planeToPosition_["Strip_Telescope_Downstream5"] = 15;
    planeToPosition_["Dut0"]                        = 16;
    planeToPosition_["Dut1"]                        = 17;
//    planeToPosition_["Dut2"]                        = 24;
//    planeToPosition_["Dut3"]                        = 25;
//    planeToPosition_["Dut4"]                        = 26;
//    planeToPosition_["Dut5"]                        = 27;

    positionToPlane_[0 ]  = "Strip_Telescope_Upstream0"  ;
    positionToPlane_[1 ]  = "Strip_Telescope_Upstream1"  ;
    positionToPlane_[2 ]  = "Strip_Telescope_Upstream2"  ;
    positionToPlane_[3 ]  = "Strip_Telescope_Upstream3"  ;
    positionToPlane_[4 ]  = "Strip_Telescope_Upstream4"  ;
    positionToPlane_[5 ]  = "Strip_Telescope_Upstream5"  ;
    positionToPlane_[6 ]  = "Telescope_Downstream0"      ;
    positionToPlane_[7 ]  = "Telescope_Downstream1"      ;
    positionToPlane_[8 ]  = "Telescope_Downstream2"      ;
    positionToPlane_[9 ]  = "Telescope_Downstream3"      ;
    positionToPlane_[10]  = "Strip_Telescope_Downstream0";
    positionToPlane_[11]  = "Strip_Telescope_Downstream1";
    positionToPlane_[12]  = "Strip_Telescope_Downstream2";
    positionToPlane_[13]  = "Strip_Telescope_Downstream3";
    positionToPlane_[14]  = "Strip_Telescope_Downstream4";
    positionToPlane_[15]  = "Strip_Telescope_Downstream5";
    positionToPlane_[16]  = "Dut0"                       ;
    positionToPlane_[17]  = "Dut1"                       ;
//    positionToPlane_[24]  = "Dut2"                       ;
//    positionToPlane_[25]  = "Dut3"                       ;
//    positionToPlane_[26]  = "Dut4"                       ;
//    positionToPlane_[27]  = "Dut5"                       ;

    positionToMonicelliPlane_[0 ]  = "Station: 0 - Plaq: 0";
    positionToMonicelliPlane_[1 ]  = "Station: 0 - Plaq: 1";
    positionToMonicelliPlane_[2 ]  = "Station: 0 - Plaq: 2";
    positionToMonicelliPlane_[3 ]  = "Station: 0 - Plaq: 3";
    positionToMonicelliPlane_[4 ]  = "Station: 0 - Plaq: 4";
    positionToMonicelliPlane_[5 ]  = "Station: 0 - Plaq: 5";

    positionToMonicelliPlane_[6 ]  = "Station: 4 - Plaq: 0";
    positionToMonicelliPlane_[7 ]  = "Station: 4 - Plaq: 1";
    positionToMonicelliPlane_[8 ]  = "Station: 4 - Plaq: 2";
    positionToMonicelliPlane_[9 ]  = "Station: 4 - Plaq: 3";

    positionToMonicelliPlane_[10]  = "Station: 1 - Plaq: 0";
    positionToMonicelliPlane_[11]  = "Station: 1 - Plaq: 1";
    positionToMonicelliPlane_[12]  = "Station: 1 - Plaq: 2";
    positionToMonicelliPlane_[13]  = "Station: 1 - Plaq: 3";
    positionToMonicelliPlane_[14]  = "Station: 1 - Plaq: 4";
    positionToMonicelliPlane_[15]  = "Station: 1 - Plaq: 5";
//    positionToMonicelliPlane_[22]  = "Station: 4 - Plaq: 0";
//    positionToMonicelliPlane_[23]  = "Station: 4 - Plaq: 1";
    positionToMonicelliPlane_[16]  = "Station: 5 - Plaq: 0";
    positionToMonicelliPlane_[17]  = "Station: 5 - Plaq: 1";
//    positionToMonicelliPlane_[24]  = "Station: 1 - Plaq: 2";
//    positionToMonicelliPlane_[25]  = "Station: 4 - Plaq: 0";
//    positionToMonicelliPlane_[26]  = "Station: 4 - Plaq: 1";
//    positionToMonicelliPlane_[27]  = "Station: 3 - Plaq: 0";


    positionToStation_[0 ]  = 0;
    positionToStation_[1 ]  = 0;
    positionToStation_[2 ]  = 0;
    positionToStation_[3 ]  = 0;
    positionToStation_[4 ]  = 0;
    positionToStation_[5 ]  = 0;

    positionToStation_[6 ]  = 4;
    positionToStation_[7 ]  = 4;
    positionToStation_[8 ]  = 4;
    positionToStation_[9 ]  = 4;

    positionToStation_[10]  = 1;
    positionToStation_[11]  = 1;
    positionToStation_[12]  = 1;
    positionToStation_[13]  = 1;
    positionToStation_[14]  = 1;
    positionToStation_[15]  = 1;
//    positionToStation_[22]  = 4;
//    positionToStation_[23]  = 4;
    positionToStation_[16]  = 5;
    positionToStation_[17]  = 5;
//    positionToStation_[24]  = 1;
//    positionToStation_[25]  = 4;
//    positionToStation_[26]  = 4;
//    positionToStation_[27]  = 3;

    positionToPlaquette_[0 ]  = 0;
    positionToPlaquette_[1 ]  = 1;
    positionToPlaquette_[2 ]  = 2;
    positionToPlaquette_[3 ]  = 3;
    positionToPlaquette_[4 ]  = 4;
    positionToPlaquette_[5 ]  = 5;
    positionToPlaquette_[6 ]  = 0;
    positionToPlaquette_[7 ]  = 1;
    positionToPlaquette_[8 ]  = 2;
    positionToPlaquette_[9 ]  = 3;
    positionToPlaquette_[10]  = 0;
    positionToPlaquette_[11]  = 1;
    positionToPlaquette_[12]  = 2;
    positionToPlaquette_[13]  = 3;
    positionToPlaquette_[14]  = 4;
    positionToPlaquette_[15]  = 5;
    positionToPlaquette_[16]  = 0;
    positionToPlaquette_[17]  = 1;
//    positionToPlaquette_[24]  = 2;
//    positionToPlaquette_[25]  = 0;
//    positionToPlaquette_[26]  = 1;
//    positionToPlaquette_[27]  = 0;

    positionToType_[0 ]  = 1;
    positionToType_[1 ]  = 1;
    positionToType_[2 ]  = 1;
    positionToType_[3 ]  = 1;
    positionToType_[4 ]  = 1;
    positionToType_[5 ]  = 1;
    positionToType_[6 ]  = 0;
    positionToType_[7 ]  = 0;
    positionToType_[8 ]  = 0;
    positionToType_[9 ]  = 0;
    positionToType_[10]  = 1;
    positionToType_[11]  = 1;
    positionToType_[12]  = 1;
    positionToType_[13]  = 1;
    positionToType_[14]  = 1;
    positionToType_[15]  = 1;
    positionToType_[16]  = 0;
    positionToType_[17]  = 0;
//    positionToType_[24]  = 1;
//    positionToType_[25]  = 0;
//    positionToType_[26]  = 0; //1 is strip 0 is pixels
//    positionToType_[27]  = 0;

}

//////////////////////////////////////////////////////////////////////////
PlanesMapping::~PlanesMapping()
{
}

//////////////////////////////////////////////////////////////////////////
std::string PlanesMapping::getMonicelliPlaneName(int position)
{
    if(positionToMonicelliPlane_.find(position) != positionToMonicelliPlane_.end())
        return positionToMonicelliPlane_[position];
    else
    {
        std::stringstream ss;
        ss << "ERROR: Plane number " << position << " doesn't exist!";
        STDLINE(ss.str(), ACRed);
    }
    return "";
}

//////////////////////////////////////////////////////////////////////////
std::string PlanesMapping::getPlaneName(int position)
{
    if(positionToPlane_.find(position) != positionToPlane_.end())
        return positionToPlane_[position];
    else
    {
        std::stringstream ss;
        ss << "ERROR: Plane number " << position << " doesn't exist!";
        STDLINE(ss.str(), ACRed);
    }
    return "";
}

//////////////////////////////////////////////////////////////////////////
int PlanesMapping::getStation(int position)
{
    if(positionToStation_.find(position) != positionToStation_.end())
        return positionToStation_[position];
    else
    {
        std::stringstream ss;
        ss << "ERROR: Plane number " << position << " doesn't exist!";
        STDLINE(ss.str(), ACRed);
        return -1;
    }
}

//////////////////////////////////////////////////////////////////////////
int PlanesMapping::getPlaquette(int position)
{
    if(positionToPlaquette_.find(position) != positionToPlaquette_.end())
        return positionToPlaquette_[position];
    else
    {
        std::stringstream ss;
        ss << "ERROR: Plane number " << position << " doesn't exist!";
        STDLINE(ss.str(), ACRed);
        return -1;
    }
}

//////////////////////////////////////////////////////////////////////////
int PlanesMapping::getPlaneType(int position)
{
    if(positionToType_.find(position) != positionToType_.end())
        return positionToType_[position];
    else
    {
        std::stringstream ss;
        ss << "ERROR: Plane number " << position << " doesn't exist!";
        STDLINE(ss.str(), ACRed);
        return -1;
    }
}

//////////////////////////////////////////////////////////////////////////
int PlanesMapping::getPlanePosition(std::string plane)
{
    if(planeToPosition_.find(plane) != planeToPosition_.end())
        return planeToPosition_[plane];
    else
        STDLINE("ERROR: Plane name " + plane + " doesn't exist!", ACRed);
    return -1;
}
